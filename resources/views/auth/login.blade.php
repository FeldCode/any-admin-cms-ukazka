<?php
    $emptyLayout = isset($_GET["without-layout"]);
?>

@extends($emptyLayout ? 'admin.layouts.empty' : 'admin.layouts.app')

@section('content')

    @if($emptyLayout)
        <style type="text/css" rel="stylesheet">
            .login-page{
                background-color: rgb(248, 249, 250);
            }
            .login-page .container{
                padding: 60px;
            }
        </style>
    @endif

<div class="login-page">
    <div class="container">
        <div class="row justify-content-center align-items-center" style="{!! !$emptyLayout ? 'min-height:100vh;' : 'min-height:400px;' !!}">
            <div class="col-md-6" style="margin-bottom:120px;">
                <div class="card">
                    <div class="card-header bg-primary text-light"><i class="fa fa-unlock-alt"></i> {{ __('auth.login.title') }}</div>

                    <div class="card-body">
                        @if($errors->any())
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger"><i class="fa fa-times"></i> {{ $error }}</div>
                            @endforeach
                        @endif

                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="login" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="login" type="text" class="form-control" name="login" value="{{ old('login') }}" required autocomplete="off" autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required autocomplete="current-password">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-sign-in-alt"></i> {{ __('auth.login.button') }}
                                    </button>

                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

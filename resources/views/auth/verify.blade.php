<?php
    $redirectToApp = env('FRONT_APP_URL', '');
?>

@extends('web.layouts.leepa-backend')

@section('content')
<div class="container">
    <div class="row justify-content-center align-items-center" style="height:100vh;">
        <div class="col-md-6" style="margin-bottom:120px;">
            <div class="card">
                <div class="card-header bg-primary text-light">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif
                    @if (isset($_GET["verified"]))
                        <div class="alert alert-success" role="alert">
                            {{ __('Email address successfully verified.') }}
                        </div>

                        @if(!empty($redirectToApp))
                            <div class="text-center">
                                <a href="{{$redirectToApp}}" class="btn btn-primary btn-lg">{{ __('Continue to the application') }} <i class="fa fa-angle-double-right"></i> </a>
                            </div>
                            <script type="text/javascript">
                                window.location.href = '{{$redirectToApp}}';
                            </script>
                        @endif
                    @else
                        <div class="alert alert-danger" role="alert">
                            {{ __('Invalid link.') }}
                        </div>

                        {{--{{ __('Before proceeding, please check your email for a verification link.') }}
                        {{ __('If you did not receive the email') }},
                        <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                            @csrf
                            <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
                        </form>--}}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

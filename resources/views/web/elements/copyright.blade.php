
<?php
    //set creation year
    $creation_year = "2020";

    //get today year
    $this_year = strftime("%Y", time());
    if($this_year > $creation_year)
        $copyrightyears = $creation_year."-".$this_year;
    else
        $copyrightyears = $creation_year;

    $copyRight = '&copy; '.$copyrightyears.' '.env('APP_NAME', '').', '.__('All rights reserved.');
?>
<?php echo $copyRight; ?><br>
<?php echo __("Created by"); ?> <a target="_blank" href="http://feldcode.cz">FeldCode.cz</a>

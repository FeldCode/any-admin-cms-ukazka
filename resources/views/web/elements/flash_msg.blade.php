<?php
    $states = \App\Helper::getFlashStates();
    /* get keep now */
    $keepNow = (Session::has('keep-now') && Session::get('keep-now'));

    /* forget keep now */
    if(Session::has('keep-now')) Session::forget('keep-now');
?>
@foreach (['error', 'warning', 'success', 'info'] as $flashState)
    @if(Session::has($flashState))
        <?php
            $flash = Session::get($flashState);

            /* forget current message when displayed once */
            if(!$keepNow)
                Session::forget($flashState);

            $swalData = \App\Helper::getSwalDataFromFlash($flashState, $flash);
        ?>
            <script type="text/javascript">
                Swal.fire({!! json_encode($swalData) !!});
            </script>
    @endif
@endforeach

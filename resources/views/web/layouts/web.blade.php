<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- META -->
        @section('meta')
            <meta charset="utf-8">
            <meta name="author" content="David Feldstein (FeldCode), support@feldcode.cz" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <!-- CSRF Token -->
            <meta name="csrf-token" content="{{ csrf_token() }}">
        @show

        <!-- SEO -->
        @section('seo')
            {!! SEO::generate() !!}
        @show

        <!-- links -->
        @section('head_links')
            @if(!empty($appDef["data"]["favicon"]))
                <link rel="shortcut icon" href="{{ asset($appDef["data"]["favicon"]) }}">
            @endif
            <link rel="stylesheet" type="text/css" href="{{ asset('web/css/app.css') }}" />
            <script type="text/javascript"> var baseUrl = '{{ url("") }}'; </script>
        @show
    </head>
    <body>
        <!-- SEO H1 -->
        <h1 style="display:none;">{{ SEOMeta::getTitle() }}</h1>

        <!-- content -->
        @yield('content')

        <!-- copyright -->
        @section('footer')
            <div class="p-5 bg-secondary">
                <div class="container text-center text-white">
                    @include('web.elements.copyright')
                </div>
            </div>
        @show

        <!-- scripts -->
        @section('scripts')
            <script type="text/javascript" src="{{ asset('web/js/app.js') }}"></script>
        @show

        <!-- flash message -->
        @section('flash')
            @include('web.elements.flash_msg')
        @show
    </body>
</html>

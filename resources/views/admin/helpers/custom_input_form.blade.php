
@include('admin.inc.flash_msg')

<div class="text-left">
    <form method="POST" id="{{ $form_id }}" enctype="multipart/form-data" onsubmit="{!! isset($_GET["hide-submit"]) ? 'return false;' : '' !!}" class="input-form custom-input-form">
        @if(!isset($_GET["hide-submit"]))
            @csrf
            <input type="hidden" name="_form_sent" value="1">
            <input type="hidden" name="_ref" value="{{ request()->_ref }}">
        @endif

        <div class="row">
            @foreach($inputsContainer["inputs"] as $inputKey => $input)
                @include('admin.inc.elements._input')
            @endforeach

            @if(!isset($_GET["hide-submit"]))
                <div class="col-md-12">
                    <div class="form-section text-center">
                        <button type="submit" class="btn btn-lg btn-success">
                            <i class="fa fa-check"></i> {{ __('Continue') }}
                        </button>
                    </div>
                </div>
            @endif
        </div>
    </form>
</div>

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ !empty($page["data"]["title"]) ? __($page["data"]["title"])." | " : "" }}{{ config('app.name', 'AnyAdmin') }}</title>
    @if(!empty($appDef["data"]["favicon"]))
        <link rel="shortcut icon" href="{{ asset($appDef["data"]["favicon"]) }}">
    @endif

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('admin/css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script type="text/javascript">
        var baseUrl = '{{ url("") }}';
        var csrfToken = '{{ csrf_token() }}';
        var activePage = '{{ request()->_page }}';
        var maxUploadFileSizeShort = '{{ \App\File::getMaxUploadFileSize(true) }}';
        var maxUploadFileSize = {{ \App\File::getMaxUploadFileSize() }};
        var tempusDominusDateFormat = '{{ env('APP_TEMPUS_DOMINUS_DATE_FORMAT', 'YYYY-MM-DD') }}';
        var tempusDominusTimeFormat = '{{ env('APP_TEMPUS_DOMINUS_TIME_FORMAT', 'HH:mm:ss') }}';
		window.translations = {!! Cache::get('translations') !!};
    </script>
    <script src="{{ asset('admin/js/app.js') }}"></script>
</head>
<body>
    <div id="app">
        @if(isset($migrationNeeded) && $migrationNeeded)
            <main>
                <div id="content" class="py-4">
                    @yield('content')
                </div>
            </main>
        @else
            @include('admin.inc.navbar')
            <main class="{!! Auth::check() ? 'has-sidebar' : '' !!}">
                @if(Auth::check())
                    @include('admin.inc.sidebar')
                @endif
                <div id="content" class="py-4 page-{{ request()->has('_page') ? request()->input('_page') : '' }}">
                    <div class="container-fluid">
                        @include('admin.inc.flash_msg')
                        @yield('content')
                    </div>
                </div>
            </main>
        @endif
    </div>

    <!-- The Modal -->
    <div class="modal" id="bsModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                </div>

                <!-- Modal body -->
                <div class="modal-body"></div>
            </div>
        </div>
    </div>
</body>
</html>

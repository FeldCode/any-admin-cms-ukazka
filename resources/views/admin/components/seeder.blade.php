<div class="text-center bg-light pt-5 pb-5">
    <h1 class="page-header mb-0">Welcome to AnyAdmin!</h1>
    <div style="max-width:600px;margin:0 auto;" class="mb-5">
        AnyAdmin is a Framework for rapid development of web based administrative systems build on Laravel.
        The main idea behind the project is finding a balance between
        <b>simplifying the work</b> of the software developer and <b>limiting his possibilities</b>.
    </div>

    <h2 class="page-header">Refresh database</h2>
    <div>
        The application contains automatically generated test data.<br>
        If you wish to regenerate it or delete it, press the relevant button below.
    </div>
    <br>
    @include("admin.inc.elements._button", ["btn" => $seed_btn])
    @include("admin.inc.elements._button", ["btn" => $clear_btn])
</div>

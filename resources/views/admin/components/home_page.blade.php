<div class="text-center bg-dark pt-5 pb-5">
    <div class="text-white">
        <img src="{{ asset("img/logo.png") }}" />
        <br>
        <h1 class="page-header mb-0 mt-4">Welcome!</h1>
        <div style="max-width:600px;margin:0 auto;">
            AnyAdmin is a Framework for rapid development of web based administrative systems build on Laravel.
            The main idea behind the project is finding a balance between
            <b>simplifying the work</b> of the software developer and <b>limiting his possibilities</b>.
        </div>
    </div>
    <br>
</div>

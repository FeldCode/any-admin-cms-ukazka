<?php
    $isPopup = (request()->has('_dom_target') && request()->input('_dom_target') == 'popup');
?>

<div class="detail-component">
    @include('admin.inc.flash_msg')

    <div class="input-form detail-form">
        <div class="row">
            @if(count($pivotInputs) > 0)
                <div class="form-section-wrapper col-12">
                    <div class="form-section pivot-section text-center">
                        <h2 class="form-header border-bottom">{{ __("Extra information saved with current relationship") }}</h2>
                    </div>
                </div>

                <div class="form-section-wrapper col-12">
                    <div class="form-section pivot-section">
                        <div class="row">
                            @foreach($pivotInputs as $inputKey => $input)
                                @include('admin.inc.elements._detail_field', [
                                            "entityInstance" => $pivotData
                                        ])
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif

            <div class="form-section-wrapper col-12">
                <div class="form-section text-center">
                    <h2 class="form-header border-bottom">{{ __("Detail of :instance (:entity)", ["instance" => strip_tags($data[$entityDef["data"]["title_field"]]), "entity" => __($entityDef["data"]["title"])]) }}</h2>
                </div>
            </div>

            @foreach($entityDef["data"]["input_form"] as $section)
                <?php
                $sectionEmpty = true;
                foreach ($section["inputs"] as $input){
                    if($input["display_in_detail"])
                        $sectionEmpty = false;
                }

                if(!$sectionEmpty):
                ?>
                    <div class="form-section-wrapper col-12 {{ $section["wrapper_classes"] }}">
                        <div class="form-section {{ $section["classes"] }}">
                            @if($section["display_header"])
                                <h2 class="section-header">
                                    @if(!empty($section["icon"]))
                                        <i class="section-header-icon {{ $section["icon"] }}"></i>
                                    @endif
                                    {{ __($section["title"]) }}
                                </h2>
                            @endif
                            <div class="row">
                                @foreach($section["inputs"] as $inputKey => $input)
                                    @if($input["display_in_detail"])
                                        @include('admin.inc.elements._detail_field', [
                                                    "entityInstance" => $data
                                                ])
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            @endforeach
            <div class="col-md-12">
                <div class="form-section text-center">
                    <?php $actionAdditionalData = ["entity" => $entityDef["name"], "id" => $id, "_source" => "detail".($isPopup ? '-popup' : '')]; ?>
                    @if($userCan["hide"])
                        <?php $btn = $entityDef["data"]["hide_operation"]["button"]; ?>
                        @include('admin.inc.elements._button', ["btn" => $btn, "actionAdditionalData" => $actionAdditionalData, 'entityInstance' => $data])
                    @endif
                    @if($userCan["edit"])
                        <?php
                            $btn = $entityDef["data"]["edit_operation"]["button"];

                            //open edit in popup if detail is popup
                            if($isPopup){
                                $btn["action"]["type"] = "component:popup";
                            }
                        ?>
                        @include('admin.inc.elements._button', ["btn" => $btn, "actionAdditionalData" => $actionAdditionalData, 'entityInstance' => $data])
                    @endif
                    @if($userCan["delete"])
                        <?php $btn = $entityDef["data"]["delete_operation"]["button"]; ?>
                        @include('admin.inc.elements._button', ["btn" => $btn, "actionAdditionalData" => $actionAdditionalData, 'entityInstance' => $data])
                    @endif
                    @foreach($entityDef["data"]["custom_actions"] as $btn)
                        @include('admin.inc.elements._button', ["btn" => $btn, "actionAdditionalData" => $actionAdditionalData, 'entityInstance' => $data])
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

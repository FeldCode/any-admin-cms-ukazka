<div class="form-component">
    <?php
        $isPopup = (request()->has('_dom_target') && request()->input('_dom_target') == 'popup');
        $isSuccess = Session::has('success');
    ?>

    @include('admin.inc.flash_msg')

    @if(!$edit && $isSuccess && !$isPopup)
        @include('admin.inc.component_redirect_back')
    @elseif(!$edit && $isSuccess && $isPopup)
        <script type="text/javascript">

            @if($action["ref"] == "app:custom_actions.add_remote_entity_as_relation.action")
                <?php
                    $itemToAdd = [
                        "id" => $justAdded->id,
                        "title" => $justAdded->getEntityTitle(),
                        "icon" => $justAdded->getEntityIcon()
                    ];
                ?>
                @if(request()->has('_is_simple_select') && request()->_is_simple_select == 1)
                    window.simpleSelectAddOption('#{{ request()->_picker_id }}', {!! json_encode($itemToAdd) !!});
                @else
                    window.entityPickerSelectItem('#{{ request()->_picker_id }}', {!! json_encode($itemToAdd) !!});
                @endif
            @else
                window.Front.componentReload($('#component'));
            @endif

            $("#bsModal").css({"visibility":"hidden"});
            setTimeout(function(){
                window.Front.closeModal();
                $("#bsModal").css({"visibility":"visible"});
            }, 600);
        </script>
    @else
        @if($isPopup && $edit && $isSuccess)
            <script type="text/javascript">
                window.Front.componentReload($('#component'));
            </script>
        @endif

        <form method="POST" enctype="multipart/form-data" action="{{ url('admin/component/form') }}" onsubmit="handleDropzoneFiles(this);Front.submitFormAjax(this, 'component'{!! $request->has('_dom_target') ? ", '".$request->input('_dom_target')."'" : '' !!});return false;" class="input-form {{ $entityDef["name"] }}-form {{ $edit ? 'edit' : 'add' }}-form">
            @csrf
            <input type="hidden" name="_form_sent" value="1">
            <input type="hidden" name="_page" value="{{ request()->_page }}">
            <input type="hidden" name="_ref" value="{{ request()->_ref }}">
            <input type="hidden" name="_dom_target" value="{{ request()->_dom_target }}">
            @if(request()->has('_add_to_file_structure_id'))
                <input type="hidden" name="_add_to_file_structure_id" value="{{ request()->_add_to_file_structure_id }}">
            @endif
            @if($edit)
                <input type="hidden" name="id" value="{{ $action["data"]["id"] }}">
            @endif
            @if(request()->has('entity'))
                <input type="hidden" name="entity" value="{{ request()->entity }}">
            @endif
            @if(request()->has('_picker_id'))
                <input type="hidden" name="_picker_id" value="{{ request()->_picker_id }}">
            @endif
            @if(request()->has('_is_simple_select'))
                <input type="hidden" name="_is_simple_select" value="{{ request()->_is_simple_select }}">
            @endif

            <div class="row">
                <div class="form-section-wrapper col-12">
                    <div class="form-section text-center">
                        @if($edit)
                            <h2 class="form-header border-bottom">{{ __("Edit :instance (:entity)", ["instance" => strip_tags($data[$entityDef["data"]["title_field"]]), "entity" => __($entityDef["data"]["title"])]) }}</h2>
                        @else
                            <h2 class="form-header border-bottom">{{ __("Add new :entity", ["entity" => __($entityDef["data"]["title"])]) }}</h2>
                        @endif
                    </div>
                </div>

                @foreach($entityDef["data"]["input_form"] as $section)
                    <?php
                        $sectionEmpty = true;
                        foreach ($section["inputs"] as $input){
                            if($input["display_in_form"])
                                $sectionEmpty = false;
                        }

                        if(!$sectionEmpty):
                    ?>
                        <div class="form-section-wrapper col-12 {{ $section["wrapper_classes"] }}">
                            <div class="form-section {{ $section["classes"] }}">
                                @if($section["display_header"])
                                    <h2 class="section-header">
                                        @if(!empty($section["icon"]))
                                            <i class="section-header-icon {{ $section["icon"] }}"></i>
                                        @endif
                                        {{ __($section["title"]) }}
                                    </h2>
                                @endif
                                <div class="row">
                                    @foreach($section["inputs"] as $inputKey => $input)
                                        @include('admin.inc.elements._input')
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                @endforeach
                <div class="col-md-12 order-last">
                    <div class="form-section text-center">
                        <button type="submit" class="btn btn-lg btn-success">
                            @if($edit)
                                <i class="fa fa-check"></i> {{ __('Save changes') }}
                            @else
                                <i class="fa fa-plus-square"></i> {{ __('Add new :entity_name', ["entity_name" => __($entityDef["data"]["title"])]) }}
                            @endif
                        </button>
                    </div>
                </div>
            </div>
        </form>
    @endif
</div>

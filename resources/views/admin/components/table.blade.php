
<?php
    $isInsideDetail = request()->has('_is_inside_detail') ? request()->input('_is_inside_detail') : false;
    $filterGroupId = str_replace([":", ".", ","], "_", $action["ref"]).'-filter-group';
?>

<div class="table-component{!! $entityDef["data"]["sortable"] ? ' sortable' : '' !!}" data-filter-group="{{ $filterGroupId }}">
    <div class="d-flex justify-content-between align-items-center">
        <div class="table-total-records">
            {{ __("Total") }}: {{ __(":cnt records", ["cnt" => $data->total()]) }}
        </div>

        <div class="btn-group mb-3">
            @if($userCanGlobal["add"] && in_array("add", $allowedOperationsByAction))
                <?php $btn = $entityDef["data"]["add_operation"]["button"]; ?>
                @include('admin.inc.elements._button')
            @endif
            @if($userCanGlobal["download_csv"] && in_array("download_csv", $allowedOperationsByAction))
                <?php
                $btn = $entityDef["data"]["download_csv_operation"]["button"];
                /* copy current component state (filters set, etc.) */
                $actionAdditionalData = request()->all();
                ?>
                @include('admin.inc.elements._button', ["btn" => $btn, "actionAdditionalData" => $actionAdditionalData])
            @endif
            @if($userCanGlobal["generate_form"] && in_array("generate_form", $allowedOperationsByAction))
                <?php $btn = $entityDef["data"]["generate_form_operation"]["button"]; ?>
                @include('admin.inc.elements._button', ["btn" => $btn, "actionAdditionalData" => ["entity"=>$entityDef["name"]]])
            @endif
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-hover" data-entity="{{ $entityDef["name"] }}">
            <thead>
                <tr>
                    <?php
                        $hasAnyFilter = false;
                    ?>
                    @foreach($colsToShow as $inputKey)
                        <?php
                            /* GET EXTERNAL ENTITY FIELD */
                            $isExternal = false;
                            if(!isset($entityDef["data"]["_inputs"][$inputKey])){
                                $isExternal = true;
                                $externalEntityDef = $externalInputs[$inputKey]["external_entity_def"];
                                $srcInputDef = $externalInputs[$inputKey]["src_input_def"];
                                $input = $externalInputs[$inputKey]["target_input_def"];
                            }

                            /* GET SAME ENTITY FIELD */
                            else{
                                $input = $entityDef["data"]["_inputs"][$inputKey];
                            }

                            $hasFilter = $useFilters && in_array($inputKey, array_keys($filtersToShow));

                            if(!$hasAnyFilter && $hasFilter){
                                $hasAnyFilter = true;
                            }
                        ?>
                        <th{!! $hasFilter ? ' class="has-filter"' : '' !!}>
                            @if(!empty($input["icon"]))
                                <i class="{{ $input["icon"] }}"></i>
                            @endif
                            @if($isExternal && !$externalInputs[$inputKey]["target_is_in_pivot"])
                                @if($externalInputs[$inputKey]["is_inverse"] && isset($externalInputs[$inputKey]["_original_src_input_def"]))
                                    {{ __($externalInputs[$inputKey]["_original_src_input_def"]["title"]) }} ({{ __($input["title"]) }})
                                @else
                                    {{ __($externalInputs[$inputKey]["is_inverse"] ? $externalEntityDef["data"]["title"] : $srcInputDef["title"]) }} ({{ __($input["title"]) }})
                                @endif
                            @else
                                {{ __($input["title"]) }}
                            @endif

                            @if($hasFilter)
                                @include('admin.inc.elements._table_th_filter', ["inputKey" => $inputKey, "input" => $input, "filter" => $filtersToShow[$inputKey]])
                            @endif
                        </th>
                    @endforeach
                    <th class="table-tools{!! $hasAnyFilter ? ' has-filter' : '' !!}">
                        {{ __("Tools") }}
                        @if($hasAnyFilter)
                            <span data-toggle="tooltip" title="{{ __("Clear all filters") }}" class="table-th-filter-btn table-th-filter-clear-all-btn" data-filter-group="{{ $filterGroupId }}">
                                <i class="filter-icon fa fa-filter"></i>
                                <i class="filter-active-icon fa fa-trash-alt"></i>
                            </span>
                        @endif
                    </th>
                </tr>
            </thead>
            <tbody data-entity="{{ $entityDef["name"] }}">
                @if(count($data) > 0)
                    @foreach($data as $entityInstance)
                        <?php
                            /* get highlight function for rows */
                            if(isset($action["data"]["highlights"]["row"])){
                                $func = $action["data"]["highlights"]["row"];
                                $highlights = $entityInstance->$func();
                            }
                            else{
                                $highlights = $entityInstance->highlightRow();
                            }

                            if(!isset($highlights["classes"])) $highlights["classes"] = "";
                            if(!isset($highlights["styles"])) $highlights["styles"] = "";
                        ?>

                        <tr id="table-row-{{ $entityDef["name"] }}-{{ $entityInstance->id }}" data-entity="{{ $entityDef["name"] }}" data-id="{{ $entityInstance->id }}" class="{!! $entityInstance->is_hidden == 1 ? 'row-hidden' : '' !!} {{ is_array($highlights["classes"]) ? implode(" ", $highlights["classes"]) : $highlights["classes"] }}" style="{{ is_array($highlights["styles"]) ? implode(" ", $highlights["styles"]) : $highlights["styles"] }}">
                            <?php $firstCellInRow = true; ?>
                            @foreach($colsToShow as $inputKey)

                                <?php
                                    /* get highlight function for cells */
                                    if(isset($action["data"]["highlights"]["cell"])){
                                        $func = $action["data"]["highlights"]["cell"];
                                        $highlights = $entityInstance->$func($inputKey);
                                    }
                                    else{
                                        $highlights = $entityInstance->highlightCell($inputKey);
                                    }

                                    if(!isset($highlights["classes"])) $highlights["classes"] = "";
                                    if(!isset($highlights["styles"])) $highlights["styles"] = "";
                                ?>

                                <td class="{{ is_array($highlights["classes"]) ? implode(" ", $highlights["classes"]) : $highlights["classes"] }}" style="{{ is_array($highlights["styles"]) ? implode(" ", $highlights["styles"]) : $highlights["styles"] }}">
                                    @if($firstCellInRow && $entityDef["data"]["sortable"])
                                        <i class="fa fa-ellipsis-v sortable-handle" data-toggle="tooltip" title="{{ __("Sort by drag & drop") }}"></i>
                                    @endif

                                    <?php
                                        /* GET EXTERNAL ENTITY FIELD */
                                        $isExternal = false;
                                        $externalEntityInstances = null;
                                        if(!isset($entityDef["data"]["_inputs"][$inputKey])){
                                            $isExternal = true;

                                            if($externalInputs[$inputKey]["is_inverse"]){
                                                $externalEntityInstances = $entityInstance->getInverseRelationship($externalInputs[$inputKey]["external_entity"], $externalInputs[$inputKey]["src_input"], true);
                                            }
                                            else{
                                                $externalEntityInstances = $entityInstance->getRelationship($externalInputs[$inputKey]["src_input"], true);
                                            }

                                            /* filter by parent entity if aplicable - related items displayed as table in detail component */
                                            if(request()->has('_partner_entity') && request()->has('_partner_id') && request()->input('_partner_entity') == $externalInputs[$inputKey]["external_entity"]){
                                                $externalEntityInstances->where('id', request()->input('_partner_id'));
                                            }

                                            /* get external instances */
                                            $externalEntityInstances = $externalEntityInstances->get();
                                        }

                                        /* GET ENTITY OWN FIELD */
                                        else{
                                            $input = $entityDef["data"]["_inputs"][$inputKey];
                                        }
                                    ?>

                                    @if(!$isExternal)
                                        @include('admin.inc.elements._input_value', [
                                            "isExternal" => false,
                                            "inputKey" => $inputKey,
                                            "input" => $input,
                                            "entityInstance" => $entityInstance
                                        ])
                                    @elseif($externalEntityInstances)
                                        <ul class="table-cell-list{!! count($externalEntityInstances) <= 1 ? ' single-item' : '' !!}">
                                            @foreach($externalEntityInstances as $externalEntityInstance)
                                                <?php
                                                    /* get external instance as pivot table */
                                                    if($externalInputs[$inputKey]["target_is_in_pivot"]){
                                                        $externalEntityInstance = $externalEntityInstance->pivot;
                                                    }
                                                ?>
                                                <li>
                                                    @include('admin.inc.elements._input_value', [
                                                        "isExternal" => true,
                                                        "inputKey" => $externalInputs[$inputKey]["target_input"],
                                                        "input" => $externalInputs[$inputKey]["target_input_def"],
                                                        "entityInstance" => $externalEntityInstance
                                                    ])
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif

                                </td>
                                <?php $firstCellInRow = false; ?>
                            @endforeach
                            <td class="table-tools">
                                <div class="btn-group">
                                    <?php
                                        $userCan = Auth()->user()->getAllowedEntityOperations($entityDef, $entityInstance->id);
                                        $actionAdditionalData = ["entity" => $entityDef["name"], "id" => $entityInstance->id];
                                    ?>
                                    @if($userCan["detail"] && in_array("detail", $allowedOperationsByAction))
                                        <?php
                                            $btn = $entityDef["data"]["detail_operation"]["button"];

                                            // if table is inside of a detail component
                                            if(isset($action["data"]["_is_inside_detail"]) && $action["data"]["_is_inside_detail"]){

                                                //display component in popup
                                                $btn["action"]["type"] = "component:popup";

                                                //pass on parent entity info (to display potential data from pivot table)
                                                if(request()->has('_src_input_ref') && request()->has('_partner_entity') && request()->has('_partner_id')){
                                                    $actionAdditionalData['_src_input_ref'] = request()->input('_src_input_ref');
                                                    $actionAdditionalData['_partner_entity'] = request()->input('_partner_entity');
                                                    $actionAdditionalData['_partner_id'] = request()->input('_partner_id');
                                                }
                                            }
                                        ?>
                                        @include('admin.inc.elements._button', ["btn" => $btn, "actionAdditionalData" => $actionAdditionalData, "noTitle" => true, 'entityInstance' => $entityInstance])
                                    @endif
                                    @if($userCan["edit"] && in_array("edit", $allowedOperationsByAction))
                                        <?php $btn = $entityDef["data"]["edit_operation"]["button"]; ?>
                                        @include('admin.inc.elements._button', ["btn" => $btn, "actionAdditionalData" => $actionAdditionalData, "noTitle" => true, 'entityInstance' => $entityInstance])
                                    @endif
                                    @if($userCan["hide"] && in_array("hide", $allowedOperationsByAction))
                                        <?php $btn = $entityDef["data"]["hide_operation"]["button"]; ?>
                                        @include('admin.inc.elements._button', ["btn" => $btn, "actionAdditionalData" => $actionAdditionalData, "noTitle" => true, 'entityInstance' => $entityInstance])
                                    @endif
                                    @if($userCan["delete"] && in_array("delete", $allowedOperationsByAction))
                                        <?php $btn = $entityDef["data"]["delete_operation"]["button"]; ?>
                                        @include('admin.inc.elements._button', ["btn" => $btn, "actionAdditionalData" => $actionAdditionalData, "noTitle" => true, 'entityInstance' => $entityInstance])
                                    @endif
                                    @if(!$isInsideDetail)
                                        @foreach($entityDef["data"]["custom_actions"] as $btn)
                                            @include('admin.inc.elements._button', ["btn" => $btn, "actionAdditionalData" => $actionAdditionalData, "noTitle" => true, 'entityInstance' => $entityInstance])
                                        @endforeach
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr><td colspan="{{ count($colsToShow) + 1 }}"><div class="alert alert-info"><i class="fa fa-info-circle"></i> {{ __('No records') }}</div></td></tr>
                @endif
            </tbody>
        </table>

        <div class="table-component-pagination" data-table-component-pagination>
            {{ $data->render() }}
        </div>
    </div>
</div>

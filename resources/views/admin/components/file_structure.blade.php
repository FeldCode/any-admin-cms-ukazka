
<div class="table-component file_structure-component {!! $levelSettings["sortable"] ? 'sortable' : '' !!}">
    <div class="d-flex justify-content-between align-items-center">
        <div class="table-total-records">
            {{ __("Total") }}: {{ __(":cnt records", ["cnt" => $data->total()]) }}
        </div>

        <div class="btn-group mb-3">
            <?php
                $canAddAny = false;
                foreach($allowedEntitiesToHandle as $entKey => $entDef){
                    if(isset($userCanByEntity[$entKey]) && $userCanByEntity[$entKey]["add"]){
                        $canAddAny = true;
                        break;
                    }
                }
            ?>

            @if($canAddAny)
                <div class="input-group mb-3">
                    <select id="entity-add-to-folder-structure-selector" data-parent-id="{{ $parentId }}" class="form-control bootstrap-select-input">
                        @foreach($allowedEntitiesToHandle as $entKey => $entDef)
                            @if(isset($userCanByEntity[$entKey]) && $userCanByEntity[$entKey]["add"])

                                <?php
                                    $entityIcon = $entDef["data"]["icon"];
                                ?>
                                <option value="{{ $entDef["data"]["add_operation"]["button"]["action"]["ref"] }}" data-content='
                                    <span class="file-structure-icon fa-stack fa-2x {!! $entDef["data"]["is_folder"] ? 'file-structure-icon-is-folder' : '' !!}">
                                        <i class="{!! $entDef["data"]["is_folder"] ? 'fas fa-folder' : (!empty($entityIcon) ? $entityIcon : 'fas fa-file-alt') !!} fa-stack-2x"></i>
                                        @if($entDef["data"]["is_folder"] && $entDef["data"]["use_folder_stacked_icon"])
                                            <i class="{{ $entityIcon }} fa-stack-1x fa-inverse"></i>
                                        @endif
                                    </span>
                                    {{ __($entDef["data"]["title"]) }}'></option>
                            @endif
                        @endforeach
                    </select>
                    <div class="input-group-append">
                        <button class="btn btn-success btn-primary" onclick="Front.addToFolderStructure();"><i class="fa fa-plus-square"></i> {{ __("Add new") }}</button>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <?php
                $bcCnt = count($breadcrumbs);
                $i = 1;
            ?>
            @foreach($breadcrumbs as $bcPath => $title)
                @if($i >= $bcCnt)
                    <li class="breadcrumb-item active" aria-current="page"><b>{{ $title }}</b></li>
                @else
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);" onclick="@include('admin.inc.elements._action', [
                            "action" => $action,
                            "actionAdditionalData" => [
                                "path" => $bcPath,
                                "page" => 1
                            ]
                        ])">
                            {{ $title }}
                        </a>
                    </li>
                @endif
                <?php $i++; ?>
            @endforeach
        </ol>
    </nav>

    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>
                        {{ __('Title') }}
                    </th>
                    <th>
                        {{ __('Items count') }}
                    </th>
                    <th>
                        {{ __('Created') }}
                    </th>
                    <th class="table-tools">
                        {{ __("Tools") }}
                    </th>
                </tr>
                @if($upPath !== false)
                    <tr>
                        <td colspan="4" class="file-structure-level-up">
                            <div class="file-structure-cell-content">
                                <a href="javascript:void(0);" class="folder-link" onclick="@include('admin.inc.elements._action', [
                                    "action" => $action,
                                    "actionAdditionalData" => [
                                        "path" => $upPath,
                                        "page" => 1
                                    ]
                                ])">
                                    <i class='file-structure-icon fas fa-folder-open'></i>
                                    <span><i class="fa fa-chevron-up"></i> {{ __('Level up') }}</span>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endif
            </thead>
            <tbody data-file-structure-id="{{ $parentId }}">
                @if(count($data) > 0)
                    @foreach($data as $contentInfoInstance)
                        <?php
                            $entityInstance = $contentInfoInstance->entity;
                            if(!$entityInstance)
                                continue;

                            $entityDef = $entityInstance->getDef();
                            $titleFieldInputKey = $entityDef["data"]["title_field"];
                        ?>

                        <tr id="table-row-{{ $entityDef["name"] }}-{{ $entityInstance->id }}" data-entity="{{ $entityDef["name"] }}" data-content-info-id="{{ $contentInfoInstance->id }}" data-id="{{ $entityInstance->id }}" class="{!! $entityInstance->is_hidden == 1 ? 'row-hidden' : '' !!}">
                            <td>
                                <div class="file-structure-cell-content">
                                    @if($levelSettings["sortable"])
                                        <i class="fa fa-ellipsis-v sortable-handle" data-toggle="tooltip" title="{{ __("Sort by drag & drop") }}"></i>
                                    @endif

                                    @if($entityDef["data"]["is_folder"])
                                        <a href="javascript:void(0);" class="folder-link" onclick="@include('admin.inc.elements._action', [
                                            "action" => $action,
                                            "actionAdditionalData" => [
                                                "path" => $contentInfoInstance->url,
                                                "page" => 1
                                            ]
                                        ])">
                                    @endif

                                    <?php
                                        $entityIcon = $entityInstance->getEntityIcon();
                                    ?>
                                    <span class="file-structure-icon fa-stack fa-2x {!! $entityDef["data"]["is_folder"] ? 'file-structure-icon-is-folder' : '' !!}">
                                      <i class="{!! $entityDef["data"]["is_folder"] ? 'fas fa-folder' : (!empty($entityIcon) ? $entityIcon : 'fas fa-file-alt') !!} fa-stack-2x"></i>
                                        @if($entityDef["data"]["is_folder"] && $entityDef["data"]["use_folder_stacked_icon"])
                                            <i class="{{ $entityIcon }} fa-stack-1x fa-inverse"></i>
                                        @endif
                                    </span>



                                        @include('admin.inc.elements._input_value', [
                                            "isExternal" => false,
                                            "inputKey" => $titleFieldInputKey,
                                            "input" => $entityDef["data"]["_inputs"][$titleFieldInputKey],
                                            "entityInstance" => $entityInstance
                                        ])

                                    @if($entityDef["data"]["is_folder"])
                                        </a>
                                    @endif
                                </div>
                            </td>
                            <td>
                                @if($entityDef["data"]["is_folder"])
                                    {{ $contentInfoInstance->childern()->count() }}
                                @else
                                    -
                                @endif
                            </td>
                            <td>
                                @include('admin.inc.elements._input_value', [
                                    "isExternal" => false,
                                    "inputKey" => "created_at",
                                    "input" => $entityDef["data"]["_inputs"]["created_at"],
                                    "entityInstance" => $entityInstance
                                ])
                            </td>
                            <td class="table-tools">
                                <div class="btn-group">
                                    <?php
                                        $userCan = Auth()->user()->getAllowedEntityOperations($entityDef, $entityInstance->id);
                                        $actionAdditionalData = ["entity" => $entityDef["name"], "id" => $entityInstance->id];
                                    ?>
                                    @if($userCan["detail"])
                                        <?php $btn = $entityDef["data"]["detail_operation"]["button"]; ?>
                                        @include('admin.inc.elements._button', ["btn" => $btn, "actionAdditionalData" => $actionAdditionalData, "noTitle" => true, 'entityInstance' => $entityInstance])
                                    @endif
                                    @if($userCan["edit"])
                                        <?php $btn = $entityDef["data"]["edit_operation"]["button"]; ?>
                                        @include('admin.inc.elements._button', ["btn" => $btn, "actionAdditionalData" => $actionAdditionalData, "noTitle" => true, 'entityInstance' => $entityInstance])
                                    @endif
                                    @if($userCan["hide"])
                                        <?php $btn = $entityDef["data"]["hide_operation"]["button"]; ?>
                                        @include('admin.inc.elements._button', ["btn" => $btn, "actionAdditionalData" => $actionAdditionalData, "noTitle" => true, 'entityInstance' => $entityInstance])
                                    @endif
                                    @if($userCan["delete"])
                                        <?php $btn = $entityDef["data"]["delete_operation"]["button"]; ?>
                                        @include('admin.inc.elements._button', ["btn" => $btn, "actionAdditionalData" => $actionAdditionalData, "noTitle" => true, 'entityInstance' => $entityInstance])
                                    @endif
                                    @foreach($entityDef["data"]["custom_actions"] as $btn)
                                        @include('admin.inc.elements._button', ["btn" => $btn, "actionAdditionalData" => $actionAdditionalData, "noTitle" => true, 'entityInstance' => $entityInstance])
                                    @endforeach
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr><td colspan="4"><div class="alert alert-info"><i class="fa fa-info-circle"></i> {{ __('No records') }}</div></td></tr>
                @endif
            </tbody>
        </table>

        <div class="table-component-pagination" data-table-component-pagination>
            {{ $data->render() }}
        </div>
    </div>
</div>

<nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm fixed-top" id="top-navbar">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ url('admin/') }}">
            @if(!empty($appDef["data"]["logo"]))
                <img src="{{ asset($appDef["data"]["logo"]) }}" alt="{{ config('app.name', 'AnyAdmin') }}" />
            @else
                {{ config('app.name', 'AnyAdmin') }}
            @endif
        </a>

    @if(Auth::check())
        <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                <button type="button" id="sidebarCollapse" class="btn btn-outline-">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </ul>
    @endif

    <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}"><i class="fa fa-unlock-alt"></i> {{ __('auth.login.title') }}</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <i class="far fa-user"></i> <span class="d-none d-md-inline">{{ Auth::user()->name }}</span> <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right user-dropdown-menu pt-0" aria-labelledby="navbarDropdown">
                        <div class="text-center bg-secondary text-light border-bottom pb-2 pt-2"><i class="fa fa-user"></i> <b>{{ Auth::user()->name }}</b></div>
                        <a class="dropdown-item pt-2" href="javascript:void(0);" onclick="@include("admin.inc.elements._action", ['action' => $definitions->get("app")["data"]["custom_actions"]["edit_user_account"]["action"], 'actionAdditionalData' => ["entity" => "user", "id" => Auth()->user()->id]])">
                            <i class="fa fa-user-cog"></i> {{ __('Account settings') }}
                        </a>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                            <i class="fa fa-power-off"></i> {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
        </ul>
    </div>
</nav>
<div class="navbar-placeholder"></div>

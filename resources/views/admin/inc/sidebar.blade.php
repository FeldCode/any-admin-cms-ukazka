<nav id="sidebar">
    <ul id="sidebar-menu" class="list-unstyled components fixed-top">
        @foreach($appDef["data"]["menu"] as $itemKey => $item)
            <?php
            $hasRights = false;
            $active = false;
            if(!empty($item["target"]) && !is_array($item["target"])){
                $hasRights = $definitions->userHasRights(Auth::user(), "pages.".$item["target"]);
                $active = (isset($page) && $page["name"] == $item["target"]);
            }
            elseif(is_array($item["target"])){
                if(isset($item["access"])){
                    $hasRightsLink = Auth::user()->hasRights($item); //check if has rights for the link
                    if(!$hasRightsLink){
                        continue;
                    }
                }

                foreach ($item["target"] as $item2){
                    if(!empty($item2["target"]) && !is_array($item2["target"])){
                        if(!$hasRights)
                            $hasRights = $definitions->userHasRights(Auth::user(), "pages.".$item2["target"]);

                        if(!$active)
                            $active = (isset($page) && $page["name"] == $item2["target"]);
                    }
                }
            }
            ?>
            @if($hasRights)
                <li{!! $active ? ' class="active"' : '' !!}>
                    @if(is_array($item["target"]))
                        <a href="#submenu-{{ $itemKey }}" data-toggle="collapse" aria-expanded="{{ $active ? 'true' : 'false' }}" class="dropdown-toggle"{!! !empty($item["tooltip"]) ? ' data-tooltip data-placement="bottom" title="'.$item["tooltip"].'"' : '' !!}>@if(!empty($item["icon"]))<i class="menu-icon {{ $item["icon"] }}"></i> @endif {{ __($item["title"]) }}</a>
                        <ul class="collapse list-unstyled{{ $active ? ' show' : '' }}" id="submenu-{{ $itemKey }}">
                            @foreach($item["target"] as $item2)
                                <?php
                                if(empty($item2["target"]) || is_array($item2["target"])){
                                    $item2["target"] = "";
                                }
                                else{
                                    $targetDef = $definitions->get('pages.'.$item2["target"]);
                                    $hasRights = $definitions->userHasRights(Auth::user(), "pages.".$item2["target"]);

                                    if(empty($item2["title"])){
                                        $item2["title"] = !empty($targetDef["data"]["title"]) ? $targetDef["data"]["title"] : "";
                                    }
                                    if(empty($item2["icon"])){
                                        $item2["icon"] = !empty($targetDef["data"]["icon"]) ? $targetDef["data"]["icon"] : "";
                                    }
                                }
                                ?>
                                @if($hasRights)
                                    <li><a{!! !empty($item2["tooltip"]) ? ' data-tooltip data-placement="bottom" title="'.$item2["tooltip"].'"' : '' !!} href="{{ !empty($item2["target"]) ? url('admin/p/'.$item2["target"]) : "#x" }}">@if(!empty($item2["icon"]))<i class="menu-icon {{ $item2["icon"] }}"></i> @endif {{ __($item2["title"]) }}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    @else
                        <?php
                        if(!empty($item["target"])){
                            $targetDef = $definitions->get('pages.'.$item["target"]);

                            if(empty($item["title"])){
                                $item["title"] = !empty($targetDef["data"]["title"]) ? $targetDef["data"]["title"] : "";
                            }
                            if(empty($item["icon"])){
                                $item["icon"] = !empty($targetDef["data"]["icon"]) ? $targetDef["data"]["icon"] : "";
                            }
                        }
                        ?>
                        <a href="{{ !empty($item["target"]) ? url('admin/p/'.$item["target"]) : "#x" }}">@if(!empty($item["icon"]))<i class="menu-icon {{ $item["icon"] }}"></i> @endif {{ __($item["title"]) }}</a>
                    @endif
                </li>
            @endif
        @endforeach
        <li class="menu-logout-link">
            <a href="{{ url('admin/logout') }}"><i class="menu-icon fa fa-power-off"></i> {{ __('Logout') }}</a>
        </li>
    </ul>
</nav>

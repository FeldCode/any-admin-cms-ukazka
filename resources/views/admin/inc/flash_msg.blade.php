<?php
    $states = \App\Helper::getFlashStates();
    /* get keep now */
    $keepNow = (Session::has('keep-now') && Session::get('keep-now'));

    /* forget keep now */
    if(Session::has('keep-now')) Session::forget('keep-now');
?>
@foreach (['error', 'warning', 'success', 'info'] as $flashState)
    @if(Session::has($flashState))
        <?php
            $flash = Session::get($flashState);

            /* forget current message when displayed once */
            if(!$keepNow)
                Session::forget($flashState);

            if(is_array($flash) && isset($flash["inLineReport"]) && $flash["inLineReport"] && isset($flash["html"])){
                if(!is_array($flash["html"])){
                    $flash["html"] = array($flash["html"]);
                }
                foreach ($flash["html"] as $flashMsg){
                    if(!is_array($flashMsg)){
                        $flashMsg = [
                            "state" => $flashState,
                            "msg" => $flashMsg
                        ];
                    }
                    ?>
                        <div class="alert alert-{{ $states[$flashMsg["state"]]["color"] }} alert-dismissible fade show">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <i class="{{ $states[$flashMsg["state"]]["icon"] }}"></i> {{ $flashMsg["msg"] }}
                        </div>
                    <?php
                }
            }
            else{
                $swalData = \App\Helper::getSwalDataFromFlash($flashState, $flash);
        ?>
                <script type="text/javascript">
                    Swal.fire({!! json_encode($swalData) !!});
                </script>
        <?php } ?>
    @endif
@endforeach

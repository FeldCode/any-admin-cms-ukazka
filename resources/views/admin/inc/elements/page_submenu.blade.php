

@foreach($submenu as $btnKey => $btn)
    <?php
        /* autoload entity ID */
        $entityId = null;
        if(isset($btn["action"]["data"]["id"]))
            $entityId = $btn["action"]["data"]["id"];

        elseif(isset($actionAdditionalData["id"]))
            $entityId = $actionAdditionalData["id"];
    ?>

    @if(!$definitions->userHasRights(Auth::user(), $btn["action"]["ref"], $entityId))
        <?php unset($submenu[$btnKey]); ?>
    @endif
@endforeach

@if(count($submenu) > 1)
    <?php $first = true; ?>
    @foreach($submenu as $btn)
        <li class="nav-item">
            <?php
                $btn["classes"] .= " nav-link";
                if($first)
                    $btn["classes"] .= " active";
            ?>
            @include('admin.inc.elements._button', ["btn" => $btn, "applyBtnClasses" => false, "attributes" => [
                'data-toggle="tab"',
                'data-hash-ref="'.$btn["action"]["ref"].'"'
            ]])
        </li>
        <?php $first = false; ?>
    @endforeach
@endif

@if(count($submenu) > 0)
    <?php $componentToLoad = array_shift($submenu); ?>
    <script type="text/javascript">
		$(document).ready(function(){
		    /* LOAD FIRST COMPONENT, IF NO HASH IS PRESENT */
		    if(!Front.ComponentHashExists()){
                @include('admin.inc.elements._action', [
                    'action' => $componentToLoad["action"],
                    'actionAdditionalData' => []
                ])
            }
		});
    </script>
@endif

<?php
    $inputType = \App\DefProcessor::parseInputType($input["type"]);

    $textMaxChars = 100;

    if(!isset($isDetail)) $isDetail = false;
    if(!isset($isExternal)) $isExternal = false;

?>
<div class="entity-instance-input-value {{ $inputType[0] }}-type-value">

            @if(!empty($input["prefix"]))
                <span class="entity-instance-input-value-prefix">{{ $input["prefix"] }}</span>
            @endif

            <?php
                $formattedMethodExists = false;
                /* function formattedMethodExists doesn't exists in pivot table */
                try{
                    $formattedMethodExists = $entityInstance->formattedMethodExists($inputKey);
                }
                catch(\Exception $e){

                }
            ?>
            @if($formattedMethodExists)
                {!! $entityInstance->getFormattedValue($inputKey) !!}
            @else
                @switch($inputType[0])
                    @case('boolean')
                        @if($entityInstance->$inputKey)
                            <i class="fa fa-check text-success"></i>
                            {{ __("Yes") }}
                        @else
                            <i class="fa fa-times text-danger"></i>
                            {{ __("No") }}
                        @endif
                    @break
                    @case('html')
                    @case('text')
                        <?php
                            $text = strip_tags($entityInstance->$inputKey);

                            if(strlen($text) > $textMaxChars)
                                $text = substr($text, 0, $textMaxChars)."...";

                        ?>
                        {!! $text !!}
                    @break
                    @case('file')
                        <?php

                            //pivot table does not have relation methods, select item directly, not through getRelationship() method
                            $file = null;
                            if(!empty($entityInstance->$inputKey)){
                                $file = \App\File::find($entityInstance->$inputKey);
                            }

                            if(!$file){
                                $thumbLink = url('admin/img/no-file.png');
                            }
                            else{
                                if($file->is_image)
                                    $thumbLink = url('media/image/'.$file->file_name.'/50/50');
                                else
                                    $thumbLink = url('admin/img/file.png');
                            }
                        ?>
                        @if($file)
                            <a href="{{ url("storage/".$file->file_name) }}" target="_blank">
                                <div style="width:60px;"><img src="{{ $thumbLink }}" class="img-thumbnail" /></div>
                                {{ $file->upload_name.' ('.\App\Helper::formatBytes($file->size).'b)' }}
                            </a>
                        @else
                            <div style="width:60px;"><img src="{{ $thumbLink }}" class="img-thumbnail" /></div>
                        @endif
                    @break
                    @case('files')
                        <?php
                            $rel = \App\DefProcessor::getRelationName($inputKey);
                            $files = $entityInstance->$rel;

                            $file = count($files) > 0 ? $files[0] : false;
                            if(!$file){
                                $thumbLink = url('media/image/no-file.png/50/50');
                            }
                            else{
                                $thumbLink = url('media/image/'.($file->is_image ? $file->file_name : 'file.png').'/50/50');
                            }
                        ?>

                        @if($file)
                            <a href="{{ url("storage/".$file->file_name) }}" target="_blank">
                                <div style="width:80px;"><img src="{{ $thumbLink }}" class="img-thumbnail" /></div>
                                {{ $file->upload_name.' ('.\App\Helper::formatBytes($file->size).'b)' }}
                            </a><br>
                        @else
                            <div style="width:80px;"><img src="{{ $thumbLink }}" class="img-thumbnail" /></div>
                        @endif

                        @if(count($files) > 1)
                            +{{ count($files) - 1 }} {{ __('more') }}
                        @endif
                    @break
                    @case('select')
                    @case('select_inv')
                    @case('multiselect_inv')
                    @case('multiselect')
                        <?php
                            $partnerDef = $definitions->get("entities.".$inputType[1]);
                            $titleField = $partnerDef["data"]["title_field"];

                            $partnerItemIds = [];

                            /* items from inverse relationship */
                            if(in_array($inputType[0], ["multiselect_inv", "select_inv"])){
                                $partnerItems = $entityInstance->getInverseRelationship($inputType[1], $inputType[2]);
                            }

                            /* items from entity own select/multiselect */
                            else{
                                //pivot table does not have relation methods, select item directly, not through getRelationship() method
                                if(in_array($inputType[0], ["select"])){
                                    $partnerItems = [];
                                    if(!empty($entityInstance->$inputKey)){
                                        $partnerItems = $partnerDef["class_name_full"]::find([$entityInstance->$inputKey]);
                                    }
                                }
                                else{
                                    $partnerItems = $entityInstance->getRelationship($inputKey);
                                }
                            }

                            if(!empty($partnerItems)){
                                $partnerItemIds = $partnerItems->pluck("id")->toArray();
                            }
                        ?>
                        @if($partnerItems)
                            @if($isDetail && $input["relation_show_items_as_table"])
                                <div class="subcomponent-table" id="subcomponent-table-{{$entityInstance->id}}-{{$inputKey}}"></div>
                                <script type="text/javascript">
                                    /* LOAD REMOTE PARTNER TABLE COMPONENT */
                                    <?php
                                        $actionAdditionalData = array_merge(
                                            [
                                                "_is_inside_detail" => true,
                                                "_allow_operations" => ["detail"],

                                                //pass on parent entity info (to display filter items by parent and to display potential data from pivot table)
                                                "_src_input_ref" => $input["ref"],
                                                "_partner_entity" => $entityDef ? $entityDef["name"] : "", //for external data in table
                                                "_partner_id" => $entityInstance->id,

                                                "entity" => $partnerDef["name"],
                                                "cond" => !empty($partnerItemIds) ? "id in (".implode(",", $partnerItemIds).")" : "0=1",
                                                "show_fields" => [$titleField],
                                                "order" => $titleField.":asc"
                                            ],
                                            is_array($input["relation_show_items_as_table"]) ? $input["relation_show_items_as_table"] : []
                                        );
                                    ?>
                                    @include('admin.inc.elements._action', [
                                        "action" => [
                                            "type" => "component:#subcomponent-table-".$entityInstance->id."-".$inputKey,
                                            "target" => "table",
                                            "ref" => "app:custom_actions.load_remote_partner_table_component.action",
                                            "confirm" => false,
                                            "inputs" => [],
                                            "background_action" => false,
                                        ],
                                        "actionAdditionalData" => $actionAdditionalData
                                    ])
                                </script>
                            @else
                                @foreach($partnerItems as $partner)
                                    <?php
                                        $actionAdditionalData = [
                                            "entity" => $partnerDef["name"],
                                            "id" => $partner->id
                                        ];

                                        //pass on parent entity info (to display potential data from pivot table)
                                        if(in_array($inputType[0], ["multiselect", "multiselect_inv"])){
                                            $actionAdditionalData = array_merge($actionAdditionalData, [
                                                "_src_input_ref" => $input["ref"],
                                                "_partner_entity" => $entityInstance->getDef()["name"],
                                                "_partner_id" => $entityInstance->id,
                                            ]);
                                        }
                                    ?>
                                    <div class="remote-entity-pill" onclick="@include('admin.inc.elements._action', [
                                        "action" => [
                                            "type" => "component:popup",
                                            "target" => "detail",
                                            "ref" => "app:custom_actions.load_remote_partner_detail.action",
                                            "confirm" => false,
                                            "inputs" => [],
                                            "background_action" => false,
                                        ],
                                        "actionAdditionalData" => $actionAdditionalData
                                    ])">
                                        @if(!empty($partner->getEntityIcon()))
                                            <i class="{{ $partner->getEntityIcon() }}"></i>
                                        @endif
                                        {{ $partner->getEntityTitle() }}
                                    </div>
                                @endforeach
                            @endif
                        @endif
                    @break
                    @case('color')
                        @if(!empty($entityInstance->$inputKey))
                            <span data-toggle="tooltip" title="{{ $entityInstance->$inputKey }}" style="position:relative;width:40px;height:40px;" class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="background-color: {{ $entityInstance->$inputKey }};"></span></span>
                        @endif
                    @break
                    @case('calculated')
                        {{ $entityInstance->$inputKey }}
                    @break
                    @case('int')
                    @case('float')
                        <?php
                            $decimals = $inputType[0] == "float" ? $inputType[2] : 0;
                        ?>
                        {!! !is_null($entityInstance->$inputKey) ? number_format(($entityInstance->$inputKey + 0), $decimals, ".", " ") : "" !!}
                    @break
                    @case('password')
                        @if(!empty($entityInstance->$inputKey))
                            ********
                        @else
                            <i class="empty-value">-- {{ __('No password') }} --</i>
                        @endif
                    @break
                    @case('icon')
                        @if(!empty($entityInstance->$inputKey))
                            <i style="font-size:200%;" data-tooltip title="{{ $entityInstance->$inputKey }}" class="{{ $entityInstance->$inputKey }}"></i>
                        @else
                            <span class="d-inline-flex justify-content-center align-items-center">
                                <i style="font-size:200%;opacity:0.5;margin-right:3px;" class="fa fa-ban text-danger"></i>
                                <i>{{ __('none') }}</i>
                            </span>
                        @endif
                    @break
                    @case('datetime')
                    @case('date')

                        <?php
                            /* if not empty and is not Carbon instance, make it Carbon (can happen with inputs inside of pivot table (not casted as dates)) */
                            $entityInstance->$inputKey = \App\Traits\EntityTrait::toCarbon($entityInstance->$inputKey);
                        ?>

                        {{ (!empty($entityInstance->$inputKey) ? $entityInstance->$inputKey->format(env('APP_DATE_FORMAT', 'Y-m-d').($inputType[0] == 'datetime' ? ' '.env('APP_TIME_FORMAT', 'H:i:s') : '')) : '') }}
                    @break
                    @default
                        {{ $entityInstance->$inputKey }}
                    @break
                @endswitch

                @if((is_null($entityInstance->$inputKey) || $entityInstance->$inputKey == '') && in_array($inputType[0], [
                    "string",
                    "int",
                    "float",
                    "date",
                    "time",
                    "datetime",
                    "html",
                    "text",
                    "color",
                    "select",
                    "calculated"
                ]))
                    {{--<i class="empty-value">-- {{ __('No value') }} --</i>--}}
                @endif
            @endif

            @if(!empty($input["suffix"]))
                <span class="entity-instance-input-value-prefix">{{ $input["suffix"] }}</span>
            @endif
</div>

<?php
    /* autoload entity ID */
    $entityId = null;
    if(isset($btn["action"]["data"]["id"]))
        $entityId = $btn["action"]["data"]["id"];

    elseif(isset($actionAdditionalData["id"]))
        $entityId = $actionAdditionalData["id"];

    $confirmation = is_array($action["confirm"]) ? json_encode($action['confirm']) : ($action['confirm'] ? 'true' : 'false');
?>

@if($definitions->userHasRights(Auth::user(), $action["ref"], $entityId))
    Front.action('{{ $action["type"] }}', '{{ $action["target"] }}', '{{ $action["ref"] }}', {{ $confirmation }}, {{ count($action["inputs"]) > 0 ? 'true' : 'false' }}, {!! (isset($actionAdditionalData) ? str_replace('"', "'", json_encode($actionAdditionalData)) : "{}") !!}, {{ $action["background_action"] ? 'true' : 'false' }});
@endif

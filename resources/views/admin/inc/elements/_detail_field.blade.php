<?php
    $inputId = str_replace([".", ":"], "-", $input["ref"]);
    $inputType = \App\DefProcessor::parseInputType($input["type"]);

    $fullSizeTypes = ["text", "html", "file", "files", "multiselect", "multiselect_inv"];
    $defaultColClass = in_array($inputType[0], $fullSizeTypes) ? "col-12" : "col";
    $disabled = true;
?>
    <div class="form-group {{ $defaultColClass }} {{ $input["wrapper_classes"] }}">
        <label class="control-label" for="{{ $inputId }}">{{ __($input["title"]) }}</label>
        <div class="input-group" {!! !empty($input["tooltip"]) ? 'data-toggle="tooltip" data-placement="right" title="'.__($input["tooltip"]).'"' : '' !!}>

            <?php
                $formattedMethodExists = false;
                /* function formattedMethodExists doesn't exists in pivot table */
                try{
                    $formattedMethodExists = $entityInstance->formattedMethodExists($inputKey);
                }
                catch(\Exception $e){

                }
            ?>
            @if($formattedMethodExists)
                {!! $entityInstance->getFormattedValue($inputKey) !!}
            @else
                <?php
                    /* MUTUAL DEFAULT VALUE */
                    $defaultValue = "";
                    if($inputType[0] != "calculated")
                        $defaultValue = $entityInstance->$inputKey;
                ?>

                @switch($inputType[0])

                    @case('html')
                        {!! $entityInstance->$inputKey !!}
                    @break
                    @case('text')
                        {!! nl2br($entityInstance->$inputKey) !!}
                    @break
                    @case('file')
                        <?php
                            /* SETTINGS */
                                $attributtes = [];
                                $acceptedFiles = $inputType;
                                unset($acceptedFiles[0]);
                                $acceptedFiles = implode(" ", $acceptedFiles);

                                $attributtes[] = 'data-max-file-size="'.\App\File::getMaxUploadFileSize(true).'"';
                                $attributtes[] = 'data-allowed-file-extensions="'.$acceptedFiles.'"';
                            /* SETTINGS */

                            /* SHOW DEFAULT FILE */
                                $fileId = $entityInstance->$inputKey;
                                $file = \App\File::find($fileId);

                                if(!empty($file)){
                                    $attributtes[] = 'data-id="'.$file->id.'"';
                                    if($file->is_image)
                                        $attributtes[] = 'data-default-file="'.url('media/image/'.$file->file_name).'"';
                                    else
                                        $attributtes[] = 'data-default-file="'.url('storage/'.$file->file_name).'"';
                                }
                            /* SHOW DEFAULT FILE */
                        ?>
                        <input id="{{ $inputId }}" name="{{ $inputKey }}" type="file" class="dropify {{ $input["classes"] }}" {!! $disabled ? 'disabled="disabled"' : '' !!} data-height="150" {!! implode(" ", $attributtes) !!}>
                        @if(!empty($file))
                            <div class="text-right w-100">
                                <a target="_blank" href="{{ url("storage/".$file->file_name) }}"><i class="fa fa-search"></i> {{ __("Open file in browser") }}</a>
                            </div>
                        @endif
                    @break
                    @case('files')
                        <?php
                            $dzFiles = [];
                            $rel = \App\DefProcessor::getRelationName($inputKey);
                            $dzFiles = $entityInstance->$rel;
                        ?>
                        <div class="dropzone dropzone-disabled">
                            @if(count($dzFiles) > 0)
                                @foreach($dzFiles as $dzFile)
                                    <div class="dz-disabled-file">
                                        <img class="img-fluid rounded" src="{{ url('media/image/'.($dzFile->is_image ? $dzFile->file_name : 'file.png')."/120/120") }}" />
                                        <div class="file-name">{{ $dzFile->upload_name }} ({{ \App\Helper::formatBytes($dzFile->size) }}b)</div>
                                        <a href="{{ url('storage/'.($dzFile->file_name)) }}" target="_blank"><i class="fa fa-search"></i> {{ __("Open") }}</a>
                                    </div>
                                @endforeach
                            @else
                                <div class="dz-default dz-message w-100"><i class="dz-default-icon fa fa-ban"></i> {{ __("No files were uploaded") }}</div>
                            @endif
                        </div>
                    @break
                    @default
                        @include("admin.inc.elements._input_value", ["isDetail" => true])
                    @break
                @endswitch
            @endif
        </div>
    </div>

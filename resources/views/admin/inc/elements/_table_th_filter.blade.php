<?php

    /* default filter values for filter types */
    $filterActive = false;
    $filterValue = '';
    switch ($filter["filter_type"]){
        case 'text':
        case 'select':
            $filterActive = request()->has('filters') && isset(request()->input('filters')[$inputKey]) && request()->input('filters')[$inputKey] != '';
            $filterValue = $filterActive ? request()->input('filters')[$inputKey] : '';
        break;
        case 'multiselect':
            $filterActive = request()->has('filters') && isset(request()->input('filters')[$inputKey]) && !empty(request()->input('filters')[$inputKey]);
            $filterValue = $filterActive ? request()->input('filters')[$inputKey] : [];
        break;
        case 'between':
            $filterActive = request()->has('filters') && isset(request()->input('filters')[$inputKey]) && !empty(request()->input('filters')[$inputKey]) && count(request()->input('filters')[$inputKey]) == 2 && (request()->input('filters')[$inputKey][0] != '' || request()->input('filters')[$inputKey][1] != '');
            $filterValue = $filterActive ? request()->input('filters')[$inputKey] : ['',''];
        break;
    }

    /* GET DEFAULT INPUT ATTRIBUTTES */
    $filterId = md5(rand(1, 1000000)).'-'.str_replace([":", ".", ","], "_", $action["ref"]).'-'.str_replace([":", ".", ","], "_", $inputKey).'-filter';
    $filterInputId = $filterId."-input";

    if(in_array($filter["filter_type"], ["text", "between"])){
        $inputAttrType = "text";
        $classes = "";
        $attributtes = [];
        $attributtes1 = [];
        $attributtes2 = [];

        switch($filter["input_type"][0]){
            case 'int':
                $inputAttrType = "number";
                $attributtes[] = 'step="1"';
            break;
            case 'float':
                $inputAttrType = "number";
                $step = "0.";
                for($i = 0; $i < $filter["input_type"][2]-1; $i++){
                    $step .= "0";
                }
                $step .= "1";
                $attributtes[] = 'step="'.$step.'"';
            break;
            case 'date':
                $classes .= " datetimepicker-input datepicker";
                $attributtes[] = 'data-toggle="datetimepicker"';
                if($filter["filter_type"] == "between"){
                    $attributtes1[] = 'data-target="#'.$filterInputId.'_from"';
                    $attributtes2[] = 'data-target="#'.$filterInputId.'_to"';
                }
                else{
                    $attributtes[] = 'data-target="#'.$filterInputId.'"';
                }
            break;
            case 'datetime':
                $classes .= " datetimepicker-input datetimepicker";
                $attributtes[] = 'data-toggle="datetimepicker"';
                if($filter["filter_type"] == "between"){
                    $attributtes1[] = 'data-target="#'.$filterInputId.'_from"';
                    $attributtes2[] = 'data-target="#'.$filterInputId.'_to"';
                }
                else{
                    $attributtes[] = 'data-target="#'.$filterInputId.'"';
                }
            break;
            case 'time':
                $classes .= " datetimepicker-input timepicker";
                $attributtes[] = 'data-toggle="datetimepicker"';
                if($filter["filter_type"] == "between"){
                    $attributtes1[] = 'data-target="#'.$filterInputId.'_from"';
                    $attributtes2[] = 'data-target="#'.$filterInputId.'_to"';
                }
                else{
                    $attributtes[] = 'data-target="#'.$filterInputId.'"';
                }
            break;
            case 'color':
                $classes .= " inp-minicolors";
            break;
        }
    }

    $label = '';
    switch ($filter["filter_type"]){
        case 'between':
            switch ($filter["input_type"][0]){
                case 'int':
                case 'float':
                    $label = __("Number");
                break;
                case 'date':
                case 'datetime':
                    $label = __('Date');
                break;
                case 'time':
                    $label = __("Time");
                break;
                case 'select_inv':
                case 'multiselect':
                case 'multiselect_inv':
                    $label = __("Number of records");
                break;
                case 'files':
                    $label = __("Number of files");
                break;
                default:
                    $label = __("Records");
                break;
            }
        break;
    }
?>
<span data-toggle="tooltip" title="{{ __("Apply filters") }}" class="table-th-filter-btn table-th-filter-show-btn" data-filter="{{ $filterId }}">
    <i class="filter-icon fa fa-filter"></i>
    @if($filterActive)
        <i class="filter-active-icon fa fa-exclamation-circle"></i>
    @endif
</span>
<div class="table-th-filter-wrapper {{ $filter["filter_type"] }}-filter" data-filter-name="{{ $inputKey }}" data-filter-type="{{ $filter["filter_type"] }}" id="{{ $filterId }}">
    <div class="row">
        <div class="col-12">
            <div class="table-th-filter">
                @switch($filter["filter_type"])
                    @case('text')
                        <div class="input-group"> <!-- input group start -->
                            @if(!empty($label))
                                <label for="{{ $filterInputId }}">{{ $label }}:</label>
                            @endif
                            @if($filter["input_type"][0] == 'icon')
                                <?php
                                    $classes = " inp-icon-picker";
                                    $fontAwesomeIcons = \App\Helper::getFontAwesomeIconsList();
                                ?>
                                <select name="{{ $inputKey }}" class="form-control {{ $classes }}" id="{{ $filterInputId }}">
                                    <option value="" data-tooltip>No icon</option>
                                    @foreach($fontAwesomeIcons as $icon)
                                        <option {!! $icon == $filterValue ? 'selected="selected"' : '' !!}>{{ $icon }}</option>
                                    @endforeach
                                </select>
                            @else
                                <input autocomplete="off" id="{{ $filterInputId }}" {!! implode(" ", $attributtes) !!} type="{{ $inputAttrType }}" name="{{ $inputKey }}" class="form-control {{ $classes }}" placeholder="{{ __('Search') }}" value="{{ $filterValue }}">
                            @endif
                    @break
                    @case('select')
                    @case('multiselect')
                        <?php
                            switch ($filter["input_type"][0]){
                                case 'boolean':
                                    $filter["options"] = [
                                        1 => __('Yes'),
                                        0 => __('No')
                                    ];
                                break;
                                case 'password':
                                    $filter["options"] = [
                                        1 => __('Has password'),
                                        0 => __('No password')
                                    ];
                                break;
                                case 'file':
                                case 'files':
                                    $filter["options"] = [
                                        1 => __('Has files'),
                                        0 => __('No files')
                                    ];
                                break;
                                case 'text':
                                case 'html':
                                    $filter["options"] = [
                                        1 => __('Has text'),
                                        0 => __('No text')
                                    ];
                                break;
                            }
                        ?>
                        <div class="input-group"> <!-- input group start -->
                            @if(!empty($label))
                                <label for="{{ $filterInputId }}">{{ $label }}:</label>
                            @endif
                            <select name="{{ $inputKey }}" class="form-control bootstrap-select-input" id="{{ $filterInputId }}" {!! $filter["filter_type"] == "multiselect" ? 'multiple data-actions-box="true" title=" -- '.__('all').' -- "' : '' !!}>
                                @if($filter["filter_type"] != "multiselect")
                                    <option value="" data-tooltip> -- {{ __('all') }} -- </option>
                                @endif
                                @foreach($filter["options"] as $optKey => $option)
                                    <?php
                                        $selected = $filter["filter_type"] == "select" ? (string)$optKey == (string)$filterValue : in_array($optKey, $filterValue);
                                    ?>
                                    <option value="{{ $optKey }}" {!! $selected ? 'selected="selected"' : '' !!} data-content='{{ $option }}'></option>
                                @endforeach
                            </select>
                    @break
                    @case('between')
                        @if(!empty($label))
                            <label for="{{ $filterInputId }}_from">{{ $label }}:</label>
                        @endif
                        <input autocomplete="off" id="{{ $filterInputId }}_from" {!! implode(" ", array_merge($attributtes, $attributtes1)) !!} type="{{ $inputAttrType }}" name="{{ $inputKey }}_from" class="form-control {{ $classes }}" placeholder="{{ __('From') }}" value="{{ $filterValue[0] }}">
                        <div class="input-group"> <!-- input group start -->
                            <input autocomplete="off" id="{{ $filterInputId }}_to" {!! implode(" ", array_merge($attributtes, $attributtes2)) !!} type="{{ $inputAttrType }}" name="{{ $inputKey }}_to" class="form-control {{ $classes }}" placeholder="{{ __('To') }}" value="{{ $filterValue[1] }}">
                    @break
                    @default
                        <div class="input-group"> <!-- input group start -->
                    @break
                @endswitch
                            <div class="input-group-append">
                                <button class="btn btn-primary filters-submit-btn"><i class="fa fa-search"></i></button>
                                <button class="btn btn-danger filters-clear-btn" data-filter="{{ $filterId }}"><i class="fa fa-trash"></i></button>
                            </div>
                        </div> <!-- input group end -->
            </div>
        </div>
    </div>
</div>

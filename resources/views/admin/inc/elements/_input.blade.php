<?php
    $isPopup = (request()->has('_dom_target') && request()->input('_dom_target') == 'popup');
    $isCustomInputsForm = isset($_is_custom_input_form) ? $_is_custom_input_form : false;
    if(!isset($form_sent)) $form_sent = false;
    if(!isset($edit)) $edit = false;

    if($input["display_in_form"]){
        $inputId = str_replace([".", ":"], "-", $input["ref"]);
        $inputType = \App\DefProcessor::parseInputType($input["type"]);

        $fullSizeTypes = ["text", "html", "file", "files", "multiselect", "multiselect_inv", "select_inv"];
        $defaultColClass = in_array($inputType[0], $fullSizeTypes) ? "col-12" : "col";

        $disabled = false;

        /* DISABLE FIELD IF */
            if($edit && !$input["fillable_edit"]) //if not fillable while editing
                $disabled = true;

            if(!$edit && !$input["fillable_add"]) //if not fillable while adding
                $disabled = true;

            if($inputType[0] == "calculated") //if calculated value
                $disabled = true;

            if(!$definitions->UserHasRights($request->user(), $input["ref"], ($edit ? $data->id : null))) //if user doesnt have rights to edit particular field
                $disabled = true;
        /* DISABLE FIELD IF */

        /* HIDE FIELD IF */
            if(!$edit && $inputType[0] == "calculated") //adding new and is calculated field
                $input["display_in_form"] = false;
        /* HIDE FIELD IF */

        if(isset($errors) && $errors->any()){
            $input["wrapper_classes"] .= $errors->has($inputKey) ? ' has-feedback has-error' : '';
        }
    }
?>
@if($input["display_in_form"])
    <div class="form-group {{ $defaultColClass }} {{ $input["wrapper_classes"] }}">
        <label class="control-label" for="{{ $inputId }}">{{ __($input["title"]) }}</label>
        <div class="input-group" {!! !empty($input["tooltip"]) ? 'data-toggle="tooltip" data-placement="bottom" title="'.__($input["tooltip"]).'"' : '' !!}>

            @if(!empty($input["icon"]))
                <div class="input-group-prepend"><span class="input-group-text"><i class="{{ $input["icon"] }}"></i></span></div>
            @endif
            @if(!empty($input["prefix"]))
                <div class="input-group-prepend"><span class="input-group-text">{{ $input["prefix"] }}</span></div>
            @endif


            <?php
                /* MUTUAL DEFAULT VALUE */
                $defaultValue = "";
                if($inputType[0] != "calculated"){
                    if(in_array($inputType[0], ["date", "datetime"])){
                        /* if not empty and is not Carbon instance, make it Carbon (can happen with inputs inside of pivot table (not casted as dates)) */
                        new \Carbon\Carbon(); //instance must be called first
                        if(!$form_sent && $edit && !empty($data->$inputKey) && !($data->$inputKey instanceof \Carbon\Carbon)){
                            $data->$inputKey = new \Carbon\Carbon($data->$inputKey);
                        }

                        $defaultValue = $form_sent ? ($request->has($inputKey) ? $request->input($inputKey) : "") : ($edit && !empty($data->$inputKey) ? $data->$inputKey->format(env('APP_DATE_FORMAT', 'Y-m-d').($inputType[0] == 'datetime' ? ' '.env('APP_TIME_FORMAT', 'H:i:s') : '')) : "");
                    }
                    else{
                        $defaultValue = $form_sent ? ($request->has($inputKey) ? $request->input($inputKey) : "") : ($edit ? $data->$inputKey : "");
                    }
                }
            ?>
            @switch($inputType[0])

                @case('boolean')
                    <?php
                        $checked = false;
                        if($form_sent){
                            $checked = $request->has($inputKey) && $request->input($inputKey) == 1;
                        }
                        elseif($edit){
                            $checked = $data->$inputKey;
                        }
                    ?>
                    <input id="{{ $inputId }}" name="{{ $inputKey }}" class="{{ $input["classes"] }}" type="checkbox" data-bs-toggle {!! $disabled ? 'disabled="disabled"' : '' !!} value="1" {!! $checked ? 'checked="checked"' : '' !!}>
                @break
                @case('html')
                    <textarea id="{{ $inputId }}" name="{{ $inputKey }}" data-keep-formatting="{!! $input["html_keep_formatting"] ? 'true' : 'false' !!}" class="summernote {{ $input["classes"] }}" {!! $disabled ? 'disabled="disabled"' : '' !!}>{{ $defaultValue }}</textarea>
                @break
                @case('text')
                    <textarea id="{{ $inputId }}" name="{{ $inputKey }}" class="form-control {{ $input["classes"] }}" {!! $disabled ? 'disabled="disabled"' : '' !!}>{{ $defaultValue }}</textarea>
                @break
                @case('icon')
                    @if($disabled)
                        @include('admin.inc.elements._input_value', ["entityInstance" => $data])
                    @else
                        <?php
                            $input["classes"] .= " inp-icon-picker";
                            $fontAwesomeIcons = \App\Helper::getFontAwesomeIconsList();
                        ?>
                        <select name="{{ $inputKey }}" class="form-control {{ $input["classes"] }}" id="{{ $inputId }}" {!! $disabled ? 'disabled="disabled"' : '' !!}>
                            <option value="" data-tooltip>No icon</option>
                            @foreach($fontAwesomeIcons as $icon)
                                <option {!! $icon == $defaultValue ? 'selected="selected"' : '' !!}>{{ $icon }}</option>
                            @endforeach
                        </select>
                    @endif
                @break
                @case('file')
                    <?php
                        /* SETTINGS */
                            $attributtes = [];
                            $acceptedFiles = $inputType;
                            unset($acceptedFiles[0]);
                            $acceptedFiles = implode(" ", $acceptedFiles);

                            $attributtes[] = 'data-max-file-size="'.\App\File::getMaxUploadFileSize(true).'"';
                            $attributtes[] = 'data-allowed-file-extensions="'.$acceptedFiles.'"';
                        /* SETTINGS */

                        /* SHOW DEFAULT FILE */
                            $file = $fileId = null;
                            $inputKeyUploadedId = $inputKey."-uploaded-id";
                            if($form_sent && $request->has($inputKeyUploadedId)){
                                $fileId = $request->$inputKeyUploadedId;
                            }
                            elseif($edit){
                                $fileId = $data->$inputKey;
                            }

                            //pivot table does not have relation methods, select item directly, not through getRelationship() method
                            if(!empty($fileId)){
                                $file = \App\File::find($fileId);
                            }

                            if(!empty($file)){
                                $attributtes[] = 'data-id="'.$file->id.'"';
                                $attributtes[] = 'data-description="'.$file->description.'"';
                                if($file->is_image)
                                    $attributtes[] = 'data-default-file="'.url('media/image/'.$file->file_name).'"';
                                else
                                    $attributtes[] = 'data-default-file="'.url('storage/'.$file->file_name).'"';
                            }
                        /* SHOW DEFAULT FILE */
                    ?>
                    <input id="{{ $inputId }}" name="{{ $inputKey }}" type="file" class="dropify {{ $input["classes"] }}" {!! $disabled ? 'disabled="disabled"' : '' !!} data-height="150" {!! implode(" ", $attributtes) !!}>
                    <input type="hidden" name="{{ $inputKey }}-uploaded-id" value="{{ $fileId }}">
                    @if(!empty($file))
                        <div class="text-right w-100 {{ $inputId }}-dropify-file-action-links">
                            <a target="_blank" href="{{ url("storage/".$file->file_name) }}"><i class="fa fa-search"></i> {{ __("Open file in browser") }}</a>
                            <a href="javascript:void(0);" data-dropify-edit-caption data-dropify-id="{{ $inputId }}"><i class="fa fa-pencil-alt"></i> {{ __('Edit file caption') }}</a>
                        </div>
                    @endif
                @break
                @case('files')
                    <?php
                        $dzFiles = [];
                        if($edit){
                            $dzFiles = $data->getRelationship($inputKey, true)->orderBy('sort')->get();
                        }
                        elseif($form_sent){
                            if($request->has($inputKey) && !empty($request->input($inputKey)))
                                $dzFiles = \App\File::whereIn('id', json_decode($request->input($inputKey)))->orderBy('sort')->get();
                        }
                    ?>
                    @if($disabled)
                        <div class="dropzone dropzone-disabled">
                            @if(count($dzFiles) > 0)
                                @foreach($dzFiles as $dzFile)
                                    <div class="dz-disabled-file">
                                        <img class="img-fluid rounded" src="{{ url('media/image/'.($dzFile->is_image ? $dzFile->file_name : 'file.png')."/120/120") }}" />
                                        <div class="file-name">{{ $dzFile->upload_name }} ({{ \App\Helper::formatBytes($dzFile->size) }}b)</div>
                                        <a href="{{ url('storage/'.($dzFile->file_name)) }}" target="_blank"><i class="fa fa-search"></i> {{ __("Open") }}</a>
                                    </div>
                                @endforeach
                            @else
                                <div class="dz-default dz-message w-100"><i class="dz-default-icon fa fa-ban"></i> {{ __("File uploading disabled") }}</div>
                            @endif
                        </div>
                    @else
                        <?php
                        /* SETTINGS */
                            $attributtes = [];

                            $acceptedFiles = $inputType;
                            unset($acceptedFiles[0]);
                            $acceptedFiles = ".".implode(", .", $acceptedFiles);

                            $attributtes[] = 'data-ref="'.$input["ref"].'"';

                            if(isset($edit) && $edit){
                                $attributtes[] = 'data-rel-type="'.$entityDef["class_name_full"].'"';
                                $attributtes[] = 'data-rel-id="'.$data->id.'"';
                            }

                            if(!empty($acceptedFiles))
                                $attributtes[] = 'data-accepted-files="'.$acceptedFiles.'"';
                            /* SETTINGS */
                        ?>
                        <div id="{{ $inputId }}-dropzone" data-field-name="{{ $inputKey }}" class="dropzone {{ $input["classes"] }}" {!! $disabled ? 'disabled="disabled"' : '' !!} {!! implode(" ", $attributtes) !!}></div>
                        <script type="text/javascript">
                            $(function(){
                                @foreach($dzFiles as $dzFile)
                                    var mockFile = { serverId : {{ $dzFile->id }}, name: "{{ $dzFile->upload_name }}", size: {{ $dzFile->size }}, type: "{{ $dzFile->type }}", description: '{!! str_replace(["'", "\r\n", "\n\r", "\r", "\n"], ["\\'", "\\r\\n", "\\n\\r", "\\r", "\\n"], $dzFile->description) !!}' };
                                    attachedDropzones['{{ $inputId }}-dropzone'].emit("addedfile", mockFile);
                                    attachedDropzones['{{ $inputId }}-dropzone'].emit("success", mockFile, {"id": {{ $dzFile->id }}, "file_name": '{{ $dzFile->file_name }}' });
                                    attachedDropzones['{{ $inputId }}-dropzone'].emit("thumbnail", mockFile, "{{ url('media/image/'.($dzFile->is_image ? $dzFile->file_name : 'file.png')."/120/120") }}");
                                    attachedDropzones['{{ $inputId }}-dropzone'].emit("complete",mockFile);
                                @endforeach
                            });
                        </script>
                    @endif
                @break
                @case('select')
                    <?php
                        $partnerDef = $definitions->get("entities.".$inputType[1]);
                        $options = $partnerDef["class_name_full"]::all();

                        /* check if user can add new partner entity in form */
                        $userCan = $request->user()->getAllowedEntityOperations($partnerDef);
                        $allowAddNew = false;
                        if($userCan["add"] && !$isPopup && !$isCustomInputsForm){ //do not allow add new in popup (add new is triggered inside popup by itself)
                            $allowAddNew = $input["relation_allow_add_new"];
                            $addNewBtn = $definitions->getByRef("app:custom_actions.add_remote_entity_as_relation");
                        }
                    ?>
                    <select name="{{ $inputKey }}" class="form-control {{ $input["classes"] }}" id="{{ $inputId }}" {!! $disabled ? 'disabled="disabled"' : '' !!}>
                        <option value=""> -- {{ __("choose") }} -- </option>
                        @foreach($options as $opt)
                            <option value="{{ $opt["id"] }}" {!! $opt["id"] == $defaultValue ? 'selected="selected"' : '' !!}>{{ $opt[$partnerDef["data"]["title_field"]] }}</option>
                        @endforeach
                    </select>

                    @if($allowAddNew)
                        @include('admin.inc.elements._button', [
                                    "btn" => $addNewBtn,
                                    "actionAdditionalData" => [
                                        "entity" => $partnerDef["name"],
                                        "_picker_id" => $inputId,
                                        "_is_simple_select" => 1
                                    ]
                                ])
                    @endif
                @break
                @case('select_inv')
                @case('multiselect_inv')
                @case('multiselect')
                    <?php
                        $partnerDef = $definitions->get("entities.".$inputType[1]);

                        /* check if user can add new partner entity in form */
                        $userCan = $request->user()->getAllowedEntityOperations($partnerDef);
                        $allowAddNew = false;
                        if($userCan["add"] && !$isPopup && !$isCustomInputsForm){ //do not allow add new in popup (add new is triggered inside popup by itself)
                            $allowAddNew = $input["relation_allow_add_new"];
                            $addNewBtn = $definitions->getByRef("app:custom_actions.add_remote_entity_as_relation");
                        }

                        /* GET RELATION EXTRA INPUTS INFO */
                        if($inputType[0] == "multiselect"){
                            $extraInputsRef = $input["ref"].'.relation_extra_inputs';
                            $hasRelationExtraInputs = count($input["relation_extra_inputs"]) > 0;
                            $relationMaxItems = $input["relation_max_items"];
                        }
                        elseif($inputType[0] == "multiselect_inv"){
                            $extraInputsRef = $partnerDef["data"]["_inputs"][$inputType[2]]["ref"].'.relation_extra_inputs';
                            $hasRelationExtraInputs = count($partnerDef["data"]["_inputs"][$inputType[2]]["relation_extra_inputs"]) > 0;
                            $relationMaxItems = $partnerDef["data"]["_inputs"][$inputType[2]]["relation_max_items"];
                        }
                        else{
                            $relationMaxItems = null; //unlimited
                            $extraInputsRef = '';
                            $hasRelationExtraInputs = false;
                        }

                        /* is sortable? */
                        $isSortable = in_array($inputType[0], ["multiselect", "multiselect_inv"]);

                        $selectedDataJson = "";
                        if($form_sent){
                            if($request->has($inputKey) && !empty($request->input($inputKey)))
                                $selectedDataJson = $request->get($inputKey);
                        }
                        elseif($edit){
                            /* selected items from inverse relationship */
                            if(in_array($inputType[0], ["multiselect_inv", "select_inv"])){
                                $selectedItems = $data->getInverseRelationship($inputType[1], $inputType[2], true);

                                if($inputType[0] == "multiselect_inv"){
                                    $selectedItems = $selectedItems->withPivot('sort as pvt_sort')->orderBy('pvt_sort');
                                }

                                $selectedItems = $selectedItems->get();
                            }

                            /* selected items from entity own multiselect */
                            else{
                                $selectedItems = $data->getRelationship($inputKey, true)->withPivot('sort as pvt_sort')->orderBy('pvt_sort')->get();
                            }

                            $selectedDataJson = [];
                            foreach ($selectedItems as $sItem){
                                $itemData = [
                                    "id" => $sItem->id,
                                    "title" => $sItem->getEntityTitle(),
                                    "icon" => $sItem->getEntityIcon()
                                ];

                                if($hasRelationExtraInputs){
                                    $pivotData = $sItem->pivot->toArray();
                                    $pivotDataBaseColumns = \App\DefProcessor::getEntityPivotTableBaseColumns($entityDef["name"], $partnerDef["name"]);
                                    foreach ($pivotDataBaseColumns as $baseColumnKey => $baseColumn){
                                        unset($pivotData[$baseColumnKey]);
                                    }

                                    $itemData = array_merge(
                                        $itemData,
                                        ["relation_extra_inputs_data" => $pivotData]
                                    );
                                }

                                $selectedDataJson[] = $itemData;
                            }

                            $selectedDataJson = json_encode($selectedDataJson);

                            if($selectedDataJson == "[]" || $selectedDataJson == "{}"){
                                $selectedDataJson = "";
                            }
                        }
                    ?>

                    <div class="entity-picker" id="{{ $inputId }}" data-bound-mode="{{ $input["relation_bound_mode"] ? 'true' : 'false' }}" data-entity="{{ $partnerDef["name"] }}" data-entity-icon="{{ $partnerDef["data"]["icon"] }}" data-extra-inputs-ref="{{ $extraInputsRef }}" data-requires-extra-inputs="{{ $hasRelationExtraInputs ? 'true' : 'false' }}" data-is-sortable="{{ $isSortable ? 'true' : 'false' }}" data-max-items="{{ $relationMaxItems }}" {!! $disabled ? 'disabled' : '' !!}>
                        <div class="input-group">
                            @if(!$input["relation_bound_mode"])
                                <input type="text" autocomplete="off" class="form-control entity-picker-search" value="" placeholder="{{ __('Start typing...') }}" {!! $disabled ? 'disabled="disabled"' : '' !!}>
                            @endif
                            @if($allowAddNew)
                                @include('admin.inc.elements._button', [
                                            "btn" => $addNewBtn,
                                            "actionAdditionalData" => [
                                                "entity" => $partnerDef["name"],
                                                "_picker_id" => $inputId,
                                                "_is_simple_select" => 0
                                            ]
                                        ])
                            @endif
                        </div>
                        <div class="entity-picker-loader">
                            <i class="spinner spinner-border text-primary"></i>
                        </div>
                        <textarea style="display:none;" name="{{ $inputKey }}" class="entity-picker-data">{{ $selectedDataJson }}</textarea>
                        <div class="entity-picker-items"></div>
                        <script type="text/javascript">window.entityPickerRender('#{{ $inputId }}');</script>
                    </div>
                @break
                @default
                    <?php
                        //int, float, date, time, datetime, color, password
                        $inputAttrType = "text";
                        $attributtes = [];
                        if($inputType[0] == "int"){
                            $inputAttrType = "number";
                            $attributtes[] = 'step="1"';
                        }
                        if($inputType[0] == "float"){
                            $inputAttrType = "number";
                            $step = "0.";
                            for($i = 0; $i < $inputType[2]-1; $i++){
                                $step .= "0";
                            }
                            $step .= "1";
                            $attributtes[] = 'step="'.$step.'"';
                        }
                        if($inputType[0] == "date"){
                            $input["classes"] .= " datetimepicker-input datepicker";
                            $attributtes[] = 'data-toggle="datetimepicker"';
                            $attributtes[] = 'data-target="#'.$inputId.'"';
                        }
                        if($inputType[0] == "datetime"){
                            $input["classes"] .= " datetimepicker-input datetimepicker";
                            $attributtes[] = 'data-toggle="datetimepicker"';
                            $attributtes[] = 'data-target="#'.$inputId.'"';
                        }
                        if($inputType[0] == "time"){
                            $input["classes"] .= " datetimepicker-input timepicker";
                            $attributtes[] = 'data-toggle="datetimepicker"';
                            $attributtes[] = 'data-target="#'.$inputId.'"';
                        }
                        if($inputType[0] == "color"){
                            $input["classes"] .= " inp-minicolors";
                        }

                        if($edit && $inputType[0] == "calculated"){
                            $inputValue = $data->$inputKey;
                        }
                        else{
                            $inputValue = $defaultValue;
                        }

                        if($inputType[0] == "password"){
                            $defaultValue = "";
                            $inputAttrType = "password";
                            $inputValue = "";
                            $isRequired = (strpos($input["rules"], 'required') !== false);
                            $noPwDisabled = $disabled;

                            if(!$disabled) //disabled due to no PW set
                                $disabled = $edit && !$isRequired && empty($data->$inputKey);
                        }

                    ?>

                        <input autocomplete="off" id="{{ $inputId }}" {!! implode(" ", $attributtes) !!} type="{{ $inputAttrType }}" name="{{ $inputKey }}" value="{{ $inputValue }}" class="form-control {{ $input["classes"] }}" placeholder="{{ $input["title"] }}" {!! $disabled ? 'disabled="disabled"' : '' !!}>

                    @if($inputType[0] == "password")
                        </div><div class="mt-2 input-group">
                        <input autocomplete="off" id="{{ $inputId }}_confirmation" {!! implode(" ", $attributtes) !!} type="{{ $inputAttrType }}" name="{{ $inputKey }}_confirmation" value="{{ $inputValue }}" class="form-control {{ $input["classes"] }}" placeholder="{{ __("Confirm") }} {{ $input["title"] }}" {!! $disabled ? 'disabled="disabled"' : '' !!}>
                        @if($edit && !$isRequired)
                            </div><div class="mt-2 input-group">
                            <label><input value="1" {!! $noPwDisabled ? 'disabled="disabled"' : '' !!} {!! empty($data->$inputKey) ? 'checked' : '' !!} type="checkbox" id="{{ $inputId }}_empty" name="{{ $inputKey }}_empty" onclick="$('#{{ $inputId }}, #{{ $inputId }}_confirmation').attr('disabled', this.checked)"> {{ __('No password') }}</label>
                        @endif
                    @endif
                @break
            @endswitch

            @if(!empty($input["suffix"]))
                <div class="input-group-append"><span class="input-group-text">{{ $input["suffix"] }}</span></div>
            @endif
        </div>

        @if(isset($errors) && $errors->any())
            @if($errors->has($inputKey))
                @foreach ($errors->get($inputKey) as $message)
                    <div class="invalid-feedback"><i class="fa fa-times"></i> {{ $message }}</div>
                @endforeach
            @endif
        @endif
    </div>
@endif

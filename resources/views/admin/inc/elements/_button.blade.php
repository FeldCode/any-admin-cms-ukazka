<?php
    /* autoload entity ID */
    $entityId = null;
    if(isset($btn["action"]["data"]["id"]))
        $entityId = $btn["action"]["data"]["id"];

    elseif(isset($actionAdditionalData["id"]))
        $entityId = $actionAdditionalData["id"];


?>

@if(!isset($entityInstance) || empty($entityInstance) || empty($btn["action"]["cond"]) || \App\DefProcessor::customActionCondPassed($btn["action"]["cond"], $entityInstance))
    @if($definitions->userHasRights(Auth::user(), $btn["action"]["ref"], $entityId))
        <?php
            $noTitle = isset($noTitle) ? $noTitle : false;
            $applyBtnClasses = isset($applyBtnClasses) ? $applyBtnClasses : true;
            if($noTitle && empty($btn["tooltip"])) $btn["tooltip"] = $btn["title"];
            $tooltip = !empty($btn["tooltip"]) ? 'data-toggle="tooltip" data-tooltip title="'.__($btn["tooltip"]).'"' : '';
            $attributes = isset($attributes) ? $attributes : [];

            if($applyBtnClasses && !$btn["simple_link"]){
                $btn["classes"] .= " btn".(!empty($btn["bs_color"]) ? ' btn-'.$btn["bs_color"] : '');
            }
        ?>
        <a href="javascript:void(0);" class="{{ $btn["classes"] }}" {!! implode(" ", $attributes) !!} {!! $tooltip !!} onclick="@include('admin.inc.elements._action', ['action' => $btn["action"]]){{ $btn["onclick"] }}">
            {!! !empty($btn["icon"]) ? '<i class="'.$btn["icon"].'"></i> ' : '' !!}
            @if(!$noTitle)
                {{ __($btn["title"]) }}
            @endif
        </a>
    @endif
@endif

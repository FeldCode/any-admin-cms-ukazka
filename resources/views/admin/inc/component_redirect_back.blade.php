<script type="text/javascript">window.Front.goBackOrHome();</script>
<div class="component-placeholder d-flex justify-content-center align-items-center">
    <div class="component-redirecting">
        <h2><i class="spinner spinner-border text-primary"></i> {{ __('Redirecting') }}...</h2>
        <p>{{ __("Please wait for redirection to finish. If nothing is happening, simple press browser's back back button or go to the different page.") }}</p>
    </div>
</div>

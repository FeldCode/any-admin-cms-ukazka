@extends('admin.layouts.app')

@section('content')
    <div id="page" class="page-{{ request()->has('_page') ? request()->input('_page') : '' }}">
        <div class="white-box page-content">

            <div id="page-header-wrapper">
                <div class="d-flex justify-content-between align-items-center">
                    <h1 class="page-header">
                        @if(!empty($page["data"]["icon"]))<i class="page-header-icon {{ $page["data"]["icon"] }}"></i> @endif
                        {{ __($page["data"]["title"]) }}
                    </h1>

                    <div class="btn-group">
                        @foreach($page["data"]["buttons"] as $btn)
                            @include('admin.inc.elements._button')
                        @endforeach
                    </div>
                </div>

                <ul class="page-menu nav nav-tabs" role="tablist">
                    <?php $submenu = $page["data"]["menu"]; ?>
                    @include('admin.inc.elements.page_submenu')
                </ul>
            </div>

            <div id="component">

            </div>
        </div>
    </div>
@endsection

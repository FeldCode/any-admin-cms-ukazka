<?php
    $invoice = $data;
    $form_sent = false;
    $edit = false;
?>
<html>
    <head>
        <title>{{ __("Invoice") }} {{ $invoice->no }}</title>

        <!-- Styles -->
        {{--<link rel="stylesheet" href="{{ asset('plugins/font-awesome-v5.5.0/css/all.min.css') }}">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    </head>
    <body>
    <div class="navbar-placeholder"></div>
    <div class="container">
        <div class="row">
            @foreach($entityDef["data"]["input_form"] as $section)
                <div class="form-section-wrapper col-12 {{ $section["wrapper_classes"] }}">
                    <div class="form-section {{ $section["classes"] }}">
                        @if($section["display_header"])
                            <h2 class="section-header">
                                @if(!empty($section["icon"]))
                                    <i class="section-header-icon {{ $section["icon"] }}"></i>
                                @endif
                                {{ $section["title"] }}
                            </h2>
                        @endif
                        <div class="row">
                            @foreach($section["inputs"] as $inputKey => $input)
                                <?php $entityInstance = $data; ?>
                                @include('admin.inc.elements._detail_field')
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    </body>
</html>

@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center align-items-center" style="height:100vh;">
            <div class="col-lg-9" style="margin-bottom:120px;">
                <div class="card">
                    <div class="card-header bg-primary text-light"><i class="fa fa-cogs"></i> {{ __('migrate.update_needed') }}</div>

                    <div class="card-body">
                        <div class="alert alert-info">
                            <i class="fa fa-info-circle"></i>
                            {{ __('migrate.info_msg') }}
                        </div>
                        @if($isDestructive)
                            <div class="alert alert-warning">
                                <i class="fa fa-exclamation-triangle"></i>
                                {{ __('migrate.warning_msg') }}
                            </div>
                        @endif

                        <h5>{{ __('migrate.waiting') }}:</h5>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                @foreach($waitingMigrations as $migKey => $mig)
                                    <?php $rowDesctructive = (!empty($mig["info"]["operations"]["delete"]) && count($mig["info"]["operations"]["delete"]) > 0); ?>
                                    @if($rowDesctructive)
                                        <tr class="table-warning">
                                    @else
                                        <tr>
                                    @endif
                                        <td>
                                            @if($rowDesctructive)
                                                <i class="fa fa-exclamation-triangle alert-warning"></i>
                                            @endif
                                            {{ $mig["name"] }}<br>
                                            <a href="javascript:void(0);" onclick="$('#mig-info-{{ $migKey }}').slideToggle(200);"><i class="fa fa-angle-down"></i> {{ __('migrate.show_more') }}</a>
                                            <div id="mig-info-{{ $migKey }}" style="display:none;">
                                                @if(empty($mig["info"]))
                                                    <strong>{{ __('creates default app structure') }}</strong>
                                                @else
                                                    @if(in_array($mig["info"]["type"], array("create", "create_pivot")))
                                                        {{ __('creates entities structure table') }}: <strong>{{ $mig["info"]["table"] }}</strong>
                                                    @else
                                                        {{ __("updates entities structure table") }}: <strong>{{ $mig["info"]["table"] }}</strong>
                                                        <ul>
                                                            @foreach($mig["info"]["operations"] as $operationKey => $columns)
                                                                @foreach($columns as $colKey => $col)
                                                                    <li>{!! ($operationKey == "delete" ? '<i class="fa fa-exclamation-triangle alert-warning"></i> ' : '') !!}{{ __($operationKey) }} {{ __("column") }} <strong>{{ $colKey }}</strong></li>
                                                                @endforeach
                                                            @endforeach
                                                        </ul>
                                                    @endif
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>

                        <form method="POST" id="migration-form" action="" onsubmit="confirmMigration();return false;">
                            @csrf
                            <input type="hidden" name="run-migrations" value="1">
                            <button class="btn btn-success"><i class="fa fa-play"></i> {{ __('migrate.run') }}</button>
                        </form>
                        <script type="text/javascript">
                            function confirmMigration() {
                                return Swal.fire({
                                    title: '{{ __('Are you sure?') }}',
                                    text: '{{ __('migrate.confirm_run') }}',
                                    icon: 'warning',
                                    showCancelButton: true,
                                    confirmButtonText: '{{ __('Yes') }}',
                                    cancelButtonText: '{{ __('No') }}',
                                }).then((result) => {
                                    if (result.value) {
                                        $("#migration-form")[0].submit();
                                    }
                                });
                            }
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

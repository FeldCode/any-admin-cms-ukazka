/* DROPZONE - CANCEL AUTODISCOVER */
Dropzone.autoDiscover = false;
window.attachedDropzones = {};

$(document).ready(function(){
    window.$.fn.selectpicker.Constructor.BootstrapVersion = '4';

    /* SIDEBAR TOGGLE FUNCTIONALITY */
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
        $('#app main').toggleClass('sidebar-active');
    });

    /* Datetimepicker - Tempus Dominus */
        var datetimepickerDefaults = {
            locale:"en",
            icons:{
                time: 'far fa-clock',
                clear: 'fas fa-trash-alt',
                close: 'fa fa-check',
                today: 'far fa-calendar-check'
            },
            sideBySide: true,
            buttons: {
                showClear: true,
                showClose: true
            },
            widgetPositioning:{
               horizontal: 'auto',
               vertical: 'bottom'
            },
            format: window.tempusDominusDateFormat + ' ' + window.tempusDominusTimeFormat
        };

        var datepickerDefaults = _.cloneDeep(datetimepickerDefaults);
        var timepickerDefaults = _.cloneDeep(datetimepickerDefaults);
        datepickerDefaults.format = window.tempusDominusDateFormat;
        timepickerDefaults.format = window.tempusDominusTimeFormat;

        $('body').on('focus', '.datetimepicker', function(){
            datetimepickerDefaults.date = moment($(this).val(), datetimepickerDefaults.format).toDate();
            $(this).datetimepicker(datetimepickerDefaults);
        });



        $('body').on('focus', '.datepicker', function(){
            datepickerDefaults.date = moment($(this).val(), datepickerDefaults.format).toDate();
            $(this).datetimepicker(datepickerDefaults);
        });
        $('body').on('focus', '.timepicker', function(){
            timepickerDefaults.date = moment($(this).val(), timepickerDefaults.format).toDate();
            $(this).datetimepicker(timepickerDefaults);
        });
    /* Datetimepicker - Tempus Dominus */

    /* DROPZONE REMOVING FILE */
    $('body').on('click', "[data-dz-delete]", function() {
        if($(this).attr("data-id")) {
            var fileId = $(this).attr("data-id");
            Swal.fire({
                title: __('Are you sure?'),
                text: __('Do you want to delete this file?'),
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: '<i class="fa fa-check"></i> ' + __('Yes'),
                cancelButtonText: '<i class="fa fa-times"></i> ' + __('No'),
                showLoaderOnConfirm: true,
                preConfirm: function(){
                    return $.ajax({
                        url: baseUrl + "/admin/files/" + fileId,
                        type: 'DELETE',
                        data: {
                            "id": fileId,
                            "_token": csrfToken,
                            "_page": activePage,
                        },
                        success: function (response) {
                            /* hide img */
                            $('.dz-preview[data-id="'+fileId+'"]').remove();
                        }
                    });
                }
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        title: __('Deleted!'),
                        text: __('The file has been deleted.'),
                        icon: 'success'
                    });
                }
            });
        }
    });

    /* DROPZONE EDIT CAPTION */
    $('body').on('click', "[data-dz-edit]", function() {
        if($(this).attr("data-id")) {
            let data = {
                "form_sent":1,
                "id": $(this).attr("data-id"),
                "description": $(this).parents('.dz-preview').first().attr("data-description")
            };

            Front.action("function", "edit-file-caption", "app:custom_actions.edit_file_caption.action", false, true, data, false);
        }
    });

    /* DROPIFY EDIT CAPTION */
    $('body').on('click', "[data-dropify-edit-caption]", function() {
        let dropifyEl = $('#'+$(this).attr("data-dropify-id"));
        if(dropifyEl){
            let data = {
                "form_sent":1,
                "id": dropifyEl.attr("data-id"),
                "description": dropifyEl.attr("data-description")
            };

            Front.action("function", "edit-file-caption", "app:custom_actions.edit_file_caption.action", false, true, data, false);
        }
    });

    /* BOOTSTRAP TOOLTIP */
    $('body').tooltip({
        selector: '[data-toggle="tooltip"],[data-tooltip],.fip-icons-container .fip-box',
        trigger : 'hover'
    });
    $('body').on('click', function(){
        $(".tooltip").remove();
    });

    loadPluginsOnElements(false); //on page
});


window.loadPluginsOnElements = function(ajax){

    /* DROPIFY */
        $(".dropify").each(function() {
            let dropifyAttached = $(this).attr('data-dropify-attached') && $(this).attr('data-dropify-attached') == 'true';
            if(!dropifyAttached) {
                let _thisDropify = new Dropify(this);
                $(this).attr('data-dropify-attached', 'true');
                $(this).on('dropify.beforeClear', function (event, element) {
                    if ($(this).attr("data-id")) {
                        inpId = $(this).attr("id");
                        fileId = $(this).attr("data-id");
                        Swal.fire({
                            title: __('Are you sure?'),
                            text: __('Do you want to delete this file?'),
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonText: '<i class="fa fa-check"></i> ' + __('Yes'),
                            cancelButtonText: '<i class="fa fa-times"></i> ' + __('No'),
                            showLoaderOnConfirm: true,
                            preConfirm: function () {
                                return $.ajax({
                                    url: baseUrl + "/admin/files/" + fileId,
                                    type: 'DELETE',
                                    data: {
                                        "id": fileId,
                                        "_token": csrfToken,
                                        "_page": activePage,
                                    },
                                    success: function (response) {
                                        /* reset dropify */
                                        _thisDropify.resetFile();
                                        _thisDropify.input.val('');
                                        _thisDropify.resetPreview();

                                        /* hide file action links */
                                        $('.'+inpId+'-dropify-file-action-links').hide();
                                    }
                                });
                            }
                        }).then((result) => {
                            if (result.value) {
                                Swal.fire({
                                    title: __('Deleted!'),
                                    text: __('The file has been deleted.'),
                                    icon: 'success'
                                });
                            }
                        });

                        return false;
                    } else {
                        return true;
                    }
                });
            }
        });
    /* DROPIFY */

    /* SUMMERNOTE */
    $('.summernote').summernote({
        width: "100%",
        height: 200,
        toolbar: [
            // [groupName, [list of button]]
            ['tools', ['undo','redo']],
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            ['fontsettings', ['fontname', 'fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['attachments', ['link','picture','video']],
            ['misc', ['fullscreen','codeview','help']]
        ],
        callbacks:{
            onPaste: function (e) {
                let keepFormatting = ($(this).attr('data-keep-formatting') && $(this).attr('data-keep-formatting') == 'true');
                if(!keepFormatting){
                    let bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    document.execCommand('insertText', false, bufferText);
                }
            }
        }
    });

    /* SELECT2 */
    $('.select2-input').select2({placeholder: __('Start typing...')});

    /* Bootstrap Select */
    $('.bootstrap-select-input').selectpicker();

    /* BOOTSTRAP TOGGLE */
        $('[data-bs-toggle]').bootstrapToggle({
            style: "bs-toggle",
            onstyle: "primary",
            offstyle: "light"
        });
    /* BOOTSTRAP TOGGLE */

    /* jQuery MiniColors */
    $("input.inp-minicolors").minicolors({
        theme: 'bootstrap',
        letterCase: 'lowercase',
        format: 'rgb',
        opacity: 'true'
    });

    /* Icon Picker */
    $(".inp-icon-picker").fontIconPicker( {
        theme: 'fip-bootstrap',
        iconsPerPage: 50
    } );


    /* DROPZONE */
    $(".dropzone:not(.dropzone-disabled)").each(function(){
        var _this = this;

        try {
            attachedDropzones[$(this).attr("id")] = new Dropzone(this, {
                url: baseUrl + "/admin/files/dz-upload",
                method: "POST",
                dictDefaultMessage: '<i class="dz-default-icon fa fa-download"></i>' + __("Drop files or click here to upload multiple files"),
                dictInvalidFileType: __("This file cannot be uploaded. Allowed file types are") + ": " + $(this).attr("data-accepted-files") + ".",
                dictFileTooBig: __("The file is too big. Max upload file size is") + ' ' + maxUploadFileSizeShort,
                dictCancelUpload: __("Cancel"),
                dictRemoveFile: __("Remove"),
                paramName: "file",
                filesizeBase: 1024,
                acceptedFiles: null || $(this).attr("data-accepted-files"),
                maxFilesize: maxUploadFileSize / 1024 / 1024,
                init: function () {
                    /* UPLOADING FILE - attach additional form data (POST) */
                    this.on("sending", function (file, xhr, formData) {
                        formData.append("_method", "POST");
                        formData.append("_token", csrfToken);
                        formData.append("_page", activePage);
                        if ($(_this).attr("data-field-name"))
                            formData.append("_input_key", $(_this).attr("data-field-name"));
                        if ($(_this).attr("data-ref"))
                            formData.append("_ref", $(_this).attr("data-ref"));

                        if ($(_this).attr("data-rel-type"))
                            formData.append("rel_type", $(_this).attr("data-rel-type"));
                        if ($(_this).attr("data-rel-id"))
                            formData.append("rel_id", $(_this).attr("data-rel-id"));
                        if ($(_this).attr("data-field-name"))
                            formData.append("rel_field", $(_this).attr("data-field-name"));
                    });

                    /* ON THUMBNAIL GENERATED - store ID */
                    this.on("thumbnail", function (file) {
                        $(file.previewElement).attr("data-id", file.serverId);
                    });

                    /* FILE UPLOADED */
                    this.on("success", function (file, response) {
                        file.serverId = response.id;
                        $(file.previewElement).attr("data-id", file.serverId);
                        $(file.previewElement).attr("data-description", file.description);

                        var btnGroupWrapper = $('<div class="text-center dz-btns"></div>');
                        var btnGroup = $('<div class="btn-group"></div>');

                        /* append move btn */
                        $(btnGroup).append('<a href="javascript:void(0);" style="cursor:move;" data-dz-move class="btn btn-secondary btn-sm" data-toggle="tooltip" title="' + __('Sort') + '"><i class="fa fa-arrows-alt"></i></a>');

                        /* append edit btn */
                        $(btnGroup).append('<a href="javascript:void(0);" data-dz-edit data-id="' + response.id + '" class="btn btn-primary btn-sm" data-toggle="tooltip" title="' + __('Edit caption') + '"><i class="fa fa-pencil-alt"></i></a>');

                        /* append delete btn */
                        $(btnGroup).append('<a href="javascript:void(0);" data-dz-delete data-id="' + response.id + '" class="btn btn-danger btn-sm" data-toggle="tooltip" title="' + __('Remove') + '"><i class="fa fa-trash-alt"></i></a>');

                        /* append open btn */
                        $(btnGroup).append('<a href="' + baseUrl + '/storage/' + response.file_name + '" target="_blank" class="btn btn-secondary btn-sm" data-toggle="tooltip" title="' + __('Open') + '"><i class="fa fa-search"></i></a>');

                        $(btnGroupWrapper).append(btnGroup);
                        $(file.previewElement).append(btnGroupWrapper);
                    });
                    this.on("error", function (file, response) {
                        console.log(response);
                    });
                }
            });
        }
        catch (e) {
            console.log(e);
        }
    });

    /* sortable - table */
    let sortableTable = document.querySelector(".table-component.sortable table tbody");
    if(sortableTable){
        new Sortable(sortableTable, {
            handle: '.sortable-handle', // handle's class
            ghostClass: 'sortable-placeholder',
            animation: 150,
            onSort: function(e){
                tableSortStart(); //table loading

                let isFileStructure = sortableTable.hasAttribute('data-file-structure-id');

                let ids = [];
                sortableTable.querySelectorAll("tr").forEach(function (node) {
                    ids.push(node.getAttribute(isFileStructure ? "data-content-info-id" : "data-id"));
                });

                let data = {};
                if(isFileStructure){
                    data = {
                        "is_file_structure": 1,
                        "parent_id": sortableTable.getAttribute("data-file-structure-id"),
                        "ids": ids
                    };
                }
                else{
                    data = {
                        "is_file_structure": 0,
                        "entity": sortableTable.getAttribute("data-entity"),
                        "ids": ids
                    };
                }

                Front.action("function", "sort-entity-items", "app:custom_actions.sort_entity_items.action", false, false, data, true);
            }
        });
    }

    /* sortable - files */
    let sortableDropzones = document.querySelectorAll('.dropzone');
    if(sortableDropzones){
        sortableDropzones.forEach(function (dzSortable) {
            new Sortable(dzSortable, {
                filter: '.dz-default',
                handle: '[data-dz-move]', // handle's class
                animation: 150,
                onSort: function(e){
                    let ids = [];
                    dzSortable.querySelectorAll(".dz-preview").forEach(function (node) {
                        ids.push(node.getAttribute("data-id"));
                    });

                    let data = {
                        "entity": '__files__',
                        "ids": ids
                    };

                    Front.action("function", "sort-entity-items", "app:custom_actions.sort_entity_items.action", false, false, data, true);
                }
            });
        });
    }


    let entityPickers = document.querySelectorAll(".entity-picker");
    if(entityPickers){
        entityPickers.forEach(function(entPicker){
            let idSelector = $(entPicker).attr('id');
            let entity = $(entPicker).attr('data-entity');
            let entityIcon = $(entPicker).attr('data-entity-icon');

            if(idSelector && entity){
                new autoComplete({
                    minChars: 1,
                    menuClass: 'entity-picker-dropdown',
                    selector: '#'+idSelector+' .entity-picker-search',
                    source: function (q, suggest) {
                        /* init loading */
                        $(entPicker).addClass('entity-picker-loading');

                        /* get selected items ids */
                        let ignoreIds = window.entityPickerGetDataIds('#'+idSelector);

                        $.ajax({
                            url: baseUrl + "/admin/helper/get-entity-items-json",
                            type: 'post',
                            data: {
                                _token: csrfToken,
                                _page: activePage,
                                entity: entity,
                                q: q,
                                ignore_ids: ignoreIds
                            },
                            complete: function(xhr){
                                if(xhr.status == 200){
                                    let response = [];
                                    try{
                                        response = $.parseJSON(xhr.responseText);
                                    }
                                    catch(e){
                                        console.log('entityPicker: Cannot parse JSON received.');
                                    }
                                    suggest(response);
                                }
                                else{
                                    suggest([]);
                                }

                                /* stop loading */
                                $(entPicker).removeClass('entity-picker-loading');
                            }
                        });
                    },
                    renderItem: function (item, search) {
                        /* get icon */
                        let icon = item.icon && item.icon != '' ? item.icon : (entityIcon && entityIcon != '') ? entityIcon : '';

                        /* highlight search */
                        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                        let re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");

                        return '<div class="autocomplete-suggestion" data-val="' + item.title + '" data-id="' + item.id + '" data-title="' + item.title + '" data-icon="' + icon + '">'+ (icon != '' ? '<i class="entity-picker-dropdown-list-icon '+icon+'"></i> ' : '') + item.title.replace(re, "<b>$1</b>") + '</div>';
                    },
                    onSelect: function (event, itemDataVal, suggestionEl) {
                        $('#'+idSelector+' .entity-picker-search').val('');

                        let item = {
                            id: $(suggestionEl).attr('data-id'),
                            title: $(suggestionEl).attr('data-title'),
                            icon: $(suggestionEl).attr('data-icon')
                        };

                        window.entityPickerSelectItem('#'+idSelector, item);
                    }
                });
            }
        });
    }
};

/* entity picker */
window.entityPickerSelectItem = function(pickerId, item){
    let data = window.entityPickerGetData(pickerId);
    let maxItems = $(pickerId).attr('data-max-items');

    if(maxItems == '' || data.length < maxItems){
        let requiresExtraInputs = $(pickerId).attr('data-requires-extra-inputs');
        let ref = $(pickerId).attr('data-extra-inputs-ref');

        if(requiresExtraInputs && requiresExtraInputs == 'true'){
            Front.getUserInputs(ref, {}, function(userInputs){
                item = Object.assign(item, {
                    "relation_extra_inputs_data": userInputs
                });

                window.entityPickerAddItem(pickerId, item);
            });
        }
        else{
            window.entityPickerAddItem(pickerId, item);
        }
    }
    else{
        Swal.fire({
            "icon": "error",
            "title": __("Error!"),
            "html": __("Max items reached!"),
            "showCloseButton": true,
            "showConfirmButton": true,
            "confirmButtonText": __("OK")
        });
    }
};

window.entityPickerAddItem = function(pickerId, item){
    if(!window.entityPickerGetItemById(pickerId, item.id)){
        let data = window.entityPickerGetData(pickerId);
        data.push(item);
        window.entityPickerSaveAndRender(pickerId, data);
    }
};

window.entityPickerEditItem = function(pickerId, itemId){
    let requiresExtraInputs = $(pickerId).attr('data-requires-extra-inputs');
    let ref = $(pickerId).attr('data-extra-inputs-ref');

    if(requiresExtraInputs && requiresExtraInputs == 'true'){
        let item = window.entityPickerGetItemById(pickerId, itemId);
        let relData = Object.assign(
                {"form_sent":1},
                item.relation_extra_inputs_data ? item.relation_extra_inputs_data : {}
            );

        if(item){
            Front.getUserInputs(ref, relData, function(userInputs){
                item = Object.assign(item, {
                    "relation_extra_inputs_data": userInputs
                });

                window.entityPickerReplaceItem(pickerId, item);
            });
        }
    }
};

window.entityPickerReplaceItem = function(pickerId, item){
    let data = window.entityPickerGetData(pickerId);

    for(let key in data){
        if(data[key].id == item.id){
            data[key] = item;
        }
    }

    window.entityPickerSaveAndRender(pickerId, data);
};

window.entityPickerGetItemById = function(pickerId, itemId){
    let data = window.entityPickerGetData(pickerId);

    for(let key in data){
        if(data[key].id == itemId){
            return data[key];
        }
    }

    return null;
};

window.entityPickerRemoveItem = function(pickerId, itemId){
    let data = window.entityPickerGetData(pickerId);

    /* CHECK FOR BOUND MODE - REMOVING ITEM COMPLETALY FROM THE DB */
    let boundMode = ($(pickerId).attr('data-bound-mode') == 'true');

    if(boundMode){
        return Swal.fire({
            title: __('Are you sure?'),
            text: '',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: '<i class="fa fa-check"></i> '+__('Yes'),
            cancelButtonText: '<i class="fa fa-times"></i> '+__('No')
        }).then((result) => {
            if (result.value) {

                /* REMOVE ENTITY INSTANCE COMPLETELY FROM THE DB */
                let entity = $(pickerId).attr('data-entity');
                Front.action('function', 'remove_entity', 'entities.'+entity+':delete_operation.button.action', false, false, {'entity':entity,'id':itemId}, true);

                /* REMOVE ENTITY INSTANCE FROM THE LIST IN THE UI */
                return window.entityPickerRemoveItemFromUI(pickerId, itemId);
            }
        });
    }
    else{
        /* REMOVE ENTITY INSTANCE FROM THE LIST IN THE UI */
        return window.entityPickerRemoveItemFromUI(pickerId, itemId);
    }
};

window.entityPickerRemoveItemFromUI = function(pickerId, itemId){
    let data = window.entityPickerGetData(pickerId);

    for(let key in data){
        if(data[key].id == itemId){
            data.splice(key, 1);
            window.entityPickerSaveAndRender(pickerId, data);

            return true;
        }
    }

    return false;
};

window.entityPickerShowItemDetail = function(e, el, pickerId, itemId){

    /* show detail only if clicked on the whole item (not on other tools) */
    if(e.target === el){
        let entity = $(pickerId).attr('data-entity');
        Front.action('component:popup', 'form', 'app:custom_actions.load_remote_partner_detail.action', false, false, {'entity':entity,'id':itemId}, false);
    }
};

window.entityPickerGetData = function(pickerId){
    let dataParsed = [];
    let data = $(pickerId).find('.entity-picker-data').val();
    if(data && data != ''){
        dataParsed = JSON.parse(data);
    }

    return dataParsed;
};
window.entityPickerGetDataIds = function(pickerId){
    let dataIds = [];
    let data = window.entityPickerGetData(pickerId);
    for(let key in data){
        dataIds.push(data[key].id);
    }

    return dataIds;
};

window.entityPickerSaveAndRender = function(pickerId, data){
    let jsonToSave = '';
    if(data.length > 0){
        jsonToSave = JSON.stringify(data);
    }

    //save data
    $(pickerId).find('.entity-picker-data').val(jsonToSave);

    window.entityPickerRender(pickerId);
};

window.entityPickerSaveSortedItems = function(pickerId){
    //save data
    let data = [];
    $(pickerId).find('.entity-picker-item').each(function(idx, item){
        let currentItemData = window.entityPickerGetItemById(pickerId, $(item).attr('data-id'));
        if(currentItemData){
            data.push(currentItemData);
        }
    });

    window.entityPickerSaveAndRender(pickerId, data);
};

window.entityPickerRender = function(pickerId){
    let requiresExtraInputs = $(pickerId).attr('data-requires-extra-inputs');
    let isSortable = $(pickerId).attr('data-is-sortable');
    let data = window.entityPickerGetData(pickerId);
    let html = '';

    for(let key in data){
        let item = data[key];

        html += '<div class="entity-picker-item remote-entity-pill" data-id="'+item.id+'" onclick="window.entityPickerShowItemDetail(event, this, \''+pickerId+'\', \''+item.id+'\');">' +
                    (item.icon != '' ? '<i class="'+item.icon+'"></i> ' : '') +
                    item.title +
                    '<span class="entity-picker-item-tools-wrapper">' +
                        ' | ' +
                        (isSortable && isSortable == 'true' ? '<a class="entity-picker-item-tool text-secondary entity-picker-item-move" href="javascript:void(0);"><i class="fa fa-arrows-alt"></i></a>' : '') +
                        (requiresExtraInputs && requiresExtraInputs == 'true' ? '<a class="entity-picker-item-tool text-primary" onclick="window.entityPickerEditItem(\''+pickerId+'\', \''+item.id+'\');" href="javascript:void(0);"><i class="fa fa-pencil-alt"></i></a>' : '') +
                        '<a class="entity-picker-item-tool text-danger" onclick="window.entityPickerRemoveItem(\''+pickerId+'\', \''+item.id+'\');" href="javascript:void(0);"><i class="far fa-trash-alt"></i></a>' +
                    '</span>' +
                '</div>';
    }

    $(pickerId).find('.entity-picker-items').html(html);

    /* refresh sortable */
    let sortableEntityPickerItems = document.querySelector(pickerId + ' .entity-picker-items');
    if(sortableEntityPickerItems){
        new Sortable(sortableEntityPickerItems, {
            handle: '.entity-picker-item-move', // handle's class
            animation: 150,
            onSort: function(e){
                window.entityPickerSaveSortedItems(pickerId);
            }
        });
    }
};

window.simpleSelectAddOption = function(pickerId, item){
    let option = $('<option value="'+item.id+'">'+item.title+'</option>');
    let selectEl = $(pickerId);

    selectEl.append(option);
    selectEl.val(item.id);
};



/* edit file caption - front */
window.editFileCaptionFront = function(id, description){
    $(".table-component.sortable table").removeClass("table-loading");

    /* dropzone */
    let dzFilePreview = $('.dz-preview[data-id="'+id+'"]');
    if(dzFilePreview.length){
        dzFilePreview.attr('data-description', description);
    }

    /* dropify */
    let dropifyFilePreview = $('.dropify[data-id="'+id+'"]');
    if(dropifyFilePreview.length){
        dropifyFilePreview.attr('data-description', description);
    }
};

/* table sort */
window.tableSortStart = function(){
    $(".table-component.sortable table").addClass("table-loading");
};

window.tableSortFinished = function(){
    $(".table-component.sortable table").removeClass("table-loading");
};

/* handle dropzone files */
window.handleDropzoneFiles = function(form){
    $(form).find(".dropzone:not(.dropzone-disabled)").each(function(){
        var dzId = $(this).attr("id");
        var fieldName = $(this).attr("data-field-name");

        var uploadedFiles = [];
        $("#"+ dzId + " .dz-preview[data-id]").each(function(){
            if($(this).attr("data-id"))
                uploadedFiles.push($(this).attr("data-id"));
        });

        $('<textarea></textarea>')
            .attr("name", fieldName)
            .css({"display":"none"})
            .val(uploadedFiles.length > 0 ? JSON.stringify(uploadedFiles) : '')
            .appendTo(form);
    });
};



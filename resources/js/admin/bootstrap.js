window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    $ = window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

//window.axios = require('axios');

//window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });



// Dropify
window.Dropify = require('dropify');

// Dropzone
window.Dropzone = $.dropzone = require('dropzone');

// Summernote
window.CodeMirror = require('codemirror');
require('codemirror/mode/htmlembedded/htmlembedded');
window.Summernote = require('bs4-summernote');

// SweetAlert
window.Swal = require('sweetalert2');

// Select2
window.select2 = require('select2');

// datetimepicker
window.moment = require('moment');
window.datetimepicker = require('tempusdominus-bootstrap-4');

// bootstrap Toggle
require('bootstrap-toggle');

// jQuery MiniColors
window.minicolors = require('@claviska/jquery-minicolors');

// Icon Picker
window.fontIconPicker = require('@fonticonpicker/fonticonpicker');

// Sortable
window.Sortable = require('sortablejs').Sortable;

// Bootstrap Select
window.selectpicker = require('bootstrap-select');

// JavaScript AutoComplete
window.autoComplete = require('../../plugins/JavaScript-autoComplete/auto-complete.min');

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// Bootstrap
require('./bootstrap');

// Front localization
require('./locale');

// Custom functions
require('./custom.functions');

// Front-End Controller
require('./front.controller');

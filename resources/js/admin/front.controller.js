
window.Front = new Object();
Front.currentComponentRequest = null;
Front.currentFunctionRequest = null;

/* PARSES HASH PARAMETERS */
Front.getHashParams = function() {
    let hash = window.location.hash;

    if(hash.length > 1){
        hash = hash.substr(1);
        let hashParamsRaw = hash.split('&');
        let hashParams = {};

        for (let key in hashParamsRaw){
            let hashParam = hashParamsRaw[key].split('=');
            hashParams[hashParam[0]] = hashParam[1] ? decodeURIComponent((hashParam[1] + '').replace(/\+/g, '%20')) : '';
        }

        return hashParams;
    }

    return {};
};

/* GO BACK IF SAME SITE */
Front.goBackOrHome = function(){
    let previousUrl = document.referrer;
    if(previousUrl && previousUrl.includes(baseUrl)){
        window.history.back();
    }
    else{
        window.location.href = baseUrl + '/admin';
    }
};

/* CHECKS HASH PARAMETERS FOR COMPONENT REFERENCE */
Front.actionToHash = function(type, target, ref, confirmation, requireUserInputs, additionalData, backgroundAction){
    if(type == "component" || type == "component:#component"){
        let hash = "#component="+target+","+ref+","+(confirmation ? 1 : 0)+","+(requireUserInputs ? 1 : 0)+","+(backgroundAction ? 1 : 0);

        for(let key in additionalData){
            hash += "&" + key + "=" + additionalData[key];
        }

        if(hash !== window.location.hash)
            window.location.hash = hash;

        /* HIGHLIGHT TAB LINK IF PRESENT */
        $('.page-menu .nav-link:not([data-hash-ref="'+ref+'"])').removeClass('active');
        $('.page-menu .nav-link[data-hash-ref="'+ref+'"]').addClass('active');
    }
};

/* CHECKS HASH PARAMETERS FOR COMPONENT REFERENCE */
Front.callActionByHash = function(){
    var hashParams = Front.getHashParams();
    if("component" in hashParams){
        let actionData = hashParams["component"].split(",");
        let additionalData = {};

        for(let key in hashParams){
            if(key !== "component"){
                additionalData[key] = hashParams[key];
            }
        }

        Front.action("component", actionData[0], actionData[1], (actionData[2] === 1), (actionData[3] === 1), additionalData, (actionData[4] === 1));
    }
};

/* CHECKS HASH PARAMETERS FOR COMPONENT REFERENCE */
Front.ComponentHashExists = function(){
    let hashParams = Front.getHashParams();
    return ("component" in hashParams);
};

/* REGISTER LISTENERS FOR HASH CHANGE */
window.addEventListener("load", Front.callActionByHash);
window.addEventListener("hashchange", Front.callActionByHash);

/* HANDLE ACTION CONFIRMATION */
Front.action = function(type, target, ref, confirmation, requireUserInputs, additionalData, backgroundAction) {

    confirmation = confirmation || false;
    requireUserInputs = requireUserInputs || false;
    additionalData = additionalData || {};
    backgroundAction = backgroundAction || false;

    /* SYNCES HASH AND PUSHES IT TO HISTORY */
    Front.actionToHash(type, target, ref, confirmation, requireUserInputs, additionalData, backgroundAction);

    if(!Array.isArray(confirmation)){
        confirmation = [confirmation, 'Are you sure?', ''];
    }

    if(confirmation[0]){
        Swal.fire({
            title: __(confirmation[1]),
            text: __(confirmation[2]),
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: '<i class="fa fa-check"></i> '+__('Yes'),
            cancelButtonText: '<i class="fa fa-times"></i> '+__('No')
        }).then((result) => {
            if (result.value) {
                if(requireUserInputs){
                    /* REQUIRE USER TO PROVIDE INPUTS FIRST */
                    Front.getUserInputs(ref, additionalData, function(userInputs){
                        /* CONTINUE TO PROCESS ACTION */
                        Front.processAction(type, target, ref, additionalData, userInputs, backgroundAction);
                    });
                }
                else{
                    /* CONTINUE TO PROCESS ACTION */
                    Front.processAction(type, target, ref, additionalData, {}, backgroundAction);
                }
            }
        });
    }
    else{
        if(requireUserInputs){
            /* REQUIRE USER TO PROVIDE INPUTS FIRST */
            Front.getUserInputs(ref, additionalData, function(userInputs){
                /* CONTINUE TO PROCESS ACTION */
                Front.processAction(type, target, ref, additionalData, userInputs, backgroundAction);
            });
        }
        else{
            /* CONTINUE TO PROCESS ACTION */
            Front.processAction(type, target, ref, additionalData, {}, backgroundAction);
        }
    }
};

/* REQUIRE USER TO PROVIDE INPUTS */
Front.getUserInputs = function(ref, additionalData, callback){

    Front.funcLoading();
    formId = "custom-input-form";

    var ajaxData = {
        _token: csrfToken,
        _ref: ref,
        _page: activePage,
        form_id: formId
    };
    ajaxData = _.merge(ajaxData, additionalData);

    this.currentFunctionRequest = $.ajax({
        url: baseUrl + "/admin/helper/custom-input-form?hide-submit",
        type: 'post',
        data: ajaxData,
        beforeSend: function(){
            if(Front.currentFunctionRequest != null) {
                Front.currentFunctionRequest.abort();
            }
        },
        complete: function(xhr){
            if(xhr.status == 200){
                Swal.fire({
                    title: __("User input required"),
                    html: xhr.responseText,
                    showConfirmButton: true,
                    showCancelButton: true,
                    confirmButtonText: '<i class="fa fa-check"></i> '+__('Continue'),
                    cancelButtonText: '<i class="fa fa-times"></i> '+__('Cancel'),
                    showLoaderOnConfirm: true,
                    preConfirm: () => {
                        var form = $("#"+formId);
                        var userInput = Front.serializedDataToObject(form.serializeArray());
                        ajaxData = _.merge(ajaxData, userInput);
                        return $.ajax({
                                url: baseUrl + "/admin/helper/custom-input-form?hide-submit&validate",
                                type: 'post',
                                data: ajaxData,
                                complete: function(xhrValidation){
                                    if(xhrValidation.status == 200) {
                                        if(!xhrValidation.responseJSON.state){
                                            $("#swal2-content").html(xhrValidation.responseJSON.html);
                                            loadPluginsOnElements(true);
                                            Swal.showValidationMessage(__('Some fields contain errors.'));
                                            console.log('yep');
                                            console.log(xhrValidation.statusText);
                                            throw new Error(xhrValidation.statusText);
                                        }
                                        else{
                                            var form = $("#"+formId);
                                            var userInput =  Front.serializedDataToObject(form.serializeArray());
                                            callback(userInput);
                                            return userInput;
                                        }
                                    }
                                    else{
                                        Swal.showValidationMessage(__('UNEXPECTED ERROR'));
                                        throw new Error(xhrValidation.statusText);
                                    }
                                }
                            });
                    }
                });
                loadPluginsOnElements(true);
            }
            else{
                Front.funcError(xhr.status, xhr.responseText);
            }
        }
    });
};

/* PROCESS ACTION */
Front.processAction = function(type, target, ref, additionalData, userInput, backgroundAction) {
    additionalData = additionalData || {};
    userInput = userInput || {};
    backgroundAction = backgroundAction || false;

    /* GET RAW TYPE AND DOM TARGET */
    let isMainComponent = true;
    let domTarget = "#component";
    type = type.split(":");
    if(type.length > 1){
        domTarget = type[1];
        isMainComponent = false;
    }
    type = type[0];

    /* TARGET IS EXTERNAL URL */
    if(type == "url"){
        let ajaxData = {
            _token: csrfToken,
            _ref: ref,
            _page: activePage
        };
        let data = $.param(_.merge(ajaxData, additionalData, userInput));
        window.open(target+'?'+data, '_blank');
    }

    /* TARGET IS EXTERNAL URL WITH POST */
    if(type == "url-post"){
        let requestData = {
            _token: csrfToken,
            _ref: ref,
            _page: activePage
        };
        requestData = JSON.stringify(_.merge(requestData, additionalData, userInput));
        let requestDataInput = $('<input>');
        requestDataInput.attr("name", "_request_data");
        requestDataInput.val(requestData); //to correctly assign json value (avoiding escape problem)

        $('<form method="post" action="'+target+'" target="_blank">' +
            '<input type="hidden" name="_token" value="'+csrfToken+'">' +
            '<input type="hidden" name="_ref" value="'+ref+'">' +
            '<input type="hidden" name="_page" value="'+activePage+'">' +
            '</form>').append(requestDataInput).appendTo('body').submit().remove();
    }

    /* TARGET IS PAGE */
    else if(type == "page"){
        window.location.href = baseUrl+"/admin/p/"+target;
    }

    /* TARGET IS COMPONENT */
    else if(type == "component"){
        /* if target is main component, hide potential popup component */
        if(domTarget == "#component"){
            Front.closeModal();
        }

        let ajaxData = {
            _token: csrfToken,
            _ref: ref,
            _page: activePage,
            _dom_target: domTarget
        };
        ajaxData = _.merge(ajaxData, additionalData, userInput);

        if($(domTarget).length || domTarget == "popup"){
            this.componentLoading(domTarget);

            /* save current component state to its attribute */
            let componentState = {
                selector: domTarget,
                target: target,
                ref: ref,
                additionalData: additionalData,
                userInput: userInput,
                backgroundAction: backgroundAction
            };

            if(domTarget == "popup") {
                $("#bsModal .modal-body").attr('data-component-state', JSON.stringify(componentState));
            }
            else {
                $(domTarget).attr('data-component-state', JSON.stringify(componentState));
            }

            /* load component */
            this.currentComponentRequest = $.ajax({
                url: baseUrl + "/admin/component/" + target,
                type: 'post',
                data: ajaxData,
                beforeSend: function(){
                    if(isMainComponent && Front.currentComponentRequest != null) {
                        Front.currentComponentRequest.abort();
                    }
                },
                complete: function(xhr){
                    if(xhr.status == 200){
                        Front.componentFillHtml(xhr.responseText, domTarget);
                        loadPluginsOnElements(true);
                    }
                    else{
                        Front.componentError(xhr.status, xhr.responseText, domTarget, target);
                    }

                    if(Front.loadingColorSwapper)
                        clearInterval(Front.loadingColorSwapper);
                }
            });
        }
        else{
            console.log("Couldn't load component. Invalid DOM target");
        }
    }

    /* TARGET IS FUNCTION */
    else if(type == "function"){
        if(!backgroundAction){
            setTimeout(function(){Swal.showLoading();}, 500);
            this.funcLoading();
        }

        let ajaxData = {
            _token: csrfToken,
            _ref: ref,
            _page: activePage
        };
        ajaxData = _.merge(ajaxData, additionalData, userInput);

        this.currentFunctionRequest = $.ajax({
            url: baseUrl + "/admin/func/" + target,
            type: 'post',
            data: ajaxData,
            beforeSend: function(){
                if(Front.currentFunctionRequest != null) {
                    Front.currentFunctionRequest.abort();
                }
            },
            complete: function(xhr){
                if(xhr.status == 200){
                    Front.funcResponse(xhr.responseText);
                    loadPluginsOnElements(true);
                }
                else{
                    Front.funcError(xhr.status, xhr.responseText);
                }
            }
        });
    }

};

/* SUBMIT AJAX FORM */
Front.submitFormAjax = function(form, resultOutput, domTarget) {
    resultOutput = resultOutput || "component";
    domTarget = domTarget || "#component";

    if(resultOutput == "component")
        Front.componentLoading(domTarget);
    else
        Front.funcLoading();

    this.currentComponentRequest = $.ajax({
        type: form.method,
        url: form.action,
        data: new FormData(form),
        processData: false,
        contentType: false,
        beforeSend: function(){
            if(Front.currentComponentRequest != null) {
                Front.currentComponentRequest.abort();
            }
        },
        complete: function(xhr){
            if(xhr.status == 200){
                Front.componentFillHtml(xhr.responseText, domTarget);
                loadPluginsOnElements(true);
            }
            else{
                Front.componentError(xhr.status, xhr.responseText, domTarget, "form");
            }
            clearInterval(Front.loadingColorSwapper);
        }
    });
};

Front.componentFillHtml = function(html, domTarget){
    domTarget = domTarget || "#component";

    if(domTarget == 'popup'){
        Front.showModal(html, '', 'xl');
    }
    else{
        $(domTarget).html(html);
        if(domTarget == "#component"){
            $('html, body').stop().animate({scrollTop:0}, 300);
        }
    }
};

/* SHOW COMPONENT LOADING */
Front.componentLoading = function(domTarget){
    domTarget = domTarget || "#component";

    if(domTarget == 'popup'){
        Front.closeModal();
        Front.funcLoading();
    }
    else {
        if ($(domTarget).length) {
            $(domTarget).html('<div class="component-placeholder d-flex justify-content-center align-items-center"><div class="component-loading text-center"><i class="spinner spinner-border text-primary"></i></div></div>');
        }

        /* CHANGING SPINNER COLORS */
        this.loadingColorSwapper = window.setInterval(function () {
            var colorChange = {
                "text-primary": "text-danger",
                "text-danger": "text-warning",
                "text-warning": "text-success",
                "text-success": "text-primary",
            };

            if ($('.component-loading .spinner').length) {
                $.each(colorChange, function (from, to) {
                    if ($('.component-loading .spinner').hasClass(from)) {
                        $('.component-loading .spinner').removeClass(from).addClass(to);
                        return false;
                    }
                });
            }

        }, 750);
    }
};

/* SHOW COMPONENT ERROR */
Front.componentError = function(errorCode, data, domTarget, target){
    domTarget = domTarget || "#component";
    if(errorCode == 0)
        return false;

    if(errorCode == 401 || errorCode == 403){
        var icon = "fa fa-user-shield";
        var text = __("Access denied");
    }
    else if(errorCode == 404 && target == 'file_structure'){
        var icon = "fa fa-folder-open";
        var text = __("Target folder not found.");
    }
    else if(errorCode == 404){
        var icon = "fa fa-cubes";
        var text = __("Target component not found.");
    }
    else{
        var icon = "fa fa-cogs";
        var text = __("Oops, unexpected error, please try again later.");
    }

    Front.componentFillHtml('<div class="component-placeholder d-flex justify-content-center align-items-center"><div class="component-error text-center"><i class="component-error-icon '+icon+'"></i> <div class="component-error-text">'+text+'</div> </div></div>', domTarget);
    console.log(data);
};

Front.componentReload = function(domElement, additionalData){
    let componentState = {};
    if(!domElement.attr('data-component-state')){
        alert('Cannot load component state (data-component-state attributte missing).');
        return;
    }

    try{
        componentState = JSON.parse(domElement.attr('data-component-state'));
    }
    catch(e){
        alert('Cannot parse component state (JSON invalid).');
        return;
    }

    additionalData = Object.assign(
        componentState.additionalData,
        additionalData
    );

    Front.processAction(
        'component:'+componentState.selector,
        componentState.target,
        componentState.ref,
        additionalData,
        componentState.userInput,
        componentState.backgroundAction
    );
};

/* SHOW FUNCTION LOADING */
Front.funcLoading = function(){
    Swal.showLoading();
};

/* SHOW FUNCTION ERROR */
Front.funcError = function(errorCode, data){
    if(errorCode == 401 || errorCode == 403){
        var errorText = __("Access denied");
    }
    else if(errorCode == 404){
        var errorText = __("Target function not found.");
    }
    else{
        var errorText = __("Oops, unexpected error, please try again later.");
    }

    Swal.fire({
        "icon": "error",
        "title": __("Error!"),
        "html": errorText,
        "showCloseButton": true,
        "showConfirmButton": true,
        "confirmButtonText": __("OK")
    });

    console.log(data);
};

/* SHOW FUNCTION RESPONSE */
Front.funcResponse = function(response){
    try {
        response = $.parseJSON(response);
    }
    catch (err) {
        Front.funcError(500, response);
        return;
    }

    if(response["attach_script"].length > 0){
        $('body').append('<script type="text/javascript">'+response["attach_script"]+'</script>');
    }

    if(response["swal_data"]){
        Swal.fire(response["swal_data"]);
    }
};

Front.serializedDataToObject = function(formArray){
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++){
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
};


/* FILTERS FUNCTIONS */
Front.loadTableFilters = function(componentDom){
    let filters = {};
    $(componentDom).find('.table-th-filter-wrapper').each(function(idx, filterItem){
        let filterName = $(filterItem).attr('data-filter-name');
        let filterType = $(filterItem).attr('data-filter-type');
        let filterValue = '';
        switch (filterType) {
            case 'text':
                filterValue = $(filterItem).find('.form-control').first().val() ? $(filterItem).find('.form-control').first().val() : '';
                break;
            case 'select':
            case 'multiselect':
                filterValue = $(filterItem).find('select.form-control').first().val() ? $(filterItem).find('select.form-control').first().val() : '';
                break;
            case 'between':
                filterValue = [
                    $(filterItem).find('input.form-control')[0].value,
                    $(filterItem).find('input.form-control')[1].value
                ];
            break;
        }
        filters[filterName] = filterValue;
    });

    return filters;
};

Front.runFilters = function(componentDom){
    /* get filters data of the filter group */
    let filters = Front.loadTableFilters(componentDom);

    /* RELOAD COMPONENT WITH FILTER DATA */
    Front.componentReload(componentDom, {
        page: 1,
        filters: filters
    });
};

Front.clearFilter = function(componentDom, filterId){
    let currentFilter = $(componentDom).find('#'+filterId);
    let currentFilterType = currentFilter.attr('data-filter-type');
    switch (currentFilterType) {
        case 'text':
            $(currentFilter).find('.form-control').first().val('');
            break;
        case 'select':
        case 'multiselect':
            $(currentFilter).find('select.form-control').first().val('');
            break;
        case 'between':
            $($(currentFilter).find('input.form-control')[0]).val('');
            $($(currentFilter).find('input.form-control')[1]).val('');
            break;
    }
};

Front.showModal = function(content, title, size){
    content = content || "";
    title = title || "";
    size = size || "";

    /* close any swal windows */
    Swal.close();

    /* set size */
    $("#bsModal .modal-dialog").attr('class', 'modal-dialog modal-dialog-centered modal-'+size);

    /* fill title */
    if(title != ""){
        $("#bsModal .modal-header").show();
        $("#bsModal .modal-header .modal-title").html(title);
    }
    else{
        $("#bsModal .modal-header").hide();
    }

    /* fill content */
    $("#bsModal .modal-body").html(content);

    /* show modal */
    $("#bsModal").modal('show');
};

Front.closeModal = function(){
    /* hide modal */
    $("#bsModal").modal('hide');
};

/* ADD ITEM TO A FOLDER STRUCTURE */
Front.addToFolderStructure = function(){
    let selector = $("#entity-add-to-folder-structure-selector");
    if(selector){
        let parentId = selector.attr('data-parent-id');
        let ref = selector.val();

        Front.action(
            'component:#component',
            'form',
            ref,
            false,
            false,
            {
                '_add_to_file_structure_id': parentId,
            },
            false
        );
    }
};

$(document).ready(function(){
    /* clear modal data after modal is hidden */
    $('#bsModal').on('hidden.bs.modal', function () {
        $("#bsModal .modal-title").html('');
        $("#bsModal .modal-body").html('');
        $("#bsModal .modal-body").removeAttr('data-component-state');
    });

    /* COMPONENT PAGINATION */
    $('body').on('click', '[data-table-component-pagination] .pagination a', function(e){
        e.preventDefault();
        let url = $(this).attr('href');
        if(url.startsWith('?')){
            url = 'http://someurl.com/some/path'+url;
        }

        /* get page number */
        url = new URL(url);
        let pageNumber = url.searchParams.get("page");

        /* get component dom element */
        let componentRoot = $(this).parents('.table-component');
        let componentDom = componentRoot.first().parent();

        let filters = Front.loadTableFilters(componentDom);
        Front.componentReload(componentDom, {
            page: pageNumber,
            filters: filters
        });
    });


    /* COMPONENT TABLE FILTERS */
    let filterShowAnimationLength = 300;

    /* show filters box */
    $('body').on('click', '.table-th-filter-show-btn', function(e){
        e.preventDefault();

        /* get component dom element */
        let componentRoot = $(this).parents('.table-component');
        let componentDom = componentRoot.first().parent();

        let filterId = $(this).attr('data-filter');

        /* hide other filters of the filter group */
        $(componentDom).find('.table-th-filter-wrapper:not(#'+filterId+')').hide(filterShowAnimationLength);

        /* show current filter */
        let sidebarBreakpoint = 768;
        let viewportRes = $(window).width();
        let sidebarBreakpointReached = viewportRes > sidebarBreakpoint;

        let sidebarActive = $('main').hasClass('sidebar-active');
        let filterThOffset = $(this).parents('th').first().position();

        let tableLeftOffset = 40;
        if((sidebarBreakpointReached && !sidebarActive) || (!sidebarBreakpointReached && sidebarActive)){
            tableLeftOffset = 290;
        }

        /* position filter correctly */
        if(filterThOffset.left - tableLeftOffset - 230 >= 0){ // 230 = width of filter
            $(componentDom).find('#'+filterId).css({
               left: "unset",
               right: "0px"
            });
        }
        else{
            $(componentDom).find('#'+filterId).css({
                left: "0px",
                right: "unset"
            });
        }

        $(componentDom).find('#'+filterId).show(filterShowAnimationLength);
    });

    /* hide filters box */
    $('body').on('click', function(e){
        /* hide all filters */
        if (!$(e.target).closest('.table-th-filter-show-btn, .table-th-filter-wrapper').length) {
            $('.table-th-filter-wrapper').hide(filterShowAnimationLength);
        }
    });

    /* run filters */
    $('body').on('click', '.filters-submit-btn', function(e){
        e.preventDefault();

        /* hide all filters */
        $('.table-th-filter-wrapper').hide(filterShowAnimationLength);

        /* get component dom element */
        let componentRoot = $(this).parents('.table-component');
        let componentDom = componentRoot.first().parent();

        /* run filters */
        Front.runFilters(componentDom);
    });

    /* run filters on enter key */
    $('body').on('keyup', '.table-th-filter input.form-control', function(e){
        e.preventDefault();
        let keyCode = (e.keyCode ? e.keyCode : e.which);
        if (keyCode == 13) {
            /* hide all filters */
            $('.table-th-filter-wrapper').hide(filterShowAnimationLength);

            /* get component dom element */
            let componentRoot = $(this).parents('.table-component');
            let componentDom = componentRoot.first().parent();

            /* run filters */
            Front.runFilters(componentDom);
        }
    });

    /* clear current filter */
    $('body').on('click', '.filters-clear-btn', function(e){
        e.preventDefault();

        /* hide all filters */
        $('.table-th-filter-wrapper').hide(filterShowAnimationLength);

        /* get component dom element */
        let componentRoot = $(this).parents('.table-component');
        let componentDom = componentRoot.first().parent();

        /* clear current filter */
        Front.clearFilter(componentDom, $(this).attr('data-filter'));

        /* run filters */
        Front.runFilters(componentDom);
    });

    /* clear all filters */
    $('body').on('click', '.table-th-filter-clear-all-btn', function(e){
        e.preventDefault();

        /* hide all filters */
        $('.table-th-filter-wrapper').hide(filterShowAnimationLength);

        /* get component dom element */
        let componentRoot = $(this).parents('.table-component');
        let componentDom = componentRoot.first().parent();

        /* clear all filters in the group */
        $(componentDom).find('.table-th-filter-wrapper').each(function (idx,filterItem) {
            Front.clearFilter(componentDom, $(filterItem).attr('id'));
        });

        /* run filters */
        Front.runFilters(componentDom);
    });
});


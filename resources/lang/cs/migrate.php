<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'update_needed' => 'Vyžadován update databáze',
    'info_msg' => 'Systém nalezl čekající migrační soubory ke spuštění. Tyto soubory byli vygenerovány na základě definičních souborů entit.',
    'warning_msg' => 'Migrační soubory obsahují destruktivní příkazy, hrozí riziko ztráty dat. Před pokračováním doporučujeme provést zálohu databáze.',
    'waiting' => 'Migrační soubory ke spuštění',
    'show_more' => 'Podrobnější informace',
    'run' => 'Spustit migrace',
    'confirm_run' => 'Jste si jistý, že chcete spustit tyto migrace?',

];

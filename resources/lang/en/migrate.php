<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'update_needed' => 'Database update needed',
    'info_msg' => 'The system has found waiting migrations to be executed. These migrations were generated based on entity definition files.',
    'warning_msg' => 'Migrations contain destructive commands, data loss may appear. Database backup is recommended before proceeding.',
    'waiting' => 'Waiting migrations',
    'show_more' => 'More info',
    'run' => 'Run migrations',
    'confirm_run' => 'Are you sure you want to run these migrations?',

];

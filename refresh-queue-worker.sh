#!/bin/bash

# Go to parent directory relative to this file location
PARENT_PATH=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd $PARENT_PATH

# KILL CURRENT QUEUE WORKERS
for pid in $(ps -ef | grep "artisan queue:work" | awk '{print $2}'); do kill -9 $pid; done

# RUN QUEUE WORKER
nohup php artisan queue:work > /dev/null 2>&1 &

exit 0

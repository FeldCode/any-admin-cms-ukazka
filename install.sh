#!/bin/bash

# Go to parent directory relative to this file location
PARENT_PATH=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd $PARENT_PATH

# Load npm
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Save vars from command line
ENV_TYPE=$1
COMPOSER_DEV=$2

# Test input variables before begin
if [ -z "$COMPOSER_DEV" ]; then
        if [ $ENV_TYPE == "production" ]
        then
            COMPOSER_DEV="-no-dev"
        else
            COMPOSER_DEV="-dev"
        fi
fi

if [ -z "$ENV_TYPE" ]; then

        echo "Usage: <environment type [local,preview,production]>"

	exit 0
fi

echo "Running deploy script for environment [$ENV_TYPE]"

# INSTALL NPM DEPENDENCIES
echo "installing npm dependencies..."
npm install

# INSTALL COMPOSER DEPENDENCIES
if [ $COMPOSER_DEV == "-no-dev" ]
then
    echo "installing composer dependencies -no-dev..."
    composer install --optimize-autoloader --no-dev
else
    echo "installing composer dependencies -dev..."
    composer install
fi

# BUILD JS AND SASS resources
echo "building resources..."
npm run prod

# COPY .env SETTINGS
echo "creating .env file..."
cp .env.$ENV_TYPE .env

# CREATE STORAGE LINK IF NOT EXISTS
echo "creating storage link..."
php artisan storage:link

# CLEAR CACHE
echo "clearing app cache..."
php artisan optimize:clear

exit 0

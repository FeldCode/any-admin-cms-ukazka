<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* HOME PAGE */
Route::get('/', 'Admin\HomeController@homePageRedirect');

/* AUTH */
Auth::routes(['register' => false, 'verify' => true]);
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/email/verify-finished', 'Auth\VerificationController@verifyFinished');

/* CORE ROUTES */
Route::any('/p/{all}', 'Admin\PagesController@init')->where('all', '.*');
Route::any('/component/{all}', 'Admin\ComponentsController@init')->where('all', '.*');
Route::any('/func/{all}', 'Admin\FuncsController@init')->where('all', '.*');

/* CORE HELPER ROUTES */
Route::any('/helper/custom-input-form', 'Admin\HelpersController@customInputForm')->where('all', '.*');
Route::any('/helper/get-entity-items-json', 'Admin\HelpersController@getEntityItemsJson')->where('all', '.*');
Route::any('/helper/get-table-component-csv', 'Admin\HelpersController@getTableComponentCsv')->where('all', '.*');

/* FILES */
Route::any('files/dz-upload', 'Admin\FilesController@dzUpload');
Route::resource('files', 'Admin\FilesController')->except(['index', 'create', 'show', 'edit', 'update']);

/* CUSTOM ROUTES */
Route::get('cr/invoice-pdf', 'Admin\CustomRoutesController@downloadInvoice');



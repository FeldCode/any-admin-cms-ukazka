<?php

use App\DefProcessor;
use App\Generator;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /* LOAD RELATIONS */
            global $definitions;
            $definitions = new DefProcessor();
            $generator = new Generator();
            $generator->registerRelations();
        /* LOAD RELATIONS */

        /* CLEAR ALL DATA */
        \App\Entities\PaymentTypeEntity::query()->truncate();

        /* GENERATING PAYMENT TYPES - FIXED VALUES */
        $pt = new \App\Entities\PaymentTypeEntity();
        $pt->id = 1;
        $pt->title = "Cash";
        $pt->account_number = null;
        $pt->save();

        $pt = new \App\Entities\PaymentTypeEntity();
        $pt->id = 2;
        $pt->title = "Bank Transfer";
        $pt->account_number = "107-1894465/0100";
        $pt->save();


        /* GENERATING CUSTOMERS - including associated projects, invoices and contact persons */
        factory(App\Entities\CustomerEntity::class, 35)->create();
    }
}

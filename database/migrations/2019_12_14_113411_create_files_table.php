<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sort')->nullable()->unsigned()->index();
            $table->boolean('is_hidden')->default(0)->index();
            $table->string('rel_type')->nullable();
            $table->integer('rel_id')->nullable()->unsigned();
            $table->string('rel_field')->nullable();
            $table->index(['rel_type', 'rel_id', 'rel_field']);
            $table->integer('created_user_id')->unsigned()->nullable()->index();
            $table->integer('updated_user_id')->unsigned()->nullable()->index();
            $table->timestamps();

            $table->string('file_name');
            $table->boolean('is_image');
            $table->string('type');
            $table->integer('size')->unsigned();
            $table->string('ext');
            $table->string('upload_name');
            $table->mediumText('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}

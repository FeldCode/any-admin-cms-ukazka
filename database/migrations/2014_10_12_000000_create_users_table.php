<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Migration type (create, update, create_pivot, update_pivot)
     */
    const MIGRATION_TYPE = "create";

    /**
     * Table that the migration works with
     */
    const MIGRATION_TABLE = "users";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sort')->nullable()->unsigned()->index();
            $table->boolean('is_hidden')->default(0)->index();
            $table->string('name')->nullable();
            $table->string('login')->unique();
            $table->string('password')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->integer('user_group')->nullable()->unsigned()->index();
            $table->rememberToken();
            $table->integer('created_user_id')->nullable()->unsigned()->index();
            $table->integer('updated_user_id')->nullable()->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

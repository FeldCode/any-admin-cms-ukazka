<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\User;

class CreateAdminUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $ug = new User();
        $ug->id = 1;
        $ug->name = "Administrator";
        $ug->login = "admin";
        $ug->email = "admin@admin";
        $ug->password = bcrypt('admin');
        $ug->user_group = 1;
        $ug->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        User::find(1)->delete();
    }
}

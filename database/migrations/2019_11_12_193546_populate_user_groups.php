<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\UserGroup;

class PopulateUserGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $ug = new UserGroup();
        $ug->id = 1;
        $ug->code_name = "admins";
        $ug->name = "Admins";
        $ug->admin_access = true;
        $ug->save();

        $ug = new UserGroup();
        $ug->id = 2;
        $ug->code_name = "anonymous";
        $ug->name = "Anonymous";
        $ug->admin_access = false;
        $ug->save();

        $ug = new UserGroup();
        $ug->id = 3;
        $ug->code_name = "power_users";
        $ug->name = "Power Users";
        $ug->admin_access = true;
        $ug->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        UserGroup::find(1)->delete();
        UserGroup::find(2)->delete();
    }
}

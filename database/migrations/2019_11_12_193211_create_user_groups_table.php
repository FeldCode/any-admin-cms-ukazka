<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserGroupsTable extends Migration
{
    /**
     * Migration type (create, update, create_pivot, update_pivot)
     */
    const MIGRATION_TYPE = "create";

    /**
     * Table that the migration works with
     */
    const MIGRATION_TABLE = "user_groups";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sort')->nullable()->unsigned()->index();
            $table->boolean('is_hidden')->default(0)->index();
            $table->string('code_name')->unique();
            $table->string('name')->nullable();
            $table->boolean('admin_access')->index()->default(0);

            $table->integer('created_user_id')->nullable()->unsigned()->index();
            $table->integer('updated_user_id')->nullable()->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_groups');
    }
}

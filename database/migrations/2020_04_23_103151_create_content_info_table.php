<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentInfoTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('content_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sort')->nullable()->unsigned()->index();
            $table->string('rel_type')->nullable();
            $table->integer('rel_id')->nullable()->unsigned();
            $table->string('ident')->nullable()->unique(); //text identification (SEO)
            $table->string('url')->nullable()->index(); //ident of parents + current ident
            $table->integer('parent_id')->nullable()->unsigned()->index(); //enables entities in a folder structure
            $table->nullableTimestamps();

            $table->index(['rel_type', 'rel_id']);
        });
    }
    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('content_info');
    }
}

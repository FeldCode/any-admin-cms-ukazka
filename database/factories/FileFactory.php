<?php

use Faker\Generator as Faker;

use App\Recipe;
use Illuminate\Support\Facades\DB;

$factory->define(\App\File::class, function (Faker $faker) {

    return [
        'rel_type' => "",
        'rel_id' => 0,
        'rel_field' => "",
        'file_name' => "",
        'is_image' => 0,
        'type' => "",
        'size' => 0,
        'ext' => "",
        'upload_name' => "",
        'description' => "",

    ];
});

$factory->afterCreating(\App\File::class, function ($file, $faker) {
    if($file->is_image == 1){
        $fName = "logo".$faker->numberBetween(1,3);
        $fileType = "jpg";
    }
    else{
        $fName = "file";
        $fileType = $faker->randomElement(['pdf', 'docx']);
    }

    $newFileName = $file->id.".".$fileType;
    $source = public_path('dummy-files/'.$fName.'.'.$fileType);
    $destination = storage_path('app/public/'.$newFileName);
    copy($source,$destination);

    $file->file_name = $newFileName;
    $file->ext = $file->type = $fileType;
    $file->size = filesize($source);
    $file->upload_name = $faker->text(10).$fileType;
    $file->save();
});

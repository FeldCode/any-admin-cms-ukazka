<?php

use Faker\Generator as Faker;

use App\Recipe;
use Illuminate\Support\Facades\DB;

$factory->define(\App\Entities\CustomerEntity::class, function (Faker $faker) {
    $ident_number = $faker->randomNumber(8);

    return [
        'name' => $faker->company(),
        'address' => $faker->address(),
        'ident_number' => $ident_number,
        'vat_id' => "CZ".$ident_number
    ];
});

$factory->afterCreating(\App\Entities\CustomerEntity::class, function ($customer, $faker) {

    /* CREATING COUPLE PROJECTS FOR THE CUSTOMER */
    $projects = factory(App\Entities\ProjectEntity::class, $faker->numberBetween(1,6))->create();
    foreach ($projects as $project){
        $project->rel_customer()->associate($customer);
        $project->save();

        /* CREATE INVOICES FOR THE PROJECT */
        $invoicesNumber = $faker->numberBetween(1,4);
        $price = $project->cost / $invoicesNumber;
        for($i = 1; $i <= $invoicesNumber; $i++){
            $customer->createInvoice((object)[
                "amount" => $price,
                "payment_type" => 2,
                "description" => $faker->paragraph(3),
                "project" => $project->id,
            ]);
        }
    }

    /* LINKING TO RANDOM CONTACT PERSONS */
    $contactPersons = factory(App\Entities\ContactPersonEntity::class, $faker->numberBetween(1,2))->create();
    foreach ($contactPersons as $cp){
        $customer->rel_contact_persons()->save($cp);
    }

});

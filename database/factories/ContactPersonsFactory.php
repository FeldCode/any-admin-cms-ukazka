<?php

use Faker\Generator as Faker;

use App\Recipe;
use Illuminate\Support\Facades\DB;

$factory->define(\App\Entities\ContactPersonEntity::class, function (Faker $faker) {
    return [
        'name' => $faker->name(),
        'mail' => $faker->email(),
        'phone' => $faker->phoneNumber()
    ];
});

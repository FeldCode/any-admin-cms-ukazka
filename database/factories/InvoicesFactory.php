<?php

use Faker\Generator as Faker;

use App\Recipe;
use Illuminate\Support\Facades\DB;

$factory->define(\App\Entities\InvoiceEntity::class, function (Faker $faker) {
    $ident_number = $faker->randomNumber(8);

    return [
        'name' => $faker->text(50),
        'address' => $faker->text(200),
        'ident_number' => $ident_number,
        'vat_id' => "CZ".$ident_number
    ];
});

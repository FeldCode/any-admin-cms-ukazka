<?php

use Faker\Generator as Faker;

use App\Recipe;
use Illuminate\Support\Facades\DB;

$factory->define(\App\Entities\ProjectEntity::class, function (Faker $faker) {

    $projectName = str_replace("", "-", $faker->text(10).$faker->randomElement(['com', 'cz', 'eu', 'info']));

    return [
        'name' => strtoupper($projectName),
        'url' => "http://".strtolower($projectName),
        'customer' => null,
        'cost' => $faker->numberBetween(25,200)*1000,
        'logo' => null,
        'description' => "<h4><u>".strtoupper($projectName)."</u></h4><p>".$faker->paragraph(6)."</p>"
    ];
});

$factory->afterCreating(\App\Entities\ProjectEntity::class, function ($project, $faker) {
    /* DUMMY LOGO */
    $logo = factory(App\File::class, 1)->create(["is_image" => 1])->first();
    $logo->rel_type = "App\Entities\ProjectEntity";
    $logo->rel_id = $project->id;
    $logo->rel_field = "logo";
    $logo->save();
    $project->logo = $logo->id;
    $project->save();

    /* DUMMY PROJECT FILES */
    $files = factory(App\File::class, $faker->numberBetween(2,4))->create();
    foreach ($files as $f){
        $f->rel_type = "App\Entities\ProjectEntity";
        $f->rel_id = $project->id;
        $f->rel_field = "files";
        $f->save();
    }
});

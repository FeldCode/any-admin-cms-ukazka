 @echo off

echo Installing composer packages...
 call composer install

echo Installing npm packages...
 call npm install

echo Copying env file...
 copy .env.example .env

echo generating app key...
 php artisan key:generate

echo creating storage link...
 php artisan storage:link

 pause


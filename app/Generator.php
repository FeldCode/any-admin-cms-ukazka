<?php

namespace App;

use App\Entities\CustomerEntity;
use App\Entities\ProjectEntity;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class Generator
{
    /**
     * CREATES ENTITIES CLASSES IF THEY DON'T EXIST
     * Old files should be removed manually avoid loosing data
     */
    public function createEntityModels(){
        $definitions = DefProcessor::loadDefinitionCodeNames();

        foreach ($definitions as $def){
            $def = explode(".", $def);
            if($def[0] == "entities" && $def[1])
                Artisan::call('make:entity '.Entity::getClassNameFromStr($def[1]));
        }
    }

    public static function getAllExistingMigrations(){

        $dbMigrations = [];
        if(Schema::hasTable("migrations"))
            $dbMigrations = DB::table("migrations")->get();

        $migrationsPath = base_path("database/migrations");
        $existingMigrations = [];
        foreach (scandir($migrationsPath) as $mfKey => &$mf){
            if($mf == "." || $mf == ".." || is_dir($migrationsPath."/".$mf)){
                unset($existingMigrations[$mfKey]);
                continue;
            }

            $mfOrg = $mf;
            $mf = str_replace(array($migrationsPath, ".php"), "", $mf);

            $ran = false;
            $batch = null;
            foreach ($dbMigrations as $dbMig){
                if($dbMig->migration == $mf){
                    $ran = true;
                    $batch = $dbMig->batch;
                    break;
                }
            }

            /* GET NAME WITHOUT TIMESTAMP */
            $timeStringLength = 18;
            $nameWithoutTimestamp = substr($mf, $timeStringLength, strlen($mf) - $timeStringLength);

            /* GET CLASS NAME */
            $className = self::getMigrationClassName($nameWithoutTimestamp);

            if(!class_exists($className))
                include_once $migrationsPath."/".$mfOrg;

            $isGenerated = false;
            $migrationInfo = [];
            if(defined($className.'::GENERATOR_MIGRATION')
                && $className::GENERATOR_MIGRATION){
                $isGenerated = true;
                $migrationInfo = [
                    "type" => $className::MIGRATION_TYPE,
                    "table" => $className::MIGRATION_TABLE,
                    "operations" => $className::getOperationsList()
                ];
            }

            $existingMigrations[] = [
                "ran" => $ran,
                "name" => $mf,
                "name_without_timestamp" => $nameWithoutTimestamp,
                "class_name" => $className,
                "is_generated" => $isGenerated,
                "info" => $migrationInfo,
                "batch" => $batch
            ];
        }

        return $existingMigrations;
    }

    public static function getMigrationClassName($migrationName){
        return ucfirst(Str::camel($migrationName));
    }

    public static function getGeneratorMigrations(){
        $existingMigrations = self::getAllExistingMigrations();
        foreach ($existingMigrations as $migKey => $migration){
            if(!$migration["is_generated"])
                unset($existingMigrations[$migKey]);
        }

        return $existingMigrations;
    }

    public static function getWaitingMigrations($generatedOnly = false){
        if($generatedOnly)
            $waitingMigrations = self::getGeneratorMigrations();
        else
            $waitingMigrations = self::getAllExistingMigrations();

        foreach ($waitingMigrations as $migKey => $mig){
            if($mig["ran"]){
                unset($waitingMigrations[$migKey]);
            }
        }

        return $waitingMigrations;
    }

    /**
     * CHECKS IF USER TRIGGERED MIGRATION RUN
     */
    public function userTriggerRunMigrations($request){
        if($request->isMethod('post') && $request->get('run-migrations')){
            Artisan::call("migrate --force");
            $request->session()->flash('success', ["title" => __("Success!"), "html" => __('Database migrations successfully executed, you can now continue using the application.'), "timer" => null, "showConfirmButton" => true]);
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * CREATES MIGRATIONS FOR ENTITIES IF NEEDED
     */
    public function checkAndCreateMigrations(){
        global $definitions;

        /* DELETE ALL OLD UNRAN MIGRATIONS */
        $migrationsPath = base_path("database/migrations");
        $waitingMigrations = self::getWaitingMigrations(true);
        foreach ($waitingMigrations as $mf){
            $migFileName = $migrationsPath."/".$mf["name"].".php";
            if(file_exists($migFileName))
                unlink($migFileName);
        }

        /* LOOP THROUGHT ENTITIES AND GENERATE NEW MIGRATION FILES */
        foreach ($definitions->getAll() as $def){
            if($def["type"] == "entities"){
                Artisan::call("make:entitymigration ".$def["name"]);
            }
        }

        /* 3) NOTIFY USER ABOUT CHANGES AND ALLOW HIM TO RUN MIGRATIONS*/
        return self::getWaitingMigrations();
    }

    /**
     * CREATES RELATIONSHIP METHODS FOR EACH CREATED ENTITY MODELS
     */
    public function registerRelations(){
        global $definitions;

        foreach ($definitions->getAll() as $def){
            if($def["type"] == "entities"){
                foreach ($def["data"]["_inputs"] as $inputKey => $input) {
                    $entity = $def["name"];
                    $inputArgs = explode(":", $input["type"]);
                    $inputType = $inputArgs[0];

                    if(in_array($inputType, ["select", "multiselect"])){

                        if(isset($inputArgs[1])){
                            $partner = $inputArgs[1];
                        }
                        else{
                            throw new \ErrorException($entity.': Input '.$inputKey.' is type of "'.$inputType.':[partner]" but does not have defined partner entity.');
                        }

                        if(!$definitions->check("entities.".$partner)){
                            throw new \ErrorException('Input "'.$inputKey.'" of entity "'.$entity.'": Partner entity "'.$partner.'"" does not exist.');
                        }

                        /* GET EXTRA FIELDS FROM PIVOT */
                        $pivotExtra = [];
                        if(count($input["relation_extra_inputs"]) > 0)
                            $pivotExtra = array_merge($pivotExtra, array_keys($input["relation_extra_inputs"]));

                        if($inputType == "select") $type = "N-1";
                        if($inputType == "multiselect") $type = "N-N";

                        /* REGISTER SOURCE ENTITY RELATION FUNCTION */
                        $this->registerRelation($entity, $partner, $inputKey, $type, false, $pivotExtra);

                        /* REGISTER INVERSE RELATION FUNCTION FROM PARTNER ENTITY */
                        $this->registerRelation($partner, $entity, $inputKey, DefProcessor::getInverseRelationType($type), true, $pivotExtra);
                    }

                    /* REGISTER SINGLE FILE RELATIONS */
                    elseif(in_array($inputType, ["file"])){
                        $def["class_name_full"]::addDynamicRelation(DefProcessor::getRelationName($inputKey), function ($model, $data) {
                            return $model->belongsTo('\App\File', $data);
                        }, $inputKey);
                    }

                    /* REGISTER MULTIPLE FILES RELATIONS */
                    elseif(in_array($inputType, ["files"])){
                        $def["class_name_full"]::addDynamicRelation(DefProcessor::getRelationName($inputKey), function ($model, $data) {
                            return $model->morphMany('App\File', 'rel')->where('rel_field', $data);
                        }, $inputKey);
                    }
                }


                /* REGISTER USER AUTHORSHIP RELATION */
                $this->registerRelation("user", $entity, "created_user_id", "1-N", true);
                $this->registerRelation("user", $entity, "updated_user_id", "1-N", true);
            }
        }
    }

    public function registerRelation($source, $partner, $fieldName, $type, $isInverse = false, $pivotExtra = []){
        global $definitions;
        $def = is_array($source) ? $source : $definitions->get("entities.".$source);
        $defPartner = is_array($partner) ? $partner : $definitions->get("entities.".$partner);
        $relInverseName = DefProcessor::getInverseRelationName($defPartner["name"], $fieldName);

        $data = [
            "field_name" => $fieldName,
            "rel_name" => DefProcessor::getRelationName($fieldName),
            "is_inverse" => $isInverse,
            "rel_inverse_name" => $relInverseName,
            "rel_type" => $type,
            "source_class" => $def["class_name_full"],
            "source_entity" => $def["name"],
            "partner_class" => $defPartner["class_name_full"],
            "partner_entity" => $defPartner["name"],
            "extra" => $pivotExtra
        ];

        $data["source_class"]::addDynamicRelation($data[$isInverse ? "rel_inverse_name" : "rel_name"], function ($model, $data) {
            switch ($data["rel_type"]) {
                case '1-N':
                    return $model->hasMany($data["partner_class"], $data["field_name"]);
                break;
                case 'N-1':
                    return $model->belongsTo($data["partner_class"], $data["field_name"]);
                break;
                case 'N-N':
                    $table = DefProcessor::getEntityPivotTable($data[$data["is_inverse"] ? "partner_entity" : "source_entity"], $data[$data["is_inverse"] ? "source_entity" : "partner_entity"], $data["field_name"]);
                    $rel = $model->belongsToMany($data["partner_class"], $table, DefProcessor::getEntityForeignKey($data["source_entity"]), DefProcessor::getEntityForeignKey($data["partner_entity"]));

                    if(count($data["extra"]) > 0)
                        return $rel->withPivot($data["extra"]);
                    else
                        return $rel;
                break;
            }
        }, $data);
    }
}

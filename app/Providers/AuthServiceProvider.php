<?php

namespace App\Providers;

use App\DefProcessor;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('check-permissions', function (){
            global $definitions;

            /* GET CURRENT PAGE */
            $request = resolve(\Illuminate\Http\Request::class);
            $page = $request->_page;

            /* GET AUTHENTICATED USER */
            $user = Auth::user();

            return $definitions->userHasRights($user, "pages.".$page);
        });

        Gate::define('admin-only', function (){
            $user = Auth::user();
            $userGroup = $user ? $user->getRelationship('user_group') : null;

            return $userGroup ? $userGroup->admin_access : false;
        });

        Gate::define('active-only', function (){
            $user = Auth::user();
            return $user && !$user->isDeadlocked();
        });

        Gate::define('viewWebSocketsDashboard', function ($user = null) {
            $user = Auth::user();
            $userGroup = $user ? $user->getRelationship('user_group') : null;

            return $userGroup ? $userGroup->admin_access : false;
        });
    }
}

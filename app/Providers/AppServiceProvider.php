<?php

namespace App\Providers;

use App\Helper;
use App\ReCaptcha;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\MessageBag;
use Illuminate\Support\ServiceProvider;
use function MongoDB\BSON\toJSON;

class AppServiceProvider extends ServiceProvider
{
    public $viewComposerCalled;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        /* USE PAGINATION ON COLLECTIONS */
        if (!Collection::hasMacro('paginate')) {
            Collection::macro('paginate',
                function ($perPage = 15, $page = null, $options = []) {
                    $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
                    return (new LengthAwarePaginator(
                        $this->forPage($page, $perPage), $this->count(), $perPage, $page, $options))->withPath('');
            });
        }

        view()->composer('*', function($view){
            /* definistions */
            global $definitions;
            if($definitions){
                $appDef = $definitions->get('app');
                $view->with(array('appDef' => $appDef, 'definitions' => $definitions));
            }

            /* web form submit - run only once */
            if(!$this->viewComposerCalled && isset($_SERVER["REQUEST_URI"])){
                $url = substr($_SERVER["REQUEST_URI"], 1, strlen($_SERVER["REQUEST_URI"]));
                $urlFirst = explode("/", $url)[0];
                if(!in_array($urlFirst, ["admin", "cron", "api"])){
                    if(request()->has('_form_sent') && request()->has('_form_entity') && request()->has('_form_auto_process') && request()->input('_form_auto_process') == 1 && $definitions){
                        $entity = request()->input('_form_entity');
                        $entityDef = $definitions->get('entities.'.$entity);
                        if($entityDef && $entityDef["data"]["allow_web_auto_process"]){

                            /* CHECK RECAPTCHA RESPONSE */
                            $reCaptchaOk = true;
                            if($entityDef["data"]["web_auto_process_use_recaptcha"]){
                                $reCaptchaOk = false;
                                $recaptchaResponse = request()->has('g-recaptcha-response') ? request()->input('g-recaptcha-response') : "";
                                if(!ReCaptcha::verify($recaptchaResponse)){
                                    $recaptchaError = (new MessageBag())->add('recaptcha', __('Are you sure you are not a robot?'));

                                    if(request()->has('_form_errors_in_popup') && request()->input('_form_errors_in_popup') == 1){
                                        session()->flash('error', ["title" => __("Error!"), "html" => $recaptchaError->all()]);
                                    }
                                    else{
                                        $view->withErrors($recaptchaError);
                                    }
                                }
                                else{
                                    $reCaptchaOk = true;
                                }
                            }

                            /* if recaptcha ok, continue */
                            if($reCaptchaOk){
                                $formResult = $entityDef["class_name_full"]::processInput(request());
                                if(!$formResult["state"]){
                                    if(request()->has('_form_errors_in_popup') && request()->input('_form_errors_in_popup') == 1){
                                        session()->flash('error', ["title" => __("Error!"), "html" => $formResult["errors"]->all()]);
                                    }
                                    else{
                                        $view->withErrors($formResult["errors"]);
                                    }
                                }
                                else{
                                    /* keep flash messages thorough the redirect (dont forget in flash_msg.blade.php - it does get rendered) */
                                    session()->flash('keep-now', true);

                                    /* display custom success message */
                                    if(!!$entityDef["data"]["web_auto_process_success_msg"] && !empty($entityDef["data"]["web_auto_process_success_msg"])){
                                        $successMsg = ["inLineReport" => false, "html" => $entityDef["data"]["web_auto_process_success_msg"]];
                                    }
                                    else{
                                        $successMsg = session()->get('success');
                                    }

                                    /* success message auto close */
                                    if($entityDef["data"]["web_auto_process_success_timer"]){
                                        $successMsg["timer"] = $entityDef["data"]["web_auto_process_success_timer"];
                                        $successMsg["showConfirmButton"] = false;
                                    }
                                    else{
                                        $successMsg["timer"] = null;
                                        $successMsg["showConfirmButton"] = true;
                                    }

                                    /* save/replace success message */
                                    session()->flash('success', $successMsg);

                                    /* redirect back */
                                    redirect()->back()->send();
                                }
                            }
                        }
                    }
                }
            }

            $this->viewComposerCalled = true;
        });

        /* load translations to use in the frontend */
        Cache::rememberForever('translations', function () {
            $jsonTranslations = File::exists(resource_path('lang/'.App::getLocale().".json")) ? File::get(resource_path('lang/'.App::getLocale().".json")) : '{}';
            $jsonTranslations = (array)json_decode($jsonTranslations);

            $phpFilesTranslations = (array)collect(File::allFiles(resource_path('lang/'.App::getLocale())))->flatMap(function ($file) {
                return [
                    ($translation = $file->getBasename('.php')) => trans($translation),
                ];
            });
            $phpFilesTranslations = array_pop($phpFilesTranslations);

            return json_encode(array_merge($phpFilesTranslations, $jsonTranslations));
        });
    }
}

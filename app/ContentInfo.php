<?php

namespace App;

use Behat\Transliterator\Transliterator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ContentInfo extends Model
{
    protected $table = 'content_info';
    protected $fillable = [
        "ident",
        "parent_id"
    ];
    public $identToBeCreated = null;

    public static function boot()
    {
        parent::boot();

        static::created(function ($item) {
            /* CHECK IF IDENT ALREADY EXISTS (must be unique) */
            $item->ident = $item->identToBeCreated;
            $identExists = ContentInfo::where('ident', $item->ident)->where('id', '!=', $item->id)->count() > 0;
            if($identExists){
                $item->ident = $item->id.'-'.$item->ident;
            }

            /* GET COMPLETE URL */
            $item->url = $item->getFullUrl(false);
            $item->save();
        });
    }

    public function getFullUrl($checkAndFix = true){
        $url = [];
        $parent = $this->parent;
        while($parent){
            $url[] = $parent->ident;
            $parent = $parent->parent;
        }

        $url = implode('/', array_reverse($url)).(!empty($url) ? '/' : '').$this->ident;

        if($checkAndFix && $this->url != $url){
            $this->url = $url;
            $this->save();
        }

        return $url;
    }

    public static function seoString($string){
        /* removes accents, makes characters lower and replaces special characters and spaces with '-' */
        return Transliterator::urlize($string);
    }

    public static function sortItems($ids){
        /* GET SORT OPTIONS BY ID & SORT IT */
        $sortOptions = self::find($ids)->pluck("sort")->toArray();
        sort($sortOptions, SORT_NUMERIC);

        /* APPLY SORT OPTIONS IN ORDER OF IDS RECEIVED */
        foreach ($ids as $key => $id){
            $item = self::find($id);
            if($item){
                $item->sort = $sortOptions[$key];
                $item->save();
            }
        }

        return true;
    }

    /*
     * $path = path of parent node
     * $entityFilter = array of entities that should be returned
     * $cond = array = [["table.column", "!=", ["value","value"] ], [ [...],[...] ] ] - if array has another array it is used as OR statement, like this: (column NOT IN value,value) and ( ... or ... )
     * $incHidden = include hidden items
     * $getAllSubnodesAtOnce = receive all subnodes and subnodes of all childern
     */
    public static function getSubnodes($path = "", $entityFilter = null, $cond = [], $order = ['content_info.sort', 'asc'], $incHidden = false, $getAllSubnodesAtOnce = false){
        global $definitions;

        $path = trim($path, "/");

        if($getAllSubnodesAtOnce){
            $subnodes = ContentInfo::whereNotNull('content_info.parent_id')
                                    ->where('content_info.url', '!=', $path) //exclude the parent node
                                    ->where('content_info.url', 'like', $path.'%'); //get all its children by path
        }
        else{
            /* GET PARENT NODE ID */
            $parentId = null;
            if(empty($path)){
                $parentId = 0;
            }
            $parentNode = ContentInfo::where('content_info.url', $path)->get()->first();
            if($parentNode){
                $parentId = $parentNode->id;
            }

            /* PARENT NODE DOES NOT EXIST */
            if(is_null($parentId)){
                return new Collection([]);
            }

            $subnodes = ContentInfo::where('content_info.parent_id', $parentId);
        }

        /* sort items */
        if(!empty($order)){
            $subnodes->orderBy($order[0], $order[1]);
        }

        /* apply entity filter */
        if($entityFilter){
            if(!is_array($entityFilter)){
                $entityFilter = [$entityFilter];
            }

            if(count($entityFilter) > 0){
                $subnodes->where(function ($q) use($entityFilter, $definitions){
                    foreach ($entityFilter as $ent){
                        $entityDef = $definitions->get('entities.'.$ent);
                        $q->orWhere('content_info.rel_type', trim($entityDef["class_name_full"], "\\"));
                    }
                });
            }
        }

        /* UNION JOIN all possible tables, that we need to filter */
        if(!$incHidden || !empty($cond)){ //join apply only if we need to filter

            /* get all condition tables that need to be joined */
            $condTables = [];
            $condColsByTables = [];
            if(!empty($cond)){
                foreach($cond as $condPart){
                    /* IS AND CONDITION */
                    if(is_array($condPart) && !is_array($condPart[0])){
                        $condColumnParsed = explode(".", $condPart[0]);
                        if(count($condColumnParsed) > 1 && $condColumnParsed[0] != "content_info"){
                            $condTables[] = $condColumnParsed[0];

                            /* get columns to join by table */
                            if(!isset($condColsByTables[$condColumnParsed[0]])) $condColsByTables[$condColumnParsed[0]] = [];

                            if(!in_array($condColumnParsed[1], $condColsByTables[$condColumnParsed[0]]))
                                $condColsByTables[$condColumnParsed[0]][] = $condColumnParsed[1];
                        }
                    }

                    /* IS OR CONDITION */
                    if(is_array($condPart) && is_array($condPart[0])){
                        foreach($condPart as $condOr){
                            if(is_array($condOr) && !is_array($condOr[0])){
                                $condColumnParsed = explode(".", $condOr[0]);
                                if(count($condColumnParsed) > 1 && $condColumnParsed[0] != "content_info"){
                                    $condTables[] = $condColumnParsed[0];

                                    /* get columns to join by table */
                                    if(!isset($condColsByTables[$condColumnParsed[0]])) $condColsByTables[$condColumnParsed[0]] = [];

                                    if(!in_array($condColumnParsed[1], $condColsByTables[$condColumnParsed[0]]))
                                        $condColsByTables[$condColumnParsed[0]][] = $condColumnParsed[1];
                                }
                            }
                        }
                    }
                }
            }

            /* get all tables to join */
            $tablesToJoin = [];
            foreach ($definitions->getAll() as $def){
                /* get only entities that uses content info */
                if($def["type"] == "entities" && $def["data"]["uses_content_info"]){
                    /* filter out those that we use in entity filter */
                    if(!$entityFilter || in_array($def["name"], $entityFilter)){

                        /* if we do not need to filter hidden items, check conditions array for needed table joins, if no need to join, continue */
                        if($incHidden && !in_array($def["table"], $condTables)) {
                            continue;
                        }

                        $tablesToJoin[$def["table"]] = str_replace("\\", "\\\\", trim($def["class_name_full"], "\\"));
                    }
                }
            }

            /* perform union joins */
            if($tablesToJoin > 0){
                $unitedTables = null;
                foreach ($tablesToJoin as $table => $relType){
                    $colsToSelectArr = [];
                    foreach ($condColsByTables as $srcTable => $colsToSelect){
                        foreach ($colsToSelect as $colToSelect){
                            $colsToSelectArr[] = ($srcTable == $table ? "`$table`."."`$colToSelect`" : "null")." AS ".$srcTable."__".$colToSelect;
                        }
                    }

                    $unionTable = DB::table($table)->selectRaw("`$table`.id AS _rel_id, '".$relType."' AS _rel_type, is_hidden".(count($colsToSelectArr) > 0 ? ", ".implode(", ", $colsToSelectArr) : ""));

                    if(!$unitedTables)
                        $unitedTables = $unionTable;
                    else
                        $unitedTables->union($unionTable);
                }

                $subnodes->leftJoinSub($unitedTables, "union_select", function ($join){
                    $join->on('union_select._rel_id', '=', 'content_info.rel_id')
                        ->on('union_select._rel_type', '=', 'content_info.rel_type');
                });
            }
        }

        /* skip hidden items */
        if(!$incHidden){
            $subnodes->where('union_select.is_hidden', "=", "0");
        }

        /* apply conditions */
        if(!empty($cond)){
            foreach ($cond as $condPart){
                /* IS AND CONDITION */
                if(is_array($condPart) && !is_array($condPart[0])){
                    $condColumnParsed = explode(".", $condPart[0]);
                    if(count($condColumnParsed) > 1 && $condColumnParsed[0] != "content_info"){

                        /* entity condition */
                        if(isset($condPart[1]) && isset($condPart[2])){
                            if(is_array($condPart[2])){
                                if($condPart[1] == "!="){
                                    $subnodes->whereNotIn("union_select.".$condColumnParsed[0]."__".$condColumnParsed[1], $condPart[2]);
                                }
                                else{
                                    $subnodes->whereIn("union_select.".$condColumnParsed[0]."__".$condColumnParsed[1], $condPart[2]);
                                }
                            }
                            else{
                                $subnodes->where("union_select.".$condColumnParsed[0]."__".$condColumnParsed[1], $condPart[1], $condPart[2]);
                            }
                        }

                    }
                    else{

                        /* content info condition */
                        $condColumnParsed = explode(".", $condPart[0]);
                        $colName = end($condColumnParsed);

                        if(is_array($condPart[2])){
                            if($condPart[1] == "!="){
                                $subnodes->whereNotIn("content_info.".$colName, $condPart[2]);
                            }
                            else{
                                $subnodes->whereIn("content_info.".$colName, $condPart[2]);
                            }
                        }
                        else{
                            $subnodes->where("content_info.".$colName, $condPart[1], $condPart[2]);
                        }

                    }
                }

                /* IS OR CONDITION */
                if(is_array($condPart) && is_array($condPart[0])){
                    $subnodes->where(function($q) use($condPart) {
                        foreach($condPart as $condOr){
                            if(is_array($condOr) && !is_array($condOr[0])){
                                $condColumnParsed = explode(".", $condOr[0]);
                                if(count($condColumnParsed) > 1 && $condColumnParsed[0] != "content_info") {

                                    /* entity condition */
                                    if (isset($condOr[1]) && isset($condOr[2])) {
                                        if (is_array($condOr[2])) {
                                            if ($condOr[1] == "!=") {
                                                $q->orWhereNotIn("union_select." . $condColumnParsed[0] . "__" . $condColumnParsed[1], $condOr[2]);
                                            } else {
                                                $q->orWhereIn("union_select." . $condColumnParsed[0] . "__" . $condColumnParsed[1], $condOr[2]);
                                            }
                                        } else {
                                            $q->orWhere("union_select." . $condColumnParsed[0] . "__" . $condColumnParsed[1], $condOr[1], $condOr[2]);
                                        }
                                    }

                                }
                                else{

                                    /* content info condition */
                                    $condColumnParsed = explode(".", $condOr[0]);
                                    $colName = end($condColumnParsed);

                                    if(is_array($condOr[2])){
                                        if($condOr[1] == "!="){
                                            $q->orWhereNotIn("content_info.".$colName, $condOr[2]);
                                        }
                                        else{
                                            $q->orWhereIn("content_info.".$colName, $condOr[2]);
                                        }
                                    }
                                    else{
                                        $q->orWhere("content_info.".$colName, $condOr[1], $condOr[2]);
                                    }

                                }
                            }
                        }
                    });
                }
            }
        }

        /* dump sql query */
        /*$sql = Str::replaceArray('?', $subnodes->getBindings(), $subnodes->toSql());
        dd($sql);*/

        return $subnodes->get();
    }

    public function parent(){
        return $this->belongsTo('App\ContentInfo');
    }

    public function childern(){
        return $this->hasMany('App\ContentInfo', 'parent_id');
    }

    public function entity(){
        return $this->morphTo('rel');
    }
}

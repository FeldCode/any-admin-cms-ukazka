<?php

namespace App\Components;

use App\Component;
use App\DefProcessor;
use Doctrine\DBAL\Schema\Schema;
use Illuminate\Http\Request;

class DetailComponent extends Component
{

    public function handle(Request $request, $action){
        global $definitions;

        $action["data"] = array_merge(
            $action["data"],
            $request->all()
        );

        /* entity data */
        $entityDef = $definitions->get('entities.'.$action["data"]["entity"]);
        $id = isset($action["data"]["id"]) && !is_null($action["data"]["id"]) ? $action["data"]["id"] : null;
        $data = $entityDef["class_name_full"]::find($id);

        /* OPERATION RIGHTS */
            $userCan = $request->user()->getAllowedEntityOperations($entityDef, $id);
        /* OPERATION RIGHTS */

        /* check if user can display component */
        if(!$userCan["detail"]){
            abort(401);
        }

        /* potential pivot extra data */
        $pivotInputs = [];
        $pivotData = [];
        if($request->has('_src_input_ref') && $request->has('_partner_entity') && $request->has('_partner_id')){

            //process incoming data
            $refPathParsed = explode(".", request()->input('_src_input_ref'));
            $srcInputKey = end($refPathParsed);
            $srcInputDef = $definitions->getByRef(request()->input('_src_input_ref'));
            $partnerEntityName = request()->input('_partner_entity');
            $partnerForeignKey = DefProcessor::getEntityForeignKey($partnerEntityName);
            $partnerId = request()->input('_partner_id');

            if($srcInputDef){
                //get relation extra inputs
                $srcInputType = DefProcessor::parseInputType($srcInputDef["type"]);
                if($srcInputType[0] == "multiselect_inv"){
                    $partnerDef = $definitions->get('entities.'.$srcInputType[1]);
                    $pivotInputs = $partnerDef["data"]["_inputs"][$srcInputType[2]]["relation_extra_inputs"];
                }
                else{
                    $pivotInputs = $srcInputDef["relation_extra_inputs"];
                }

                if(count($pivotInputs) > 0){
                    if($srcInputType[0] == "multiselect_inv"){
                        $pivotData = $data->getRelationship($srcInputType[2], true);
                    }
                    else{
                        $pivotData = $data->getInverseRelationship($partnerEntityName, $srcInputKey, true);
                    }

                    $pivotData = $pivotData->where($partnerForeignKey, $partnerId)->get()->first()->pivot;
                }
            }
        }

        return view('admin.components.'.$this->getName())->with([
            'entityDef' => $entityDef,
            'data' => $data,
            'id' => $id,
            'userCan' => $userCan,
            'action' => $action,
            'pivotInputs' => $pivotInputs,
            'pivotData' => $pivotData
        ]);
    }
}


<?php

namespace App\Components;

use App\Component;
use Illuminate\Http\Request;

class SeederComponent extends Component
{
    public function handle(Request $request, $action){
        global $definitions;
        $appDef = $definitions->get("app");
        $seed_btn = $appDef["data"]["custom_actions"]["seed_db"];
        $clear_btn = $appDef["data"]["custom_actions"]["clear_db"];

        return view('admin.components.'.$this->getName())->with([
            "seed_btn" => $seed_btn,
            "clear_btn" => $clear_btn
        ]);
    }
}


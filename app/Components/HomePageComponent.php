<?php

namespace App\Components;

use App\Component;
use Illuminate\Http\Request;

class HomePageComponent extends Component
{
    public function handle(Request $request, $action){
        return view('admin.components.'.$this->getName());
    }
}


<?php

namespace App\Components;

use App\Component;
use App\DefProcessor;
use App\File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

class FormComponent extends Component
{
    public function processRequest(Request $request, $edit, $action, $entityDef){
        /* GET ADDITIONAL DATA FROM REQUEST */
        $action["data"] = array_merge(
            $action["data"],
            $request->all()
        );

        /* GET ID OF ENTITY */
        $editId = null;
        if($edit){
            $editId = isset($action["data"]["id"]) && !is_null($action["data"]["id"]) ? $action["data"]["id"] : null;
        }

        return $entityDef["class_name_full"]::processInput($request, $editId);
    }

    public function handle(Request $request, $action){
        global $definitions;

        /* GET ADDITIONAL DATA FROM REQUEST */
        $action["data"] = array_merge(
            $action["data"],
            $request->all()
        );

        /* CHECK IF ENTITY EXISTS */
        if(!$definitions->check('entities.'.$action["data"]["entity"])){
            $request->session()->now('error', ["inLineReport" => true, "html" => __("Entity ':entity' does not exist, form couldn't be displayed.", ["entity" => $action["data"]["entity"]])]);
            return view('admin.inc.flash_msg');
        }

        /* GET ENTITY DEFINITION */
        $entityDef = $definitions->get('entities.'.$action["data"]["entity"]);

        /* GET FORM DATA */
        $requestData = $request;
        $data = null;
        $form_sent = $request->has("_form_sent");
        $editId = isset($action["data"]["id"]) && !is_null($action["data"]["id"]) ? $action["data"]["id"] : ($request->has("id") ? $request->input('id') : null);
        $edit = !is_null($editId);


        /* OPERATION RIGHTS */
            $userCan = $request->user()->getAllowedEntityOperations($entityDef, $editId);

            if(($edit && !$userCan["edit"])
            || (!$edit && !$userCan["add"])){
                abort(401);
            }
        /* OPERATION RIGHTS */

        if($edit) {
            $data = $entityDef["class_name_full"]::find($editId);
            if (!$data) {
                $request->session()->now('error', ["inLineReport" => true, "html" => __(":entity_name with id: ':entity_id' does not exist, form couldn't be displayed.", ["entity_name" => $entityDef["data"]["title"], "entity_id" => $editId])]);
                return view('admin.inc.flash_msg');
            }
        }

        /* PROCESS THE FORM */
        $processedForm = $this->processRequest($request, $edit, $action, $entityDef);
        $errors = $processedForm["errors"];

        /* RESETING THE FORM ON SUCCESS */
        $justAdded = null;
        if($form_sent && $processedForm["state"]){
            if($edit) {
                $data = $entityDef["class_name_full"]::find($editId);
            }
            else{
                $justAdded = $processedForm["entity"];
            }
            $form_sent = false;
        }

        return view('admin.components.'.$this->getName())->with([
            'entityDef' => $entityDef,
            'form_sent' => $form_sent,
            'request' => $requestData,
            'data' => $data,
            'edit' => $edit,
            'action' => $action,
            'justAdded' => $justAdded
        ])->withErrors($errors);
    }
}


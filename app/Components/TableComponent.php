<?php

namespace App\Components;

use App\Component;
use App\DefProcessor;
use Behat\Transliterator\Transliterator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class TableComponent extends Component
{
    public function handle(Request $request, $action, $renderView = true){
        global $definitions;

        /* KEEP PAGE AND FILTERS IN SESSION (only when in main component) */
        if($request->has('_dom_target') && $request->input('_dom_target') == '#component'){
            /* KEEP PAGE AND FILTERS IN SESSION */
                $currentComponentKey = $request->input('_dom_target').'__'.str_replace(".", "_", $action["data"]["ref"]);
                $tableDataSession = [];
                if($request->session()->has('table-data-session')){
                    $tableDataSession = $request->session()->get('table-data-session');
                }
                if(!isset($tableDataSession[$currentComponentKey])){
                    $tableDataSession[$currentComponentKey] = [];
                }

                if($request->has('filters')){
                    $tableDataSession[$currentComponentKey]["filters"] = $request->input('filters');
                }
                if($request->has('page')){
                    $tableDataSession[$currentComponentKey]["page"] = $request->input('page');
                }

                $request->session()->put('table-data-session', $tableDataSession);
            /* KEEP PAGE AND FILTERS IN SESSION */

            /* GET PAGE AND FILTERS FROM SESSION */
                if(!$request->has('filters') && isset($tableDataSession[$currentComponentKey]["filters"])){
                    $request->merge(['filters' => $tableDataSession[$currentComponentKey]["filters"]]);
                }
                if(!$request->has('page') && isset($tableDataSession[$currentComponentKey]["page"])){
                    $request->merge(['page' => $tableDataSession[$currentComponentKey]["page"]]);
                }
            /* GET PAGE AND FILTERS FROM SESSION */
        }

        /* GET ADDITIONAL DATA FROM REQUEST */
        $action["data"] = array_merge(
            $action["data"],
            $request->all()
        );

        /* CHECK IF ENTITY EXISTS */
        if(!$definitions->check('entities.'.$action["data"]["entity"])){
            $request->session()->now('error', ["inLineReport" => true, "html" => __("Entity ':entity' does not exist, table couldn't be displayed.", ["entity" => $action["data"]["entity"]])]);
            return view('admin.inc.flash_msg');
        }

        /* GET ENTITY DEFINITION */
        $entityDef = $definitions->get('entities.'.$action["data"]["entity"]);

        /* GET COLUMNS TO SHOW */
            $colsToShow = [];
            if(!empty($action["data"]["show_fields"])){
                if(!is_array($action["data"]["show_fields"])){
                    $colsToShow = explode(",", $action["data"]["show_fields"]);
                }
                else{
                    $colsToShow = $action["data"]["show_fields"];
                }
            }
            else{
                /* GET COLUMNS BASED ON INPUT DEFINITION */
                    if(empty($entityDef["data"]["listings_column_sort"])){
                        foreach ($entityDef["data"]["_inputs"] as $inputKey => $input){
                            if(!in_array($inputKey, $colsToShow) && $input["display_in_listings"]){
                                $colsToShow[] = $inputKey;
                            }
                        }
                    }
                /* GET COLUMNS BASED ON INPUT DEFINITION */

                /* GET COLUMNS BY DEFINITION FILE */
                    /* replace array indexes */
                    else{
                        /* FLIP AND MERGE ARRAYS */
                        $colsToShow = array_merge(array_flip($colsToShow), array_flip($entityDef["data"]["listings_column_sort"]));

                        /* SORT ARRAY AND ASSIGN POSITION TO ARRAY ITEMS */
                        asort($colsToShow, SORT_NUMERIC);
                        $i = 0;
                        foreach($colsToShow as $key => &$item){
                            $item = $i;
                            $i++;
                        }

                        /* FLIP ARRAY BACK */
                        $colsToShow = array_flip($colsToShow);
                    }
                /* SORT COLUMNS BY DEFINITION */
            }


            /* add title field if missing in cols to show */
            if(!in_array($entityDef["data"]["title_field"], $colsToShow)){
                $colsToShow = array_merge([$entityDef["data"]["title_field"]], $colsToShow);
            }
        /* GET COLUMNS TO SHOW */

        /* CHECK COLUMNS FOR EXTERNAL INPUTS */
            $externalInputs = [];
            foreach ($colsToShow as $colKey => $inputKey){
                if(!isset($entityDef["data"]["_inputs"][$inputKey])){
                    $externalInput = $entityDef["class_name_full"]::getInputByString($inputKey);
                    if($externalInput){
                        $externalInputs[$inputKey] = $externalInput;
                    }
                    else{
                        unset($colsToShow[$colKey]);
                    }
                }
            }
        /* CHECK COLUMNS FOR EXTERNAL INPUTS */


        /* GET TABLE DATA */
            /* APPLY ORDERING */
            /* format order */
            if(!empty($action["data"]["order"])){
                $order = explode(":", $action["data"]["order"]);
                $sortCol = $order[0];

                if(strpos($sortCol, ".") === false){
                    $sortCol = $entityDef["table"].'.'.$sortCol;
                }

                if(!isset($order[1])) $order[1] = "asc";
                $action["data"]["order"] = $sortCol.":".$order[1];
            }

            if(empty($action["data"]["order"])){
                if($entityDef["data"]["sortable"]){
                    $action["data"]["order"] = $entityDef["table"].".sort:asc";
                }
                else{
                    $action["data"]["order"] = $entityDef["table"].".id:desc";
                }
            }
            elseif($entityDef["data"]["sortable"] && $action["data"]["order"] != $entityDef["table"].'.sort:asc'){
                $entityDef["data"]["sortable"] = false;
            }

            $order = explode(":", $action["data"]["order"]);
            if(!isset($order[1])) $order[1] = "asc";
            $data = $entityDef["class_name_full"]::orderBy($order[0], $order[1]);

            /* APPLY CONDITIONS FOR COMPONENT */
            if(!empty($action["data"]["cond"])){
                $data = $data->whereRaw($action["data"]["cond"]);
            }

            /* APPLY FILTERS */
            $actionDefinedFilters = false;
            $useFilters = $action["data"]["use_filters"];
            $filtersToShow = [];
            if($useFilters){
                /* GET FILTERS TO SHOW FROM ACTION DATA */
                if(!empty($action["data"]["show_filters"]) && is_array($action["data"]["show_filters"])){
                    $filtersToShow = $action["data"]["show_filters"];
                    $actionDefinedFilters = true;
                }

                /* entity inputs */
                $inputsMerged = array_merge(
                    $entityDef["data"]["_inputs"],
                    $externalInputs
                );

                foreach ($inputsMerged as $filterKey => $input){

                    /* skip filters settings for columns that are not shown */
                    if(!in_array($filterKey, $colsToShow)){
                        if(isset($filtersToShow[$filterKey])) unset($filtersToShow[$filterKey]);
                        continue;
                    }

                    $isExternal = isset($input["_is_external_input"]) && $input["_is_external_input"];
                    if($isExternal){
                        $externalInputSettings = $input;
                        $input = $externalInputSettings["target_input_def"];

                        $targetIsInPivot = $externalInputSettings["target_is_in_pivot"];
                        $inputKey = $externalInputSettings["target_input"];
                        $currentEntityDef = $externalInputSettings["external_entity_def"];
                    }
                    else{
                        $targetIsInPivot = false;
                        $inputKey = $filterKey;
                        $currentEntityDef = $entityDef;
                    }

                    /* get filterable fields or action defined filters */
                    if((!$actionDefinedFilters && $input["filterable"]) || ($actionDefinedFilters && isset($filtersToShow[$filterKey]))){
                        $inputType = DefProcessor::parseInputType($input["type"]);
                        $filterType = $actionDefinedFilters ? $filtersToShow[$filterKey] : $input["filter_type"];

                        /* skip, if filter is not allowed */
                        if(!in_array($filterType, DefProcessor::allowedFiltersOnInputType($inputType[0]))){
                            if(isset($filtersToShow[$filterKey])) unset($filtersToShow[$filterKey]);
                            continue;
                        }

                        /* default filter settings structure */
                        $filtersToShow[$filterKey] = [
                            "filter_type" => $filterType,
                            "input_type" => $inputType,
                            "is_external" => $isExternal,
                            "options" => []
                        ];

                        /* get filter options for select and multiselect filter type */
                        if(in_array($filterType, ["select", "multiselect"])){

                            /* no filter options needed for these types */
                            if(!in_array($inputType[0], [
                                "text",
                                "html",
                                "boolean",
                                "password",
                                "file",
                                "files"
                            ])){
                                $options = [];

                                /* related content */
                                if(in_array($inputType[0], [
                                    "select",
                                    "select_inv",
                                    "multiselect",
                                    "multiselect_inv"
                                ])){
                                    $partnerDef = $definitions->get("entities.".$inputType[1]);
                                    $optionsDb =  $partnerDef["class_name_full"]::orderBy($partnerDef["data"]["title_field"])->get();
                                    foreach ($optionsDb as $item){
                                        /* icon */
                                        $icon = "";
                                        if(!empty($item->getEntityIcon())){
                                            $icon = '<i class="'.$item->getEntityIcon().'"></i> ';
                                        }

                                        $options[$item->id] = $icon.$item->getEntityTitle();
                                    }
                                }

                                /* calculated values */
                                elseif($inputType[0] == "calculated"){
                                    $items = $currentEntityDef["class_name_full"]::all();
                                    foreach ($items as $item){
                                        if(!is_null($item->$inputKey) && $item->$inputKey != '' && !in_array($item->$inputKey, array_keys($options))){
                                            $options[$item->$inputKey] = $item->getFormattedValue($inputKey);
                                        }
                                    }
                                    $items = null;

                                    asort($options);
                                }

                                /* options from entity own table */
                                else{
                                    if($targetIsInPivot){
                                        /* get pivot table */
                                        if($externalInputSettings["is_inverse"]){
                                            $pivotTable = DefProcessor::getEntityPivotTable($externalInputSettings["external_entity"], $entityDef["name"], $externalInputSettings["src_input"]);
                                        }
                                        else{
                                            $pivotTable = DefProcessor::getEntityPivotTable($entityDef["name"], $externalInputSettings["external_entity"], $externalInputSettings["src_input"]);
                                        }

                                        $optionsDb = DB::table($pivotTable)->whereNotNull($inputKey)->where($inputKey, '!=', '')->groupBy($inputKey)->orderBy($inputKey)->get();
                                    }
                                    else{
                                        $optionsDb = $currentEntityDef["class_name_full"]::whereNotNull($inputKey)->where($inputKey, '!=', '')->groupBy($inputKey)->orderBy($inputKey)->get();
                                    }

                                    foreach ($optionsDb as $item){
                                        if(!is_null($item->$inputKey)){
                                            if(in_array($inputType[0], ["date", "datetime"]) && ($targetIsInPivot || !$item->formattedMethodExists($inputKey))){
                                                /* get raw DB date */
                                                $rawDbDate = $targetIsInPivot ? $item->$inputKey : $item->attributesToArray()[$inputKey];

                                                /* convert date to Carbon if needed */
                                                $item->$inputKey = \App\Traits\EntityTrait::toCarbon($item->$inputKey);

                                                /* get option with value as raw DB and text as formatted date */
                                                $options[$rawDbDate] = !empty($item->$inputKey) ? $item->$inputKey->format(env('APP_DATE_FORMAT', 'Y-m-d').($inputType[0] == 'datetime' ? ' '.env('APP_TIME_FORMAT', 'H:i:s') : '')) : '';
                                            }
                                            elseif(!$targetIsInPivot){
                                                $options[$item->$inputKey] = $item->getFormattedValue($inputKey);
                                            }
                                            else{
                                                $options[$item->$inputKey] = $item->$inputKey;
                                            }

                                            if($inputType[0] == "icon"){
                                                $options[$item->$inputKey] = '<i class="'.$item->$inputKey.'"></i> '.$options[$item->$inputKey];
                                            }
                                            if($inputType[0] == "color"){
                                                $options[$item->$inputKey] = '<span style="position:relative;width:25px;height:25px;" class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="background-color: '.$item->$inputKey.';"></span></span> '.$options[$item->$inputKey];
                                            }
                                        }
                                    }
                                }

                                $filtersToShow[$filterKey]["options"] = $options;
                            }
                        }
                    }
                }

                /* GROUP BY ENTITY ID - filters may apply joined tables */
                $data->select($entityDef["table"].'.*')->groupBy($entityDef["table"].".id");

                /* if filter on calculated field - all data must be retrieved from the database and filtered later in code */
                $hasCalculatedFilter = false;

                /* keep list of joined tables of external fields */
                $joinedTables = [];

                /* APPLY FILTERS IF SET */
                if($request->has('filters') && !empty($request->input('filters'))){
                    foreach($request->input('filters') as $fKey => $fVal){

                        /* filter setting exists */
                        if(!isset($filtersToShow[$fKey])){
                            continue;
                        }

                        /* check if filter is active */
                        $filterSettings = $filtersToShow[$fKey];
                        $fType = $filterSettings["filter_type"];
                        $inpType = $filterSettings["input_type"];
                        $filterActive = false;
                        switch($fType){
                            case 'text':
                            case 'select':
                                $filterActive = ($fVal != '');
                                break;
                            case 'multiselect':
                                $filterActive = !empty($fVal);
                                break;
                            case 'between':
                                $filterActive = !empty($fVal) && count($fVal) == 2 && ($fVal[0] != '' || $fVal[1] != '');
                            break;
                        }

                        if($filterActive){

                            /* filter type is not calculated (calculated type is not filtered throuht database) */
                            if($inpType[0] == "calculated"){
                                $hasCalculatedFilter = true;
                                continue;
                            }

                            /* CHECK IF IS EXTERNAL (external fields contain ':') */
                            $isExternal = $filterSettings["is_external"];

                            if($isExternal){
                                $externalInputSettings = $inputsMerged[$fKey];

                                /* join table name = join_by_(_[src_entity])_[src_input] */
                                $joinTableName = 'join_by'.($externalInputSettings["is_inverse"] ? '_'.$externalInputSettings["external_entity"] : '').'_'.$externalInputSettings["src_input"];

                                /* JOIN EXTERNAL TABLE BY THE CURRENT FIELD IF NOT PREVIOUSLY JOINED ALREADY */

                                $srcInputDef = $externalInputSettings["src_input_def"];
                                $srcInputType = DefProcessor::parseInputType($srcInputDef["type"]);

                                if($srcInputType[0] == 'select'){

                                    /* JOIN EXTERNAL TABLE - SELECT */
                                    if(!in_array($joinTableName, $joinedTables)){
                                        if($externalInputSettings["is_inverse"]){
                                            $data->leftJoin($externalInputSettings["external_entity_def"]["table"].' as '.$joinTableName, $entityDef["table"].'.id', '=', $joinTableName.'.'.$externalInputSettings["src_input"]);
                                        }
                                        else{
                                            $data->leftJoin($externalInputSettings["external_entity_def"]["table"].' as '.$joinTableName, $entityDef["table"].'.'.$externalInputSettings["src_input"], '=', $joinTableName.'.id');
                                        }

                                        $joinedTables[] = $joinTableName;
                                    }
                                }
                                elseif($srcInputType[0] == 'multiselect'){
                                    $partnerDef = $externalInputSettings["external_entity_def"];

                                    /* get foreign keys and partner table */
                                    $foreignKeyEntity = DefProcessor::getEntityForeignKey($entityDef["name"]);
                                    $foreignKeyPartner = DefProcessor::getEntityForeignKey($partnerDef["name"]);
                                    $partnerTable = $partnerDef["table"];


                                    /* JOIN PIVOT TABLE - MULTISELECT */
                                    if(!in_array($joinTableName.'_pvt', $joinedTables)){

                                        /* get pivot table */
                                        if($externalInputSettings["is_inverse"]){
                                            $pivotTable = DefProcessor::getEntityPivotTable($partnerDef["name"], $entityDef["name"], $externalInputSettings["src_input"]);
                                        }
                                        else{
                                            $pivotTable = DefProcessor::getEntityPivotTable($entityDef["name"], $partnerDef["name"], $externalInputSettings["src_input"]);
                                        }

                                        /* join table */
                                        $data->leftJoin($pivotTable.' as '.$joinTableName.'_pvt', $entityDef["table"].'.id', '=', $joinTableName.'_pvt'.'.'.$foreignKeyEntity);
                                        $joinedTables[] = $joinTableName.'_pvt';
                                    }

                                    /* JOIN EXTERNAL TABLE - MULTISELECT */
                                    if(!in_array($joinTableName, $joinedTables) && !$externalInputSettings["target_is_in_pivot"]){
                                         $data->leftJoin($partnerTable.' as '.$joinTableName, $joinTableName.'_pvt'.'.'.$foreignKeyPartner, '=', $joinTableName.'.id');
                                         $joinedTables[] = $joinTableName;
                                    }
                                }

                                $filterTable = $externalInputSettings["target_is_in_pivot"] ? $joinTableName.'_pvt' : $joinTableName;

                                $currentEntityDef = $externalInputSettings["external_entity_def"];
                                $relBaseTable = $filterTable;
                                $inputKey = $externalInputSettings["target_input"];
                                $relBaseType = ltrim($externalInputSettings["external_entity_def"]["class_name_full"], "\\");
                                $filterColumn = $filterTable.'.'.$inputKey;
                            }
                            else{
                                $currentEntityDef = $entityDef;
                                $relBaseTable = $entityDef["table"];
                                $inputKey = $fKey;
                                $relBaseType = ltrim($entityDef["class_name_full"], "\\");
                                $filterColumn = $relBaseTable.'.'.$inputKey;
                            }

                            /* FORMATING DATES */
                            if(in_array($inpType[0], ["date", "datetime"])){
                                $dateFormat = 'Y-m-d';
                                $timeFormat = 'H:i:s';
                                if(in_array($fType, ["between", "multiselect"])){
                                    foreach ($fVal as &$fValItem){
                                        if(!empty($fValItem))
                                            $fValItem = Carbon::parse($fValItem)->format($dateFormat.($inpType[0] == "datetime" ? ' '.$timeFormat : ''));
                                    }
                                }
                                else{
                                    if(!empty($fVal))
                                        $fVal = Carbon::parse($fVal)->format($dateFormat.($inpType[0] == "datetime" ? ' '.$timeFormat : ''));
                                }
                            }

                            /* APPLY FILTERS */
                            switch($fType){
                                case 'text':
                                    switch($inpType[0]){
                                        case 'int':
                                        case 'float':
                                        case 'icon':
                                        case 'date':
                                        case 'datetime':
                                        case 'time':
                                            $data->where($filterColumn, $fVal);
                                        break;
                                        case 'select':
                                        case 'select_inv':
                                            $partnerDef = $definitions->get("entities.".$inpType[1]);
                                            $partnerTable = $partnerDef["table"];

                                            $data->whereExists(function ($subQ) use($relBaseTable, $partnerTable, $inpType, $filterColumn, $fVal, $partnerDef){
                                                $subQ->select(DB::raw(1))
                                                    ->from($partnerTable);

                                                if($inpType[0] == 'select_inv') {
                                                    $subQ->whereRaw($relBaseTable . '.id' . '=' . $partnerTable . '.' . $inpType[2]);
                                                }
                                                else{
                                                    $subQ->whereRaw($filterColumn . '=' . $partnerTable . '.id');
                                                }

                                                $subQ->where($partnerTable.'.'.$partnerDef["data"]["title_field"], 'like', '%'.$fVal.'%');
                                            });
                                        break;
                                        case 'multiselect':
                                        case 'multiselect_inv':
                                            $partnerDef = $definitions->get("entities.".$inpType[1]);

                                            /* get foreign keys and partner table */
                                            $foreignKeyEntity = DefProcessor::getEntityForeignKey($currentEntityDef["name"]);
                                            $foreignKeyPartner = DefProcessor::getEntityForeignKey($partnerDef["name"]);
                                            $partnerTable = $partnerDef["table"];

                                            /* get pivot table */
                                            if($inpType[0] == 'multiselect_inv') {
                                                $pivotTable = DefProcessor::getEntityPivotTable($partnerDef["name"], $currentEntityDef["name"], $inpType[2]);
                                            }
                                            else{
                                                $pivotTable = DefProcessor::getEntityPivotTable($currentEntityDef["name"], $partnerDef["name"], $inputKey);
                                            }

                                            $data->whereExists(function ($subQ) use($relBaseTable, $partnerTable, $pivotTable, $foreignKeyEntity, $foreignKeyPartner, $fVal, $partnerDef){
                                                $subQ->select(DB::raw(1))
                                                    ->from($pivotTable)
                                                    ->whereRaw($relBaseTable . '.id = '. $pivotTable . '.' . $foreignKeyEntity)

                                                    /* join partner table to apply filters */
                                                    ->leftJoin($partnerTable, $partnerTable . '.id', '=', $pivotTable.'.'.$foreignKeyPartner )

                                                    /* apply filter */
                                                    ->where($partnerTable.'.'.$partnerDef["data"]["title_field"], 'like', '%'.$fVal.'%');
                                            });
                                        break;
                                        case 'file':
                                            $data->whereExists(function ($subQ) use ($filterColumn, $fVal){
                                                $subQ->select(DB::raw(1))
                                                    ->from('files')
                                                    ->whereRaw('files.id = '. $filterColumn)
                                                    ->where(function ($q) use ($fVal){
                                                        $q->where('files.upload_name', 'like', '%'.$fVal.'%')
                                                            ->orWhere('files.description', 'like', '%'.$fVal.'%');
                                                    });
                                            });
                                        break;
                                        case 'files':
                                            $data->whereExists(function ($subQ) use ($relBaseTable, $relBaseType, $inputKey, $fVal){
                                                $subQ->select(DB::raw(1))
                                                    ->from('files')
                                                    ->whereRaw('files.rel_id = '. $relBaseTable.'.id')
                                                    ->where('files.rel_type', '=', $relBaseType)
                                                    ->where('files.rel_field', '=', $inputKey)
                                                    ->where(function ($q) use ($fVal){
                                                        $q->where('files.upload_name', 'like', '%'.$fVal.'%')
                                                            ->orWhere('files.description', 'like', '%'.$fVal.'%');
                                                    });
                                            });
                                        break;
                                        default:
                                            $data->where($filterColumn, 'like', '%'.$fVal.'%');
                                        break;
                                    }
                                break;
                                case 'select':
                                    switch($inpType[0]){
                                        case 'password':
                                        case 'file':
                                        case 'text':
                                        case 'html':
                                            if($fVal == 0){
                                                $data->whereNull($filterColumn);
                                            }
                                            else{
                                                $data->whereNotNull($filterColumn);
                                            }
                                        break;
                                        case 'select_inv':
                                            $partnerDef = $definitions->get("entities.".$inpType[1]);
                                            $partnerTable = $partnerDef["table"];

                                            $data->where(DB::raw($relBaseTable . '.id'), function ($subQ) use($relBaseTable, $partnerTable, $inpType, $fVal){
                                                $subQ->select($inpType[2])
                                                    ->from($partnerTable)
                                                    ->where($partnerTable . '.id', '=', $fVal);
                                            });
                                        break;
                                        case 'multiselect':
                                        case 'multiselect_inv':
                                            $partnerDef = $definitions->get("entities.".$inpType[1]);

                                            /* get foreign keys and partner table */
                                            $foreignKeyEntity = DefProcessor::getEntityForeignKey($currentEntityDef["name"]);
                                            $foreignKeyPartner = DefProcessor::getEntityForeignKey($partnerDef["name"]);

                                            /* get pivot table */
                                            if($inpType[0] == 'multiselect_inv') {
                                                $pivotTable = DefProcessor::getEntityPivotTable($partnerDef["name"], $currentEntityDef["name"], $inpType[2]);
                                            }
                                            else{
                                                $pivotTable = DefProcessor::getEntityPivotTable($currentEntityDef["name"], $partnerDef["name"], $inputKey);
                                            }

                                            $data->whereExists(function ($subQ) use($relBaseTable, $pivotTable, $foreignKeyEntity, $foreignKeyPartner, $fVal){
                                                $subQ->select(DB::raw(1))
                                                    ->from($pivotTable)
                                                    ->whereRaw($relBaseTable . '.id = '. $pivotTable . '.' . $foreignKeyEntity)

                                                    /* apply filter */
                                                    ->where($pivotTable.'.'.$foreignKeyPartner, $fVal);
                                            });
                                        break;
                                        case 'files':
                                            $existsFunc = $fVal == 0 ? 'whereNotExists' : 'whereExists';
                                            $data->$existsFunc(function ($subQ) use ($relBaseTable, $relBaseType, $inputKey){
                                                $subQ->select(DB::raw(1))
                                                    ->from('files')
                                                    ->whereRaw('files.rel_id = '. $relBaseTable.'.id')
                                                    ->where('files.rel_type', '=', $relBaseType)
                                                    ->where('files.rel_field', '=', $inputKey);
                                            });
                                        break;
                                        default:
                                            $data->where($filterColumn, $fVal);
                                        break;
                                    }
                                break;
                                case 'multiselect':
                                    switch($inpType[0]) {
                                        case 'select_inv':
                                            $partnerDef = $definitions->get("entities.".$inpType[1]);
                                            $partnerTable = $partnerDef["table"];

                                            $data->whereIn(DB::raw($relBaseTable . '.id'), function ($subQ) use($relBaseTable, $partnerTable, $inpType, $fVal){
                                                $subQ->select($inpType[2])
                                                    ->from($partnerTable)
                                                    ->whereIn($partnerTable . '.id', $fVal);
                                            });
                                        break;
                                        case 'multiselect':
                                        case 'multiselect_inv':
                                            $partnerDef = $definitions->get("entities.".$inpType[1]);

                                            /* get foreign keys and partner table */
                                            $foreignKeyEntity = DefProcessor::getEntityForeignKey($currentEntityDef["name"]);
                                            $foreignKeyPartner = DefProcessor::getEntityForeignKey($partnerDef["name"]);

                                            /* get pivot table */
                                            if($inpType[0] == 'multiselect_inv') {
                                                $pivotTable = DefProcessor::getEntityPivotTable($partnerDef["name"], $currentEntityDef["name"], $inpType[2]);
                                            }
                                            else{
                                                $pivotTable = DefProcessor::getEntityPivotTable($currentEntityDef["name"], $partnerDef["name"], $inputKey);
                                            }

                                            $data->whereExists(function ($subQ) use($relBaseTable, $pivotTable, $foreignKeyEntity, $foreignKeyPartner, $fVal){
                                                $subQ->select(DB::raw(1))
                                                    ->from($pivotTable)
                                                    ->whereRaw($relBaseTable . '.id = '. $pivotTable . '.' . $foreignKeyEntity)

                                                    /* apply filter */
                                                    ->whereIn($pivotTable.'.'.$foreignKeyPartner, $fVal);
                                            });
                                        break;
                                        default:
                                            $data->whereIn($filterColumn, $fVal);
                                        break;
                                    }
                                break;
                                case 'between':
                                    switch($inpType[0]) {
                                        case 'select_inv':
                                            $partnerDef = $definitions->get("entities.".$inpType[1]);
                                            $partnerTable = $partnerDef["table"];

                                            foreach ($fVal as $fValKey => $fValItem){
                                                $comparsion = $fValKey == 0 ? '<=' : '>=';
                                                if($fValItem != ''){
                                                    $data->where(DB::raw($fValItem), $comparsion, function ($subQ) use($relBaseTable, $partnerTable, $inpType){
                                                        $subQ->select(DB::raw('COUNT(*)'))
                                                            ->from($partnerTable)
                                                            ->whereRaw($relBaseTable . '.id' . '=' . $partnerTable . '.' . $inpType[2]);
                                                    });
                                                }
                                            }
                                        break;
                                        case 'multiselect':
                                        case 'multiselect_inv':
                                            $partnerDef = $definitions->get("entities.".$inpType[1]);

                                            /* get foreign keys and partner table */
                                            $foreignKeyEntity = DefProcessor::getEntityForeignKey($currentEntityDef["name"]);

                                            /* get pivot table */
                                            if($inpType[0] == 'multiselect_inv') {
                                                $pivotTable = DefProcessor::getEntityPivotTable($partnerDef["name"], $currentEntityDef["name"], $inpType[2]);
                                            }
                                            else{
                                                $pivotTable = DefProcessor::getEntityPivotTable($currentEntityDef["name"], $partnerDef["name"], $inputKey);
                                            }

                                            foreach ($fVal as $fValKey => $fValItem){
                                                $comparsion = $fValKey == 0 ? '<=' : '>=';
                                                if($fValItem != ''){
                                                    $data->where(DB::raw($fValItem), $comparsion, function ($subQ) use($relBaseTable, $pivotTable, $foreignKeyEntity){
                                                        $subQ->select(DB::raw('COUNT(*)'))
                                                            ->from($pivotTable)
                                                            ->whereRaw($relBaseTable . '.id = '. $pivotTable . '.' . $foreignKeyEntity);
                                                    });
                                                }
                                            }
                                        break;
                                        case 'files':
                                            foreach ($fVal as $fValKey => $fValItem){
                                                $comparsion = $fValKey == 0 ? '<=' : '>=';
                                                if($fValItem != ''){
                                                    $data->where(DB::raw($fValItem), $comparsion, function ($subQ) use($relBaseTable, $relBaseType, $inputKey){
                                                        $subQ->select(DB::raw('COUNT(*)'))
                                                            ->from('files')
                                                            ->whereRaw('files.rel_id = '. $relBaseTable.'.id')
                                                            ->where('files.rel_type', '=', $relBaseType)
                                                            ->where('files.rel_field', '=', $inputKey);
                                                    });
                                                }
                                            }
                                        break;
                                        default:
                                            if ($fVal[0] != '')
                                                $data->where($filterColumn, '>=', $fVal[0]);

                                            if ($fVal[1] != '')
                                                $data->where($filterColumn, '<=', $fVal[1]);
                                        break;
                                    }
                                break;
                                default:
                                    $data->where($filterColumn, $fVal);
                                break;
                            }
                        }
                    }
                }
            }


        /* FILTERS ON CALCULATED FIELDS - NEED TO PAGINATE COLLECTION, NOT DB QUERY */
            if($hasCalculatedFilter){
                $data = $data->get(); //receive all data from the database and filter it as a collection

                foreach($request->input('filters') as $fKey => $fVal){

                    /* filter setting exists */
                    if(!isset($filtersToShow[$fKey])){
                        continue;
                    }

                    /* input type is calculated */
                    if($filtersToShow[$fKey]["input_type"][0] != "calculated"){
                        continue;
                    }

                    /* check if filter is active */
                    $filterSettings = $filtersToShow[$fKey];
                    $fType = $filterSettings["filter_type"];
                    $filterActive = false;
                    switch($fType){
                        case 'text':
                        case 'select':
                            $filterActive = ($fVal != '');
                            break;
                        case 'multiselect':
                            $filterActive = !empty($fVal);
                            break;
                        case 'between':
                            $filterActive = !empty($fVal) && count($fVal) == 2 && ($fVal[0] != '' || $fVal[1] != '');
                            break;
                    }

                    if($filterActive){
                        $data = $data->filter(function($item) use($fType, $fKey, $fVal, $filterSettings, $inputsMerged){

                            /* by default check value */
                            $itemsToCheck = [$item];

                            /* if external field, check values of external items */
                            if($filterSettings["is_external"]){
                                $externalInputSettings = $inputsMerged[$fKey];
                                $fKey = $externalInputSettings["target_input"];

                                if($externalInputSettings["is_inverse"]){
                                    $itemsToCheck = $item->getInverseRelationship($externalInputSettings["external_entity"], $externalInputSettings["src_input"], true)->get();
                                }
                                else{
                                    $itemsToCheck = $item->getRelationship($externalInputSettings["src_input"], true)->get();
                                }
                            }

                            $filterResult = false;
                            foreach ($itemsToCheck as $item){
                                switch($fType){
                                    case 'text':
                                        $filterResult = preg_match( '/(.*)'.strtolower(Transliterator::unaccent($fVal)).'(.*)/', strtolower(Transliterator::unaccent($item->$fKey))) ? true : false;
                                    break;
                                    case 'select':
                                        $filterResult = $item->$fKey == $fVal;
                                    break;
                                    case 'multiselect':
                                        $filterResult = in_array($item->$fKey, $fVal);
                                    break;
                                    case 'between':
                                        $filterResult = (!isset($fVal[0]) || ($fVal[0] != '' && $item->$fKey >= $fVal[0])) && (!isset($fVal[1]) || ($fVal[1] != '' && $item->$fKey <= $fVal[1]));
                                    break;
                                }

                                if($filterResult) return $filterResult;
                            }

                            return $filterResult;
                        });
                    }
                }
            }

            /* dump sql query */
            /*$sql = Str::replaceArray('?', $data->getBindings(), $data->toSql());
            dd($sql);*/

            /* PAGINATE (only if view should be rendered, otherwise, return all) */
            if($renderView){
                $data = $data->paginate($action["data"]["page_limit"])->appends(['_ref' => $request->_ref, '_page' => $request->_page]);
            }
            else{
                if(!$hasCalculatedFilter)
                    $data = $data->get();
            }
        /* GET FORM DATA */

        /* OPERATION RIGHTS */
            $userCanGlobal = $request->user()->getAllowedEntityOperations($entityDef);

            //allowed operations by def
            $allowedOperationsByAction = [];
            if(isset($action["data"]["_allow_operations"])){
                foreach ($userCanGlobal as $ucgKey => &$ucg){
                    if(!in_array($ucgKey, $action["data"]["_allow_operations"])){
                        $ucg = false;
                    }
                    else{
                        $allowedOperationsByAction[] = $ucgKey;
                    }
                }
            }
            else{
                $allowedOperationsByAction = array_keys($userCanGlobal);
            }
        /* OPERATION RIGHTS */

        $dataToReturn = [
            "entityDef" => $entityDef,
            "colsToShow" => $colsToShow,
            "externalInputs" => $externalInputs,
            "data" => $data,
            "userCanGlobal" => $userCanGlobal,
            "allowedOperationsByAction" => $allowedOperationsByAction,
            "useFilters" => $useFilters,
            "filtersToShow" => $filtersToShow,
            "action" => $action
        ];

        if($renderView){
            return view('admin.components.'.$this->getName())->with($dataToReturn);
        }
        else{
            return $dataToReturn;
        }
    }
}


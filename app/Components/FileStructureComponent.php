<?php

namespace App\Components;

use App\Component;
use App\ContentInfo;
use App\DefProcessor;
use Behat\Transliterator\Transliterator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FileStructureComponent extends Component
{
    public function handle(Request $request, $action, $renderView = true){
        global $definitions;

        /* KEEP PAGE AND FILTERS IN SESSION (only when in main component) */
        if($request->has('_dom_target') && $request->input('_dom_target') == '#component'){
            /* KEEP PAGE AND FILTERS IN SESSION */
                $currentComponentKey = $request->input('_dom_target').'__'.str_replace(".", "_", $action["data"]["ref"]);
                $tableDataSession = [];
                if($request->session()->has('file-structure-data-session')){
                    $tableDataSession = $request->session()->get('file-structure-data-session');
                }
                if(!isset($tableDataSession[$currentComponentKey])){
                    $tableDataSession[$currentComponentKey] = [];
                }

                if($request->has('filters')){
                    $tableDataSession[$currentComponentKey]["filters"] = $request->input('filters');
                }
                if($request->has('page')){
                    $tableDataSession[$currentComponentKey]["page"] = $request->input('page');
                }

                $request->session()->put('file-structure-data-session', $tableDataSession);
            /* KEEP PAGE AND FILTERS IN SESSION */

            /* GET PAGE AND FILTERS FROM SESSION */
                if(!$request->has('filters') && isset($tableDataSession[$currentComponentKey]["filters"])){
                    $request->merge(['filters' => $tableDataSession[$currentComponentKey]["filters"]]);
                }
                if(!$request->has('page') && isset($tableDataSession[$currentComponentKey]["page"])){
                    $request->merge(['page' => $tableDataSession[$currentComponentKey]["page"]]);
                }
            /* GET PAGE AND FILTERS FROM SESSION */
        }

        /* GET ADDITIONAL DATA FROM REQUEST */
        $action["data"] = array_merge(
            $action["data"],
            $request->all()
        );

        /* HANDLE NAVIGATION */
            /* base path - from definition */
            $basePath = $action["data"]["base_path"];
            $basePath = trim($basePath, "/");

            /* full active path  */
            $fullPath = $action["data"]["path"];

            /* user cannot go up from the base path */
            if(substr($fullPath, 0, strlen($basePath)) != $basePath)
                $fullPath = $basePath;

            /* get up path */
            if($fullPath != $basePath){
                $pathParsed = explode("/", $fullPath);
                array_pop($pathParsed);
                $upPath = implode("/", $pathParsed);
            }
            else{
                $upPath = false;
            }

            /* active path as addition to base path */
            $path = substr($fullPath, strlen($basePath), strlen($fullPath));
            $path = trim($path, "/");

            /* level - how deep in the structure we are */
            $level = empty($path) ? 0 : count(explode("/", $path));

            /* BREADCRUMBS */
            $rootName = __('Root');
            if(!empty($basePath)){
                $rootFolder = ContentInfo::where("url", $basePath)->get()->first();
                if($rootFolder->entity){
                    $rootName = $rootFolder->entity->getEntityTitle();
                }
            }

            $breadcrumbs = [
                $basePath => $rootName
            ];

            $pathDone = [];
            foreach (explode("/", $path) as $p){
                $itemPath = trim($basePath.'/'.(!empty($pathDone) ? implode('/', $pathDone).'/' : '').$p, '/');
                $currentItem = ContentInfo::where("url", $itemPath)->get()->first();

                if($currentItem && $currentItem->entity){
                    $breadcrumbs[$itemPath] = $currentItem->entity->getEntityTitle();
                }

                $pathDone[] = $p;
            }

        /* HANDLE NAVIGATION */

        /* CURRENT LEVEL SETTINGS - OPERATION RIGHTS */
        $allowedEntitiesToHandle = [];
        $userCanByEntity = [];

        /* check if path settings for current path exist (go from back to front to find closest path) */
        $pathSettings = null;
        $pathParsed = explode("/", $path);
        $pathLength = count($pathParsed);
        if(!empty($action["data"]["path_settings"])){
            for($i = ($pathLength - 1); $i >= 0; $i--){
                $currentPath = implode("/", $pathParsed);
                if(isset($action["data"]["path_settings"][$currentPath])){
                    /* path settings found */
                    $pathSettings = $action["data"]["path_settings"][$currentPath];

                    /* if path settings were found, set level to path difference to get proper settings */
                    $level = $pathLength - 1 - $i;

                    break;
                }

                /* unset path part and continue */
                unset($pathParsed[$i]);
            }
        }

        /* if not, check if path settings for base path exist */
        if(!$pathSettings)
            $pathSettings = !empty($action["data"]["path_settings"]) && isset($action["data"]["path_settings"][$basePath]) ? $action["data"]["path_settings"][$basePath] : null;

        /* by default use default settings */
        $levelSettings = $defaults = DefProcessor::checkAndFillDefaults("_path_level_setting", "_path_level_setting", []);

        /* load level settings from path settings & check rights  */
        if($pathSettings && isset($pathSettings["levels"][$level])){
            $levelSettings = $pathSettings["levels"][$level];

            /* check if user has access to particular folder in the structure */
            if(isset($pathSettings["levels"][$level]["access"])){
                if(!$definitions->UserHasRights($request->user(), $pathSettings["levels"][$level]["ref"], null))
                    abort(401);
            }

            /* get entities that make up the folder contents */
            if(!empty($pathSettings["levels"][$level]["allowed_entities"])) {
                /* load all entity definitions */
                foreach ($definitions->getAll() as $def) {
                    if ($def["type"] == "entities" && $def["data"]["uses_content_info"]) {
                        $allowedEntitiesToHandle[$def["name"]] = $def;
                    }
                }

                /* check allowed definitions by settings and use them if they exist */
                if ($pathSettings["levels"][$level]["allowed_entities"] != "*") {
                    $allowedBySettings = explode(",", $pathSettings["levels"][$level]["allowed_entities"]);

                    foreach ($allowedEntitiesToHandle as $currentEntityKey => $currentEntityDef){
                        if(!in_array($currentEntityKey, $allowedBySettings)){
                            unset($allowedEntitiesToHandle[$currentEntityKey]);
                        }
                    }
                }
            }

            /* check what the user can do with folder contents (add them, delete them, etc.) */
            foreach ($allowedEntitiesToHandle as $currentEntityKey => $currentEntityDef){
                $userCanByEntity[$currentEntityKey] = $request->user()->getAllowedEntityOperations($currentEntityDef);
            }
        }

        /* OPERATION RIGHTS */

        /* GET CURRENT ACTIVE PATH DATA */
            /* get parent folder content info instance */
            $parentId = 0;
            $parentFolder = null;
            if(!empty($fullPath)){
                $parentFolder = ContentInfo::where("url", $fullPath)->get()->first();
                if(!$parentFolder)
                    abort(404);

                $parentId = $parentFolder->id;
            }

            /* get parent folder childern */
            if($parentFolder)
                $data = ContentInfo::where("parent_id", $parentFolder->id);
            else
                $data = ContentInfo::where("parent_id", 0);

            /* sort items */
            if($levelSettings["sortable"])
                $data->orderBy('sort');
            else{
                $order = [];
                if(!empty($levelSettings["order"])){
                    $order = explode(':', $levelSettings["order"]);
                }
                if(count($order) < 2){
                    $order = ['content_info.id', 'desc'];
                }

                $data->orderBy($order[0], $order[1]);
            }

            /* dump sql query */
            /*$sql = Str::replaceArray('?', $data->getBindings(), $data->toSql());
            dd($sql);*/

            /* PAGINATE (only if view should be rendered, otherwise, return all) */
            if($renderView){
                $data = $data->paginate($levelSettings["page_limit"])->appends(['_ref' => $request->_ref, '_page' => $request->_page]);
            }
            else{
                $data = $data->get();
            }
        /* GET CURRENT ACTIVE PATH DATA */

        $dataToReturn = [
            "parentId" => $parentId,
            "basePath" => $basePath,
            "fullPath" => $fullPath,
            "upPath" => $upPath,
            "breadcrumbs" => $breadcrumbs,
            "levelSettings" => $levelSettings,
            "allowedEntitiesToHandle" => $allowedEntitiesToHandle,
            "data" => $data,
            "userCanByEntity" => $userCanByEntity,
            "action" => $action
        ];

        if($renderView){
            return view('admin.components.'.$this->getName())->with($dataToReturn);
        }
        else{
            return $dataToReturn;
        }
    }
}


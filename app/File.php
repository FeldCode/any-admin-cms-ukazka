<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Intervention\Image\ImageManagerStatic;

class File extends Model
{
    protected $appends = array('file_url', 'file_url_resizer');

    public static function boot() {
        parent::boot();

        static::deleting(function($item) { // before delete() method call this
            /* DISSOCIATE IF HAS PARENT WITH BELONGS TO */
                if(!is_null($item->parent()) && $item->parent){
                    $parent = $item->parent;
                    $relField = $item->rel_field;
                    $rel = DefProcessor::getRelationName($item->rel_field);
                    if($parent->$rel() instanceof \Illuminate\Database\Eloquent\Relations\BelongsTo){
                        $parent->$relField = null;
                        $parent->save();
                    }
                }
            /* DISSOCIATE IF HAS PARENT WITH BELONGS TO */

            /* DELETE THE FILE */
            Storage::delete('public/'.$item->id.".".$item->ext);
        });

        static::creating(function($item){
            $request = request();

            /* ASSOSIATE WITH CURRENT USER */
            if($request->user())
                $item->author()->associate($request->user());

            if($request->user())
                $item->updated_author()->associate($request->user());
        });

        static::updating(function($item){
            $request = request();

            /* ASSOSIATE WITH CURRENT USER */
            if($request->user())
                $item->updated_author()->associate($request->user());
        });
    }

    public static function getMaxUploadFileSize($short = false){
        $spareSize = Helper::returnBytes("0.5M");
        $maxSize = Helper::returnBytes(ini_get("upload_max_filesize")) - $spareSize;

        return $short ? Helper::formatBytes($maxSize) : $maxSize;
    }

    public static function getDangerousFileTypes(){
        return ["php", "phtml", "html", "js", "css", "exe", "asp", "aspx", "htacess"];
    }

    public static function dzError($msg){
        header("HTTP/1.0 400 Bad Request");
        echo $msg;
        exit;
    }

    public static function store(Request $request, $fileField, $ref, $isExternalUploader = false, $resize = false)
    {
        global $definitions;
        $file = null;

        /* CHECK CONDITIONS */
        $input = $definitions->getByRef($ref);
        if(!$input){
            $errors = new MessageBag();
            $errors->add($fileField, __("Unauthorized"));
            return ["state" => false, "data" => [], "errors" => $errors];
        }

        /* VALIDATE - only if external uploader */
        $inputNames = [$fileField => __($input["title"])]; //universal name
        $validation[$fileField] = DefProcessor::getInputValidation($input, $isExternalUploader);
        $validator = Validator::make($request->all(), $validation, [], $inputNames);

        if($validator->fails()){
            return ["state" => false, "data" => [], "errors" => $validator->errors()];
        }


        // Handle file upload
        if($request->hasFile($fileField)){
            $imageExtensions = ["jpg", "jpeg", "png", "gif", "bmp", "tiff"];

            // Get file name with the extension
            $fileNameWithExt = $request->file($fileField)->getClientOriginalName();
            // Get just file name
            $fileName = $request->file($fileField)->getRealPath();
            // Get just the extension
            $ext = pathinfo($fileNameWithExt, PATHINFO_EXTENSION);

            /* CHECK DANGEROUS FILE TYPES */
            if(in_array($ext, self::getDangerousFileTypes())){
                $msg = __("The file of this type cannot be uploaded.");
                $validator->errors()->add($fileField, $msg);
                return ["state" => false, "data" => [], "errors" => $validator->errors()];
            }

            $lastFileId = DB::table('files')->orderby('id', 'desc')->first();
            $lastFileId = $lastFileId ? $lastFileId->id : 0;

            // Create File
            $file = new File();

            /* sorting */
            $file->sort = $lastFileId + 1; //insert in the end

            /* relation */
            $file->rel_type = isset($request->rel_type) ? ltrim($request->rel_type, "\\") : '';
            $file->rel_id = isset($request->rel_id) ? $request->rel_id : 0;
            $file->rel_field = isset($request->rel_field) ? $request->rel_field : '';

            /* file info */
            $file->file_name = '';
            $file->is_image = in_array($ext, $imageExtensions);
            $file->type = mime_content_type($fileName);
            $file->size = filesize($fileName); //this will save original filesize
            $file->ext = $ext;
            $file->upload_name = $fileNameWithExt;
            $file->description = "";
            $file->save();

            // File name to store
            $fileNameToStore = $file->id.".".$ext;

            // Resize & Upload
            if($file->is_image && $resize && (isset($resize["w"]) || isset($resize["h"]))){
                $modeFunc = [
                    "resize" => "resize", //resize image to given resolutions by longest side
                    "fit" => "fit", //fit image (crop out the rest)
                    "fill" => "resize"  //fill image (resize image to given resolutions by shortest side)
                ];

                $w = isset($resize["w"]) ? $resize["w"] : null;
                $h = isset($resize["h"]) ? $resize["h"] : null;
                $mode = isset($resize["mode"]) && isset($modeFunc[$resize["mode"]]) ? $resize["mode"] : "resize";
                $resizeFunc = $modeFunc[$mode];

                $image_resize = ImageManagerStatic::make($request->file($fileField)->getRealPath())->orientate();

                if($mode == "fill" && !empty($w) && !empty($h)){
                    if($image_resize->width() > $image_resize->height())
                        $w = null;
                    else
                        $h = null;
                }

                $image_resize->$resizeFunc($w, $h, function($constraint) use ($resizeFunc){
                    if($resizeFunc != "fit")
                        $constraint->aspectRatio();

                    $constraint->upsize();
                });

                $image_resize->save(storage_path('app/public').'/'.$fileNameToStore);

                $file->size = filesize(storage_path('app/public').'/'.$fileNameToStore);
                $file->save();
            }

            // Upload only
            else{
                $request->file($fileField)->storeAs('public', $fileNameToStore);
                chmod(storage_path('app/public').'/'.$fileNameToStore, 0755);
            }

            $file->file_name = $fileNameToStore;
            $file->save();
        }

        return ["state" => true, "data" => $file, "errors" => new MessageBag()];
    }

    public function getFileUrl($resizer = true, $w = null, $h = null){
        if($this->is_image && $resizer){
            $url = url("media/image/".$this->file_name."/".(!empty($w) ? $w.'/' : '').(!empty($h) ? $h : ''));
        }
        else{
            $url = url("storage/".$this->file_name);
        }

        return $url;
    }

    public function getFileUrlAttribute()
    {
        return $this->getFileUrl(false);
    }

    public function getFileUrlResizerAttribute()
    {
        return $this->getFileUrl(true);
    }

    public function parent(){
        if(!empty($this->rel_type) && !empty($this->rel_id))
            return $this->morphTo('rel');
        else
            return null;
    }

    public function author(){
        return $this->belongsTo('App\User', 'created_user_id');
    }

    public function updated_author(){
        return $this->belongsTo('App\User', 'updated_user_id');
    }

    public function asResource(){
        $resource = clone $this;
        return $resource->toArray();
    }

    /* sort items - get sort options of ids and sort only them */
    public static function sortItems($ids){
        /* GET SORT OPTIONS BY ID & SORT IT */
        $sortOptions = get_called_class()::find($ids)->pluck("sort")->toArray();
        sort($sortOptions, SORT_NUMERIC);

        /* APPLY SORT OPTIONS IN ORDER OF IDS RECEIVED */
        foreach ($ids as $key => $id){
            $item = get_called_class()::find($id);
            if($item){
                $item->sort = $sortOptions[$key];
                $item->save();
            }
        }

        return true;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Entity extends Model
{

    public static function getClassNameFromStr($str, $full = false){
        if(DefProcessor::isSystemEntity($str))
            return ($full ? "\App\\" : "").ucfirst(Str::camel($str));
        else
            return ($full ? "\App\Entities\\" : "").ucfirst(Str::camel($str)."Entity");
    }

    public static function getDefinitionNameFromStr($str){
        $str = str_replace(["App\Entities\\", "App\\", "Entity"], "", $str);
        return Str::snake($str);
    }
}

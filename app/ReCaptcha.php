<?php

namespace App;

class ReCaptcha
{
    public static function verify($responseKey){
        $fields_string = '';
        $fields = array(
            'secret' => env('RECAPTCHA_SECRET_KEY'),
            'response' => $responseKey
        );

        foreach($fields as $key=>$value)
            $fields_string .= $key . '=' . $value . '&';
        $fields_string = rtrim($fields_string, '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);

        $result = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($result, true);

        return $result["success"];
    }
}


<?php

namespace App;

use App\Traits\EntityTrait;
use App\Traits\HasDynamicRelation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserGroup extends Model
{
    use HasDynamicRelation;
    use EntityTrait;

    public const GROUP_ADMINS = 1;
    public const GROUP_ANONYMOUS = 2;
    public const GROUP_POWER_USERS = 3;

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($item) { // before delete() method call this
            if($item->id == self::GROUP_ADMINS){
                session()->now("error", __("Admins group cannot be deleted."));
                return false;
            }

            if($item->id == self::GROUP_ANONYMOUS){
                session()->now("error", __("Anonymous group cannot be deleted."));
                return false;
            }

            /* move things from deleted category to other category */
            DB::table("users")->where('user_group', $item->id)->update(["user_group" => self::GROUP_ANONYMOUS]);
        });
    }

    public function getUsersNumberAttribute(){
        $rel = DefProcessor::getInverseRelationName("user", "user_group");
        $usersNumber = $this->$rel ? count($this->$rel) : 0;
        return $usersNumber;
    }
}

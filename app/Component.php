<?php

namespace App;


use Illuminate\Support\Str;

class Component
{
    public static function componentExists($component){
        $component = self::getComponentClassName($component);
        return class_exists($component);
    }

    public static function getComponentClassName($component, $incNamespace = true){
        return ($incNamespace ? "\App\Components\\" : "").ucfirst(Str::camel($component))."Component";
    }

    public function getName(){
        return strtolower(Str::snake(str_replace(["App\Components\\", "Component"], "", get_called_class())));
    }
}

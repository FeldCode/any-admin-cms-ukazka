<?php

namespace App\Traits;

use App\ContentInfo;
use App\DefProcessor;
use App\Entity;
use App\File;
use App\Notifications\EntityCreatedNotification;
use App\Notifications\EntityDeletedNotification;
use App\Notifications\EntityUpdatedNotification;
use App\User;
use App\UserGroup;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Str;

trait EntityTrait
{
    protected $def = [];
    public $contentInfoParentId = null;

    public static function bootEntityTrait() {

        static::deleted(function($item) { // after delete() method call this (this can be stopped in deleting method (ie. users - attempt to delete admin))
            $entityDef = $item->getDef();

            $relInputTypes = [
                "select",
                "select_inv",
                "multiselect",
                "multiselect_inv",
                "file",
                "files"
            ];
            $relInverseInputTypes = [
                "select_inv",
                "multiselect_inv",
            ];

            /* get cascade delete from entity definition */
            $cascadeData = [];
            foreach ($entityDef["data"]["_inputs"] as $inputKey => $input){
                $inputType = DefProcessor::parseInputType($input["type"]);

                // input is of relation type and not already in cascade
                if(in_array($inputType[0], $relInputTypes) && !in_array($inputKey, array_keys($cascadeData))){

                    /* get real key (inverse relationships) */
                    $cascadeKey = $inputKey;
                    if(in_array($inputType[0], $relInverseInputTypes)){
                        $cascadeKey = $inputType[1].".".$inputType[2];
                    }

                    /* CHECK ENTITY ITEMS FOR CASCADE DELETE */
                    $shouldDelete = in_array($inputKey, $entityDef["data"]["cascade_delete"]) || in_array($cascadeKey, $entityDef["data"]["cascade_delete"]) || $input["relation_cascade_delete"];

                    $cascadeData[$cascadeKey] = [
                        "action" => $shouldDelete ? "delete" : "detach",
                        "input_type" => $inputType
                    ];
                }
            }

            /*
             * Find all possible partner entities to detach or delete
            */
            global $definitions;
            foreach ($definitions->getAll() as $def){
                if($def["type"] == "entities" && $def["name"] != $entityDef["name"]){
                    foreach ($def["data"]["_inputs"] as $inputKey => $input){
                        $inputType = DefProcessor::parseInputType($input["type"]);
                        $cascadeKey = $def["name"].".".$inputKey;

                        $shouldDelete = in_array($cascadeKey, $entityDef["data"]["cascade_delete"]);

                        if(in_array($inputType[0], $relInputTypes) // input is of relation type
                            && !in_array($inputType[0], $relInverseInputTypes) // is not inverse relation type
                            && $inputType[1] == $entityDef["name"] // in relation with current entity
                            && !in_array($cascadeKey, array_keys($cascadeData))){ //not already in cascade
                            $cascadeData[$cascadeKey] = [
                                "action" => $shouldDelete ? "delete" : "detach",
                                "input_type" => $inputType
                            ];
                        }
                    }
                }
            }

            /* AUTO DELETE RELATED CONTENT_INFO */
            if($item->_content_info){
                $item->_content_info->delete();
            }

            /* CASCADE DELETE OR DETACH PARTNER ENTITIES */
            foreach($cascadeData as $targetString => $cascade){
                $targetString = explode(".", $targetString);
                $isInverse = count($targetString) > 1;

                /* get items */
                $relItems = null;
                try{
                    if($isInverse){
                        $relItems = $item->getInverseRelationship($targetString[0], $targetString[1], true);
                    }
                    else{
                        $relItems = $item->getRelationship($targetString[0], true);
                    }
                }catch (\Exception $e){}

                if($relItems){ // if relation doesnt exists, $relItems will be null (try/catch), no need to check input type then

                    /* delete items */
                    if($cascade["action"] == "delete"){
                        foreach ($relItems->get() as $relItem){
                            $relItem->delete();
                        }
                    }
                    elseif($cascade["action"] == "detach"){
                        switch($cascade["input_type"][0]){
                            case "file":
                                if(!$isInverse){
                                    $relItem = $relItems->get()->first();
                                    if($relItem){
                                        $relItem->rel_type = null;
                                        $relItem->rel_id = null;
                                        $relItem->rel_field = null;
                                        $relItem->save();
                                    }
                                }

                            case "select":
                            case "select_inv":
                                if($isInverse){ //select and file is save directly on the entity, if the entity is deleted, not need to detach it.. do it only on inverse relations
                                    $relItem = $relItems->get()->first();
                                    if($relItem){
                                        $relField = $targetString[1];
                                        $relItem->$relField = null;
                                        $relItem->save();
                                    }
                                }
                            break;
                            case "multiselect":
                            case "multiselect_inv":
                                $relItems->detach();
                            break;
                            case "files":
                                foreach ($relItems->get() as $relItem){
                                    $relItem->rel_type = null;
                                    $relItem->rel_id = null;
                                    $relItem->rel_field = null;
                                    $relItem->save();
                                }
                            break;
                        }
                    }
                }
            }

            /* send notifications */
            $item->sendEntityNotifications('deleted');
        });

        static::creating(function($item){
            $request = request();

            /* ASSOSIATE WITH CURRENT USER */
            if($request->user())
                $item->author()->associate($request->user());

            if($request->user())
                $item->updated_author()->associate($request->user());
        });
        static::updating(function($item){
            $request = request();

            /* ASSOSIATE WITH CURRENT USER */
            if($request->user())
                $item->updated_author()->associate($request->user());
        });

        static::created(function($item){
            /* sort - add item to the end */
            $item->sort = $item->id;
            $item->save();

            /* save item content info (for SEO and other uses) */
            $itemDef = $item->getDef();
            if($itemDef["data"]["uses_content_info"]){
                $titleField = $itemDef["data"]["title_field"];
                $title = $item->$titleField;

                $sortNumberInCurrentFolder = 0;
                $lastItem = ContentInfo::where('parent_id', $item->contentInfoParentId)->orderBy('sort', 'desc')->take(1)->get()->first();
                if($lastItem)
                    $sortNumberInCurrentFolder = $lastItem->sort + 1;

                $contentInfo = new ContentInfo();
                $contentInfo->identToBeCreated = ContentInfo::seoString($title);
                $contentInfo->parent_id = $item->contentInfoParentId;
                $contentInfo->sort = $sortNumberInCurrentFolder;
                $contentInfo->save();

                $item->_content_info()->save($contentInfo);
            }

            $item->sendEntityNotifications('created');
        });

        static::updated(function($item){
            $justCreatedTimeLimit = 5;
            $justCreated = false;

            if(!empty($item->created_at)){
                $timeDif = Carbon::now()->timestamp - $item->toCarbon($item->created_at)->timestamp;
                $justCreated = ($timeDif <= $justCreatedTimeLimit);
            }

            if(!$justCreated)
                $item->sendEntityNotifications('updated');
        });

    }

    public function sendEntityNotifications($type){
        $entityDef = $this->getDef();

        if(isset($entityDef["data"]["notifications"][$type])){
            $receiverType = $entityDef["data"]["notifications"][$type]["receiver_type"];
            $receivers = $entityDef["data"]["notifications"][$type]["receivers"];
            if(!is_array($receivers)) $receivers = [$receivers];

            $notifTypes = [
                "created" => EntityCreatedNotification::class,
                "updated" => EntityUpdatedNotification::class,
                "deleted" => EntityDeletedNotification::class
            ];

            $userId = request()->user() ? request()->user()->id : null;

            foreach ($receivers as $receiver){
                switch ($receiverType){
                    case "email":
                        Notification::route('mail', $receiver)->notify(new $notifTypes[$type]($entityDef, $this, $userId));
                    break;
                    case "user":
                        $user = User::find($receiver);
                        if(!$user){
                            $user = User::where('login', '=', $receiver)->get()->first();
                        }

                        if($user){
                            Notification::route('mail', $user->email)->notify(new $notifTypes[$type]($entityDef, $this, $userId));
                        }
                    break;
                    case "user_group":
                        $receiversGroup = UserGroup::where('code_name', $receiver)->get()->first();
                        if($receiversGroup){
                            $receiversGroup = $receiversGroup->getInverseRelationship("user", "user_group");
                            foreach ($receiversGroup as $user){
                                Notification::route('mail', $user->email)->notify(new $notifTypes[$type]($entityDef, $this, $userId));
                            }
                        }
                    break;
                    case "def_field":
                        $receiver = DefProcessor::checkForEntityReference($receiver);
                        Notification::route('mail', $receiver)->notify(new $notifTypes[$type]($entityDef, $this, $userId));
                    break;
                }
            }
        }
    }

    public function __construct(array $attributes = [])
    {
        $this->registerCastedVariables();
        parent::__construct($attributes);
    }

    public static function getTableName(){
        return (new self)->getTable();
    }

    public function getDef(){
        return self::getDefStatic();
    }

    public static function getDefStatic(){
        global $definitions;

        if($definitions)
            return $definitions->get("entities." . self::getDefinitionName());
        else
            return null;
    }

    static public function getDefinitionName(){
        return Entity::getDefinitionNameFromStr(get_called_class());
    }

    public static function sortItems($ids){
        /* GET SORT OPTIONS BY ID & SORT IT */
        $sortOptions = get_called_class()::find($ids)->pluck("sort")->toArray();
        sort($sortOptions, SORT_NUMERIC);

        /* APPLY SORT OPTIONS IN ORDER OF IDS RECEIVED */
        foreach ($ids as $key => $id){
            $item = get_called_class()::find($id);
            if($item){
                $item->sort = $sortOptions[$key];
                $item->save();
            }
        }

        return true;
    }

    public function registerCastedVariables(){
        if($this->getDef()){
            foreach ($this->getDef()["data"]["_inputs"] as $inputKey => $input) {
                $inputType = DefProcessor::parseInputType($input["type"])[0];

                /* REGISTER DATES */
                if(in_array(strtolower($inputType), array("date", "datetime"))){
                    $this->dates[] = $inputKey;
                }

                /* REGISTER FILLABLE FIELDS */
                if(!in_array($inputType, [
                    "select_inv",
                    "multiselect",
                    "multiselect_inv",
                    "files",
                    "calculated" => "",
                ])){
                    $this->fillable[] = $inputKey;
                }

                /* REGISTER CALCULATED FIELDS */
                if($inputType == "calculated"){
                    $this->appends[] = $inputKey;
                }
            }
        }
    }

    public function hasAttribute($attr)
    {
        return array_key_exists($attr, $this->attributesToArray());
    }

    public function getFormattedMethodName($valueKey){
        return 'get'.ucfirst(Str::camel($valueKey)).'Formatted';
    }

    public function formattedMethodExists($valueKey){
        $formatMethodName = $this->getFormattedMethodName($valueKey);
        return method_exists(self::class, $formatMethodName);
    }

    public function getFormattedValue($valueKey){
        if(!$this->hasAttribute($valueKey)){
            return '';
        }
        else{
            if($this->formattedMethodExists($valueKey)){
                $formatMethodName = $this->getFormattedMethodName($valueKey);
                return $this->$formatMethodName($this->$valueKey);
            }
            else{
                return $this->$valueKey;
            }
        }
    }

    public static function toCarbon($date){
        /* if not empty and is not Carbon instance, make it Carbon (can happen with inputs inside of pivot table (not casted as dates)) */
        new \Carbon\Carbon(); //instance must be called first
        if(!empty($date) && !($date instanceof \Carbon\Carbon)){
            $date = new \Carbon\Carbon($date);
        }

        return $date;
    }
/*
    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);
        if(in_array($key, $this->dates)) {
            return $this->getWhateverDateAttribute($key, $value);
        }
        else{
            return $value;
        }
    }

    public function getWhateverDateAttribute($key, $date) {
        $format = $this->datesFormat;
        if($this->getDef()){
            foreach ($this->getDef()["data"]["_inputs"] as $inputKey => $input){
                if($inputKey == $key){
                    if($input["type"] == "date"){
                        $format = 'm/d/Y';
                    }
                }
            }
        }

        return $date ? $date->format($format) : null;
    }*/

    public function getRelationship($field, $instance = false){
        $rel = DefProcessor::getRelationName($field);

        if($instance)
            return $this->$rel();
        else
            return $this->$rel;
    }

    public function getInverseRelationship($entity, $field, $instance = false){
        $rel = DefProcessor::getInverseRelationName($entity, $field);

        if($instance)
            return $this->$rel();
        else
            return $this->$rel;
    }

    public function author(){
        return $this->belongsTo('App\User', 'created_user_id');
    }

    public function updated_author(){
        return $this->belongsTo('App\User', 'updated_user_id');
    }


    /**
     * Changes request to entity values and allows to overwrite fields that are in
     * $allowedFields
     *
     * INPUTS:
     *  -$request = Illuminate\Http\Request
     *  -$allowedFields = []
     *
     */
    static function fillRequestWithEntityData($request, $entity, $allowedFields = null){
        $entityDef = self::getDefStatic();
        $edit = !is_null($entity);

        $doNotFillInputTypes = [
            "calculated"
        ];

        /* entity base columns */
        $baseColumns = array_keys(DefProcessor::getEntityBaseColumns($entityDef["name"]));

        foreach ($entityDef["data"]["_inputs"] as $inputKey => $input){
            /* PARSE INPUT TYPE */
            $inputType = DefProcessor::parseInputType($input["type"]);

            /* DO NOT FILL THESE TYPES */
            if(in_array($inputType[0], $doNotFillInputTypes)) {
                continue;
            }

            /* DO NOT FILL BASE COLUMNS */
            if(in_array($inputKey, $baseColumns)){
                continue;
            }

            /* Fill in fields missing in request based on allowedFields */
            if(!is_null($allowedFields) && is_array($allowedFields)){
                $shouldFill = !in_array($inputKey, $allowedFields) || (in_array($inputKey, $allowedFields) && !$request->has($inputKey));
            }

            /* Fill in fields missing in request based on entityDef */
            else{
                global $definitions;

                /* fill request with entity data if: */
                $shouldFill = ((!$input["display_in_form"]) //if not in form
                                || (!$edit && !$input["fillable_edit"]) //or cannot be filled while editing
                                || (!$edit && !$input["fillable_add"]) //or cannot be filled while adding
                                || (!$definitions->UserHasRights($request->user(), $input["ref"], ($edit ? $entity->id : null))) //or user doesnt have rights to edit particular field
                              );
            }

            if($shouldFill){

                /* PASSWORD */
                if($inputType[0] == "password"){
                    if($edit){
                        $isEmpty = empty($entity->$inputKey);
                        $defaultVal = '';
                    }
                    else{
                        $isEmpty = false;
                        $defaultVal = '';
                    }

                    // by default always empty in edit form (means use current)
                    $request->merge([
                        $inputKey => $defaultVal,
                        $inputKey."_empty" => $isEmpty ? 1 : 0
                    ]);
                }

                /* FILE UPLOAD */
                elseif($inputType[0] == 'file') {
                    if($edit){
                        $defaultVal = $entity->$inputKey;
                    }
                    else{
                        $defaultVal = null;
                    }

                    $inputKeyUploadedId = $inputKey."-uploaded-id";
                    $request->merge([
                        $inputKey => null,
                        $inputKeyUploadedId => $defaultVal
                    ]);
                }

                /* MULTI-RELATIONAL FIELDS (files) - expects json */
                elseif(in_array($inputType[0], ["files"])){
                    if($edit){
                        $ids = $entity->getRelationship($inputKey, true)->orderBy('sort')->get()->pluck('id')->toArray();
                    }
                    else{
                        $ids = [];
                    }

                    $request->merge([
                        $inputKey => json_encode($ids)
                    ]);
                }

                /* MULTI-RELATIONAL FIELDS (other) - expects array */
                elseif(in_array($inputType[0], ["multiselect", "multiselect_inv", "select_inv"])){

                    if($edit) {
                        /* get relationship */
                        if (in_array($inputType[0], ["multiselect_inv", "select_inv"])) {
                            $ids = $entity->getInverseRelationship($inputType[1], $inputType[2], true);
                        } else {
                            $ids = $entity->getRelationship($inputKey, true);
                        }

                        /* sort data */
                        if ($inputType[0] != "select_inv") {
                            $ids->withPivot('sort as pvt_sort')->orderBy('pvt_sort');
                        }

                        /* get data */
                        $ids = $ids->select("id")->get()->toArray();

                        /* clear unwanted pivot data */
                        foreach ($ids as $idKey => &$id) {
                            if (isset($id["pvt_sort"])) unset($id["pvt_sort"]);
                            if (isset($id["pivot"])) unset($id["pivot"]);
                        }
                    }
                    else{
                        $ids = [];
                    }

                    /* correct multiselect fields -> convert to json */
                    $ids = json_encode($ids);

                    $request->merge([
                        $inputKey => $ids
                    ]);
                }

                /* DEFAULT BEHAVIOUR (other field types) */
                else{
                    if($edit){
                        $defaultVal = $entity->$inputKey;
                    }
                    else{
                        $defaultVal = null;
                    }

                    $request->merge([
                        $inputKey => $defaultVal
                    ]);
                }
            }
        }

        return $request;
    }

    /**
     * This function handles adding and editing of an entity
     */
    public static function processInput(Request $request, $id = null, $validate = true, $skipValidationFields = [], $customErrorMessages = [], $allowedFields = null){
        global $definitions;
        $edit = !is_null($id);
        $entityDef = $definitions->get("entities." . self::getDefinitionName());

        $validatorFailed = false;
        $errors = new MessageBag();

        /* INITIATE NEW ENTITY OBJECT OR GET BY ID */
        if($edit){
            $entity = $entityDef["class_name_full"]::find($id);
        }
        else{
            $entity = new $entityDef["class_name_full"]();
        }

        if($request->has('_form_sent')){

            /* PASS ONLY ALLOWED FIELDS IN THE REQUEST - FILLOUT THE REST */
            $request = self::fillRequestWithEntityData($request, ($edit ? $entity : null), $allowedFields);

            /* VALIDATE THE INITIAL REQUEST */
            if($validate){
                /* get validation array */
                $validationArray = DefProcessor::getValidationArray($entityDef, $request, false, $edit, $id);

                /* skip validation fields */
                if(!empty($skipValidationFields)){
                    foreach ($skipValidationFields as $field){
                        if(isset($validationArray["rules"][$field]))
                            unset($validationArray["rules"][$field]);
                    }
                }

                $validator = Validator::make($request->all(), $validationArray["rules"], $customErrorMessages, $validationArray["input_names"]);
                $validatorFailed = $validator->fails();
                if($validatorFailed){
                    $errors = $validator->errors();
                }
            }
            /* VALIDATE THE INITIAL REQUEST */

            $toAttach = []; //relationship fields to be attached after
            $baseColumns = array_keys(DefProcessor::getEntityBaseColumns($entityDef["name"]));

            foreach ($entityDef["data"]["_inputs"] as $inputKey => $input){
                /* INGNORE BASE COLUMNS DATA (cannot be changed) */
                if(in_array($inputKey, $baseColumns)){
                    continue;
                }

                /* PARSE INPUT TYPE */
                $inputType = DefProcessor::parseInputType($input["type"]);
                if($inputType[0] != "calculated"){

                    /* FILL VALUES, IF MISSING IN FORM AND APPLY GENERATE FUNCTIONS */
                    //fill values missing in form
                    if($edit && (!$input["display_in_form"] || !$input["fillable_edit"])){
                        $value = $entity->hasAttribute($inputKey) ? $entity->$inputKey : null;

                        if($inputType[0] == "boolean"){ //boolean exception (checkbox not send from form)
                            if($value){
                                $request->merge([$inputKey => $value]);
                            }
                        }
                        else{
                            $request->merge([$inputKey => $value]);
                        }
                    }
                    /* FILL VALUES, IF MISSING IN FORM AND APPLY GENERATE FUNCTIONS */

                    /* resolve input data */
                    $resolvedInput = self::resolveInputField($request, $edit, $validatorFailed, $errors, $inputKey, $input, $entity);

                    /* process data from resolved input */
                    $toAttach = array_merge($toAttach, $resolvedInput["to_attach"]);
                    if($resolvedInput["has_result_value"]){
                        $entity->$inputKey = $resolvedInput["result_value"];
                    }
                    $validatorFailed = $resolvedInput["validator_failed"]; //continue only if validator ok (file upload can change this)
                    $errors = $resolvedInput["errors"]; //appends file upload errors
                }
            }

            /* CONTINUE ONLY IF VALIDATOR OK */
            if(!$validatorFailed){
                /* GENERATING VALUES - AFTER FORM IS PROCCESSED TO HAVE ALL DATA AVAILABLE */
                foreach ($entityDef["data"]["_inputs"] as $inputKey => $input){
                    //generate value
                    if(!empty($input["generate_value_func"])){
                        if(!$edit || ($edit && $input["regenerate_on_edit"])){
                            $func = $input["generate_value_func"];
                            $value = $entity->$func($inputKey, $input, $request, $edit);

                            $entity->$inputKey = $value;
                        }
                    }
                }
                /* GENERATING VALUES - AFTER FORM IS PROCCESSED TO HAVE ALL DATA AVAILABLE */

                /* ADD THE ENTITY IN THE CORRECT FILE STRUCTURE */
                if($entityDef["data"]["uses_content_info"] && $request->has('_add_to_file_structure_id')){
                    $entity->contentInfoParentId = $request->input('_add_to_file_structure_id');
                }

                /* SAVE THE ENTITY */
                $entity->save();

                /* ATTACH RELATIONSHIPS */
                foreach ($toAttach as $attachKey => $attachData){
                    if(in_array($attachData["type_data"][0], ["file", "files"])) {
                        if(!empty($attachData["data"])){
                            $fileIds = [];
                            if($attachData["type_data"][0] == "file")
                                $fileIds = [$attachData["data"]];
                            elseif($attachData["type_data"][0] == "files"){
                                $fileIds = json_decode($attachData["data"], true);
                            }

                            if(!empty($fileIds)){
                                foreach ($fileIds as $key => $fid){
                                    $file = File::find($fid);
                                    if($file){
                                        $file->sort = $key;
                                        $file->rel_type = ltrim($entityDef["class_name_full"], "\\");
                                        $file->rel_id = $entity->id;
                                        $file->rel_field = $attachKey;
                                        $file->save();
                                    }
                                }
                            }
                        }
                    }
                    elseif(in_array($attachData["type_data"][0], ["multiselect", "multiselect_inv"])) {
                        $idsToSync = [];
                        if($attachData["data"] && !empty($attachData["data"])){
                            $attachData["data"] = json_decode($attachData["data"], true); //convert JSON
                            foreach ($attachData["data"] as $key => $currentItem){
                                $idsToSync[$currentItem["id"]] = [
                                    "sort" => $key //sync items in given order
                                ];

                                //relation extra inputs data
                                if(isset($currentItem["relation_extra_inputs_data"]) && count($currentItem["relation_extra_inputs_data"]) > 0){

                                    /* clear relation extra inputs data from unknown data */
                                    if($attachData["type_data"][0] == "multiselect"){
                                        $relationExtraFields = $attachData["input_def"]["relation_extra_inputs"];
                                    }
                                    else{
                                        $partnerDef = $definitions->get("entities.".$attachData["type_data"][1]);
                                        $extraInputsRef = $partnerDef["data"]["_inputs"][$attachData["type_data"][2]]["ref"].'.relation_extra_inputs';
                                        $relationExtraFields = $definitions->getByRef($extraInputsRef);
                                    }

                                    $dataToSave = [];
                                    foreach ($relationExtraFields as $extraInpKey => $extraInp){
                                        $extraInpType = DefProcessor::parseInputType($extraInp["type"]);

                                        /* CORRECT THE VALUE */
                                        switch ($extraInpType[0]){
                                            case 'boolean':
                                                $dataToSave[$extraInpKey] = isset($currentItem["relation_extra_inputs_data"][$extraInpKey]) ? ($currentItem["relation_extra_inputs_data"][$extraInpKey] == 1 ? 1 : 0) : 0;
                                            break;
                                            case 'date':
                                            case 'datetime':
                                                $dataToSave[$extraInpKey] = isset($currentItem["relation_extra_inputs_data"][$extraInpKey]) && !empty($currentItem["relation_extra_inputs_data"][$extraInpKey]) ? Carbon::parse($currentItem["relation_extra_inputs_data"][$extraInpKey]) : null;
                                            break;
                                            case 'password':
                                                $isRequired = (strpos($input["rules"], 'required') !== false);

                                                if(!$isRequired && isset($currentItem["relation_extra_inputs_data"][$extraInpKey.'_empty']) && $currentItem["relation_extra_inputs_data"][$extraInpKey.'_empty'] == 1){
                                                    $dataToSave[$extraInpKey] = null;
                                                }
                                                else{
                                                    $dataToSave[$extraInpKey] = isset($currentItem["relation_extra_inputs_data"][$extraInpKey]) && !empty($currentItem["relation_extra_inputs_data"][$extraInpKey]) ? bcrypt($currentItem["relation_extra_inputs_data"][$extraInpKey]) : null;
                                                }
                                            break;

                                            default:
                                                $dataToSave[$extraInpKey] = isset($currentItem["relation_extra_inputs_data"][$extraInpKey]) && $currentItem["relation_extra_inputs_data"][$extraInpKey] != "" ? $currentItem["relation_extra_inputs_data"][$extraInpKey] : null;
                                            break;
                                        }
                                    }

                                    $idsToSync[$currentItem["id"]] = array_merge($idsToSync[$currentItem["id"]], $dataToSave);
                                }
                            }
                        }

                        if($attachData["type_data"][0] == "multiselect") {
                            $entity->getRelationship($attachKey, true)->sync($idsToSync);
                        }
                        else{
                            $entity->getInverseRelationship($attachData["type_data"][1], $attachData["type_data"][2], true)->sync($idsToSync);
                        }
                    }
                    elseif($attachData["type_data"][0] == "select_inv") {
                        $partnerDef = $definitions->get('entities.'.$attachData["type_data"][1]);
                        $rel_field = $attachData["type_data"][2];

                        $idsToSync = [];
                        if($attachData["data"] && !empty($attachData["data"])){
                            $attachData["data"] = json_decode($attachData["data"], true); //convert JSON
                            foreach ($attachData["data"] as $key => $currentItem){
                                $idsToSync[] = $currentItem["id"];
                            }
                        }

                        /* clear all currently related items */
                        $currentlyRelated = $entity->getInverseRelationship($attachData["type_data"][1], $attachData["type_data"][2]);
                        foreach ($currentlyRelated as $relItem){
                            if(($key = array_search($relItem->id, $idsToSync)) !== false){
                                unset($idsToSync[$key]); //already attached, no need to sync
                            }
                            else{
                                //clear items not in sync array
                                $relItem->$rel_field = null;
                                $relItem->save();
                            }
                        }

                        /* save new items */
                        $newRelatedItem = $partnerDef["class_name_full"]::find($idsToSync);
                        foreach($newRelatedItem as $relItem){
                            if($relItem->$rel_field != $entity->id){
                                $relItem->$rel_field = $entity->id;
                                $relItem->save();
                            }
                        }
                    }
                }

                $msg = $edit ? __('Changes successfully saved.') : __('New :entity_name successfully added to the database.', ["entity_name" => __($entityDef["data"]["title"])]);
                $request->session()->flash('success', ["inLineReport" => $edit, "html" => $msg]);
            }
            else{
                $msg = __('The form contains errors, please correct them and send it again.');
                $request->session()->flash('error', ["inLineReport" => true, "html" => $msg]);
            }
        }

        $state = $request->has('_form_sent') ? !$validatorFailed : false;

        return [
            "state" => $state,
            "errors" => $errors,
            "entity" => $entity
        ];
    }

    public static function resolveInputField($request, $edit, $validatorFailed, $errors, $inputKey, $input, $entity = null){

        /* PARSE INPUT TYPE */
        $inputType = DefProcessor::parseInputType($input["type"]);

        /* defaults */
        $toAttach = [];
        $hasResultValue = false;

        /* FILE UPLOAD - EVEN IF VALIDATOR FAILS (keeps the file in form), but do not attempt to upload if the field has errors */
        if($inputType[0] == "file" && !$errors->has($inputKey) && !($edit && (!$input["display_in_form"] || !$input["fillable_edit"]))){

            $hasResultValue = true;
            $resultValue = null;
            $inputKeyUploadedId = $inputKey."-uploaded-id";
            if(!empty($request->file($inputKey))){
                $fileUpload = File::store($request, $inputKey, $input["ref"], false);

                if($fileUpload["state"]) {
                    // DELETE OLD FILE IF EDITING
                    if($edit && $entity){
                        $rel = DefProcessor::getRelationName($inputKey);
                        if($entity->$rel)
                            $entity->$rel->delete();
                    }

                    $file = $fileUpload["data"];
                    $request->merge([$inputKeyUploadedId => $file->id]);
                    $resultValue = $file->id;
                }
                else{
                    foreach($fileUpload["errors"]->get($inputKey) as $error){
                        $errors->add($inputKey, $error);
                    }
                    $validatorFailed = true;
                }
            }
            else{
                if($request->has($inputKeyUploadedId) && !empty($request->input($inputKeyUploadedId))){
                    $resultValue = $request->input($inputKeyUploadedId);
                }
            }

            /* ATTACH FILE CORRECTLY LATER AFTER SAVE */
            $toAttach[$inputKey] = [
                "type_data" => $inputType,
                "input_def" => $input,
                "data" => $resultValue
            ];
        }
        /* FILE UPLOAD - EVEN IF VALIDATOR FAILS (keeps the file in form) */

        /* CONTINUE ONLY IF VALIDATOR OK */
        elseif(!$validatorFailed){
            /* FIELDS TO ATTACH RELATIONSHIPS */
            if(in_array($inputType[0], ["files", "multiselect", "multiselect_inv", "select_inv"])){
                if(!($edit && (!$input["display_in_form"] || !$input["fillable_edit"]))) { //update only if used in form
                    $toAttach[$inputKey] = [
                        "type_data" => $inputType,
                        "input_def" => $input,
                        "data" => $request->has($inputKey) ? $request->input($inputKey) : null
                    ];
                }
            }

            /* BOOLEAN */
            elseif($inputType[0] == "boolean"){
                if(!($edit && (!$input["display_in_form"] || !$input["fillable_edit"]))) { //update only if used in form
                    $hasResultValue = true;
                    $resultValue = $request->has($inputKey) ? ($request->input($inputKey) == 1) : false;
                }
            }

            /* DATES */
            elseif(in_array($inputType[0], ["date", "datetime"])){
                $hasResultValue = true;
                $resultValue = $request->has($inputKey) && !empty($request->input($inputKey)) ? Carbon::parse($request->input($inputKey)) : null;
            }

            /* PASSWORDS */
            elseif($inputType[0] == "password"){
                $isRequired = (strpos($input["rules"], 'required') !== false);

                $hasResultValue = true;
                if(!$isRequired && $edit && $request->has($inputKey."_empty") && $request->input($inputKey."_empty") == 1){
                    $resultValue = null;
                }
                else{
                    $resultValue = $request->has($inputKey) && !empty($request->input($inputKey)) ? bcrypt($request->input($inputKey)) : ($edit ? $entity->$inputKey : null);
                }
            }

            /* OTHER FIELDS SAVE NORMALLY */
            else{
                $hasResultValue = true;
                $resultValue = $request->has($inputKey) ? $request->input($inputKey) : null;
            }
        }

        return [
            "to_attach" => $toAttach,
            "has_result_value" => $hasResultValue,
            "result_value" => isset($resultValue) ? $resultValue : null,
            "validator_failed" => $validatorFailed,
            "errors" => $errors
        ];
    }

    /* GETS THE INPUT BY STRING REPRESENTATION, COULD BE EXTERNAL */
    public static function getInputByString($inputStr){
        $entityDef = self::getDefStatic();

        /* entity own attribute */
        if(isset($entityDef["data"]["_inputs"][$inputStr])){
            return [
                "_is_external_input" => false,
                "target_input" => $inputStr,
                "target_input_def" => $entityDef["data"]["_inputs"][$inputStr]
            ];
        }

        /* external attribute */
        else{
            global $definitions;
            $inputFound = false;
            $input = null;

            /* parse external input data */
            $externalInputData = explode(":", $inputStr);
            if(count($externalInputData) > 1){
                $externalInputData[0] = explode(".", $externalInputData[0]);
                $externalInputData[1] = explode(".", $externalInputData[1]);

                $externalInputData = [
                    "entity" => isset($externalInputData[0][1]) ? $externalInputData[0][0] : "",
                    "src_input" => isset($externalInputData[0][1]) ? $externalInputData[0][1] : $externalInputData[0][0],
                    "is_inverse" => isset($externalInputData[0][1]),
                    "target_is_in_pivot" => isset($externalInputData[1][1]),
                    "target_input" => isset($externalInputData[1][1]) ? $externalInputData[1][1] : $externalInputData[1][0]
                ];


                /* external input defaults */
                $input = [
                    "_is_external_input" => true,
                    "is_inverse" => $externalInputData["is_inverse"],
                    "target_is_in_pivot" => $externalInputData["target_is_in_pivot"],
                    "external_entity" => $externalInputData["entity"],
                    "src_input" => $externalInputData["src_input"],
                    "target_input" => $externalInputData["target_input"]
                ];


                /* not inverse by parsed data, should be found in entity own inputs.. but these inputs can be inversed (select_inv, multiselect_inv) */
                if(!$externalInputData["is_inverse"]){
                    /* source input exists in source entity */
                    if($entityDef["data"]["_inputs"][$externalInputData["src_input"]]){
                        $inputDef = $entityDef["data"]["_inputs"][$externalInputData["src_input"]];
                        $inputType = DefProcessor::parseInputType($inputDef["type"]);

                        /* if is of inverse type, reset settings with inverse values and continue */
                        if(in_array($inputType[0], ["select_inv", "multiselect_inv"])){
                            $originalSrcInput = $externalInputData["src_input"];
                            $externalInputData["entity"] = $inputType[1];
                            $externalInputData["src_input"] = $inputType[2];
                            $externalInputData["is_inverse"] = true;

                            /* external input defaults */
                            $input = [
                                "_original_src_input" => $originalSrcInput,
                                "_original_src_input_def" => $inputDef,
                                "_is_external_input" => true,
                                "is_inverse" => $externalInputData["is_inverse"],
                                "target_is_in_pivot" => $externalInputData["target_is_in_pivot"],
                                "external_entity" => $externalInputData["entity"],
                                "src_input" => $externalInputData["src_input"],
                                "target_input" => $externalInputData["target_input"]
                            ];
                        }
                        /* source input is of correct type */
                        elseif(in_array($inputType[0], ["select", "multiselect"])){
                            if($definitions->check("entities.".$inputType[1])){
                                $extEntityDef = $definitions->get("entities.".$inputType[1]);
                                $targetInputDef = null;

                                /* is in pivot table */
                                if($externalInputData["target_is_in_pivot"]){
                                    /* target input exits in pivot table */
                                    if(isset($entityDef["data"]["_inputs"][$externalInputData["src_input"]]["relation_extra_inputs"][$externalInputData["target_input"]])){
                                        $targetInputDef = $entityDef["data"]["_inputs"][$externalInputData["src_input"]]["relation_extra_inputs"][$externalInputData["target_input"]];
                                    }
                                }
                                else{
                                    /* target input exits */
                                    if(isset($extEntityDef["data"]["_inputs"][$externalInputData["target_input"]])){
                                        $targetInputDef = $extEntityDef["data"]["_inputs"][$externalInputData["target_input"]];
                                    }
                                }

                                /* target input found */
                                if($targetInputDef){
                                    $inputFound = true;
                                    $input = array_merge($input, [
                                        "external_entity" => $inputType[1],
                                        "external_entity_def" => $extEntityDef,
                                        "src_input_def" => $inputDef,
                                        "target_input_def" => $targetInputDef
                                    ]);
                                }
                            }
                        }
                    }
                }

                /* is inverse relationship */
                if($externalInputData["is_inverse"]){
                    /* check if entity exists */
                    if($definitions->check("entities.".$externalInputData["entity"])){
                        $extEntityDef = $definitions->get("entities.".$externalInputData["entity"]);

                        /* check if entity source input exists - the one that entities are connected through */
                        if(isset($extEntityDef["data"]["_inputs"][$externalInputData["src_input"]])){
                            $inputDef = $extEntityDef["data"]["_inputs"][$externalInputData["src_input"]];
                            $inputType = DefProcessor::parseInputType($inputDef["type"]);
                            $targetInputDef = null;

                            /* source input is of correct type */
                            if(in_array($inputType[0], ["select", "multiselect"])){
                                /* target input is in pivot table */
                                if($externalInputData["target_is_in_pivot"]){
                                    /* target input exits in pivot table */
                                    if(isset($extEntityDef["data"]["_inputs"][$externalInputData["src_input"]]["relation_extra_inputs"][$externalInputData["target_input"]])){
                                        $targetInputDef = $extEntityDef["data"]["_inputs"][$externalInputData["src_input"]]["relation_extra_inputs"][$externalInputData["target_input"]];
                                    }
                                }
                                else{
                                    /* target input exits */
                                    if(isset($extEntityDef["data"]["_inputs"][$externalInputData["target_input"]])){
                                        $targetInputDef = $extEntityDef["data"]["_inputs"][$externalInputData["target_input"]];
                                    }
                                }

                                /* target input found */
                                if($targetInputDef){
                                    $inputFound = true;
                                    $input = array_merge($input, [
                                        "external_entity_def" => $extEntityDef,
                                        "src_input_def" => $inputDef,
                                        "target_input_def" => $targetInputDef
                                    ]);
                                }
                            }

                        }
                    }
                }

                if(!$inputFound){
                    $input = null;
                }
            }

            return $input;
        }
    }

    public function asResource(){
        $resource = clone $this;
        return $resource->toArray();
    }

    public function attachRelationships($relations = []){
        foreach ($relations as $relName => $rel){

            /* if no keys were specified as names */
            if(is_numeric($relName)){
                $relName = $rel;
            }

            if($rel == "author" || $rel == "updated_author"){
                $this[$relName] = $this->$rel;
            }
            else{
                $this[$relName] = $this->getRelationship($rel);
                unset($this["rel_".$rel]);
            }
        }

        return $this;
    }

    public function getEntityTitle($formattedToText = true){
        global $definitions;

        $def = $this->getDef();
        $titleField = $def["data"]["title_field"];

        if($formattedToText){
            $titleInput = $def["data"]["_inputs"][$titleField];
            $inputType = \App\DefProcessor::parseInputType($titleInput["type"]);

            if(in_array($inputType[0], [
                "boolean",
                "select",
                "select_inv",
                "multiselect",
                "multiselect_inv",
                "file",
                "files",
                "password"
            ])){
                switch($inputType[0]){
                    case 'boolean':
                        $str = $this->$titleField == 1 ? __('Yes') : __('No');
                    break;
                    case 'password':
                        if(empty($this->$titleField))
                            $str = '-- '.__('No password').' --';
                        else
                            $str = "********";
                    break;
                    case "select":
                    case "select_inv":
                    case "multiselect":
                    case "multiselect_inv":
                        if(in_array($inputType[0], ["multiselect_inv", "select_inv"])){
                            $partnerItems = $this->getInverseRelationship($inputType[1], $inputType[2]);
                        }

                        /* items from entity own select/multiselect */
                        else{
                            //pivot table does not have relation methods, select item directly, not through getRelationship() method
                            if(in_array($inputType[0], ["select"])){
                                $partnerDef = $definitions->get("entities.".$inputType[1]);
                                $partnerItems = [];
                                if(!empty($this->$titleField)){
                                    $partnerItems = $partnerDef["class_name_full"]::find([$this->$titleField]);
                                }
                            }
                            else{
                                $partnerItems = $this->getRelationship($titleField);
                            }
                        }

                        $str = [];
                        foreach ($partnerItems as $partnerItem) {
                            $str[] = $partnerItem->getEntityTitle($formattedToText);
                        }
                        $str = implode(", ", $str);
                    break;
                    case "file":
                        $file = $this->getRelationship('file');
                        if($file){
                            $str = $file->getFileUrl(false);
                        }
                        else{
                            $str = __('No files');
                        }
                    break;
                    case "files":
                        $files = $this->getRelationship($titleField);
                        if(count($files) > 0){
                            $str = __(":cnt files", ["cnt" => count($files)]);
                        }
                        else{
                            $str = __('No files');
                        }
                    break;
                }
            }
            else{
                $str = $this->$titleField;
            }

            /* limit string limit and strip tags */
            $strLimit = 80;
            $str = strip_tags($str);
            if(strlen($str) > $strLimit){
                $str = substr($str, 0, $strLimit).'...';
            }

            return $str;
        }
        else{
            return $this->$titleField;
        }

    }

    public function getEntityIcon(){
        $def = $this->getDef();
        $customIconField = $def["data"]["custom_icon"];

        $icon = '';
        if(!empty($customIconField) && !empty($this->$customIconField)){
            $icon = $this->$customIconField;
        }
        elseif(!empty($def["data"]["icon"])){
            $icon = $def["data"]["icon"];
        }

        return $icon;
    }

    public function _content_info(){
        return $this->morphOne('App\ContentInfo', 'rel');
    }

    public function isHidden(){
        return $this->is_hidden == 1;
    }

    public function setHidden($hide = true){
        $this->is_hidden = $hide ? 1 : 0;
        return $this->save();
    }

    public function hiddenToggle(){
        if($this->isHidden()){
            return $this->setHidden(false);
        }
        else{
            return $this->setHidden(true);
        }
    }

    public function scopeSorted($query){
        $entityDef = self::getDefStatic();
        $table = $entityDef["table"];

        if($entityDef["data"]["sortable"])
            return $query->orderBy($table.'.sort');
        else
            return $query->orderBy($table.'.id', 'desc');
    }

    public function scopeNotHidden($query){
        $entityDef = self::getDefStatic();
        $table = $entityDef["table"];

        return $query->where($table.'.is_hidden', 0);
    }

    public function scopeForWeb($query){
        return $query->sorted()->notHidden();
    }

    public function getParam($inputKey){
        $def = $this->getDef();

        if(isset($def["data"]["_inputs"][$inputKey])){
            $inputType = DefProcessor::parseInputType($def["data"]["_inputs"][$inputKey]["type"]);
            if(in_array($inputType[0], ["select", "select_inv", "multiselect", "multiselect_inv", "file", "files"])){
                if(in_array($inputType[0], ["select_inv", "multiselect_inv"])){
                    $relItems = $this->getInverseRelationship($inputType[1], $inputType[2], true);
                }
                else{
                    $relItems = $this->getRelationship($inputKey, true);
                }

                if(in_array($inputType[0], ["multiselect", "multiselect_inv"])){
                    $relItems = $relItems->withPivot('sort as pvt_sort')->orderBy('pvt_sort')->get();
                }
                else{
                    $relItems = $relItems->orderBy('sort')->get();
                }

                if(count($relItems) > 0){
                    if(in_array($inputType[0], ["select", "file"])){
                        return $relItems[0];
                    }
                    else{
                        return $relItems;
                    }
                }
                else{
                    return null;
                }
            }
            elseif(in_array($inputType[0], ["date", "datetime"])){
                $date = \App\Traits\EntityTrait::toCarbon($this->$inputKey);
                return (!empty($date) ? $date->format(env('APP_DATE_FORMAT', 'Y-m-d').($inputType[0] == 'datetime' ? ' '.env('APP_TIME_FORMAT', 'H:i:s') : '')) : '');
            }
            else{
                return $this->$inputKey;
            }
        }

        return null;
    }

    public function getSeoParam($param, $paramIsSeoField = false){
        $def = $this->getDef();

        if($paramIsSeoField){
            $seoField = $param;
        }
        else{
            $seoField = isset($def["data"]["seo"][$param]) ? $def["data"]["seo"][$param] : '';
        }

        if($seoField){
            if(isset($def["data"]["_inputs"][$seoField])){
                $inputType = DefProcessor::parseInputType($def["data"]["_inputs"][$seoField]["type"]);
                $param = $this->getParam($seoField);

                if(in_array($inputType[0], ["select", "select_inv", "multiselect", "multiselect_inv", "file", "files"])){
                    if($param){

                        /* get first item */
                        if(in_array($inputType[0], ["select_inv", "multiselect", "multiselect_inv", "files"])){
                            $param = $param[0];
                        }

                        /* get file url */
                        if(in_array($inputType[0], ["file", "files"])){
                            return $param->getFileUrl(true, 1200, 627);
                        }

                        /* get title of partner entity */
                        else{
                            return $param->getEntityTitle();
                        }
                    }
                }
                else{
                    return $param;
                }
            }
        }

        return null;
    }

    /*
        Function will return classes and styles to highlight entity instance row, by default it is empty.
        This can be overwritten inside of a entity model
     */
    public function highlightRow(){
        return [
            "styles" => "",
            "classes" => "",
        ];
    }

    /*
        Function will return classes and styles to highlight a specific cell in a table, by default it is empty.
        This can be overwritten inside of a entity model.
        Function will receive $inputKey of column to hightlight, this could be even related content, like [select_input]:[partner_entity_column]
    */
    public function highlightCell($inputKey){
        return [
            "styles" => "",
            "classes" => "",
        ];
    }
}

<?php

namespace App\Entities;

use App\Traits\EntityTrait;
use App\Traits\HasDynamicRelation;
use Illuminate\Database\Eloquent\Model;

class ContactPersonEntity extends Model
{
    use HasDynamicRelation;
    use EntityTrait;

    protected $table = 'ent_contact_person';

    //
}

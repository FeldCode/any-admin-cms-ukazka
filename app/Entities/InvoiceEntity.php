<?php

namespace App\Entities;

use App\DefProcessor;
use App\Traits\EntityTrait;
use App\Traits\HasDynamicRelation;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class InvoiceEntity extends Model
{
    use HasDynamicRelation;
    use EntityTrait;

    protected $table = 'ent_invoice';

    public function generateInvoiceNo($inputKey = "invoice_no", $input = [], $request = [], $edit = false){
        $y = strftime("%Y");
        $m = strftime("%m");
        $lastInvoice = self::whereRaw("no LIKE '$y%'")->orderBy('id', 'desc')->take(1)->first();
        if($lastInvoice)
            $lastInvoiceNum = (int)substr($lastInvoice->no, -4, 4);
        else
            $lastInvoiceNum = 0;

        $lastInvoiceNum++;
        $value = $y.$m.str_pad($lastInvoiceNum, 4, "0", STR_PAD_LEFT);

        return $value;
    }

    public function generateDueDate($inputKey = "due_date", $input = [], $request = [], $edit = false){
        $value = $this->$inputKey;

        if(empty($value)) {
            $invoiceDate = new Carbon($this->invoice_date);
            $dueDate = $invoiceDate->addDays(14);
            return $dueDate;
        }

        return new Carbon($value);
    }

    public function generateInvoiceDate($inputKey = "due_date", $input = [], $request = [], $edit = false){
        $value = $this->$inputKey;

        if(empty($value)) {
            $invoiceDate = Carbon::now();
            return $invoiceDate;
        }

        return new Carbon($value);
    }

    public function fillByCustomer($inputKey, $input = [], $request = [], $edit = false){
        $value = $this->$inputKey;

        if(empty($value)) {
            $rel = DefProcessor::getRelationName("customer");
            $customer = $this->$rel;

            if (!empty($customer->$inputKey))
                return $customer->$inputKey;
            else
                return $this->$inputKey;
        }

        return $value;
    }
}

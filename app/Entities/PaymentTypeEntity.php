<?php

namespace App\Entities;

use App\Traits\EntityTrait;
use App\Traits\HasDynamicRelation;
use Illuminate\Database\Eloquent\Model;

class PaymentTypeEntity extends Model
{
    use HasDynamicRelation;
    use EntityTrait;

    protected $table = 'ent_payment_type';

    //
}

<?php

namespace App\Entities;

use App\DefProcessor;
use App\Traits\EntityTrait;
use App\Traits\HasDynamicRelation;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CustomerEntity extends Model
{
    use HasDynamicRelation;
    use EntityTrait;

    protected $table = 'ent_customer';

    public function createInvoice($data){
        $invoice = new InvoiceEntity();
        $invoice->no = $invoice->generateInvoiceNo();
        $invoice->customer = $this->id;
        $invoice->project = isset($data->project) && !empty($data->project) ? $data->project : null;
        $invoice->name = isset($data->name) && !empty($data->name) ? $data->name : $this->name;
        $invoice->address = isset($data->address) && !empty($data->address) ? $data->address : $this->address;
        $invoice->ident_number = isset($data->ident_number) && !empty($data->ident_number) ? $data->ident_number : $this->ident_number;
        $invoice->vat_id = isset($data->vat_id) && !empty($data->vat_id) ? $data->vat_id : $this->vat_id;
        $invoice->invoice_date = isset($data->invoice_date) && !empty($data->invoice_date) ? $data->invoice_date : Carbon::now();
        $invoice->due_date = isset($data->due_date) && !empty($data->due_date) ? $data->due_date : $invoice->generateDueDate();

        $invoice->amount = $data->amount;
        $invoice->payment_type = $data->payment_type;
        $invoice->description = $data->description;
        $invoice->save();
    }

    public function getProjectsNumberAttribute(){
        $rel = DefProcessor::getInverseRelationName("project", "customer");

        $projectsNumber = $this->$rel ? count($this->$rel) : 0;
        return $projectsNumber;
    }

    public function getTotalIncomeAttribute(){
        $rel = DefProcessor::getInverseRelationName("invoice", "customer");

        $total = 0;
        if($this->$rel){
            foreach ($this->$rel as $invoice){
                $total += $invoice->amount;
            }
        }

        return number_format($total, 2, ".", " ");
    }
}


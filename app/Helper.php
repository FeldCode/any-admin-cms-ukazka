<?php

namespace App;

use Illuminate\Support\Facades\File;

class Helper
{
    public static function formatBytes($bytes, $precision = 2) {
        $units = array('', 'K', 'M', 'G', 'T');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        $bytes /= pow(1024, $pow);

        return round($bytes, $precision) . $units[$pow];
    }

    public static function returnBytes($val) {
        $val = trim($val);

        $last = strtolower($val[strlen($val)-1]);
        switch($last) {
            // The 'G' modifier is available since PHP 5.1.0
            case 't':
                $val = floatval($val)*1024;
            case 'g':
                $val = floatval($val)*1024;
            case 'm':
                $val = floatval($val)*1024;
            case 'k':
                $val = floatval($val)*1024;
        }

        return (int)$val;
    }

    public static function getFlashStates(){
        $states = [
            "error" => ["icon" => "fa fa-times", "color" => "danger"],
            "warning" => ["icon" => "fa fa-exclamation-triangle", "color" => "warning"],
            "success" => ["icon" => "fa fa-check", "color" => "success"],
            "info" => ["icon" => "fa fa-info-circle", "color" => "info"]
        ];

        return $states;
    }

    public static function sessionKeepFlashMessages(){
        foreach (['error', 'warning', 'success', 'info'] as $flashState){
            if(session()->has($flashState)){
                session()->keep($flashState); //keep flash through the redirect
            }
        }
    }

    public static function getSwalDataFromFlash($flashState, $flash){
        $states = self::getFlashStates();
        $swalData = [];
        if(is_array($flash) && isset($flash[0])){
            $swalData["html"] = $flash;
        }
        elseif(is_array($flash)){
            $swalData = $flash;
        }
        else{
            $swalData["title"] = $flash;
        }

        /* HTML ALERT MESSAGES AS ARRAY */
        if(isset($swalData["html"]) && is_array($swalData["html"])){
            $flashHTML = $swalData["html"];
            $swalData["html"] = '<div class="text-left card-body bg-light swal-messages">';
            foreach ($flashHTML as $flashMsg){
                if(!is_array($flashMsg)){
                    $flashMsg = [
                        "state" => $flashState,
                        "msg" => $flashMsg
                    ];
                }

                $flashMsg = [
                    "icon" => $states[$flashMsg["state"]]["icon"]." text-".$states[$flashMsg["state"]]["color"],
                    "msg" => $flashMsg["msg"]
                ];

                $swalData["html"] .= '<i class="msg-icon '.$flashMsg["icon"].'"></i> <span class="msg-text">'.$flashMsg["msg"].'</span><br>';
            }
            $swalData["html"] .= '</div>';
        }

        /* DEFAULT SWAL SETTINGS */
        $swalDefaults = [
            "icon" => $flashState,
            "title" => __(ucfirst($flashState)."!"),
            "showCloseButton" => ($flashState != 'success'),
            "showConfirmButton" => ($flashState != 'success'),
            "confirmButtonText" => __("OK"),
            "timer" => ($flashState == 'success' ? 1500 : null),
        ];
        foreach ($swalDefaults as $key => $val){
            if(!array_key_exists($key, $swalData)) $swalData[$key] = $val;
        }

        return $swalData;
    }

    public static function getFontAwesomeIconsList(){
        $yaml = File::exists(base_path('node_modules/@fortawesome/fontawesome-free/metadata/icons.yml')) ? File::get(base_path('node_modules/@fortawesome/fontawesome-free/metadata/icons.yml')) : '';
        $yaml = \Spyc::YAMLLoad($yaml);

        $icons = [];
        foreach ($yaml as $icon => $value) {
            if($value["styles"]){
                foreach ($value["styles"] as $style) {
                    $icons[] = 'fa'.substr($style, 0 ,1).' fa-'.$icon;
                }
            }
        }

        return $icons;
    }
}

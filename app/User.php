<?php

namespace App;

use App\Traits\EntityTrait;
use App\Traits\HasDynamicRelation;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject, MustVerifyEmail
{
    use Notifiable;
    use HasDynamicRelation;
    use EntityTrait;

    public const ADMIN_USER_ID = 1;

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($item) { // before delete() method call this
            if($item->id == self::ADMIN_USER_ID){
                session()->now("error", __("Admin user cannot be deleted."));
                return false;
            }
        });

        static::updating(function ($item) { // before delete() method call this
            if($item->id == self::ADMIN_USER_ID && $item->isDeadlocked()){
                session()->now("error", __("Admin user cannot be deadlocked."));
                return false;
            }
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function hasRights($def, $entityId = null){
        global $definitions;

        $refParsed = DefProcessor::parseRef($def["ref"]);
        $groups = explode(",", trim($def["access"]["groups"]));
        $hasRights = true;

        if($def["access"]["type"] == "denied" && in_array($this->rel_user_group->code_name, $groups)){
            $hasRights = false;
        }
        elseif($def["access"]["type"] == "allowed" && !in_array($this->rel_user_group->code_name, $groups)){
            $hasRights = false;
        }

        /* CHECK RIGHTS FOR ENTITY OWNER */
        $ownerGroup = "_owner_";
        if($refParsed["type"] == "entities" && !empty($entityId) && in_array($ownerGroup, $groups)){
            $isOwner = false;
            $fullDef = $definitions->get("entities.".$refParsed["name"]);
            $entityInstance = $fullDef["class_name_full"]::find($entityId);

            if($entityInstance){
                if($fullDef["name"] == "user"){
                    $isOwner = $entityInstance->id == $this->id; //same user
                }
                else{
                    $isOwner = $entityInstance->created_user_id == $this->id; //entity author
                }
            }

            if($def["access"]["type"] == "denied" && in_array($ownerGroup, $groups)){ //if _owner_ in denied, always deny owner
                $hasRights = !$isOwner;
            }
            elseif(!$hasRights && $def["access"]["type"] == "allowed" && in_array($ownerGroup, $groups)){ //allow _owner_ if user wouldnt have rights otherwise
                $hasRights = $isOwner;
            }
        }

        return $hasRights;
    }

    public function getAllowedEntityOperations($entityDef, $entityId = null){
        global $definitions;

        $userCan = [
            "add" => false,
            "edit" => false,
            "delete" => false,
            "detail" => false,
            "hide" => false,
            "download_csv" => false,
            "generate_form" => false
        ];

        if(!empty($entityDef)){
            if(is_string($entityDef)){
                $entityDef = $definitions->get('entities.'.$entityDef);
            }

            $userCan = [
                "add" => $entityDef["data"]["add_operation"]["allowed"] && $definitions->UserHasRights($this, $entityDef["data"]["add_operation"]["button"]["action"]["ref"], $entityId),
                "edit" => $entityDef["data"]["edit_operation"]["allowed"] && $definitions->UserHasRights($this, $entityDef["data"]["edit_operation"]["button"]["action"]["ref"], $entityId),
                "delete" => $entityDef["data"]["delete_operation"]["allowed"] && $definitions->UserHasRights($this, $entityDef["data"]["delete_operation"]["button"]["action"]["ref"], $entityId),
                "detail" => $entityDef["data"]["detail_operation"]["allowed"] && $definitions->UserHasRights($this, $entityDef["data"]["detail_operation"]["button"]["action"]["ref"], $entityId),
                "hide" => $entityDef["data"]["hide_operation"]["allowed"] && $definitions->UserHasRights($this, $entityDef["data"]["hide_operation"]["button"]["action"]["ref"], $entityId),
                "generate_form" => $entityDef["data"]["generate_form_operation"]["allowed"] && $definitions->UserHasRights($this, $entityDef["data"]["generate_form_operation"]["button"]["action"]["ref"], $entityId),
                "download_csv" => $entityDef["data"]["download_csv_operation"]["allowed"] && $definitions->UserHasRights($this, $entityDef["data"]["download_csv_operation"]["button"]["action"]["ref"], $entityId)
            ];
        }

        return $userCan;
    }

    public function authorOf($entity, $instance = false){
        return $this->getInverseRelationship($entity, "created_user_id", $instance);
    }

    public function lastEditAuthorOf($entity, $instance = false){
        return $this->getInverseRelationship($entity, "updated_user_id", $instance);
    }

    public function isDeadlocked(){
        return $this->isHidden();
    }

    public function generateLogin($inputKey = "login", $input = [], $request = [], $edit = false){
        $def = $this->getDef();

        $login = $this->login;

        /* generate login only if not fillable or empty */
        if((!$edit && !$def["data"]["_inputs"]["login"]["fillable_add"]) || ($edit && !$def["data"]["_inputs"]["login"]["fillable_edit"]) || empty($login)){
            $login = $this->email;

            if($edit && $this->id == self::ADMIN_USER_ID){
                $login = $this->login; //do not change login if admin user
            }
        }

        return $login;
    }

    /* JWT TOKEN AUTHENTICATION */
        public function getJWTIdentifier()
        {
            return $this->getKey();
        }

        public function getJWTCustomClaims()
        {
            return [];
        }
    /* JWT TOKEN AUTHENTICATION */

    /* ------------------- FACEBOOK & GOOGLE AUTHENTIFICATION -------------------- */

        /**
         *  Returns FB user data
         * */
        public static function facebookAuthGetUserData($input_token){
            $url = 'https://graph.facebook.com/v3.3/me?fields=id%2Cname%2Cemail&access_token='.$input_token;

            // Get cURL resource
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_USERAGENT => 'Codular Sample cURL Request'
            ]);
            // Send the request & save response to $resp
            $data = curl_exec($curl);
            // Close request to clear up some resources
            curl_close($curl);

            $data = json_decode($data, true);

            return !isset($data["error"]) ? $data : false;
        }

        /**
         *  Returns google user data
         * */
        public static function googleAuthGetUserData($input_token){
            $client = new \Google_Client();  // Specify the CLIENT_ID of the app that accesses the backend

            try {
                $data = $client->verifyIdToken($input_token);
            }
            catch (\Exception $e){
                $data = false;
            }

            /**
             * this is the data optained in $data
             *
             * $data["sub"] ..... USER_ID
             * $data["name"] .... NAME  - only if "profile" scope granted by the user
             * $data["email"] ... EMAIL - only if "profile" scope granted by the user
             *
             */

            return $data ? $data : false;
        }

        public function connectToExternalAuthProvider($externalProviderType, $externalId){
            $authStoreField = $externalProviderType."_auth_id";
            $this->$authStoreField = $externalId;
            $this->markEmailAsVerified();
            $this->save();
        }

    /* ------------------- FACEBOOK & GOOGLE AUTHENTIFICATION -------------------- */

}

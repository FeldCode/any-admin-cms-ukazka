<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DefProcessor
{
    const DEF_PATH = "/../resources/def/";
    protected $definitions = array();
    protected $definitionsData = array();

    public function __construct(){
        $this->definitions = self::loadDefinitionCodeNames();
        $this->loadDefinitions();
        $this->checkSpecialConditions();
    }

    public function loadDefinitions(){
        foreach ($this->definitions as $def){
            $defData = self::loadDefinition($def);
            $this->definitionsData[$def] = $defData;
        }
    }

    public function getAll(){
        return $this->definitionsData;
    }

    public function check($def){
        return isset($this->definitionsData[$def]);
    }

    public function get($def){
        $def = str_replace("/", ".", $def);
        if(isset($this->definitionsData[$def])) {
            return $this->definitionsData[$def];
        }
        else
            return self::checkDefinition($def);
    }

    public static function parseRef($ref){
        $parsedData = [
            "type" => "",
            "name" => "",
            "name_full" => "",
            "path" => []
        ];
        $ref = explode(":", $ref);
        $defFile = explode(".", $ref[0]);

        $parsedData["type"] = isset($defFile[1]) ? $defFile[0] : "";
        $parsedData["name"] = isset($defFile[1]) ? $defFile[1] : $defFile[0];
        $parsedData["name_full"] = $ref[0];

        if(isset($ref[1]))
            $parsedData["path"] = explode(".", $ref[1]);

        return $parsedData;
    }

    public function getByRef($ref){
        $ref = self::parseRef($ref);
        if(!$this->check($ref["name_full"]))
            return false;

        $def = $this->get($ref["name_full"]);
        $obj = $def["data"];

        if(!empty($ref["path"])){
            foreach ($ref["path"] as $path){
                if(!isset($obj[$path]))
                    return false;

                $obj = $obj[$path];
            }
        }

        return $obj;
    }

    public function UserHasRights($user, $ref, $entityId = null){
        $def = $this->getByRef($ref);

        if(!$def || !isset($def["access"]["type"])){
            return false;
        }

        if($def["access"]["type"] == "free")
            return true;

        if($user instanceof User){
            return $user->hasRights($def, $entityId);
        }
        else{
            return false;
        }
    }

    /**
     *
     * @function recursive_scan
     * @description Recursively scans a folder and its child folders
     * @param $path :: Path of the folder/file
     *
     * */
    public static function loadDefinitionCodeNames($path = "", $definitions = []){
        if(empty($path)) $path = dirname(__FILE__).self::DEF_PATH;
        $path = rtrim($path, '/');
        if(!is_dir($path)) {
            $ext = explode(".", $path);
            $ext = end($ext);
            if($ext == "json"){
                $path = str_replace(dirname(__FILE__).self::DEF_PATH, "", $path);
                $path = str_replace(".json", "", $path);
                $path = str_replace(array("/", "\\"), ".", $path);
                $definitions[] = $path;
            }
        }
        else {
            $files = scandir($path);
            foreach($files as $file)
                if($file != '.' && $file != '..'){
                    $definitions = self::loadDefinitionCodeNames($path . '/' . $file, $definitions);
                }
        }

        return $definitions;
    }

    public static function checkDefinition($def){
        $defFile = dirname(__FILE__)."/../resources/def/".str_replace(".", "/", $def).".json";

        if(file_exists($defFile)){
            return true;
        }
        else{
            $defType = explode("/", dirname($defFile));
            $defType = end($defType);

            switch ($defType) {
                case 'pages':
                    abort(404, "Definition file '".$def.".json' not found");
                break;
                default:
                    throw new \ErrorException("Definition file '".$def.".json' not found", 500);
                break;
            }

            return false;
        }
    }

    public static function loadDefinition($def){
        self::checkDefinition($def);

        $defName = explode("/", str_replace(".", "/", $def));
        $defName = end($defName);
        $defFile = dirname(__FILE__)."/../resources/def/".str_replace(".", "/", $def).".json";
        $defType = explode("/", dirname($defFile));
        $defType = end($defType);

        $json = file_get_contents($defFile);
        $data = json_decode($json, true);

        if(json_last_error() == JSON_ERROR_NONE){

            /* DEFAULT INPUTS */
            if($defType == "entities"){
                if(!isset($data["input_form"])) $data["input_form"] = [];
                $defaultInputs = array_merge(self::getEntityDefaultColumns($defName), self::getEntityBaseColumns($defName));
                foreach ($data["input_form"] as &$section){
                    foreach ($section["inputs"] as $inputKey => &$input){
                        if(in_array($inputKey, array_keys($defaultInputs))){
                            foreach ($defaultInputs[$inputKey] as $key => $val){

                                if(!isset($section["inputs"][$inputKey][$key]))
                                    $input[$key] = $defaultInputs[$inputKey][$key];
                            }

                            unset($defaultInputs[$inputKey]);
                        }
                    }
                }
            }

            if(!empty($defaultInputs)) {
                $data["input_form"] = array_merge($data["input_form"], [[
                    "display_header" => false,
                    "inputs" => $defaultInputs
                ]]);
            }

            $data = array(
                "name" => $defName,
                "class_name" => ($defType == "entities" ? Entity::getClassNameFromStr($defName) : null),
                "class_name_full" => ($defType == "entities" ? Entity::getClassNameFromStr($defName, true) : null),
                "table" => ($defType == "entities" ? (Entity::getClassNameFromStr($defName, true))::getTableName() : null),
                "file" => $defFile,
                "type" => $defType,
                "data" => self::checkAndFillDefaults($defFile, $defType, $data)
            );


            $data = self::addDefinitionShortcutsAndDefaults($data);

            return $data;
        }
        else{
            throw new \ErrorException('Definition file for "'.$def.'" is invalid JSON.');
        }
    }

    public static function addDefinitionShortcutsAndDefaults($data){
        if($data["type"] == "entities"){
            /* ENTITY INPUTS ACCESS SHORTCUT */
                $inputs = [];

                foreach ($data["data"]["input_form"] as &$section){
                    foreach ($section["inputs"] as $inputKey => &$input){
                        $input["_key"] = $inputKey;
                        $inputs[$inputKey] = $input;
                    }
                }
                $data["data"]["_inputs"] = $inputs;
            /* ENTITY INPUTS ACCESS SHORTCUT */
        }

        return $data;
    }

    public function checkSpecialConditions(){

        /* SPECIAL CONDITIONS */
        foreach ($this->getAll() as $def){
            if($def["type"] == "entities"){
                $titleFieldFound = false;

                /* SEARCH BASE COLUMNS */
                $baseColumns = self::getEntityBaseColumns($def["name"]);
                foreach ($baseColumns as $baseColKey => $baseCol){
                    /* CHECK IF TITLE FIELD EXISTS */
                    if($baseColKey == $def["data"]["title_field"]) {
                        $titleFieldFound = true;
                    }
                }


                foreach ($def["data"]["_inputs"] as $inputKey => $input){
                    $inputType = self::parseInputType($input["type"]);

                    /* CHECK IF TITLE FIELD EXISTS */
                    if($inputKey == $def["data"]["title_field"]) {
                        $titleFieldFound = true;
                    }

                    /* CHECK RELATION FUNCTIONS: PARTNER ENTITIES EXISTENCE */
                    if(in_array($inputType[0], ["select", "multiselect", "select_inv", "multiselect_inv"])){
                        if(!isset($inputType[1]) || !in_array("entities.".$inputType[1], $this->definitions)){
                            throw new \ErrorException('Definition file for "'.$def["type"].'.'.$def["name"].'" is invalid. Input field "'.$inputKey.'" of type "'.strtoupper($inputType[0]).'": Partner entity "'.$inputType[1].'" was not found.');
                        }
                    }

                    /* CHECK INVERSE RELATION FUNCTIONS: PARTNER FIELD EXISTENCE */
                    if(in_array($inputType[0], ["select_inv", "multiselect_inv"])){
                        $partnerEntityFieldFound = false;
                        if(isset($inputType[2])){
                            foreach ($this->get('entities.'.$inputType[1])["data"]["_inputs"] as $inputKeyPartner => $inputPartner){
                                $partnerInputType = self::parseInputType($inputPartner["type"])[0];
                                if($inputKeyPartner == $inputType[2]) {
                                    if($inputType[0] == "select_inv") $allowed = ["select", "multiselect"];
                                    if($inputType[0] == "multiselect_inv") $allowed = ["multiselect"];

                                    if(!in_array($partnerInputType, $allowed)){
                                        throw new \ErrorException('Definition file for "'.$def["type"].'.'.$def["name"].'" is invalid. Input field "'.$inputKey.'" of type "'.strtoupper($inputType[0]).'": Remote input field "'.$inputType[2].'" of partner entity "'.$inputType[1].'" is of type "'.$partnerInputType.'", allowed: '.implode($allowed).'.');
                                    }

                                    $partnerEntityFieldFound = true;
                                }
                            }
                        }
                        else{
                            $inputType[2] = "";
                        }

                        if(!$partnerEntityFieldFound){
                            throw new \ErrorException('Definition file for "'.$def["type"].'.'.$def["name"].'" is invalid. Input field "'.$inputKey.'" of type "'.strtoupper($inputType[0]).'": Source input field "'.$inputType[2].'" for partner entity "'.$inputType[1].'" was not found.');
                        }
                    }

                    /* CHECK CALCULATED FIELD FUNCTION EXISTENCE */
                    if($inputType[0] == "calculated"){
                        if(!method_exists($def["class_name_full"], 'get'.ucfirst(Str::camel($inputKey).'Attribute'))){
                            throw new \ErrorException('Definition file for "'.$def["type"].'.'.$def["name"].'" is invalid. Input field "'.$inputKey.'" of type "'.strtoupper($inputType[0]).'": Source function "'.('get'.ucfirst(Str::camel($inputKey).'Attribute')).'" not defined in "'.$def["class_name_full"].'" class.');
                        }
                    }

                    /* CHECK GENERATE FIELD FUNCTION EXISTENCE */
                    if(!empty($input["generate_value_func"])){
                        if(!method_exists($def["class_name_full"], $input["generate_value_func"])){
                            throw new \ErrorException('Definition file for "'.$def["type"].'.'.$def["name"].'" is invalid. Input field "'.$inputKey.'" of type "'.strtoupper($inputType[0]).'": Source function "'.$input["generate_value_func"].'" not defined in "'.$def["class_name_full"].'" class.');
                        }
                    }

                    /* CHECK FLOAT NUMBER & DIGITS SETTINGS */
                    if($inputType[0] == "float"){
                        if(!(isset($inputType[1]) && isset($inputType[2]) && is_numeric($inputType[1]) && is_numeric($inputType[2]))){
                            throw new \ErrorException('Definition file for "'.$def["type"].'.'.$def["name"].'" is invalid. Input field "'.$inputKey.'" of type "'.strtoupper($inputType[0]).'": Please specify valid length for number and digits after decimal point, syntax: float:[length_total],[decimal_length].');
                        }
                    }
                }

                if(!$titleFieldFound){
                    throw new \ErrorException('Definition file for "'.$def["type"].'.'.$def["name"].'" is invalid. Title field "'.$def["data"]["title_field"].'" was not found in inputs.');
                }
            }
        }
    }

    public static function getDefaults($defType, $data = [], $dataParent = [], $ref = "", $currentProcessedDefaultsData = []){
        $defaults = [];

        switch($defType){
            case 'def':
                $defaults = array(
                    "logo" => "",
                    "favicon" => "",
                    "home_page" => "",
                    "seo" => array("default" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_seo_default", $data, $dataParent, $ref, $currentProcessedDefaultsData);}, "single_object" => true),
                    "menu" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_menu_item", $data, $dataParent, $ref, $currentProcessedDefaultsData);},
                    "custom_actions" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_button", $data, $dataParent, $ref, $currentProcessedDefaultsData);},
                );
                break;
            case 'entities':
                $defaults = array(
                    "title" => "",
                    "icon" => "",
                    "title_field" => "",
                    "sortable" => false,
                    "custom_icon" => "",
                    "listings_column_sort" => [],
                    "cascade_delete" => [],
                    "uses_content_info" => false, //store additional content info data (like SEO ident, folder structure)
                    "is_folder" => false, //can have childern, must use content_info
                    "use_folder_stacked_icon" => false, //if true and item is a folder, entity default icon is displayed inside of a folder icon, otherwise folder icon is displayed
                    "allow_web_auto_process" => false, //allow processing form in web layer by AppServiceProvider
                    "web_auto_process_success_msg" => false, //custom success message for web auto process
                    "web_auto_process_success_timer" => 1500, //auto close msg, dont display OK after xxx ms, if false, dont auto close
                    "web_auto_process_use_recaptcha" => true, //use recaptcha when autoprocessing form
                    "seo" => array("default" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_seo", $data, $dataParent, $ref, $currentProcessedDefaultsData);}, "single_object" => true),
                    "notifications" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_entity_notification", $data, $dataParent, $ref, $currentProcessedDefaultsData);},
                    "add_operation" => array("default" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_entity_operation", $data, $dataParent, $ref, $currentProcessedDefaultsData);}, "single_object" => true),
                    "edit_operation" => array("default" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_entity_operation", $data, $dataParent, $ref, $currentProcessedDefaultsData);}, "single_object" => true),
                    "delete_operation" => array("default" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_entity_operation", $data, $dataParent, $ref, $currentProcessedDefaultsData);}, "single_object" => true),
                    "detail_operation" => array("default" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_entity_operation", $data, $dataParent, $ref, $currentProcessedDefaultsData);}, "single_object" => true),
                    "hide_operation" => array("default" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_entity_operation", $data, $dataParent, $ref, $currentProcessedDefaultsData);}, "single_object" => true),
                    "download_csv_operation" => array("default" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_entity_operation", $data, $dataParent, $ref, $currentProcessedDefaultsData);}, "single_object" => true),
                    "generate_form_operation" => array("default" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_entity_operation", $data, $dataParent, $ref, $currentProcessedDefaultsData);}, "single_object" => true),
                    "input_form" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_section", $data, $dataParent, $ref, $currentProcessedDefaultsData);},
                    "custom_actions" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_button", $data, $dataParent, $ref, $currentProcessedDefaultsData);},
                );
                break;
            case 'pages':
                $defaults = array(
                    "title" => "",
                    "icon" => "",
                    "access" => array("default" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_access", $data, $dataParent, $ref, $currentProcessedDefaultsData);}, "single_object" => true),
                    "menu" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_button", $data, $dataParent, $ref, $currentProcessedDefaultsData);},
                    "buttons" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_button", $data, $dataParent, $ref, $currentProcessedDefaultsData);}
                );
            break;

            case '_menu_item':
                $defaults = array(
                    "title" => "",
                    "icon" => "",
                    "tooltip" => "",
                    "target" => ""
                );
            break;
            case '_entity_operation':
                $operationType = explode(":", $ref);
                $operationType = end($operationType);

                $allowedDefault = true;
                if(in_array($operationType, ["generate_form_operation"])){
                    $allowedDefault = false;
                }

                $defaults = array(
                    "allowed" => $allowedDefault,
                    "button" => array("default" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_button", $data, $dataParent, $ref, $currentProcessedDefaultsData);}, "single_object" => true),
                );
            break;
            case '_entity_notification':
                $defaults = array(
                    "receiver_type" => array("default" => "email", "allowed" => array("email", "user", "user_group", "def_field")),
                    "receivers" => [],
                );
            break;
            case '_seo':
                $defaults = array(
                    "title" => "",
                    "description" => "",
                    "keywords" => "",
                    "img" => "",
                );
            break;
            case '_seo_default':
                $defaults = array(
                    "default_title" => "",
                    "default_description" => "",
                    "default_keywords" => "",
                    "default_img" => "",
                );
            break;
            case '_access':
                /* ENTITY OPERATIONS DEFAULT ACCESS */
                $defaultsAccess = [];
                if(isset($dataParent["ref"])){
                    $parentRefParsed = self::parseRef($dataParent["ref"]);
                    if($parentRefParsed["type"] == "entities"){
                        $path = implode(".", $parentRefParsed["path"]);

                        /* SPECIAL FIELDS ACCESS */
                        if($parentRefParsed["name"] == "user" && $dataParent["ref_type"] == "_input"){
                            if($parentRefParsed["path"][array_key_last($parentRefParsed["path"])] == "user_group"){
                                $defaultsAccess = [
                                    "type" => "allowed",
                                    "groups" => "admins"
                                ];
                            }
                        }

                        /* UNPROTECTED INPUT ACCESS - allow_web_auto_process (by default all inputs are allowed to edit without need to sign in) */
                        if($dataParent["ref_type"] == "_input" && isset($currentProcessedDefaultsData["allow_web_auto_process"]) && $currentProcessedDefaultsData["allow_web_auto_process"]){
                            $defaultsAccess = [
                                "type" => "free",
                                "groups" => ""
                            ];
                        }

                        /* ENTITY OPERATIONS */
                        if(in_array($path, [
                            "add_operation.button.action",
                            "edit_operation.button.action",
                            "delete_operation.button.action",
                            "detail_operation.button.action",
                            "hide_operation.button.action",
                            "download_csv_operation.button.action",
                            "generate_form_operation.button.action"
                        ])){
                            /* SPECIAL CONDITIONS FOR USERS AND USER GROUPS */
                            if($parentRefParsed["name"] == "user_group"){
                                $defaultsAccess = [
                                    "type" => "allowed",
                                    "groups" => "admins"
                                ];
                            }
                            if($parentRefParsed["name"] == "user"){
                                $defaultsAccess = [
                                    "type" => "allowed",
                                    "groups" => "admins"
                                ];

                                /* ALLOW OWNER TO EDIT HIMSELF */
                                if(in_array($path, ["edit_operation.button.action", "detail_operation.button.action"])){
                                    $defaultsAccess["groups"] .= ",_owner_";
                                }
                            }

                            /* ALLOW GENERATE FORM HTML ONLY TO ADMIN */
                            if($path == "generate_form_operation.button.action"){
                                $defaultsAccess = [
                                    "type" => "allowed",
                                    "groups" => "admins"
                                ];
                            }
                        }
                    }
                }
                /* ENTITY OPERATIONS DEFAULT ACTIONS */

                $defaults = array(
                    "type" => array("default" => isset($defaultsAccess["type"]) ? $defaultsAccess["type"] : "denied", "allowed" => array("denied", "allowed", "free")),
                    "groups" => isset($defaultsAccess["groups"]) ? $defaultsAccess["groups"] : ""
                );
            break;
            case '_button':
                /* ENTITY OPERATIONS DEFAULT BUTTONS */
                    $defaultsBtn = [];
                    if(isset($dataParent["ref"])){
                        $parentRefParsed = self::parseRef($dataParent["ref"]);
                        if($parentRefParsed["type"] == "entities"){
                            $pathLast = !empty($parentRefParsed["path"]) ? end($parentRefParsed["path"]) : "";
                            if($pathLast == "add_operation"){
                                $defaultsBtn["title"] = "New entry";
                                $defaultsBtn["icon"] = "fa fa-plus-square";
                                $defaultsBtn["bs_color"] = "success";
                            }
                            elseif($pathLast == "edit_operation"){
                                $defaultsBtn["title"] = "Edit";
                                $defaultsBtn["tooltip"] = "Edit";
                                $defaultsBtn["icon"] = "fa fa-edit";
                                $defaultsBtn["bs_color"] = "primary";
                            }
                            elseif($pathLast == "delete_operation"){
                                $defaultsBtn["title"] = "Remove";
                                $defaultsBtn["tooltip"] = "Remove";
                                $defaultsBtn["icon"] = "fas fa-trash-alt";
                                $defaultsBtn["bs_color"] = "danger";
                            }
                            elseif($pathLast == "detail_operation"){
                                $defaultsBtn["title"] = "Details";
                                $defaultsBtn["tooltip"] = "Details";
                                $defaultsBtn["icon"] = "fa fa-search";
                                $defaultsBtn["bs_color"] = "secondary";
                            }
                            elseif($pathLast == "hide_operation"){
                                /* DEADLOCK USER */
                                if($parentRefParsed["name"] == 'user'){
                                    $defaultsBtn["title"] = "Toggle deadlock user";
                                    $defaultsBtn["tooltip"] = "Toggle deadlock user";
                                    $defaultsBtn["icon"] = "fa fa-user-slash";
                                    $defaultsBtn["bs_color"] = "danger";
                                }

                                /* HIDE ITEM */
                                else{
                                    $defaultsBtn["title"] = "Toggle hide item";
                                    $defaultsBtn["tooltip"] = "Toggle hide item";
                                    $defaultsBtn["icon"] = "far fa-eye-slash";
                                    $defaultsBtn["bs_color"] = "info";
                                    $defaultsBtn["classes"] = "text-white";
                                }
                            }
                            elseif($pathLast == "download_csv_operation"){
                                $defaultsBtn["title"] = "Download CSV";
                                $defaultsBtn["tooltip"] = "Download CSV";
                                $defaultsBtn["icon"] = "fa fa-download";
                                $defaultsBtn["classes"] = "text-white";
                                $defaultsBtn["bs_color"] = "info";
                            }
                            elseif($pathLast == "generate_form_operation"){
                                $defaultsBtn["title"] = "Generate HTML for web";
                                $defaultsBtn["tooltip"] = "Generate HTML for web";
                                $defaultsBtn["icon"] = "fa fa-code";
                                $defaultsBtn["classes"] = "";
                                $defaultsBtn["bs_color"] = "secondary";
                            }
                        }
                    }
                /* ENTITY OPERATIONS DEFAULT BUTTONS */

                $defaults = array(
                    "title" => isset($defaultsBtn["title"]) ? $defaultsBtn["title"] : "",
                    "icon" => isset($defaultsBtn["icon"]) ? $defaultsBtn["icon"] : "",
                    "tooltip" => isset($defaultsBtn["tooltip"]) ? $defaultsBtn["tooltip"] : "",
                    "classes" => isset($defaultsBtn["classes"]) ? $defaultsBtn["classes"] : "",
                    "bs_color" => isset($defaultsBtn["bs_color"]) ? $defaultsBtn["bs_color"] : "secondary",
                    "onclick" => isset($defaultsBtn["onclick"]) ? $defaultsBtn["onclick"] : "",
                    "simple_link" => false,
                    "action" => array("default" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_action", $data, $dataParent, $ref, $currentProcessedDefaultsData);}, "single_object" => true)
                );
            break;
            case '_action':
                /* ENTITY OPERATIONS DEFAULT ACTIONS */
                    $defaultsAction = [];
                    if(isset($dataParent["ref"])){
                        $parentRefParsed = self::parseRef($dataParent["ref"]);
                        if($parentRefParsed["type"] == "entities"){
                            $path = implode(".", $parentRefParsed["path"]);
                            if($path == "add_operation.button"){
                                $defaultsAction["type"] = "component";
                                $defaultsAction["target"] = "form";
                            }
                            elseif($path == "edit_operation.button"){
                                $defaultsAction["type"] = "component";
                                $defaultsAction["target"] = "form";
                            }
                            elseif($path == "delete_operation.button"){
                                $defaultsAction["type"] = "function";
                                $defaultsAction["target"] = "remove_entity";
                                $defaultsAction["confirm"] = true;

                                /* DELETING USER */
                                if($parentRefParsed["name"] == 'user'){
                                    /* condition - not admin */
                                    $defaultsAction["cond"] = 'id!:'.User::ADMIN_USER_ID;
                                }

                                /* DELETING USER GROUP */
                                if($parentRefParsed["name"] == 'user_group'){
                                    /* condition - not admins, not anonymour */
                                    $defaultsAction["cond"] = 'id!:'.UserGroup::GROUP_ADMINS.','.UserGroup::GROUP_ANONYMOUS;
                                }
                            }
                            elseif($path == "detail_operation.button"){
                                $defaultsAction["type"] = "component";
                                $defaultsAction["target"] = "detail";
                            }
                            elseif($path == "hide_operation.button"){
                                $defaultsAction["type"] = "function";
                                $defaultsAction["target"] = "hide_entity";

                                /* DEADLOCKING USER - needs to confirm */
                                if($parentRefParsed["name"] == 'user'){
                                    $defaultsAction["confirm"] = true;

                                    /* condition - not admin */
                                    $defaultsAction["cond"] = 'id!:'.User::ADMIN_USER_ID;
                                }


                                /* HIDING USER GROUPS */
                                if($parentRefParsed["name"] == 'user_group'){
                                    /* condition - not admins, not anonymour */
                                    $defaultsAction["cond"] = 'id!:'.UserGroup::GROUP_ADMINS.','.UserGroup::GROUP_ANONYMOUS;
                                }
                            }
                            elseif($path == "download_csv_operation.button"){
                                $defaultsAction["type"] = "url-post";
                                $defaultsAction["target"] = "../helper/get-table-component-csv";
                            }
                            elseif($path == "generate_form_operation.button"){
                                $defaultsAction["type"] = "function";
                                $defaultsAction["target"] = "generate_form_html";
                            }
                        }
                    }
                /* ENTITY OPERATIONS DEFAULT ACTIONS */

                $defaults = array(
                    "type" => array("default" => isset($defaultsAction["type"]) ? $defaultsAction["type"] : "", "allowed" => array("url", "page", "component", "function")),
                    "access" => array("default" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_access", $data, $dataParent, $ref, $currentProcessedDefaultsData);}, "single_object" => true),
                    "confirm" => isset($defaultsAction["confirm"]) ? $defaultsAction["confirm"] : false,
                    "background_action" => isset($defaultsAction["background_action"]) ? $defaultsAction["background_action"] : false,
                    "target" => isset($defaultsAction["target"]) ? $defaultsAction["target"] : "",
                    "cond" => isset($defaultsAction["cond"]) ? $defaultsAction["cond"] : "",
                    "data" => array("default" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_action_data", $data, $dataParent, $ref, $currentProcessedDefaultsData);}, "single_object" => true),
                    "inputs" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_input", $data, $dataParent, $ref, $currentProcessedDefaultsData);},
                );
            break;
            case '_path_setting':
                $defaults = array(
                    "levels" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_path_level_setting", $data, $dataParent, $ref, $currentProcessedDefaultsData);},
                );
            break;
            case '_path_level_setting':
                $defaults = array(
                    "allowed_entities" => "", //"allowed_entities": "test,test2", || "allowed_entities": "*",
                    "sortable" => true,
                    "order" => "",
                    "page_limit" => 50,
                    "access" => array("default" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_access", $data, $dataParent, $ref, $currentProcessedDefaultsData);}, "single_object" => true),
                );
            break;
            case '_action_data':
                /* ENTITY OPERATIONS DEFAULT ACTIONS */
                    $defaultsAction = [];
                    if(isset($dataParent["ref"])){
                        $parentRefParsed = self::parseRef($dataParent["ref"]);
                        if($parentRefParsed["type"] == "entities"){
                            $path = implode(".", $parentRefParsed["path"]);
                            if($path == "add_operation.button.action"){
                                $defaultsAction["entity"] = $parentRefParsed["name"];
                            }
                            elseif($path == "edit_operation.button.action"){
                                $defaultsAction["entity"] = $parentRefParsed["name"];
                            }
                            elseif($path == "delete_operation.button.action"){
                                $defaultsAction["entity"] = $parentRefParsed["name"];
                            }
                            elseif($path == "detail_operation.button.action"){
                                $defaultsAction["entity"] = $parentRefParsed["name"];
                            }
                            elseif($path == "hide_operation.button.action"){
                                $defaultsAction["entity"] = $parentRefParsed["name"];
                            }
                            elseif($path == "download_csv_operation.button.action"){
                                $defaultsAction = array(
                                    "entity" => $parentRefParsed["name"],
                                    "order" => "",
                                    "page_limit" => 50,
                                    "use_filters" => true,
                                    "show_filters" => [],
                                    "filters" => [],
                                    "show_fields" => "",
                                    "cond" => "",
                                    "highlights" => [],
                                );
                            }
                        }
                    }
                /* ENTITY OPERATIONS DEFAULT ACTIONS */

                if(explode(":", $dataParent["type"])[0] == "component"){
                    if($dataParent["target"] == "file_structure"){
                        $defaults = array(
                            "base_path" => "",
                            "path" => "",
                            "path_settings" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_path_setting", $data, $dataParent, $ref, $currentProcessedDefaultsData);}
                        );
                    }
                    if($dataParent["target"] == "table"){
                        $defaults = array(
                            "entity" => "",
                            "order" => "",
                            "page_limit" => 50,
                            "use_filters" => true,
                            "show_filters" => [],
                            "filters" => [],
                            "show_fields" => "",
                            "cond" => "",
                            "highlights" => [],
                        );
                    }
                    elseif($dataParent["target"] == "form"){
                        $defaults = array(
                            "entity" => "",
                            "id" => null
                        );
                    }
                }
                else{
                    $defaults = array();
                }

                $defaults = array_merge($defaults, $defaultsAction);
            break;
            case '_section':
                $defaults = array(
                    "title" => "",
                    "icon" => "",
                    "classes" => "",
                    "wrapper_classes" => "",
                    "display_header" => true,
                    "inputs" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_input", $data, $dataParent, $ref, $currentProcessedDefaultsData);}
                );
                break;
            case '_input':
                /* defaults by type */
                $defaultInputType = "string";
                $defaultFilterType = "";
                $defaultIndex = "";
                $defaultCascadeDelete = false;

                /* get current input type */
                $inputType = $defaultInputType;
                if(isset($data["type"]))
                    $inputType = self::parseInputType($data["type"])[0];

                /* change default index on foreign keys */
                if($inputType == "select"){
                    $defaultIndex = "index";
                }

                /* change default cascade delete on files fields to auto delete */
                if(in_array($inputType, ["file", "files"])){
                    $defaultCascadeDelete = true;
                }

                $allowedFilters = self::allowedFiltersOnInputType($inputType);
                if(!empty($allowedFilters)){
                    $defaultFilterType = $allowedFilters[0];
                }

                $allowedTypes = array(
                    "string",
                    "int",
                    "float", //float:[max_numbers],[digits]
                    "date",
                    "time",
                    "datetime",
                    "boolean",
                    "html",
                    "text",
                    "color",
                    "select", //select:[entity]
                    "select_inv",
                    "multiselect", //multiselect:[entity]
                    "multiselect_inv", //multiselect:[entity],[field]
                    "file", //file:[ext1],[ext2],...
                    "files", //files:[ext1],[ext2],...
                    "calculated",
                    "password",
                    "icon"
                );

                /* disable certain types in pivot tables (relation extra fields in multiselect) */
                if(isset($dataParent["ref"])){
                    $parentRef = explode(".", $dataParent["ref"]);
                    if(isset($dataParent["ref"]) && end($parentRef) == "multiselect"){

                        $disabledTypes = [
                            "select_inv",
                            "multiselect",
                            "multiselect_inv",
                            "file",
                            "files",
                            "calculated",
                        ];

                        foreach ($allowedTypes as $allowedKey => $allowedType){
                            if(in_array($allowedType, $disabledTypes)){
                                unset($allowedTypes[$allowedKey]);
                            }
                        }
                    }
                }

                $defaults = array(
                    "access" => array("default" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_access", $data, $dataParent, $ref, $currentProcessedDefaultsData);}, "single_object" => true),
                    "title" => "",
                    "icon" => "",
                    "tooltip" => "",
                    "type" => array(
                        "default" => $defaultInputType,
                        "allowed" => $allowedTypes
                    ),
                    "relation_extra_inputs" => function($defFile, $data, $dataParent, $ref)use($currentProcessedDefaultsData){return DefProcessor::checkAndFillDefaults($defFile, "_input", $data, $dataParent, $ref, $currentProcessedDefaultsData);},
                    "relation_cascade_delete" => $defaultCascadeDelete,
                    "relation_max_items" => null,
                    "relation_allow_add_new" => true,
                    "relation_show_items_as_table" => false,
                    "relation_bound_mode" => false,
                    "html_keep_formatting" => false,
                    "generate_value_func" => "",
                    "regenerate_on_edit" => false,
                    "index" => array("default" => $defaultIndex, "allowed" => array("", "index", "unique")),
                    "rules" => "",
                    "fillable_add" => true,
                    "fillable_edit" => true,
                    "filterable" => true,
                    "filter_type" => array("default" => $defaultFilterType, "allowed" => $allowedFilters),
                    "display_in_form" => true,
                    "display_in_listings" => false,
                    "display_in_detail" => true,
                    "classes" => "",
                    "wrapper_classes" => "",
                    "prefix" => "",
                    "suffix" => ""
                );
                break;
            default:
                $defaults = array();
                break;
        }

        if(empty($ref)){
            $ref = "__code__:";
        }

        $defaults["ref"] = $ref; //code reference for further use, ie. checking permissions of actions
        return $defaults;
    }

    public static function checkAndFillDefaults($defFile, $defType, $data, $dataParent = array(), $ref = "", $currentProcessedDefaultsData = []){
        if(empty($ref)) {
            $defName = explode("/", str_replace([".json", "\\"], ["", "/"], $defFile));
            $ref = ($defType != "def" ? $defType."." : '').end($defName).":";
        }
        if(!isset($data["ref"])) $data["ref"] = $ref;
        $data["ref_type"] = $defType;

        /* pass on first level data currently processed */
        if(!empty($dataParent) && empty($currentProcessedDefaultsData))
            $currentProcessedDefaultsData = $dataParent;

        $defaults = self::getDefaults($defType, $data, $dataParent, $ref, $currentProcessedDefaultsData);

        foreach ($defaults as $fieldName => $def){
            $callableSingleObject = false;

            if(is_array($def) && isset($def["default"])){
                /* CHECKING ALLOWED VALUES FOR THE FIELD */
                if(isset($def["allowed"]) && isset($data[$fieldName]) && !in_array(self::parseInputType($data[$fieldName])[0], $def["allowed"])){
                    throw new \ErrorException('Invalid value "'.$data[$fieldName].'" in definition file "'.$defFile.'". Allowed values for attribute "'.$fieldName.'"'.(isset($data["type"]) ? ' of data type "'.$data["type"].'"' : '').': '.implode(", ", $def["allowed"]), 500);
                }

                /* GET DEFAULT VALUE IF IN ARRAY */
                if(isset($def["single_object"])){
                    $callableSingleObject = $def["single_object"];
                }
                if(isset($def["default"])){
                    $def = $def["default"];
                }
                else{
                    $def = "";
                }
            }

            /* IF DEFAULT VALUE IS A FUNCTION, THEN CALL IT
               ON FIELDS DESCENDANTS
             */
            if(!is_string($def) && is_callable($def)){
                if(!isset($data[$fieldName])) $data[$fieldName] = array();

                $refDelimeter = substr($data["ref"], -1, 1) == ":" ? "" : ".";

                if($callableSingleObject){
                    $data[$fieldName] = $def($defFile, $data[$fieldName], $data, $data["ref"].$refDelimeter.$fieldName);
                }
                else{
                    foreach ($data[$fieldName] as $objKey => &$objToCheck){
                        $objToCheck = $def($defFile, $objToCheck, $data, $data["ref"].$refDelimeter.$fieldName.".".$objKey);
                    }
                }
            }

            /* IF FIELD NAME IS NOT SET, THEN SET IT TO DEFAULT */
            if(!isset($data[$fieldName]))
                $data[$fieldName] = $def;
        }

        return $data;
    }

    public static function getValidationArray($inputsObject, $request, $plainInputs = false, $edit = false, $editId = null){
        if($plainInputs){
            $inputsObject["data"]["_inputs"] = $inputsObject;
        }

        $inputNames = [];
        $validationArray = [];
        foreach ($inputsObject["data"]["_inputs"] as $inputKey => $input){
            $inputNames[$inputKey] = __($input["title"]);

            $inputType = self::parseInputType($input["type"])[0];

            /* SKIP VALIDATION IF NOT USED IN FORM */
            if(!$input["display_in_form"]){
                continue;
            }
            if($edit && !$input["fillable_edit"]){
                continue;
            }
            if(!$edit && !$input["fillable_add"]){
                continue;
            }

            /* SKIP FILE VALIDATION IF ALREADY UPLOADED & FILE IS EMPTY */
                $inputKeyUploadedId = $inputKey."-uploaded-id";
                if($inputType == "file" && !$request->hasFile($inputKey) && isset($request->$inputKeyUploadedId) && !empty($request->$inputKeyUploadedId)){
                    $file = File::find($request->$inputKeyUploadedId);
                    if($file){
                        continue;
                    }
                }
            /* SKIP FILE VALIDATION IF ALREADY UPLOADED */

            $validation = self::getInputValidation($input, false, $edit, $editId);
            if (!empty($validation))
                $validationArray[$inputKey] = $validation;

            /* REQUIRED TITLE FIELD INPUT IN ENTITY */
            if(!$plainInputs && $inputsObject["data"]["title_field"] == $inputKey){
                $validationArray[$inputKey][] = "required";
            }

            /* PASSWORD IS NOT REQUIRED WHEN EDITING */
            if($edit && $inputType == "password"){
                foreach ($validationArray[$inputKey] as $validationKey => $validation){
                    if($validation == "required")
                        unset($validationArray[$inputKey][$validationKey]);
                }
            }
        }

        return [
            "input_names" => $inputNames,
            "rules" => $validationArray
        ];
    }

    public static function getInputValidation($input, $externalUploader = false, $edit = false, $editId = null){
        $defaultValidationRules = [
            "string" => "nullable|string|max:191",
            "int" => "nullable|integer|between:-2147483648,2147483647",
            "float" => "nullable|numeric",
            "date" => "nullable|date",
            "time" => "nullable|date_format:".env('APP_TIME_FORMAT', 'H:i:s'),
            "datetime" => "nullable|date",
            "boolean" => "",
            "html" => "nullable|string|max:4294967295", //cca 64kb - lenth of mysql TEXT
            "text" => "nullable|string|max:4294967295", //cca 64kb - lenth of mysql TEXT
            "color" => "nullable|string|regex:/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/i",
            "select" => "nullable|integer|max:4294967295",
            "select_inv" => "nullable|json",
            "multiselect" => "nullable|json",
            "multiselect_inv" => "nullable|json",
            "file" => "nullable|file",
            "files" => "nullable|file",
            "calculated" => "",
            "password" => "nullable|string|max:191|min:6|confirmed",
            "icon" => "nullable|string|max:191"
        ];

        //entity that requests validation is not dropzone input or any other external uploader, therefore files should be in JSON format
        if(!$externalUploader)
            $defaultValidationRules["files"] = "nullable|json";

        $inputType = self::parseInputType($input["type"]);

        /* DEFAULT VALIDATION */
        $validation = [];
        if(isset($defaultValidationRules[$inputType[0]]))
            $validation = explode("|", $defaultValidationRules[$inputType[0]]);

        /* USER DEFINED VALIDATION */
        if(!empty($input["rules"])){
            $userDefinedRules = explode("|", $input["rules"]);
            $validation = array_merge($validation, $userDefinedRules);
        }

        //float between its length
        if($inputType[0] == "float"){
            $max = "";
            for($i = 0; $i < $inputType[1]-$inputType[2]; $i++)
                $max .= "9";

            $max .= ".";
            for($i = 0; $i < $inputType[2]; $i++){
                $max .= "9";
            }

            $validation[] = "between:-".$max.",".$max;
        }

        //file, files - max:(kb)|mimes:(accepted)
        elseif(in_array($inputType[0], ["file", "files"])){
            if(!($inputType[0] == "files" && !$externalUploader)){
                $validation[] = "max:".(File::getMaxUploadFileSize()/1024);
                if(isset($args)){
                    $validation[] = "mimes:".implode(",", $args);
                }
            }
        }

        /* UNIQUE VALIDATION */
        if($input["index"] == "unique"){
            $ref = self::parseRef($input["ref"]);

            if(!$edit){
                $validation[] = "unique:".self::getEntityTable($ref["name"]);
            }
            else{
                $validation[] = "unique:".self::getEntityTable($ref["name"]).",".$input["_key"].",".$editId;
            }
        }

        /* CALCULATED FIELDS ARE ONLY FUNCTIONS */
        if($inputType[0] == "calculated"){
            $validation = [];
        }

        return $validation;
    }

    public static function getInputForDB($input){
        $inputArgs = explode(":", $input["type"]);
        $inputType = $inputArgs[0];
        if(isset($inputArgs[1])){
            $inputArgs = explode(",", $inputArgs[1]);
        }
        else {
            $inputArgs = false;
        }

        /* CONVERSION ARRAY */
        $typesConv = [
            "string" => ["func" => "string", "func_uses_args" => false, "extra" => ["nullable"]],
            "int" => ["func" => "integer", "func_uses_args" => false, "extra" => ["nullable"]],
            "float" => ["func" => "decimal", "func_uses_args" => true, "extra" => ["nullable"]], //float:[max_numbers],[digits]
            "date" => ["func" => "date", "func_uses_args" => false, "extra" => ["nullable"]],
            "time" => ["func" => "string", "func_uses_args" => false, "extra" => ["nullable"]],
            "datetime" => ["func" => "datetime", "func_uses_args" => false, "extra" => ["nullable"]],
            "boolean" => ["func" => "boolean", "func_uses_args" => false, "extra" => [], "default" => 0],
            "html" => ["func" => "longText", "func_uses_args" => false, "extra" => ["nullable"]],
            "text" => ["func" => "longText", "func_uses_args" => false, "extra" => ["nullable"]],
            "color" => ["func" => "string", "func_uses_args" => false, "extra" => ["nullable"]],
            "select" => ["func" => "integer", "func_uses_args" => false, "extra" => ["nullable", "unsigned"]], //select:[entity]
            "select_inv" => false,
            "multiselect" => false, //multiselect:[entity]
            "multiselect_inv" => false,
            "file" => ["func" => "integer", "func_uses_args" => false, "extra" => ["nullable", "unsigned"]], //file:[ext1],[ext2],...
            "files" => false, //files:[ext1],[ext2],...
            "calculated" => false,
            "password" => ["func" => "string", "func_uses_args" => false, "extra" => ["nullable"]],
            "icon" => ["func" => "string", "func_uses_args" => false, "extra" => ["nullable"]],
        ];

        if(isset($typesConv[$inputType]) && $typesConv[$inputType]){
            /* ADDING INDEXES */
            if(isset($input["index"]) && !empty($input["index"])){
                $typesConv[$inputType]["extra"][] = $input["index"];
            }

            return array_merge(
                $typesConv[$inputType],
                ["args" => $inputArgs]
            );
        }
        else
            return array(
                "func" => false,
                "func_uses_args" => false,
                "args" => false,
                "extra" => []
            );
    }

    public static function getSystemEntities(){
        return ["user", "user_group"];
    }

    public static function isSystemEntity($entity){
        return in_array($entity, self::getSystemEntities());
    }

    public static function getEntityDefaultColumns($entity = ""){
        if(!self::isSystemEntity($entity))
            $entity = "_entity";

        $defaultColumns = [
            "_entity" => [],
            "user" => [
                "name" => [
                    "title" => "Name",
                    "rules" => "required",
                ],
                "login" => [
                    "title" => "Login",
                    "rules" => "required",
                    "index" => "unique",
                    "generate_value_func" => "generateLogin",
                    "regenerate_on_edit" => true,
                    "fillable_add" => false,
                    "fillable_edit" => false
                ],
                "email" => [
                    "title" => "Email",
                    "rules" => "required|email",
                    "index" => "unique",
                ],
                "password" => [
                    "title" => "Password",
                    "type" => "password",
                    "rules" => "required",
                ],
                "user_group" => [
                    "title" => "User Group",
                    "type" => "select:user_group",
                    "rules" => "required",
                ]
            ],
            "user_group" => [
                "code_name" => [
                    "title" => "Code name",
                    "tooltip" => "Code name is used when setting permissions in definition files",
                    "index" => "unique",
                    "rules" => "required",
                    "fillable_edit" => false
                ],
                "name" => [
                    "title" => "Name",
                    "rules" => "required",
                ],
                "admin_access" => [
                    "title" => "Can access admin",
                    "type" => "boolean",
                    "index" => "index"
                ],
                "users_number" => [
                    "title" => "Number of users",
                    "type" => "calculated",
                    "display_in_listings" => true,
                ]
            ],
        ];

        return $defaultColumns[$entity];
    }

    public static function getEntityBaseColumns($entity = ""){
        if(!self::isSystemEntity($entity))
            $entity = "_entity";

        $baseColumnCharacteristics = [
            "_is_base_column" => true,
            "display_in_listings" => false,
            "display_in_form" => false,
            "display_in_detail" => true,
        ];

        $sharedBaseColumns =[
            "id" => [
                "title" => "ID",
                "type" => "int",
            ],
            "sort" => [
                "title" => "Sort ID",
                "type" => "int",
                "display_in_detail" => false,
            ],
            "is_hidden" => [
                "title" => "Hidden",
                "type" => "boolean"
            ],
            "created_user_id" => [
                "title" => "Author",
                "type" => "select:user",
            ],
            "updated_user_id" => [
                "title" => "Last edited by user",
                "type" => "select:user",
            ],
            "created_at" => [
                "title" => "Created",
                "type" => "datetime",
                "display_in_listings" => true,
            ],
            "updated_at" => [
                "title" => "Last edited",
                "type" => "datetime",
            ],
        ];

        $baseColumns = [
            "_entity" => $sharedBaseColumns,
            "user" => array_merge($sharedBaseColumns, [
                "email_verified_at" => [
                    "title" => "Email verified at",
                    "type" => "datetime",
                ],
                "remember_token" => [
                    "title" => "Remember token",
                    "type" => "string",
                    "display_in_detail" => false,
                ],
            ]),
            "user_group" => $sharedBaseColumns,
        ];

        foreach ($baseColumns as &$entityCols){
            foreach ($entityCols as &$colData){
                foreach ($baseColumnCharacteristics as $baseColCharKey => $baseColChar){
                    if(!isset($colData[$baseColCharKey]))
                        $colData[$baseColCharKey] = $baseColChar;
                }
            }
        }

        return $baseColumns[$entity];
    }

    public static function getEntityPivotTableBaseColumns($entity, $partner){
        $baseColumnCharacteristics = [
            "_is_base_column" => true,
            "display_in_listings" => false,
            "display_in_form" => false,
            "display_in_detail" => true,
        ];

        $baseColumns = [
            "sort" => [
                "title" => "Sort ID",
                "type" => "int",
                "display_in_detail" => false,
            ],
            self::getEntityForeignKey($entity) => [
                "title" => $entity." ID",
                "type" => "int",
            ],
            self::getEntityForeignKey($partner) => [
                "title" => $partner." ID",
                "type" => "int",
            ],
        ];

        foreach ($baseColumns as &$colData){
            foreach ($baseColumnCharacteristics as $baseColCharKey => $baseColChar){
                if(!isset($colData[$baseColCharKey]))
                    $colData[$baseColCharKey] = $baseColChar;
            }
        }

        return $baseColumns;
    }

    public static function getEntityTable($entity){
        if(self::isSystemEntity($entity))
            return Str::plural($entity);
        else
            return 'ent_'.$entity;
    }

    public static function getEntityPivotTable($entity, $partner, $field){
        $tables = [$entity, $partner, $field];
        $pivotTable = implode("_", $tables);

        return 'pvt_'.$pivotTable;
    }

    public static function getEntityForeignKey($entity){
        return 'ent_'.$entity.'_id';
    }

    public static function getInverseRelationType($relType){
        $inverseType = [
            "1-N" => "N-1",
            "N-1" => "1-N",
            "N-N" => "N-N"
        ];

        return $inverseType[$relType];
    }

    public static function getRelationName($field){
        return "rel_".$field;
    }

    public static function getInverseRelationName($entity, $field){
        return "rel_".$entity."_".$field."_inv";
    }

    public static function parseInputType($inputType){
        return explode(":", str_replace(",", ":", $inputType));
    }

    public static function customActionCondPassed($cond, $entityInstance){
        if(empty($cond))
            return true;

        if(!preg_match("/([a-z_]+)(!?):(.+)/", $cond, $matches))
            return true;

        $field = $matches[1];
        $isNegative = ($matches[2] == "!");
        $value = $matches[3];
        $valueParsed = explode(",", $value);
        if(count($valueParsed) > 1){
            $value = $valueParsed;
        }

        if(is_array($value)){
            if($isNegative)
                return !in_array($entityInstance->$field, $value);
            else
                return in_array($entityInstance->$field, $value);
        }
        else{
            if($isNegative)
                return ($entityInstance->$field != $value);
            else
                return ($entityInstance->$field == $value);
        }
    }



    /**
     * checks if field value refers to an entity field
     * */
    public static function checkForEntityReference($seoField){
        global $definitions;

        if(preg_match("/([\w]+)\[([0-9]+)\]\.([\w]+)/", $seoField, $results)){
            $referedDef = $definitions->get('entities.'.$results[1]);
            $referedInstance = $referedDef["class_name_full"]::find($results[2]);
            if($referedInstance){
                $seoField = $referedInstance->getSeoParam($results[3], true);
            }
        }

        return $seoField;
    }

    /*
     * Returns array of allowed input filters, first option in that array is used as default filter for given type
     * */
    public static function allowedFiltersOnInputType($inputType = ""){
        $allowedFilters = [
            "string" => ["text", "select", "multiselect"],
            "int" => ["between", "text", "select", "multiselect"],
            "float" => ["between", "text", "select", "multiselect"],
            "date" => ["between", "text", "select", "multiselect"],
            "time" => ["between", "text", "select", "multiselect"],
            "datetime" => ["between", "text", "select", "multiselect"],
            "boolean" => ["select"],
            "html" => ["text", "select"],
            "text" => ["text", "select"],
            "color" => ["multiselect", "select", "text"],
            "select" => ["multiselect", "text", "select"],
            "select_inv" => ["multiselect", "text", "select", "between"],
            "multiselect" => ["multiselect", "text", "select", "between"],
            "multiselect_inv" => ["multiselect", "text", "select", "between"],
            "file" => ["text", "select"],
            "files" => ["text", "select", "between"],
            "calculated" => ["text", "select", "multiselect", "between"],
            "password" => ["select"],
            "icon" => ["multiselect", "select", "text"]
        ];

        return !empty($inputType) ? (isset($allowedFilters[$inputType]) ? $allowedFilters[$inputType] : []) : $allowedFilters;
    }

    public static function inputCorrespondsWithDBColumn($col, $input){
        $inputDB = self::getInputForDB($input);

        $isSame = false;
        if((strtolower($col["func"]) == strtolower($inputDB["func"])) &&
            (!$inputDB["func_uses_args"] || (implode(",", $inputDB["args"]) == implode(",", $col["args"])))){

            /* check index */
            $inputHasIndex = !empty($input["index"]);
            $colHasIndex = ($col["index"] != false);
            if(($inputHasIndex == $colHasIndex) &&
                (!$inputHasIndex || ($input["index"] == $col["index"]["type"]))){

                /* check default value */
                if(isset($inputDB["default"])){
                    $isSame = ((string)$inputDB["default"] === (string)$col["default"]);
                }
                else{
                    $isSame = true;
                }
            }
        }

        return $isSame;
    }

    static function getDbTextTypeLengths(){
        return [
            "longtext" => 4294967295,
            "mediumtext" => 16777215,
            "text" => 65535,
            "tinytext" => 255
        ];
    }

    public function getNeededTableColumnsOperations($data, $pivot = false){
        if($pivot){
            $entity = $data["entity"];
            $partner = $data["partner"];
            $field = $data["field"];

            $table = self::getEntityPivotTable($entity, $partner, $field);
            $baseColumns = array_keys(self::getEntityPivotTableBaseColumns($entity, $partner));

            /* GET INPUTS FROM ENTITY DEFINITION */
            $inputsData = [];
            $def = $this->get("entities.".$entity);
            foreach ($def["data"]["_inputs"][$field]["relation_extra_inputs"] as $inputKey => $input){
                $inputsData[$inputKey] = $input;
            }
        }
        else{
            $entity = $data;
            /* GET TABLE AND BASE COLUMN NAMES */
            $table = self::getEntityTable($entity);
            $baseColumns = array_keys(self::getEntityBaseColumns($entity));

            /* GET INPUTS FROM DEFINITION */
            $def = $this->get("entities.".$entity);
            $inputsData = $def["data"]["_inputs"];
        }

        /* LOOP THROUGH INPUTS AND UNSET THOSE WHICH DO NOT STORE IN DB */
        foreach ($inputsData as $inputKey => $input){
            /* unset base columns */
            if(in_array($inputKey, $baseColumns)){
                unset($inputsData[$inputKey]);
            }

            /* unset others */
            $inputDB = DefProcessor::getInputForDB($input);
            if(!$inputDB["func"]){
                unset($inputsData[$inputKey]);
            }
        }

        /* DEFAULT VALUES OF INPUT */
        $defaults = self::getDefaults("_input");

        /* OPERATIONS NEEDED */
        $operations = [
            "add" => [],
            "delete" => [],
            "change" => []
        ];

        /* GET EXISTING COLUMNS OF ENTITY TABLE PREVIOUSLY GENERATED */
        $tableData = DB::connection()->getDoctrineSchemaManager()->listTableDetails($table);
        $existingColumnNames = [];
        foreach ($tableData->getColumns() as $colKey => $col){
            if(!in_array($colKey, $baseColumns)) {
                $existingColumnNames[] = $colKey;

                /* GET INDEXES OF COLUMN */
                $index = false;
                foreach ($defaults["index"]["allowed"] as $indexType){
                    $indexName = $table.'_'.$colKey.'_'.$indexType;
                    if(array_key_exists($indexName, $tableData->getIndexes('name'))){
                        $index = ["type" => $indexType, "name" => $indexName];
                    }
                }

                /* GET FUNC ARGUMENTS BACK FROM COLUMN */
                $colType = $col->getType()->getName();
                $args = [];
                $func_uses_args = false;
                if($col->getType()->getName() == "decimal"){
                    $args = [$col->toArray()["precision"], $col->toArray()["scale"]];
                    $func_uses_args = true;
                }

                if($col->getType()->getName() == "text"){
                    /*$colLength = $col->toArray()["length"];
                    $lengths = array_keys(DefProcessor::getDbTextTypeLengths());
                    if(isset($lengths[$colLength])){
                        $colType = $lengths[$colLength];
                    }
                    else{
                        $colType = "text";
                    }*/

                    $colType = "longtext";
                }

                $extra = [];
                if(!$col->toArray()["notnull"])
                    $extra[] = "nullable";

                if($col->toArray()["unsigned"])
                    $extra[] = "unsigned";

                if($index)
                    $extra[] = $index["type"];

                /* SAVE COLUMN INFO */
                $col = array(
                    "func" => $colType,
                    "args" => $args,
                    "func_uses_args" => $func_uses_args,
                    "extra" => $extra,
                    "nullable" => !$col->toArray()["notnull"],
                    "unsigned" => $col->toArray()["unsigned"],
                    "index" => $index,
                    "default" => $col->getDefault(),
                );


                /* COMPARE WITH DEFINITION */

                $foundInInputs = false;
                foreach ($inputsData as $inputKey => $input){
                    if($inputKey == $colKey){
                        /* check for change or add */
                        $isSame = self::inputCorrespondsWithDBColumn($col, $input);
                        if(!$isSame){
                            $operations["change"][$inputKey] = ["input" => $input, "col" => $col];
                        }

                        $foundInInputs = true;
                        break;
                    }
                }

                if(!$foundInInputs){
                    $operations["delete"][$colKey] = ["input" => [], "col" => $col];
                }
            }
        }

        foreach ($inputsData as $inputKey => $input){
            if(!in_array($inputKey, $existingColumnNames)){
                $operations["add"][$inputKey] = ["input" => $input, "col" => []];
            }
        }

        return $operations;
    }
}


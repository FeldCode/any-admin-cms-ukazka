<?php

namespace App\Http\Controllers\Cron;

use App\Notifications\CronQueueFailed;
use App\Notifications\NewAppErrorInLog;
use App\Notifications\NewAppFeedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class CronController extends Controller
{
    public function index(){
        return "";
    }

    public function refreshQueueWorker(){
        $appRoot = dirname(__FILE__)."/../../../../";

        /* KILL AND RERUN WORKER */
        if(file_exists($appRoot.'refresh-queue-worker.sh')){
            /* make file executable */
            exec("chmod +x ".$appRoot.'refresh-queue-worker.sh');

            /* execute */
            echo shell_exec($appRoot.'refresh-queue-worker.sh');
            return;
        }

        abort(500);
    }
}

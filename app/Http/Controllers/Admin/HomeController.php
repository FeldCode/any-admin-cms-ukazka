<?php

namespace App\Http\Controllers\Admin;

class HomeController extends Controller
{
    public function homePageRedirect(){
        global $definitions;
        $appDef = $definitions->get("app");
        return redirect('admin/p/'.$appDef["data"]["home_page"]);
    }
}

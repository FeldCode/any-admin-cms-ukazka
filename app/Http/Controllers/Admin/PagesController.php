<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

/**
 * This controller handles all pages,
 * checks definition files and handles
 * user permissions to these pages through
 * middlewares
 */
class PagesController extends Controller
{

    protected $page;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);

        global $definitions;

        /* GET PAGE DEFINITION */
        $this->page = $def = $definitions ? $definitions->get("pages.".$request->_page) : null;

        /* CHECK IF CURRENT PAGE NEEDS AUTH IF IT DOES,
        THEN AUTHENTICATE AND CHECK PERMISSIONS IN GATE */
        if($def["data"]["access"]["type"] != "free"){
            $this->middleware('auth');
            $this->middleware('can:check-permissions');
        }

    }

    /**
     * Init function
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function init()
    {
        return view('admin.page')->with('page', $this->page); //returns default page view and passes information about the page
    }
}

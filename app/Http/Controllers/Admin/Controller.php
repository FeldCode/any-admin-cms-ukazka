<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);

        $this->middleware(['auth']);

        $userLogged = !empty(auth()->user());
        $url = explode("/", $request->path());
        if(!isset($url[0])) $url[0] = '';
        if(!isset($url[1])) $url[1] = '';
        if(!isset($url[2])) $url[2] = '';

        try {
            $this->authorize('admin-only');
            //$this->middleware('can:admin-only', ['except' => $except]);
        }
        catch(\Illuminate\Auth\Access\AuthorizationException $exception){
            if($userLogged && $url[1] != "logout") {
                auth()->guard()->logout();
                //$request->session()->invalidate();
                $request->session()->flash('error', ["title" => __("Error!"), "html" => __("You don't have a permission to access this page."), "timer" => null, "showConfirmButton" => true]);
                return redirect('admin/login')->send();
            }
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Components\TableComponent;
use App\DefProcessor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\MessageBag;

class HelpersController extends Controller
{
    public function customInputForm(Request $request){
        global $definitions;

        $ref = $request->has("_ref") ? $request->input("_ref") : null;
        $inputsContainer = $definitions->getByRef($ref);

        if(!isset($inputsContainer["inputs"])){
            $inputsContainer["inputs"] = $inputsContainer;
        }

        if(!$request->has("_ref"))
            abort(403);

        $formId = $request->has("form_id") ? $request->input("form_id") : "";

        /* VALIDATION */
        $form_sent = $request->has("form_sent");
        $edit = $request->has("edit");
        $errors = new MessageBag();
        if(isset($_GET["validate"])){
            $form_sent = true;
            $validationArray = DefProcessor::getValidationArray($inputsContainer["inputs"], $request, true, false);
            $validator = Validator::make($request->all(), $validationArray["rules"], [], $validationArray["input_names"]);
            $validatorFailed = $validator->fails();
            if($validatorFailed){
                $errors = $validator->errors();
            }
        }

        $html = View::make('admin.helpers.custom_input_form', [
            "inputsContainer" => $inputsContainer,
            "form_id" => $formId,
            "request" => $request,
            "form_sent" => $form_sent,
            "_is_custom_input_form" => true //used for correct inputs rendering
        ])->withErrors($errors);

        if(isset($_GET["validate"])){
            $data = [
                "state" => !$validatorFailed,
                "html" => (string)$html
            ];
            return response()->json($data);
        }
        else{
            return $html;
        }
    }

    public function getEntityItemsJson(Request $request){
        if($request->has('entity')){
            global $definitions;
            $entity = $request->input('entity');
            $entityDef = $definitions->get('entities.'.$entity);

            $selectFields = ['id', DB::raw($entityDef["data"]["title_field"].' as title')];

            /* custom icon */
            if(!empty($entityDef["data"]["custom_icon"])){
                $selectFields[] = DB::raw($entityDef["data"]["custom_icon"].' as icon');
            }
            else{
                $selectFields[] = DB::raw("CONCAT('".$entityDef["data"]["icon"]."') as icon");
            }

            $data = $entityDef["class_name_full"]::select($selectFields);

            if($request->has('q')){
                $data = $data->where($entityDef["data"]["title_field"], 'like', '%'.$request->input('q').'%');
            }

            if($request->has('ignore_ids')){
                $data = $data->whereNotIn('id', $request->input('ignore_ids'));
            }

            return json_encode($data->get()->toArray());
        }

        return '[]';
    }

    public function getTableComponentCsv(Request $request){
        /* MERGE REQUEST DATA */
        if($request->has("_request_data")) {
            $request->merge(json_decode($request->input("_request_data"), true));
        }

        if(!$request->has("_ref")){
            return "";
        }

        global $definitions;
        $action = $definitions->getByRef($request->input("_ref"));
        $data = (new TableComponent())->handle($request, $action, false);

        $entityDef = $data["entityDef"];
        $data = $data["data"];

        $dataForCsv = [];
        /* get col titles */
        foreach ($entityDef["data"]["_inputs"] as $input) {
            $dataForCsv[0][] = __($input["title"]);
        }

        /* get data */
        foreach ($data as $entityInstance){
            $item = [];

            foreach ($entityDef["data"]["_inputs"] as $inputKey => $input) {
                $inputType = DefProcessor::parseInputType($input["type"]);

                switch ($inputType[0]){
                    case 'select_inv':
                    case 'multiselect_inv':
                    case 'select':
                    case 'multiselect':
                        if(in_array($inputType[0], ["select_inv", "multiselect_inv"])){
                            $partnerItems = $entityInstance->getInverseRelationship($inputType[1], $inputType[2], true)->get();
                        }
                        else{
                            $partnerItems = $entityInstance->getRelationship($inputKey, true)->get();
                        }

                        $partnerItemsText = [];
                        foreach ($partnerItems as $partnerItem){
                            $partnerDef = $partnerItem->getDef();
                            $partnerTitleField = $partnerDef["data"]["title_field"];
                            $partnerItemsText[] = $partnerItem->$partnerTitleField;
                        }

                        if(count($partnerItemsText) > 0){
                            $item[$inputKey] = implode(", ", $partnerItemsText);
                        }
                        else{
                            $item[$inputKey] = '--'.__('none').' --';
                        }
                        break;
                    case 'file':
                    case 'files':
                        $item[$inputKey] = __(":cnt records", ["cnt" => $entityInstance->getRelationship($inputKey, true)->get()->count()]);
                        break;
                    case 'password':
                        $item[$inputKey] = !empty($entityInstance->$inputKey) ? '********' : '-- '.__('No password').' --';
                        break;
                    case 'boolean':
                        $item[$inputKey] = $entityInstance->$inputKey == 1 ? __('Yes') : __('No');
                        break;
                    default:
                        $item[$inputKey] = strip_tags($entityInstance->$inputKey);
                    break;
                }
            }

            $dataForCsv[] = $item;
        }

        /* stream CSV file */
        if(count($dataForCsv) > 0){

            /* set headers */
            $headers = [
                'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
                ,   'Content-Encoding'        => 'UTF-8'
                ,   'Content-type'        => 'text/csv; charset=UTF-8'
                ,   'Content-Disposition' => 'attachment; filename='.str_replace([":", " "], ["-", "_"], Carbon::now()).'-'.$entityDef["name"].'-export.csv'
                ,   'Expires'             => '0'
                ,   'Pragma'              => 'public'
            ];

            /* stream function */
            $callback = function() use ($dataForCsv)
            {
                $FH = fopen('php://output', 'w');
                fputs( $FH, "\xEF\xBB\xBF" ); //UTF-8 BOM
                foreach ($dataForCsv as $row) {
                    fputcsv($FH, $row, ";");
                }
                fclose($FH);
            };

            return Response::stream($callback, 200, $headers);
        }
        else{
            return "";
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Component;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

/**
 * This controller handles all components,
 * each function handles different component types
 * or request made to those components
 */
class ComponentsController extends Controller
{
    protected $page = null;
    protected $action = null;
    protected $component = null;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);

        global $definitions;

        /* user not logged */
        if(!$request->user()){
            Redirect::to('admin/login?without-layout')->send();
        }

        /* GET PAGE THAT MADE THE REQUEST */
        if($request->has('_page')){
            $page = $request->input('_page');
        }

        if(!$request->has('_ref')){
            abort(401);
        }

        /* GET PAGE DEFINITION */
        if(isset($page) && $definitions->check("pages.".$page)){
            $this->page = $definitions->get("pages.".$page);
        }

        if(is_null($this->page)){
            abort(401);
        }

        /* CHECK IF CURRENT PAGE NEEDS AUTH IF IT DOES,
        THEN AUTHENTICATE AND CHECK PERMISSIONS IN GATE */
        if($this->page["data"]["access"]["type"] != "free"){
            $this->middleware('auth');
            $this->middleware('can:check-permissions');
        }

        /* GET ACTION BY REFERENCE */
        $this->action = $definitions->getByRef($request->input("_ref"));


        /* CHECK IF USER IS AUTHORIZED FOR SOURCE ACTION BY ITS REFERENCE */
            /* autoload entity ID */
            $entityId = null;
            if(isset($btn["action"]["data"]["id"]))
                $entityId = $this->action["data"]["id"];

            elseif($request->has(["id"]))
                $entityId = $request->input("id");


        if(!$definitions->UserHasRights($request->user(), $request->input("_ref"), $entityId))
            abort(401);

        /* GET component */
        $url = explode("/", $request->path());
        if(!isset($url[2])){
            abort(404);
        }

        if(!Component::componentExists($url[2])){
            abort(404);
        }

        $this->component = $url[2];
        $request->merge(['_page' => $this->page["name"], '_component' => $url[2]]);
    }

    /**
     * Init function
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function init(Request $request)
    {
        $componentClass = Component::getComponentClassName($this->component);
        $component = new $componentClass();
        return $component->handle($request, $this->action); //zde komponenta získá data a předá je příslušné funkci, např grafu, tabulce nebo formuláři
    }
}

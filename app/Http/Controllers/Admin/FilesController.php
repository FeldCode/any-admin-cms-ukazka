<?php

namespace App\Http\Controllers\Admin;

use App\File;
use Illuminate\Http\Request;

class FilesController extends Controller
{
    protected $page = null;

    public function __construct(Request $request){
        parent::__construct($request);

        global $definitions;

        /* GET PAGE THAT MADE THE REQUEST */
        if($request->has('_page')){
            $page = $request->input('_page');
        }

        /* GET PAGE DEFINITION */
        if(isset($page) && $definitions->check("pages.".$page)){
            $this->page = $definitions->get("pages.".$page);
        }

        if(is_null($this->page)){
            abort(401);
        }

        /* CHECK IF CURRENT PAGE NEEDS AUTH IF IT DOES,
        THEN AUTHENTICATE AND CHECK PERMISSIONS IN GATE */
        if($this->page["data"]["access"]["type"] != "free"){
            $this->middleware('auth');
            $this->middleware('can:check-permissions');
        }

        $request->merge(['_page' => $this->page["name"]]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $ref, $dropzone = false)
    {
        //
    }

    public function dzUpload(Request $request)
    {
        /* CHECK CONDITIONS */
        if(!$request->has('_ref')){
            File::dzError("Unauthorized");
            return false;
        }

        $result = File::store($request, "file", $request->input('_ref'), true);

        if($result["state"]){
            return response()->json($result["data"]);
        }
        else{
            $msg = "";
            foreach ($result["errors"]->getMessages()["file"] as $error){
                $msg .= $error."\r\n";
            }
            File::dzError($msg);
            return false;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file = File::find($id);

        if($file)
            $file->delete();

        return response()->json([
            'success' => __('File removed.')
        ]);
    }
}

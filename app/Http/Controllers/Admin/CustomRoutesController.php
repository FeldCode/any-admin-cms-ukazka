<?php

namespace App\Http\Controllers\Admin;

use PDF;
use App\Entities\InvoiceEntity;
use Illuminate\Http\Request;

class CustomRoutesController extends Controller
{
    public function downloadInvoice(Request $request){
        global $definitions;

        $entity = $request->input("entity");
        $id = $request->input("id");
        $entityDef = $definitions->get('entities.'.$entity);
        $invoice = InvoiceEntity::find($id);

        if(!$invoice){
            abort(404);
        }

        $pdf = PDF::loadView('admin.custom.invoice_pdf', ["entityDef" => $entityDef, "data" => $invoice]);
        return $pdf->download($invoice->no.'.pdf');
    }
}

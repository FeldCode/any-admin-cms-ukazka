<?php

namespace App\Http\Controllers\Admin;

use App\Func;
use Illuminate\Http\Request;

/**
 * This controller handles all components,
 * each function handles different component types
 * or request made to those components
 */
class FuncsController extends Controller
{
    protected $page = null;
    protected $action = null;
    protected $function = null;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        global $definitions;

        /* GET PAGE THAT MADE THE REQUEST */
        if($request->has('_page')){
            $page = $request->input('_page');
        }

        if(!$request->has('_ref')){
            abort(401);
        }

        /* GET PAGE DEFINITION */
        if(isset($page) && $definitions->check("pages.".$page)){
            $this->page = $definitions->get("pages.".$page);
        }

        if(is_null($this->page)){
            abort(401);
        }

        /* CHECK IF CURRENT PAGE NEEDS AUTH IF IT DOES,
        THEN AUTHENTICATE AND CHECK PERMISSIONS IN GATE */
        if($this->page["data"]["access"]["type"] != "free"){
            $this->middleware('auth');
            $this->middleware('can:check-permissions');
        }

        /* CHECK IF USER IS AUTHORIZED FOR SOURCE ACTION BY ITS REFERENCE */
        if(!$definitions->UserHasRights($request->user(), $request->input("_ref")))
            abort(401);

        /* GET ACTION BY REFERENCE */
        $this->action = $definitions->getByRef($request->input("_ref"));

        /* GET component */
        $url = explode("/", $request->path());
        if(!isset($url[2])){
            abort(404);
        }

        if(!Func::functionExists($url[2])){
            abort(404);
        }

        $this->function = $url[2];
        $request->merge(['_page' => $this->page["name"], '_func' => $url[2]]);
    }

    /**
     * Init function
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function init(Request $request)
    {
        $functionClass = Func::getFunctionClassName($this->function);
        $function = new $functionClass();
        return $function->handle($request, $this->action);
    }
}

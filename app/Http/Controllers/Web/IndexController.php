<?php

namespace App\Http\Controllers\Web;

use App\Entities\TestEntity;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Url;

/**
 * This controller handles all components,
 * each function handles different component types
 * or request made to those components
 */
class IndexController extends Controller
{
    public function index(Request $request){
        return redirect('admin');
        //return view('web.pages.index');
    }
}

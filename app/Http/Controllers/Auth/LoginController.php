<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
        if ($this->attemptLogin($request)) {

                /* CHECK IF USER IS NOT DEADLOCKED */
                    $user = $this->guard()->getLastAttempted();

                    // Make sure the user is active
                    if ($user && !$user->isDeadlocked()) {
                        // Send the normal successful login response
                        return $this->sendLoginResponse($request);
                    } else {
                        // Increment the failed login attempts
                        $this->incrementLoginAttempts($request);

                        // Logout current user
                        $this->guard()->logout();
                        $request->session()->invalidate();

                        //redirect back with an error
                        return redirect()
                            ->back()
                            ->withInput($request->only($this->username(), 'remember'))
                            ->withErrors(['login' => __('Your account has been deadlocked')]);
                    }
                /* CHECK IF USER IS NOT DEADLOCKED */

            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $routeUri = [];
        if($request->route()) {
            $routeUri = $request->route()->uri;
            $routeUri = explode("/", $routeUri);
        }

        if(isset($routeUri[0]) && $routeUri[0] == "admin"){
            $this->redirectTo = 'admin/';
        }

        else{
            $this->redirectTo = '/';
        }

        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect($this->redirectTo);
    }

    public function username()
    {
        return 'login';
    }
}

API layer including api_log entity could be installed by following command:

php artisan install-aa-package:api

FILES:
app/Http/Controllers/Api/*
app/Api.php
app/Entities/ApiLogEntity.php
resources/def/entities/api_log.json
routes/api.php

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic;

class MediaController extends Controller
{
    public function displayImage($image, $width = 500, $height = null)
    {
        $file = storage_path('app/public/').$image;

        /* if file doesnt exist, try admin/img folder */
        if(!file_exists($file)){
            $file = base_path().'/public/admin/img/'.$image;
        }

        /* if still no file, use no-image */
        if(!file_exists($file)){
            $file = base_path().'/public/admin/img/no-image.png';
        }

        $mode = !is_null($height) ? 'fit' : 'resize';

        $image_resize = ImageManagerStatic::make($file);
        $image_resize->$mode($width, $height, function($constraint)use($mode){
            $constraint->upsize();
            if($mode != "fit")
                $constraint->aspectRatio();
        });

        return $image_resize->response('jpg', 100);
    }

}

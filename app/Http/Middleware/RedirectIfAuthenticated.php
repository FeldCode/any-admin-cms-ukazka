<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $routeUri = $request->route()->uri;
            $routeUri = explode("/", $routeUri);

            if(isset($routeUri[0]) && $routeUri[0] == "admin"){
                $redirectTo = 'admin/';
            }

            else{
                $redirectTo = '/';
            }

            return redirect($redirectTo);
        }

        return $next($request);
    }
}

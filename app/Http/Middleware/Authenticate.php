<?php

namespace App\Http\Middleware;

use App\Entities\ApiLogEntity;
use App\Helper;
use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\HeaderBag;

class Authenticate extends Middleware
{

    public function handle($request, Closure $next, ...$guards){

        /* IF AUTH FOR API, FORCE USE JSON RESPONSE */
        if(in_array("api", $guards)) {
            $request->server->set('HTTP_ACCEPT', 'application/json');
            $request->headers = new HeaderBag($request->server->getHeaders());
        }

        $this->authenticate($request, $guards);

        /* AUTHORIZE, ACTIVE USERS ONLY */
        /* ACTIVE USERS ONLY - logout deadlocked users */
        $userLogged = !empty($request->user());

        if($userLogged){
            $url = explode("/", $request->path());
            if(!isset($url[0])) $url[0] = '';
            if(!isset($url[1])) $url[1] = '';
            if(!isset($url[2])) $url[2] = '';

            try {
                Gate::authorize('active-only');
            }
            catch(\Illuminate\Auth\Access\AuthorizationException $exception){
                if($userLogged && $url[1] != "logout") {
                    /* logout current user */
                    auth()->guard()->logout();

                    /* set flash message if not api */
                    if($url[0] != 'api'){
                        $request->session()->flash('error', ["title" => __("Error!"), "html" => __("Your account has been deadlocked"), "timer" => null, "showConfirmButton" => true]);
                    }

                    /* user is not authenticated */
                    $this->unauthenticated($request, $guards);
                }
            }
        }

        return $next($request);
    }

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        /* KEEP FLASH MESSAGES */
        Helper::sessionKeepFlashMessages();

        $routeUri = $request->route()->uri;
        $routeUri = explode("/", $routeUri);

        if(isset($routeUri[0]) && $routeUri[0] == "api"){
            ApiLogEntity::logResponse(array(
                "message" => __("Unauthenticated.")
            ), 401);
        }

        if(isset($routeUri[0]) && $routeUri[0] == "admin"){
            $redirectTo = 'admin/login';
        }

        else{
            $redirectTo = 'login';
        }

        if (!$request->expectsJson()) {
            return url($redirectTo);
        }
    }
}

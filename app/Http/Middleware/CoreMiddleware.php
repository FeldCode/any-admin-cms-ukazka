<?php

namespace App\Http\Middleware;

use App\DefProcessor;
use App\Generator;
use App\User;
use Closure;
use Illuminate\Support\Facades\Schema;

class CoreMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /* SET GLOBAL DATE FORMAT */
        \Carbon\Carbon::setToStringFormat(env('APP_DATE_FORMAT', 'Y-m-d').' '.env('APP_TIME_FORMAT', 'H:i:s'));

        /* Initiate DefProcessor - loads and checks all definition files */
        global $definitions;
        $generator = new Generator(); //get generator
        $generator->createEntityModels(); //creates entity model files if they don't exist
        $definitions = new DefProcessor(); // load definitions

        /* SAVE CURRENT PAGE NAME IF REQUEST MADE TO PAGE ROUTE */
        $url = explode("/", $request->path());
        if(!isset($url[0])) $url[0] = '';
        if(!isset($url[1])) $url[1] = '';
        if(!isset($url[2])) $url[2] = '';

        if($url[0] == "admin" && $url[1] == "p"){
            $request->merge(['_page' => $url[2]]);
        }

        /* checks if user triggered migrations and runs them */
        if($generator->userTriggerRunMigrations($request)){
            return redirect()->back();
        }



        /* register all needed relation functions */
        $generator->registerRelations();

        $usersTableExists = Schema::hasTable('users');
        if($usersTableExists) {
            //do not allow user to trigger migrations if not admin authenticated
            $userLogged = !empty(auth()->user());
            $userGroup = $userLogged ? auth()->user()->getRelationship('user_group') : null;
            $userAdminAccess = (
                $userLogged &&
                $userGroup &&
                $userGroup->admin_access
            );
        }
        else{
            $userLogged = $userAdminAccess = true; //if users table doesn't exists, allow migration
        }

        /* creates new migrations if needed and prompts user to run them */
        $waitingMigrations = $generator->checkAndCreateMigrations();

        if(count($waitingMigrations) > 0){
            if($url[0] != "admin" && $usersTableExists){
                abort(500);
            }

            if($userAdminAccess){
                $isDestructive = false;
                foreach ($waitingMigrations as $mig){
                    if(!empty($mig["info"]["operations"]) && count($mig["info"]["operations"]["delete"]) > 0){
                        $isDestructive = true;
                        break;
                    }
                }

                return response(view('admin.migrate')->with([
                    "migrationNeeded" => true,
                    "waitingMigrations" => $waitingMigrations,
                    "isDestructive" => $isDestructive
                ]));
            }
        }

        return $next($request);
    }
}

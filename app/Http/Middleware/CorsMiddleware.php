<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;

class CorsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /* ORIGIN BY ENVIRONMENT */
        /*$originsByEnv = [
            "local" => "http://localhost:8081",
            "preview" => "https://preview-app.leepa.cz",
            "production" => "https://app.leepa.cz",
        ];
        $origin = isset($originsByEnv[config('app')["env"]]) ? $originsByEnv[config('app')["env"]] :"";*/

        /* LISTING ALLOWED ORIGIN */
        $http_origin = !empty($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : '';
        $allowedOrigins = [
            /* local */
            "http://localhost:8081",
            "http://192.168.1.3:8081",
            "http://192.168.1.7:8081",
            "http://192.168.1.101:8081",
            "https://leepa-app.loc",

            /* preview */
            "https://preview-app.leepa.cz",

            /* production */
            "https://app.leepa.cz",
        ];

        $origin = "";
        if (in_array($http_origin, $allowedOrigins)){
            $origin = $http_origin;
        }

        $headers = array(
            'Access-Control-Allow-Origin' => $origin,
            'Access-Control-Allow-Headers' => 'Credentials, Authorization, Origin, X-Requested-With, Content-Type, Accept, Content-Language, Accept-Language, x-socket-id, x-api-key',
            'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS',
            'Access-Control-Allow-Credentials' => 'true'
        );

        if (Request::getMethod() == "OPTIONS") {
            /* CORS: THE BROWSER TESTS RESPONSE BEFORE MAKING ACTUAL REQUEST - EVEN IF THERE IS ERROR (4xx), it needs to return 200 (ok) + CORS headers */
            return Response::make('', 200, $headers);
        }
        else {

            $closure = $next($request);

            /* DO NOT APPEND HEADERS ON STREAMED RESPONSE */
            if($closure instanceof StreamedResponse || $closure instanceof BinaryFileResponse){
                return $closure;
            }
            else{
                return $closure->withHeaders($headers);
            }
        }
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class CronMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = sha1(env('APP_KEY', 'default-cron-key'));

        if(!$request->has('token') || $token != $request->input('token')){
            abort(401);
        }

        return $next($request);
    }
}

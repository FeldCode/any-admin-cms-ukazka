<?php

namespace App\Http\Middleware;

use App\ContentInfo;
use App\DefProcessor;
use Artesaos\SEOTools\Facades\JsonLd;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\SEOMeta;
use Closure;

class SeoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /* seo params null by default */
        $seoTitle = null;
        $seoDesc = null;
        $seoKeywords = null;
        $seoImg = null;

        /* Predicting seo data based on ident (url end) found in content_info */
        $url = $_SERVER["REQUEST_URI"];

        /* get last url part */
        $url = explode("/", $url);
        $url = end($url);

        /* remove query parameters and hash */
        $url = explode('?', $url)[0];
        $url = explode('#', $url)[0];

        /* SEARCH FOR PARTICULAR ENTITY */
        $contentInfo = ContentInfo::where('ident', $url)->get()->first();

        if($contentInfo){
            $item = $contentInfo->entity;

            /* entity instance found */
            if($item){
                $seoTitle = $item->getSeoParam('title');
                $seoDesc = $item->getSeoParam('description');
                $seoKeywords = $item->getSeoParam('keywords');
                $seoImg = $item->getSeoParam('img');

                /* if seo title is null, get title field */
                if(!$seoTitle){
                    $seoTitle = $item->getEntityTitle();
                }
            }
        }

        /* SEARCH APP DEFINITION FILE FOR DEFAULTS */
        if(!$seoTitle || !$seoDesc || !$seoKeywords || !$seoImg){
            global $definitions;
            $appDef = $definitions->get("app");

            if(!$seoTitle && !empty($appDef["data"]["seo"]["default_title"])){
                $seoTitle = $appDef["data"]["seo"]["default_title"];
                $seoTitle = DefProcessor::checkForEntityReference($seoTitle);
            }
            if(!$seoDesc && !empty($appDef["data"]["seo"]["default_description"])){
                $seoDesc = $appDef["data"]["seo"]["default_description"];
                $seoDesc = DefProcessor::checkForEntityReference($seoDesc);
            }
            if(!$seoKeywords && !empty($appDef["data"]["seo"]["default_keywords"])){
                $seoKeywords = $appDef["data"]["seo"]["default_keywords"];
                $seoKeywords = DefProcessor::checkForEntityReference($seoKeywords);
            }
            if(!$seoImg && !empty($appDef["data"]["seo"]["default_img"])){
                $seoImg = asset($appDef["data"]["seo"]["default_img"]);
                $seoImg = DefProcessor::checkForEntityReference($seoImg);
            }
        }

        if($seoTitle){
            SEOMeta::setTitle($seoTitle);
            OpenGraph::setTitle($seoTitle);
            JsonLd::setTitle($seoTitle);
        }
        if($seoDesc){
            SEOMeta::setDescription($seoDesc);
            OpenGraph::setDescription($seoDesc);
            JsonLd::setDescription($seoDesc);
        }
        if($seoKeywords){
            SEOMeta::setKeywords($seoKeywords);
        }
        if($seoImg){
            OpenGraph::addImage($seoImg);
            JsonLd::addImage($seoImg);
        }

        return $next($request);
    }
}

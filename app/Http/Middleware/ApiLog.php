<?php

namespace App\Http\Middleware;

use Closure;

class ApiLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /* Log requests */
        \App\Entities\ApiLogEntity::logRequest($request);

        return $next($request);
    }
}

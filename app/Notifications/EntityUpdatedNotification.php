<?php

namespace App\Notifications;

use App\DefProcessor;
use App\Generator;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EntityUpdatedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $entityDef;
    public $item;
    public $userId;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($entityDef, $item, $userId)
    {
        $this->entityDef = $entityDef;
        $this->item = $item;
        $this->userId = $userId;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        global $definitions;
        if(!$definitions) {
            $definitions = new DefProcessor();
            $generator = new Generator();
            $generator->registerRelations();
        }

        $user = User::find($this->userId);

        /* get item title */
        $itemTitleKey = $this->entityDef["data"]["title_field"];
        $itemTitle = $this->item->getOriginal($itemTitleKey);

        if($user){
            $msg = __('User <b>:user_name (:user_login)</b> has edited a record of type :entity_name: <b>:entity_instance_title</b>.', ["user_name" => $user->name, "user_login" => $user->login, "entity_name" => __($this->entityDef["data"]["title"]), "entity_instance_title" => __($itemTitle)]);
        }
        else{
            $msg = __('A record of type :entity_name: <b>:entity_instance_title</b> has been updated in the :app_name app.', ["app_name" => env('APP_NAME', 'AnyAdmin'), "entity_name" => __($this->entityDef["data"]["title"]), "entity_instance_title" => __($itemTitle)]);
        }

        /* get details table */
        $detailsTable = '<table cellspacing="10">
                        <tr>
                            <th>'.__('Field name').'</th>
                            <th>'.__('Original value').'</th>
                            <th>'.__('New value').'</th>
                        </tr>';

        foreach ($this->entityDef["data"]["_inputs"] as $inputKey => $input){
            $inputType = DefProcessor::parseInputType($input["type"]);

            $isDirty = false;
            switch ($inputType[0]) {
                case 'select_inv':
                case 'multiselect_inv':
                case 'multiselect':
                case 'files':
                case 'calculated':
                    //multi relationships and calculated cannot be tracked, always not dirty
                    $isDirty = false;
                break;
                default:
                    $isDirty = $this->item->isDirty($inputKey) && $this->item->getOriginal($inputKey) != $this->item->$inputKey;
                break;
            }


            if($isDirty){
                $detailsTable .= '<tr><td valign="top" align="left">'.__($input["title"]).'</td><td>';

                    $originalValue = $this->item->getOriginal($inputKey);
                    switch ($inputType[0]){
                        case 'select':
                            global $definitions;
                            $partnerDef = $definitions->get('entities.'.$inputType[1]);

                            $partnerItems = [];
                            if(!is_null($originalValue))
                                $partnerItems = $partnerDef["class_name_full"]::find($originalValue);

                            $partnerItemsText = [];
                            foreach ($partnerItems as $partnerItem){
                                $partnerTitleField = $partnerDef["data"]["title_field"];
                                $partnerItemsText[] = $partnerItem->$partnerTitleField;
                            }

                            if(count($partnerItemsText) > 0){
                                $detailsTable .= implode(", ", $partnerItemsText);
                            }
                            else{
                                $detailsTable .= '<i>--'.__('none').' --</i>';
                            }
                            break;
                        case 'file':
                            $detailsTable .= __(":cnt records", ["cnt" => is_null($originalValue) ? 0 : 1]);
                        break;
                        case 'password':
                            $detailsTable .= !empty($originalValue) ? '********' : '<i>-- '.__('No password').' --</i>';
                        break;
                        case 'boolean':
                            $detailsTable .= $originalValue == 1 ? __('Yes') : __('No');
                        break;
                        default:
                            $detailsTable .= $originalValue;
                        break;
                    }

                $detailsTable .= '</td><td>';

                    switch ($inputType[0]){
                        case 'select_inv':
                        case 'multiselect_inv':
                        case 'select':
                        case 'multiselect':
                            if(in_array($inputType[0], ["select_inv", "multiselect_inv"])){
                                $partnerItems = $this->item->getInverseRelationship($inputType[1], $inputType[2], true)->get();
                            }
                            else{
                                $partnerItems = $this->item->getRelationship($inputKey, true)->get();
                            }

                            $partnerItemsText = [];
                            foreach ($partnerItems as $partnerItem){
                                $partnerDef = $partnerItem->getDef();
                                $partnerTitleField = $partnerDef["data"]["title_field"];
                                $partnerItemsText[] = $partnerItem->$partnerTitleField;
                            }

                            if(count($partnerItemsText) > 0){
                                $detailsTable .= implode(", ", $partnerItemsText);
                            }
                            else{
                                $detailsTable .= '<i>--'.__('none').' --</i>';
                            }
                            break;
                        case 'file':
                        case 'files':
                            $detailsTable .= __(":cnt records", ["cnt" => $this->item->getRelationship($inputKey, true)->get()->count()]);
                        break;
                        case 'password':
                            $detailsTable .= !empty($this->item->$inputKey) ? '********' : '<i>-- '.__('No password').' --</i>';
                        break;
                        case 'boolean':
                            $detailsTable .= $this->item->$inputKey == 1 ? __('Yes') : __('No');
                        break;
                        default:
                            $detailsTable .= $this->item->$inputKey;
                        break;
                    }

                $detailsTable .= '</td></tr>';
            }
        }

        $detailsTable .= '</table>';

        return (new MailMessage)
            ->subject(__(':entity_name edited in app :app_name', ["entity_name" => __($this->entityDef["data"]["title"]), "app_name" => env('APP_NAME', 'AnyAdmin')]))
            ->line($msg)
            ->line('<br><h1><u>'.__('Detected changes').'</u></h1>')
            ->line($detailsTable)
            ->action('Visit administration', url('/admin'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

<?php

namespace App\Notifications;

use App\DefProcessor;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserDeadlockedNotification extends Notification
{
    use Queueable;

    public $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($this->user->isDeadlocked()){
            return (new MailMessage)
                ->subject(__('Your account has been deadlocked'))
                ->line(__('We are sorry to inform you, that your account <b> :user_login </b> has been deadlocked under a suspicion of violation or rules of using the application.', ["user_login" => $this->user->login]))
                ->line(__('If you disagree with this decision, please let us know.'))
                ->line('<br>');
        }
        else{
            return (new MailMessage)
                ->subject(__('Your account has been unblocked'))
                ->line(__('We have unblocked your account <b> :user_login </b>. You can continue using the application as before.', ["user_login" => $this->user->login]))
                ->line('<br>');
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

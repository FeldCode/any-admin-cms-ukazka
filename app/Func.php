<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class Func
{
    /**
     * Converts response data to JSON.
     * Do not remove this function
     *
     */
    public static function response($data, $noSwal = false){
        if(!isset($data["state"])) $data["state"] = "error";
        if(!isset($data["msg"])){
            if($data["state"] == "success")
                $data["msg"] = __("Done!");
            elseif($data["state"] == "error")
                $data["msg"] = ["title" => __("Error!"), "html" => __("Oops, unexpected error, please try again later.")];
            else
                $data["msg"] = "";
        }

        if(!isset($data["attach_script"])) $data["attach_script"] = "";

        /*if(!isset($data["actions"])) $data["actions"] = [];
        if(!empty($data["actions"])){
            foreach ($data["actions"] as &$action) {
                $action = DefProcessor::getByRef($action);
            }
        }*/

        /* SWAL DATA */
        if(!$noSwal)
            $data["swal_data"] = Helper::getSwalDataFromFlash($data["state"], $data["msg"]);

        return json_encode($data);
    }

    public static function functionExists($function){
        $function = self::getFunctionClassName($function);
        return class_exists($function);
    }

    public static function getFunctionClassName($function, $incNamespace = true){
        return ($incNamespace ? "\App\Functions\\" : "").ucfirst(Str::camel($function))."Function";
    }

    public function getName(){
        return strtolower(Str::snake(str_replace(["App\Functions\\", "Function"], "", get_called_class())));
    }
}

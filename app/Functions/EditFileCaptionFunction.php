<?php

namespace App\Functions;

use App\Component;
use App\File;
use App\Func;
use Illuminate\Http\Request;

class EditFileCaptionFunction extends Func
{
    public function handle(Request $request, $action){
        global $definitions;
        $id = $request->has("id") ? $request->input("id") : null;
        $description = $request->has("description") && !is_null($request->input("description")) ? $request->input("description") : "";

        $file = File::find($id);
        if($file){
            $file->description = strip_tags($description);
            if($file->save()){
                $attachScript = 'window.editFileCaptionFront("'.$id.'", "'.str_replace(['"', "\r\n", "\n\r", "\r", "\n"], ['\\"', "\\r\\n", "\\n\\r", "\\r", "\\n"], $file->description).'");';
                return $this->response(["state" => "success", "msg" => __("Success!"), "attach_script" => $attachScript]);
            }
            else{
                return $this->response(["state" => "error", "msg" => ["title" => __("Error!"), "html" => __("Oops, unexpected error, please try again later.")]]);
            }
        }

    }
}


<?php

namespace App\Functions;

use App\Component;
use App\Func;
use App\Notifications\UserDeletedNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class RemoveEntityFunction extends Func
{
    public function handle(Request $request, $action){
        global $definitions;
        $entityName = $action["data"]["entity"];
        $entityDef = $definitions->get("entities.".$entityName);
        $id = $request->input("id");

        $source = ($request->has('_source') ? $request->input('_source') : 'table');

        $entityInstance = $entityDef["class_name_full"]::find($id);
        if($entityInstance->delete()){

            /* NOTIFY THE USER ABOUT REMOVING HIS ACCOUNT */
            if($entityDef["name"] == "user"){
                Notification::route('mail', $entityInstance->email)->notify(new UserDeletedNotification($entityInstance));
            }

            $attachScript = '$("#table-row-'.$entityName.'-'.$id.'").fadeOut(300).remove();';

            if($source == 'detail'){
                $attachScript .= 'window.Front.goBackOrHome();';
            }
            elseif($source == 'detail-popup'){
                $attachScript .= 'Front.closeModal();';
            }

            return $this->response(["state" => "success", "msg" => __("Deleted!"), "attach_script" => $attachScript]);
        }
        else{
            if(session()->has("error")){
                return $this->response(["state" => "error", "msg" => ["title" => __("Error!"), "html" => session()->get("error")]]);
            }
            else{
                return $this->response(["state" => "error", "msg" => ["title" => __("Error!"), "html" => __("Item couldn't be removed.")]]);
            }
        }

    }
}


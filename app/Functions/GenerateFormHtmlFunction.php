<?php

namespace App\Functions;

use App\Component;
use App\DefProcessor;
use App\File;
use App\Func;
use Illuminate\Http\Request;

class GenerateFormHtmlFunction extends Func
{
    public function handle(Request $request, $action){
        global $definitions;
        $entity = $request->has("entity") ? $request->input("entity") : "";
        $entityDef = $definitions->get('entities.'.$entity);
        $baseColumns = DefProcessor::getEntityBaseColumns($entity);

        $html = '<form method="post" action="" enctype="multipart/form-data" class="form form-horizontal">
                    @csrf
                    <input type="hidden" name="_form_entity" value="'.$entity.'">
                    <input type="hidden" name="_form_auto_process" value="1">
                    <input type="hidden" name="_form_errors_in_popup" value="0">
                    <input type="hidden" name="_form_sent" value="1">';

        foreach ($entityDef["data"]["_inputs"] as $inputKey => $input){

            $inputId = str_replace([".", ":"], "-", $input["ref"]);
            $inputType = DefProcessor::parseInputType($input["type"]);

            /* skip base columns */
            if(in_array($inputKey, array_keys($baseColumns))){
                continue;
            }

            /* skip calculated columns */
            if($inputType[0] == "calculated"){
                continue;
            }

            $html .= '<?php
                            $wrapperClasses = "";
                            if(isset($errors) && $errors->any()){
                                $wrapperClasses = $errors->has("'.$inputKey.'") ? " has-feedback has-error" : "";
                            }
                        ?>

                    <div class="form-group row '.$input["wrapper_classes"].' <?php echo $wrapperClasses; ?>">
                            <label class="control-label col-sm-2 text-right" for="'.$inputId.'">'.__($input["title"]).':</label>
                            <div class="col-sm-10">
                                <div class="input-group" '.(!empty($input["tooltip"]) ? 'data-toggle="tooltip" data-placement="bottom" title="'.__($input["tooltip"]).'"' : '').'>
                                    '.(!empty($input["icon"]) ? '<div class="input-group-prepend"><span class="input-group-text"><i class="'.$input["icon"].'"></i></span></div>' : '').'
                                    '.(!empty($input["prefix"]) ? '<div class="input-group-prepend"><span class="input-group-text">'.$input["prefix"].'</span></div>' : '').'
                                ';

                        switch ($inputType[0]){
                            case 'boolean':
                                $html .= '
                                        <?php $selected = request()->has("'.$inputKey.'") && request()->input("'.$inputKey.'") == "1"; ?>
                                        <input type="checkbox" name="'.$inputKey.'" class="form-control '.$input["classes"].'" id="'.$inputId.'" value="1" <?php if($selected): ?> checked="checked"<?php endif; ?>>
                                    ';
                            break;

                            case 'file':
                            case 'files':
                                $html .= '<input type="file" name="'.$inputKey.'" class="form-control '.$input["classes"].'" id="'.$inputId.'">';
                            break;

                            case 'html':
                            case 'text':
                                $html .= '<textarea name="'.$inputKey.'" class="form-control '.$input["classes"].'" id="'.$inputId.'" autocomplete="off" placeholder="'.__($input["title"]).'"><?php echo request()->has("'.$inputKey.'") ? request()->input("'.$inputKey.'") : ""; ?></textarea>';
                            break;

                            case 'select':
                            case 'multiselect':
                            case 'select_inv':
                            case 'multiselect_inv':
                                $partnerDef = $definitions->get('entities.'.$inputType[1]);

                                $html .= '
                                    <?php $options = '.$partnerDef["class_name_full"].'::select("id", "'.$partnerDef["data"]["title_field"].' as title")->get()->pluck("title", "id")->toArray(); ?>
                                    <select name="'.$inputKey.'" class="form-control '.$input["classes"].'" id="'.$inputId.'">
                                        <?php
                                            foreach($options as $optId => $optTitle):
                                                $selected = request()->has("'.$inputKey.'") && request()->input("'.$inputKey.'") == $optId;
                                        ?>
                                                <option value="<?php echo $optId; ?>" <?php if($selected): ?>selected="selected"<?php endif; ?>><?php echo $optTitle; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    ';

                            break;

                            default:
                                $defaultValue = '<?php echo request()->has("'.$inputKey.'") ? request()->input("'.$inputKey.'") : ""; ?>';
                                $inputAttrType = "text";
                                $attributtes = [];
                                if($inputType[0] == "int"){
                                    $inputAttrType = "number";
                                    $attributtes[] = 'step="1"';
                                }
                                if($inputType[0] == "float"){
                                    $inputAttrType = "number";
                                    $step = "0.";
                                    for($i = 0; $i < $inputType[2]-1; $i++){
                                        $step .= "0";
                                    }
                                    $step .= "1";
                                    $attributtes[] = 'step="'.$step.'"';
                                }
                                if($inputType[0] == "date"){
                                    $input["classes"] .= " datetimepicker-input datepicker";
                                    $attributtes[] = 'data-toggle="datetimepicker"';
                                    $attributtes[] = 'data-target="#'.$inputId.'"';
                                }
                                if($inputType[0] == "datetime"){
                                    $input["classes"] .= " datetimepicker-input datetimepicker";
                                    $attributtes[] = 'data-toggle="datetimepicker"';
                                    $attributtes[] = 'data-target="#'.$inputId.'"';
                                }
                                if($inputType[0] == "time"){
                                    $input["classes"] .= " datetimepicker-input timepicker";
                                    $attributtes[] = 'data-toggle="datetimepicker"';
                                    $attributtes[] = 'data-target="#'.$inputId.'"';
                                }
                                if($inputType[0] == "color"){
                                    $input["classes"] .= " inp-minicolors";
                                }
                                if($inputType[0] == "password"){
                                    $inputAttrType = "password";
                                    $defaultValue = '';
                                }

                                $html .= '<input autocomplete="off"  type="'.$inputAttrType.'" '.implode(" ", $attributtes).' name="'.$inputKey.'" class="form-control '.$input["classes"].'" id="'.$inputId.'" value="'.$defaultValue.'" placeholder="'.__($input["title"]).'">';

                                if($inputType[0] == "password"){
                                    $html .= '<input autocomplete="off" type="'.$inputAttrType.'" name="'.$inputKey.'_confirmation" class="form-control '.$input["classes"].'" id="'.$inputId.'_confirmation" value="" placeholder="'.__($input["title"]).' '.__('Confirm').'">';
                                }
                            break;
                        }

            $html .= '          '.(!empty($input["suffix"]) ? '<div class="input-group-append"><span class="input-group-text">'.$input["suffix"].'</span></div>' : '').'
                            </div>
                      </div>

                      <?php if(isset($errors) && $errors->any()): ?>
                            <?php if($errors->has("'.$inputKey.'")): ?>
                                <?php foreach ($errors->get("'.$inputKey.'") as $message): ?>
                                    <div class="invalid-feedback"><i class="fa fa-times"></i> <?php echo $message; ?></div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                </div>';
        }

        $html .= '<div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                  </div>
                    </form>';

        /* convert html chars */
        $html = '<textarea style="width:100%;height:300px;">'.htmlspecialchars($html).'</textarea>';

        return $this->response(["state" => "success", "msg" => ["title" => __("Success!"), "html" => $html, 'timer' => null, 'showConfirmButton' => true]]);
    }
}


<?php

namespace App\Functions;

use App\Component;
use App\Entities\CustomerEntity;
use App\Entities\InvoiceEntity;
use App\Func;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MakeInvoiceFunction extends Func
{
    public function handle(Request $request, $action){
        global $definitions;
        $entityName = $request->input("entity");
        $entityDef = $definitions->get("entities.".$entityName);
        $id = $request->input("id");

        $customer = CustomerEntity::find($id);
        if($customer){
            $customer->createInvoice((object)$request);

            return $this->response(["state" => "success", "msg" => __("Invoice created!")]);
        }
        else{
            return $this->response(["state" => "error", "msg" => __("Customer not found!")]);
        }

    }
}


<?php

namespace App\Functions;

use App\Component;
use App\File;
use App\Func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class SeedDbFunction extends Func
{
    public function handle(Request $request, $action){
        $files = File::all();
        foreach ($files as $f){
            $f->delete();
        }

        Artisan::call('migrate:refresh --seed');
        return $this->response(["state" => "success", "msg" => __("New testing data successfully generated!")]);
    }
}


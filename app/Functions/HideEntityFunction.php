<?php

namespace App\Functions;

use App\Component;
use App\Func;
use App\Notifications\UserDeadlockedNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class HideEntityFunction extends Func
{
    public function handle(Request $request, $action){
        global $definitions;
        $entityName = $action["data"]["entity"];
        $entityDef = $definitions->get("entities.".$entityName);
        $id = $request->input("id");

        $source = ($request->has('_source') && $request->input('_source') == 'detail' ? 'detail' : 'table');

        $entityInstance = $entityDef["class_name_full"]::find($id);
        if($entityInstance->hiddenToggle()){

            /* NOTIFY THE USER ABOUT DEADLOCKING/UNBLOCKING HIS ACCOUNT */
            if($entityDef["name"] == "user"){
                Notification::route('mail', $entityInstance->email)->notify(new UserDeadlockedNotification($entityInstance));
                $successTitleHidden = 'User deadlocked!';
                $successTitleHiddenUndo = 'User unblocked!';
            }
            else{
                $successTitleHidden = 'Hidden!';
                $successTitleHiddenUndo = 'Hidden undo!';
            }

            if($source == 'detail'){
                $attachScript = 'window.Front.componentReload($("#component"));';
            }
            else{
                $attachScript = '$("#table-row-'.$entityName.'-'.$id.'").'.($entityInstance->isHidden() ? 'add' : 'remove').'Class("row-hidden");';
            }

            return $this->response(["state" => "success", "msg" => __(($entityInstance->isHidden() ? $successTitleHidden : $successTitleHiddenUndo)), "attach_script" => $attachScript], false);
        }
        else{
            if(session()->has("error")){
                return $this->response(["state" => "error", "msg" => ["title" => __("Error!"), "html" => session()->get("error")]]);
            }
            else{
                return $this->response(["state" => "error", "msg" => ["title" => __("Error!"), "html" => __("Couldn't process request.")]]);
            }
        }
    }
}


<?php

namespace App\Functions;

use App\ContentInfo;
use App\File;
use App\Func;
use Illuminate\Http\Request;

class SortEntityItemsFunction extends Func
{
    public function handle(Request $request, $action){
        global $definitions;
        $isFileStructure = ($request->has("is_file_structure") && $request->input("is_file_structure") == 1);
        $parentId = $request->has("parent_id") ? $request->input("parent_id") : ""; //in case of file structure
        $entityName = $request->has("entity") ? $request->input("entity") : ""; //in case of entity or files
        $ids = $request->has("ids") ? $request->input("ids") : [];

        if($isFileStructure){
            $attachScript = 'tableSortFinished();';
            if(ContentInfo::sortItems($ids)){
                return $this->response(["state" => "success", "msg" => __("Sorted!"), "attach_script" => $attachScript], true);
            }
            else{
                return $this->response(["state" => "error", "msg" => ["title" => __("Error!"), "html" => __("Couldn't sort items.")], "attach_script" => $attachScript]);
            }
        }
        else{
            if($entityName == "__files__"){
                if(File::sortItems($ids)){
                    return $this->response(["state" => "success", "msg" => __("Sorted!")], true);
                }
                else{
                    return $this->response(["state" => "error", "msg" => ["title" => __("Error!"), "html" => __("Couldn't sort items.")]]);
                }
            }
            else{
                $entityDef = $definitions->get("entities.".$entityName);

                $attachScript = 'tableSortFinished();';
                if($entityDef["class_name_full"]::sortItems($ids)){
                    return $this->response(["state" => "success", "msg" => __("Sorted!"), "attach_script" => $attachScript], true);
                }
                else{
                    return $this->response(["state" => "error", "msg" => ["title" => __("Error!"), "html" => __("Couldn't sort items.")], "attach_script" => $attachScript]);
                }
            }
        }

    }
}


<?php

namespace App\Functions;

use App\Component;
use App\File;
use App\Func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class ClearDbFunction extends Func
{
    public function handle(Request $request, $action){
        $files = File::all();
        foreach ($files as $f){
            $f->delete();
        }

        Artisan::call('migrate:refresh');
        return $this->response(["state" => "success", "msg" => __("Database successfully cleared!")]);
    }
}


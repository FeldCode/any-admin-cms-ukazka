<?php

namespace App\Console\Commands;

use App\DefProcessor;
use App\Generator;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class EntityMigrationMakeCommand extends Command
{
    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:entitymigration {entity}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new EntityMigration class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'EntityMigration';

    /**
     * The path to store generated migration
     *
     * @var string
     */
    protected $path = 'database/migrations';

    /**
     * The entity we are generating migration for
     *
     * @var string
     */
    protected $entity = null;

    /**
     * definition data of current entity
     *
     * @var string
     */
    protected $def = null;

    /**
     * Partner entity, if not false,
     * then we are generating pivot table
     *
     * @var string
     */
    protected $partner = false;

    /**
     * Partner entity, if not false,
     * then we are generating pivot table
     *
     * @var string
     */
    protected $partnerField = false;

    /**
     * definition data of partner entity
     *
     * @var string
     */
    protected $partnerDef = null;

    /**
     * type of migration:
     * create, update, create_pivot, update_pivot
     *
     * @var string
     */
    protected $migrationType = null;

    /**
     * Name of the migration file without timestamp
     *
     * @var string
     */
    protected $migrationName = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    /**
     * Execute the console command.
     * If $partner && $field variables are not false,
     * then function will handle pivot table between
     * two entities
     *
     * @return mixed
     */
    public function handle($partner = false, $field = false)
    {
        global $definitions;

        if(is_null($definitions))
            $definitions = new DefProcessor();

        $this->loadExistingMigrations();
        $this->entity = $this->argument('entity');
        $this->partner = $partner;
        $this->partnerField = $field;
        $this->migrationType = !Schema::hasTable($this->getTable()) ? "create" : "update";
        if($this->partner)
            $this->migrationType = !Schema::hasTable($this->getTable()) ? "create_pivot" : "update_pivot";

        $this->migrationName = $this->getMigrationName();

        if(!$definitions->check("entities.".$this->entity)){
            $this->error($this->entity.' entity does not exist!');
            return false;
        }

        if($this->partner && !$definitions->check("entities.".$this->partner)){
            $this->error($this->partner.' entity does not exist!');
            return false;
        }

        $this->def = $definitions->get("entities.".$this->entity);
        if($this->partner)
            $this->partnerDef = $definitions->get("entities.".$this->partner);

        $this->buildAndSaveMigration();
    }

    protected function buildAndSaveMigration(){
        $createMigrationFile = true;
        $neededOperations = [];

        /* CHECK IF WAITING MIGRATION FILE FOR THIS TYPE AND TABLE ALREADY EXISTS */
        $migrationsToRun = Generator::getWaitingMigrations();
        foreach ($migrationsToRun as $mig){
            if(defined($mig["class_name"].'::MIGRATION_TYPE') && defined($mig["class_name"].'::MIGRATION_TABLE')){
                if($this->migrationType == $mig["class_name"]::MIGRATION_TYPE
                    && $this->getTable() == $mig["class_name"]::MIGRATION_TABLE){
                    $createMigrationFile = false;
                    break;
                }
            }
        }

        if($createMigrationFile) {
            if (!$this->partner) {

                /* CREATE NEW TABLE */
                if ($this->migrationType == "create") {
                    $baseColumns = array_keys(DefProcessor::getEntityBaseColumns($this->entity));

                    $scriptUp = 'Schema::create(\'' . $this->getTable() . '\', function (Blueprint $table) {
                        $table->bigIncrements(\'id\')->unsigned();
                        $table->bigInteger(\'sort\')->nullable()->unsigned()->index();
                        $table->boolean(\'is_hidden\')->default(0)->index();' . "\r\n";

                            foreach ($this->def["data"]["_inputs"] as $inputKey => $input) {
                                if(!in_array($inputKey, $baseColumns))
                                    $scriptUp .= $this->getInputLine($inputKey, array("input" => $input));
                            }

                            $scriptUp .= '
                        $table->integer(\'created_user_id\')->nullable()->unsigned()->index();
                        $table->integer(\'updated_user_id\')->nullable()->unsigned()->index();
                        $table->timestamps();
                    });';

                    $scriptDown = 'Schema::dropIfExists(\'' . $this->getTable() . '\');';
                }

                /* UPDATE EXISTING TABLE */
                elseif ($this->migrationType == "update") {
                    global $definitions;
                    $neededOperations = $definitions->getNeededTableColumnsOperations($this->entity);

                    $totalOperationsCnt = 0;
                    foreach ($neededOperations as $operations)
                        $totalOperationsCnt += count($operations);

                    if ($totalOperationsCnt == 0) {
                        $createMigrationFile = false;
                    }

                    if ($createMigrationFile) {
                        $scriptUp = $scriptDown = 'Schema::table(\'' . $this->getTable() . '\', function (Blueprint $table) {' . "\r\n";

                        foreach ($neededOperations as $operationType => $operations) {
                            foreach ($operations as $inputKey => $operation) {

                                $scriptUp .= $this->getInputLine($inputKey, $operation, $operationType);

                                $operationTypeDownConv = [
                                    "add" => "delete",
                                    "delete" => "add",
                                    "change" => "change-back",
                                ];
                                $scriptDown .= $this->getInputLine($inputKey, $operation, $operationTypeDownConv[$operationType]);
                            }
                        }

                        $scriptUp .= '
                });';
                        $scriptDown .= '
                });';
                    }
                }
            }

            /* CREATE OR UPDATE PIVOT TABLE */
            else {

                /* CREATE NEW PIVOT TABLE */
                if ($this->migrationType == "create_pivot") {
                    $scriptUp = 'Schema::create(\'' . $this->getTable() . '\', function (Blueprint $table) {
                    $table->integer(\'sort\')->nullable()->unsigned()->index();
                    $table->integer(\'' . DefProcessor::getEntityForeignKey($this->entity) . '\')->nullable()->unsigned();
                    $table->integer(\'' . DefProcessor::getEntityForeignKey($this->partner) . '\')->nullable()->unsigned();
                    $table->primary([\'' . DefProcessor::getEntityForeignKey($this->entity) . '\', \'' . DefProcessor::getEntityForeignKey($this->partner) . '\'], \'' . $this->getTable() . '_primary\');' . "\r\n";

                    /* EXTRA FIELDS FROM SOURCE ENTITY DEFINITION */
                    foreach ($this->def["data"]["_inputs"] as $parentInputKey => $parentInput) {
                        if($parentInputKey == $this->partnerField) {
                            foreach ($parentInput["relation_extra_inputs"] as $inputKey => $input){
                                $scriptUp .= $this->getInputLine($inputKey, array("input" => $input));
                            }
                        }
                    }

                    $scriptUp .= '
                });';

                    $scriptDown = 'Schema::dropIfExists(\'' . $this->getTable() . '\');';
                } /* UPDATE EXISTING PIVOT TABLE */
                elseif ($this->migrationType == "update_pivot") {
                    global $definitions;
                    $neededOperations = $definitions->getNeededTableColumnsOperations(["entity" => $this->entity, "partner" => $this->partner, "field" => $this->partnerField], true);

                    $totalOperationsCnt = 0;
                    foreach ($neededOperations as $operations)
                        $totalOperationsCnt += count($operations);

                    if ($totalOperationsCnt == 0) {
                        $createMigrationFile = false;
                    }

                    if ($createMigrationFile) {
                        $scriptUp = $scriptDown = 'Schema::table(\'' . $this->getTable() . '\', function (Blueprint $table) {' . "\r\n";

                        foreach ($neededOperations as $operationType => $operations) {
                            foreach ($operations as $inputKey => $operation) {

                                $scriptUp .= $this->getInputLine($inputKey, $operation, $operationType);

                                $operationTypeDownConv = [
                                    "add" => "delete",
                                    "delete" => "add",
                                    "change" => "change-back",
                                ];
                                $scriptDown .= $this->getInputLine($inputKey, $operation, $operationTypeDownConv[$operationType]);
                            }
                        }

                        $scriptUp .= '
                    });';
                        $scriptDown .= '
                    });';
                    }
                }
            }
        }


        if($createMigrationFile){
            $stub = $this->files->get($this->getStub());

            $stub = str_replace(
                ['DummyClass', 'DummyMigrationType', 'DummyTable', 'DummyOperationsList', 'DummyScriptUp', 'DummyScriptDown'],
                [$this->getMigrationClassName(), $this->migrationType, $this->getTable(), var_export($neededOperations, true), $scriptUp, $scriptDown],
                $stub
            );

            $fname = strftime("%Y_%m_%d_%H%M%S_".$this->migrationName.".php");
            $path = $this->getPath($fname);

            $this->files->put($path, $stub);

            if($this->partner)
                $this->info($this->entity.'-'.$this->partner.': migration for pivot table created successfully.');
            else
                $this->info($this->entity.': '.$this->type.' created successfully.');
        }
        else{
            if($this->partner)
                $this->info($this->entity.'-'.$this->partner.': Pivot definition up to date, no need for migration.');
            else
                $this->info($this->entity.': '.$this->type.' Entity definition up to date, no need for migration.');
        }

        /* CHECK FOR PIVOT TABLES */
        if(!$this->partner) {
            foreach ($this->def["data"]["_inputs"] as $inputKey => $input) {
                $inputType = DefProcessor::parseInputType($input["type"])[0];
                if($inputType == "multiselect"){
                    $data = explode(":", $input["type"]);
                    if(!isset($data[1])){
                        throw new \ErrorException($this->entity.': Input '.$inputKey.' is type of "multiselect:[partner]" but does not have defined partner entity.');
                    }
                    else{
                        $this->handle($data[1], $inputKey);
                    }
                }
            }
        }
    }

    protected function getInputLine($inputKey, $operation, $type = "add"){
        $line = "";

        if($type == "add") {
            if(!empty($operation["input"]))
                $inputDB = DefProcessor::getInputForDB($operation["input"]);
            else { //down script
                $inputDB = $operation["col"];
            }
        }
        elseif($type == "delete"){
            $inputDB = $operation["col"];
            $inputDB["func"] = true;
        }
        elseif($type == "change"){
            $inputDB = DefProcessor::getInputForDB($operation["input"]);
            $inputDB["extra"][] = "change";
        }
        elseif($type == "change-back"){
            $inputDB = $operation["col"];
            $inputDB["extra"][] = "change";
        }
        else{
            $this->error('Invalid operation');
        }

        if($inputDB["func"]){
            if($type == "add" || $type == "change" || $type == "change-back") {

                /* DROPING INDEXES OF COLUMNS */
                if($type == "change" || $type == "change-back"){

                    $inputIndex = $operation["input"]["index"];
                    $colIndex = $operation["col"]["index"] ? $operation["col"]["index"]["type"] : "";

                    /* if index options are the same, remove all indexes from extra functions, since we are updating */
                    if($colIndex == $inputIndex){
                        if(!empty($operation["col"]["index"]["type"])){
                            foreach ($inputDB["extra"] as $extraFuncKey => $extraFunc){
                                if($operation["col"]["index"]["type"] == $extraFunc)
                                    unset($inputDB["extra"][$extraFuncKey]);
                            }
                        }
                    }

                    /* if index options are not the same, drop indexes if they exists and remain it in extra functions */
                    else{
                        if($type == "change" && !empty($colIndex)){
                            $indexName = $operation["col"]["index"]["name"];
                        }
                        elseif($type == "change-back" && !empty($inputIndex)){
                            $indexName = $this->getTable()."_".$inputKey."_".$operation["input"]["index"];
                        }

                        if(isset($indexName))
                            $line .= '$table->dropIndex(\'' . $indexName . '\');'."\r\n";
                    }
                }

                /* BASIC LINE */
                $line .= '$table->' . $inputDB["func"] . '(\'' . $inputKey . '\'' . ($inputDB["args"] && $inputDB["func_uses_args"] ? ', ' . implode(', ', $inputDB["args"]) : '') . ')'.($type != "add" ? '->charset(false)' : '');

                /* EXTRA FUNCTIONS */
                foreach ($inputDB["extra"] as $extraFunc) {
                    $line .= '->' . $extraFunc . '()';
                }

                /* DEFAULT VALUE */
                if(isset($inputDB["default"])) {
                    $line .= '->default('.$inputDB["default"].')';
                }

            }
            elseif($type == "delete"){
                $line = '$table->dropColumn(\'' . $inputKey . '\')';
            }

            /* LINE END */
            $line .= ";\r\n";
        }

        return $line;
    }

    protected function loadExistingMigrations(){
        $existingMigrations = Generator::getAllExistingMigrations();

        foreach ($existingMigrations as &$mf){
            $mf = $mf["name_without_timestamp"];
        }

        $this->existingMigrations = $existingMigrations;
    }

    protected function getMigrationName(){
        $migrationName = "gen_".$this->migrationType."_".$this->entity.($this->partner ? "_".$this->partner : "")."_db_structure";
        if(in_array($migrationName, $this->existingMigrations)){
            $i = 1;
            $migrationExists = true;
            while($migrationExists){
                $migrationExists = in_array($migrationName."_".$i, $this->existingMigrations);
                if(!$migrationExists){
                    $migrationName .= "_".$i;
                }

                $i++;
            }
        }

        return $migrationName;
    }

    protected function getMigrationClassName($migrationName = ""){
        if(empty($migrationName))
            $migrationName = $this->migrationName;

        return Generator::getMigrationClassName($migrationName);
    }

    protected function getTable(){
        if($this->partner && $this->partnerField)
            return DefProcessor::getEntityPivotTable($this->entity, $this->partner, $this->partnerField);
        else
            return DefProcessor::getEntityTable($this->entity);
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return  base_path('resources/stubs/EntityMigration.stub');
    }

    protected function getPath($fname = "")
    {
        return base_path($this->path."/".$fname);
    }
}

<?php

namespace App\Console\Commands;

use App\DefProcessor;
use App\Entity;
use Illuminate\Console\GeneratorCommand;

class EntityMakeCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:entity {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Entity class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Entity';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $table = '';

    public function handle()
    {
        $entity = Entity::getDefinitionNameFromStr($this->argument('name'));

        /* DO NOT CREATE FILE IF SYSTEM ENTITY */
        if(DefProcessor::isSystemEntity($entity))
            return true;

        /* SET ENTITY TABLE */
        $this->table = DefProcessor::getEntityTable($entity);

        return parent::handle();
    }

    protected function replaceClass($stub, $name)
    {
        /* REPLACE TABLE IN STUB */
        $stub = str_replace('DummyTable', $this->table, $stub);
        return parent::replaceClass($stub, $name);
    }

    /**
     *
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return  base_path('resources/stubs/Entity.stub');
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Entities';
    }
}
